﻿namespace SelfitWebLauncher {
    partial class Main {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
            this.savePathBtn = new System.Windows.Forms.Button();
            this.selfitPathTxt = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.machineIDTxt = new System.Windows.Forms.TextBox();
            this.siteIDTxt = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.statusLabel = new System.Windows.Forms.Label();
            this.launcherNotifyIcon = new System.Windows.Forms.NotifyIcon(this.components);
            this.sysTimer = new System.Windows.Forms.Timer(this.components);
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // savePathBtn
            // 
            this.savePathBtn.Font = new System.Drawing.Font("Roboto", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.savePathBtn.Location = new System.Drawing.Point(403, 156);
            this.savePathBtn.Name = "savePathBtn";
            this.savePathBtn.Size = new System.Drawing.Size(109, 34);
            this.savePathBtn.TabIndex = 7;
            this.savePathBtn.Text = "Save";
            this.savePathBtn.UseVisualStyleBackColor = true;
            this.savePathBtn.Click += new System.EventHandler(this.button2_Click);
            // 
            // selfitPathTxt
            // 
            this.selfitPathTxt.Font = new System.Drawing.Font("Roboto", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.selfitPathTxt.Location = new System.Drawing.Point(156, 111);
            this.selfitPathTxt.Name = "selfitPathTxt";
            this.selfitPathTxt.Size = new System.Drawing.Size(356, 29);
            this.selfitPathTxt.TabIndex = 6;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Roboto", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.label3.Location = new System.Drawing.Point(28, 116);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(121, 19);
            this.label3.TabIndex = 5;
            this.label3.Text = "Selfit EXE path: ";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Roboto", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.label1.Location = new System.Drawing.Point(12, 69);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(137, 19);
            this.label1.TabIndex = 8;
            this.label1.Text = "Selfit machine ID: ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Roboto", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.label2.Location = new System.Drawing.Point(86, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(63, 19);
            this.label2.TabIndex = 9;
            this.label2.Text = "Site ID: ";
            // 
            // machineIDTxt
            // 
            this.machineIDTxt.Font = new System.Drawing.Font("Roboto", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.machineIDTxt.Location = new System.Drawing.Point(156, 64);
            this.machineIDTxt.Name = "machineIDTxt";
            this.machineIDTxt.Size = new System.Drawing.Size(356, 29);
            this.machineIDTxt.TabIndex = 10;
            // 
            // siteIDTxt
            // 
            this.siteIDTxt.Font = new System.Drawing.Font("Roboto", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.siteIDTxt.Location = new System.Drawing.Point(156, 17);
            this.siteIDTxt.Name = "siteIDTxt";
            this.siteIDTxt.Size = new System.Drawing.Size(356, 29);
            this.siteIDTxt.TabIndex = 11;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.siteIDTxt);
            this.groupBox1.Controls.Add(this.machineIDTxt);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.savePathBtn);
            this.groupBox1.Controls.Add(this.selfitPathTxt);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Location = new System.Drawing.Point(0, 93);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(523, 198);
            this.groupBox1.TabIndex = 12;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = " Settings: ";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Roboto", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.label4.Location = new System.Drawing.Point(28, 33);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(59, 19);
            this.label4.TabIndex = 13;
            this.label4.Text = "Status:";
            // 
            // statusLabel
            // 
            this.statusLabel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.statusLabel.Font = new System.Drawing.Font("Roboto", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.statusLabel.Location = new System.Drawing.Point(93, 28);
            this.statusLabel.Name = "statusLabel";
            this.statusLabel.Size = new System.Drawing.Size(419, 30);
            this.statusLabel.TabIndex = 14;
            this.statusLabel.Text = "???";
            // 
            // launcherNotifyIcon
            // 
            this.launcherNotifyIcon.BalloonTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            this.launcherNotifyIcon.BalloonTipText = "Selfit Manager launcher";
            this.launcherNotifyIcon.BalloonTipTitle = "Selfit";
            this.launcherNotifyIcon.Icon = ((System.Drawing.Icon)(resources.GetObject("launcherNotifyIcon.Icon")));
            this.launcherNotifyIcon.Text = "Selfit Manager launcher";
            this.launcherNotifyIcon.MouseClick += new System.Windows.Forms.MouseEventHandler(this.launcherNotifyIcon_MouseClick);
            this.launcherNotifyIcon.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.launcherNotifyIcon_MouseDoubleClick_1);
            // 
            // sysTimer
            // 
            this.sysTimer.Interval = 500;
            this.sysTimer.Tick += new System.EventHandler(this.sysTimer_Tick);
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(530, 299);
            this.Controls.Add(this.statusLabel);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "Main";
            this.Text = "Selfit Web Launcher";
            this.Load += new System.EventHandler(this.Main_Load);
            this.Resize += new System.EventHandler(this.Main_Resize);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button savePathBtn;
        private System.Windows.Forms.TextBox selfitPathTxt;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox machineIDTxt;
        private System.Windows.Forms.TextBox siteIDTxt;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label statusLabel;
        private System.Windows.Forms.NotifyIcon launcherNotifyIcon;
        private System.Windows.Forms.Timer sysTimer;
    }
}


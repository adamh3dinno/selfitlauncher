﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using System.Diagnostics;

namespace SelfitWebLauncher {

    public partial class Main : Form {       
        private int runCounter = 0;
        // LOCAL
        private string URI = "http://localhost:53075";    
        // PROD
        //private string URI = "https://selfitmanagerwebapplication.azurewebsites.net";
        // DEV
        //private string URI = "https://selfitmanagerwebapplication-dev.azurewebsites.net";
        static HttpClient mgrhttp = new HttpClient();
        TaskData td = new TaskData();

        public Main() {
            InitializeComponent();
            mgrhttp.BaseAddress = new Uri(URI);
            mgrhttp.DefaultRequestHeaders.Accept.Clear();
            mgrhttp.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));
        }

        private void button2_Click(object sender, EventArgs e) {
            Properties.Settings.Default.SiteID = int.Parse(siteIDTxt.Text);
            Properties.Settings.Default.MachineID = int.Parse(machineIDTxt.Text);
            Properties.Settings.Default.SelfitEXEPath = selfitPathTxt.Text;
            Properties.Settings.Default.Save();
        }

        private void Main_Resize(object sender, EventArgs e) {
            if (this.WindowState == FormWindowState.Minimized) {
                Hide();
                launcherNotifyIcon.Visible = true;
            }
        }
        

        private void launcherNotifyIcon_MouseDoubleClick_1(object sender, MouseEventArgs e) {
            Show();
            this.WindowState = FormWindowState.Normal;
            launcherNotifyIcon.Visible = false;
        }

        private void launcherNotifyIcon_MouseClick(object sender, MouseEventArgs e) {
            Show();
            this.WindowState = FormWindowState.Normal;
            launcherNotifyIcon.Visible = false;
        }

        private void Main_Load(object sender, EventArgs e) {
            siteIDTxt.Text = Properties.Settings.Default.SiteID.ToString();
            machineIDTxt.Text = Properties.Settings.Default.MachineID.ToString();
            selfitPathTxt.Text = Properties.Settings.Default.SelfitEXEPath;
            statusLabel.Text = "Idle";
            if (selfitPathTxt.Text != string.Empty && Properties.Settings.Default.MachineID > 0 && Properties.Settings.Default.SiteID > 0)
                sysTimer.Enabled = true;
        }

        private void sysTimer_Tick(object sender, EventArgs e) {
            runCounter++;
            
            
            try {
                Task t = Task.Run(async () => { td = await restGetTasks(Properties.Settings.Default.SiteID, Properties.Settings.Default.MachineID); });
                Task.WaitAll(t);
            }
            catch (Exception ex) {
                MessageBox.Show(ex.ToString());
            }
            statusLabel.Text = "Scanned " + runCounter + " times, task ID: " + td.taskId + " state: " + td.state;
            if (td.state == 1) {
                sysTimer.Enabled = false;
                //if (MessageBox.Show("Task ID: " + td.taskId + "\nState: " + td.state + "\nTask data: " + td.task_data) == DialogResult.OK) 
                    // realse the task...
                    runExeApp(Properties.Settings.Default.SelfitEXEPath, td.task_data);
               
            }
        }

        ///api/Tasks?site=1&machineId=1&taskId=5
        public async Task<int> RestSetTaskCompleted(int site, int machine, int taskId) {
            int result = -1;
            try {
                using (var client = new HttpClient()) {
                    client.BaseAddress = new Uri(URI);                    
                    var response = client.PutAsync("/api/Tasks?site=" + site + "&machineId=" + machine + "&taskId=" + taskId, null).Result;                    
                    if (response.IsSuccessStatusCode) {
                        result = 0;
                    }
                }
            }
            catch (HttpRequestException e) {
                MessageBox.Show(e.Message);
            }
        
            return result;
        }

        //api/Tasks?site=1&machineId=1
        public async Task<TaskData> restGetTasks(int site, int machine) {
            TaskData tdata = new TaskData();
            try {
                using (var client = new HttpClient()) {
                    client.BaseAddress = new Uri(URI);
                    HttpResponseMessage response = await mgrhttp.GetAsync(URI + "/api/Tasks?site=" + site + "&machineId=" + machine);
                    string responseBody = await response.Content.ReadAsStringAsync();
                    tdata = JsonConvert.DeserializeObject<TaskData>(responseBody);
                    response.Dispose();
                }
            }
            catch (HttpRequestException e) {
                MessageBox.Show(e.Message);
                sysTimer.Enabled = false;
            }
            return tdata;
        }

        private void runExeApp(string appExe, string args) {
            appExe += "\\SelfitPilot.exe";                        
            Process practiceApp = new Process();
            
            practiceApp.StartInfo.FileName = appExe;
            practiceApp.StartInfo.Arguments = args;
            practiceApp.Start();
            practiceApp.EnableRaisingEvents = true;
            practiceApp.Exited += new EventHandler(practiceApp_Exited);
            practiceApp.WaitForExit();
        }

        private void practiceApp_Exited(object sender, System.EventArgs e) {
            int res = -1;
            try {
                Task t = Task.Run(async () => { res = await RestSetTaskCompleted(Properties.Settings.Default.SiteID, Properties.Settings.Default.MachineID, td.taskId); });
                Task.WaitAll(t);
            }
            catch (Exception ex) {
                MessageBox.Show(ex.ToString());
            }
            sysTimer.Enabled = true;
        }

        public class TaskData {
            public int taskId { get; set; }
            public int state { get; set; }
            public string task_data { get; set; }
            public string task_info { get; set; }
        }
    }
}

﻿namespace SelfitMain {
    partial class AddPatient {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.panel1 = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.fNameTxt = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.lNamTxt = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.pIdTxt = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.eMailTxt = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.notesTxt = new System.Windows.Forms.RichTextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.saveDataBtn = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel6.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(201)))), ((int)(((byte)(31)))));
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1798, 136);
            this.panel1.TabIndex = 0;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(201)))), ((int)(((byte)(31)))));
            this.button1.BackgroundImage = global::SelfitMain.Properties.Resources.Blue_Close_ICN;
            this.button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(201)))), ((int)(((byte)(31)))));
            this.button1.Location = new System.Drawing.Point(1684, 32);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(66, 66);
            this.button1.TabIndex = 1;
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Roboto", 80F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.label1.Location = new System.Drawing.Point(46, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(514, 89);
            this.label1.TabIndex = 0;
            this.label1.Text = "Add a Patient";
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.fNameTxt);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Location = new System.Drawing.Point(662, 204);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(580, 68);
            this.panel2.TabIndex = 1;
            // 
            // fNameTxt
            // 
            this.fNameTxt.BackColor = System.Drawing.Color.WhiteSmoke;
            this.fNameTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.fNameTxt.Font = new System.Drawing.Font("Roboto", 28F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.fNameTxt.Location = new System.Drawing.Point(165, 14);
            this.fNameTxt.Name = "fNameTxt";
            this.fNameTxt.Size = new System.Drawing.Size(397, 34);
            this.fNameTxt.TabIndex = 1;
            this.fNameTxt.Text = "?";
            this.fNameTxt.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Roboto", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.label2.Location = new System.Drawing.Point(29, 12);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(130, 39);
            this.label2.TabIndex = 0;
            this.label2.Text = "First Name";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.lNamTxt);
            this.panel3.Controls.Add(this.label3);
            this.panel3.Location = new System.Drawing.Point(662, 306);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(580, 68);
            this.panel3.TabIndex = 2;
            // 
            // lNamTxt
            // 
            this.lNamTxt.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lNamTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lNamTxt.Font = new System.Drawing.Font("Roboto", 28F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.lNamTxt.Location = new System.Drawing.Point(165, 14);
            this.lNamTxt.Name = "lNamTxt";
            this.lNamTxt.Size = new System.Drawing.Size(397, 34);
            this.lNamTxt.TabIndex = 1;
            this.lNamTxt.Text = "?";
            this.lNamTxt.TextChanged += new System.EventHandler(this.lNamTxt_TextChanged);
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Roboto", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.label3.Location = new System.Drawing.Point(29, 12);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(130, 39);
            this.label3.TabIndex = 0;
            this.label3.Text = "Last Name";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel4
            // 
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel4.Controls.Add(this.pIdTxt);
            this.panel4.Controls.Add(this.label4);
            this.panel4.Location = new System.Drawing.Point(662, 408);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(580, 68);
            this.panel4.TabIndex = 3;
            // 
            // pIdTxt
            // 
            this.pIdTxt.BackColor = System.Drawing.Color.WhiteSmoke;
            this.pIdTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.pIdTxt.Font = new System.Drawing.Font("Roboto", 28F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.pIdTxt.Location = new System.Drawing.Point(165, 14);
            this.pIdTxt.Name = "pIdTxt";
            this.pIdTxt.Size = new System.Drawing.Size(397, 34);
            this.pIdTxt.TabIndex = 1;
            this.pIdTxt.Text = "?";
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Roboto", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.label4.Location = new System.Drawing.Point(29, 12);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(130, 39);
            this.label4.TabIndex = 0;
            this.label4.Text = "Patient ID";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel5
            // 
            this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel5.Controls.Add(this.eMailTxt);
            this.panel5.Controls.Add(this.label5);
            this.panel5.Location = new System.Drawing.Point(662, 512);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(580, 68);
            this.panel5.TabIndex = 4;
            // 
            // eMailTxt
            // 
            this.eMailTxt.BackColor = System.Drawing.Color.WhiteSmoke;
            this.eMailTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.eMailTxt.Font = new System.Drawing.Font("Roboto", 28F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.eMailTxt.Location = new System.Drawing.Point(165, 14);
            this.eMailTxt.Name = "eMailTxt";
            this.eMailTxt.Size = new System.Drawing.Size(397, 34);
            this.eMailTxt.TabIndex = 1;
            this.eMailTxt.Text = "?";
            this.eMailTxt.TextChanged += new System.EventHandler(this.eMailTxt_TextChanged);
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Roboto", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.label5.Location = new System.Drawing.Point(29, 12);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(130, 39);
            this.label5.TabIndex = 0;
            this.label5.Text = "Email";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel6
            // 
            this.panel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel6.Controls.Add(this.notesTxt);
            this.panel6.Controls.Add(this.label6);
            this.panel6.Location = new System.Drawing.Point(662, 609);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(580, 399);
            this.panel6.TabIndex = 5;
            // 
            // notesTxt
            // 
            this.notesTxt.BackColor = System.Drawing.Color.WhiteSmoke;
            this.notesTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.notesTxt.Location = new System.Drawing.Point(34, 54);
            this.notesTxt.Name = "notesTxt";
            this.notesTxt.Size = new System.Drawing.Size(511, 331);
            this.notesTxt.TabIndex = 1;
            this.notesTxt.Text = "";
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Roboto", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.label6.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label6.Location = new System.Drawing.Point(29, 12);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(130, 39);
            this.label6.TabIndex = 0;
            this.label6.Text = "Notes";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // saveDataBtn
            // 
            this.saveDataBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(201)))), ((int)(((byte)(31)))));
            this.saveDataBtn.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.saveDataBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.saveDataBtn.Font = new System.Drawing.Font("Roboto", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.saveDataBtn.ForeColor = System.Drawing.Color.Black;
            this.saveDataBtn.Location = new System.Drawing.Point(662, 1032);
            this.saveDataBtn.Name = "saveDataBtn";
            this.saveDataBtn.Size = new System.Drawing.Size(591, 68);
            this.saveDataBtn.TabIndex = 6;
            this.saveDataBtn.Text = "Save";
            this.saveDataBtn.UseVisualStyleBackColor = false;
            this.saveDataBtn.Click += new System.EventHandler(this.saveDataBtn_Click);
            // 
            // AddPatient
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1800, 1100);
            this.Controls.Add(this.saveDataBtn);
            this.Controls.Add(this.panel6);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "AddPatient";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "AddPatient";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TextBox fNameTxt;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.TextBox lNamTxt;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.TextBox pIdTxt;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.TextBox eMailTxt;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.RichTextBox notesTxt;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button saveDataBtn;
    }
}
﻿namespace SelfitMain {
    partial class EditPatient {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel1 = new System.Windows.Forms.Panel();
            this.closeBtn = new System.Windows.Forms.Button();
            this.editPatientCaption = new System.Windows.Forms.Label();
            this.SelectGenderTxt = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.autoGenIDBtn = new System.Windows.Forms.Button();
            this.pIdTxt = new System.Windows.Forms.TextBox();
            this.patientIDlbl = new System.Windows.Forms.Label();
            this.panel8 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.notesTxt = new System.Windows.Forms.RichTextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.saveDataBtn = new System.Windows.Forms.Button();
            this.panel24 = new System.Windows.Forms.Panel();
            this.tenMAddBtn = new System.Windows.Forms.Button();
            this.panel25 = new System.Windows.Forms.Panel();
            this.tenMstepsTxtBox = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.panel27 = new System.Windows.Forms.Panel();
            this.tenMsecTxtBox = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.panel26 = new System.Windows.Forms.Panel();
            this.tugAddBtn = new System.Windows.Forms.Button();
            this.panel29 = new System.Windows.Forms.Panel();
            this.tugTxtBox = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.panel28 = new System.Windows.Forms.Panel();
            this.dgiAddBtn = new System.Windows.Forms.Button();
            this.panel31 = new System.Windows.Forms.Panel();
            this.dgiCombo = new System.Windows.Forms.ComboBox();
            this.label21 = new System.Windows.Forms.Label();
            this.panel30 = new System.Windows.Forms.Panel();
            this.mocaAddBtn = new System.Windows.Forms.Button();
            this.panel32 = new System.Windows.Forms.Panel();
            this.mocaCombo = new System.Windows.Forms.ComboBox();
            this.label17 = new System.Windows.Forms.Label();
            this.panel33 = new System.Windows.Forms.Panel();
            this.panel34 = new System.Windows.Forms.Panel();
            this.userLevelCombo = new System.Windows.Forms.ComboBox();
            this.label20 = new System.Windows.Forms.Label();
            this.TestsDataGridView = new System.Windows.Forms.DataGridView();
            this.Date = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Test = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Score = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Time = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Steps = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel4 = new System.Windows.Forms.Panel();
            this.fNameTxt = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.lNameTxt = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.panel7 = new System.Windows.Forms.Panel();
            this.eMailTxt = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.panel10 = new System.Windows.Forms.Panel();
            this.phoneTxt = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.panel9 = new System.Windows.Forms.Panel();
            this.panel12 = new System.Windows.Forms.Panel();
            this.birthYearCombo = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.panel17 = new System.Windows.Forms.Panel();
            this.femaleSelectBtn = new System.Windows.Forms.Button();
            this.maleSelectBtn = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.panel18 = new System.Windows.Forms.Panel();
            this.heightCMbtn = new System.Windows.Forms.Button();
            this.panel19 = new System.Windows.Forms.Panel();
            this.heightCombo = new System.Windows.Forms.ComboBox();
            this.heightINCHbtn = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.panel20 = new System.Windows.Forms.Panel();
            this.weightKGbtn = new System.Windows.Forms.Button();
            this.panel21 = new System.Windows.Forms.Panel();
            this.weightCombo = new System.Windows.Forms.ComboBox();
            this.weightPOUNDbtn = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this.panel22 = new System.Windows.Forms.Panel();
            this.shoeEUbtn = new System.Windows.Forms.Button();
            this.panel23 = new System.Windows.Forms.Panel();
            this.shoeCombo = new System.Windows.Forms.ComboBox();
            this.shoeUSbtn = new System.Windows.Forms.Button();
            this.label13 = new System.Windows.Forms.Label();
            this.panel11 = new System.Windows.Forms.Panel();
            this.panel36 = new System.Windows.Forms.Panel();
            this.LangSelectionCBX = new System.Windows.Forms.ComboBox();
            this.label22 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel1.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel24.SuspendLayout();
            this.panel25.SuspendLayout();
            this.panel27.SuspendLayout();
            this.panel26.SuspendLayout();
            this.panel29.SuspendLayout();
            this.panel28.SuspendLayout();
            this.panel31.SuspendLayout();
            this.panel30.SuspendLayout();
            this.panel32.SuspendLayout();
            this.panel33.SuspendLayout();
            this.panel34.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TestsDataGridView)).BeginInit();
            this.panel4.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel10.SuspendLayout();
            this.panel9.SuspendLayout();
            this.panel12.SuspendLayout();
            this.panel17.SuspendLayout();
            this.panel18.SuspendLayout();
            this.panel19.SuspendLayout();
            this.panel20.SuspendLayout();
            this.panel21.SuspendLayout();
            this.panel22.SuspendLayout();
            this.panel23.SuspendLayout();
            this.panel11.SuspendLayout();
            this.panel36.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(201)))), ((int)(((byte)(31)))));
            this.panel1.Controls.Add(this.closeBtn);
            this.panel1.Controls.Add(this.editPatientCaption);
            this.panel1.Location = new System.Drawing.Point(0, 1);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1813, 136);
            this.panel1.TabIndex = 25;
            // 
            // closeBtn
            // 
            this.closeBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.closeBtn.BackgroundImage = global::SelfitMain.Properties.Resources.Blue_Close_ICN;
            this.closeBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.closeBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.closeBtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(201)))), ((int)(((byte)(31)))));
            this.closeBtn.Location = new System.Drawing.Point(1120, 32);
            this.closeBtn.Name = "closeBtn";
            this.closeBtn.Size = new System.Drawing.Size(66, 66);
            this.closeBtn.TabIndex = 1;
            this.closeBtn.UseVisualStyleBackColor = true;
            this.closeBtn.Click += new System.EventHandler(this.closeBtn_Click);
            // 
            // editPatientCaption
            // 
            this.editPatientCaption.Font = new System.Drawing.Font("Roboto", 80F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.editPatientCaption.Location = new System.Drawing.Point(48, 25);
            this.editPatientCaption.Name = "editPatientCaption";
            this.editPatientCaption.Size = new System.Drawing.Size(597, 87);
            this.editPatientCaption.TabIndex = 26;
            this.editPatientCaption.Text = "Edit Patient";
            // 
            // SelectGenderTxt
            // 
            this.SelectGenderTxt.Font = new System.Drawing.Font("Roboto", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.SelectGenderTxt.ForeColor = System.Drawing.Color.Red;
            this.SelectGenderTxt.Location = new System.Drawing.Point(493, 995);
            this.SelectGenderTxt.Name = "SelectGenderTxt";
            this.SelectGenderTxt.Size = new System.Drawing.Size(229, 36);
            this.SelectGenderTxt.TabIndex = 5;
            this.SelectGenderTxt.Text = "SelectGenderTxt";
            this.SelectGenderTxt.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.autoGenIDBtn);
            this.panel5.Controls.Add(this.pIdTxt);
            this.panel5.Controls.Add(this.patientIDlbl);
            this.panel5.Location = new System.Drawing.Point(31, 375);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(574, 81);
            this.panel5.TabIndex = 2;
            // 
            // autoGenIDBtn
            // 
            this.autoGenIDBtn.BackgroundImage = global::SelfitMain.Properties.Resources.autoGen;
            this.autoGenIDBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.autoGenIDBtn.Font = new System.Drawing.Font("Roboto", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.autoGenIDBtn.Location = new System.Drawing.Point(511, 28);
            this.autoGenIDBtn.Name = "autoGenIDBtn";
            this.autoGenIDBtn.Size = new System.Drawing.Size(47, 48);
            this.autoGenIDBtn.TabIndex = 2;
            this.autoGenIDBtn.UseVisualStyleBackColor = true;
            this.autoGenIDBtn.Click += new System.EventHandler(this.autoGenIDBtn_Click);
            // 
            // pIdTxt
            // 
            this.pIdTxt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pIdTxt.Enabled = false;
            this.pIdTxt.Font = new System.Drawing.Font("Roboto", 34F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.pIdTxt.ForeColor = System.Drawing.Color.Black;
            this.pIdTxt.Location = new System.Drawing.Point(7, 28);
            this.pIdTxt.Name = "pIdTxt";
            this.pIdTxt.Size = new System.Drawing.Size(498, 48);
            this.pIdTxt.TabIndex = 1;
            this.pIdTxt.MouseClick += new System.Windows.Forms.MouseEventHandler(this.pIdTxt_MouseClick);
            this.pIdTxt.TextChanged += new System.EventHandler(this.pIdTxt_TextChanged);
            this.pIdTxt.KeyUp += new System.Windows.Forms.KeyEventHandler(this.pIdTxt_KeyUp);
            // 
            // patientIDlbl
            // 
            this.patientIDlbl.Font = new System.Drawing.Font("Roboto", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.patientIDlbl.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(181)))), ((int)(((byte)(181)))), ((int)(((byte)(181)))));
            this.patientIDlbl.Location = new System.Drawing.Point(3, 2);
            this.patientIDlbl.Name = "patientIDlbl";
            this.patientIDlbl.Size = new System.Drawing.Size(139, 39);
            this.patientIDlbl.TabIndex = 0;
            this.patientIDlbl.Text = "Patient ID";
            // 
            // panel8
            // 
            this.panel8.Controls.Add(this.panel3);
            this.panel8.Controls.Add(this.label7);
            this.panel8.Location = new System.Drawing.Point(31, 713);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(570, 289);
            this.panel8.TabIndex = 5;
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.notesTxt);
            this.panel3.ForeColor = System.Drawing.Color.Black;
            this.panel3.Location = new System.Drawing.Point(14, 33);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(544, 246);
            this.panel3.TabIndex = 16;
            // 
            // notesTxt
            // 
            this.notesTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.notesTxt.Font = new System.Drawing.Font("Roboto", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.notesTxt.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(64)))), ((int)(((byte)(71)))));
            this.notesTxt.Location = new System.Drawing.Point(3, -1);
            this.notesTxt.Name = "notesTxt";
            this.notesTxt.Size = new System.Drawing.Size(524, 226);
            this.notesTxt.TabIndex = 0;
            this.notesTxt.Text = "";
            this.notesTxt.MouseClick += new System.Windows.Forms.MouseEventHandler(this.notesTxt_MouseClick);
            this.notesTxt.KeyUp += new System.Windows.Forms.KeyEventHandler(this.notesTxt_KeyUp);
            this.notesTxt.Leave += new System.EventHandler(this.notesTxt_TextChanged);
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("Roboto", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.label7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(181)))), ((int)(((byte)(181)))), ((int)(((byte)(181)))));
            this.label7.Location = new System.Drawing.Point(7, 2);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(139, 21);
            this.label7.TabIndex = 0;
            this.label7.Text = "Notes";
            // 
            // saveDataBtn
            // 
            this.saveDataBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(196)))), ((int)(((byte)(196)))), ((int)(((byte)(196)))));
            this.saveDataBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.saveDataBtn.Font = new System.Drawing.Font("Roboto", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.saveDataBtn.ForeColor = System.Drawing.Color.White;
            this.saveDataBtn.Location = new System.Drawing.Point(1, 1031);
            this.saveDataBtn.Name = "saveDataBtn";
            this.saveDataBtn.Size = new System.Drawing.Size(1204, 68);
            this.saveDataBtn.TabIndex = 12;
            this.saveDataBtn.Text = "Save";
            this.saveDataBtn.UseVisualStyleBackColor = false;
            this.saveDataBtn.Click += new System.EventHandler(this.saveDataBtn_Click);
            // 
            // panel24
            // 
            this.panel24.Controls.Add(this.tenMAddBtn);
            this.panel24.Controls.Add(this.panel25);
            this.panel24.Controls.Add(this.panel27);
            this.panel24.Controls.Add(this.label14);
            this.panel24.Location = new System.Drawing.Point(1207, 263);
            this.panel24.Name = "panel24";
            this.panel24.Size = new System.Drawing.Size(590, 101);
            this.panel24.TabIndex = 8;
            // 
            // tenMAddBtn
            // 
            this.tenMAddBtn.BackColor = System.Drawing.Color.White;
            this.tenMAddBtn.Enabled = false;
            this.tenMAddBtn.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.tenMAddBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.tenMAddBtn.Font = new System.Drawing.Font("Roboto", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.tenMAddBtn.ForeColor = System.Drawing.Color.Black;
            this.tenMAddBtn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.tenMAddBtn.Location = new System.Drawing.Point(396, 28);
            this.tenMAddBtn.Name = "tenMAddBtn";
            this.tenMAddBtn.Size = new System.Drawing.Size(170, 68);
            this.tenMAddBtn.TabIndex = 3;
            this.tenMAddBtn.Text = "Add";
            this.tenMAddBtn.UseVisualStyleBackColor = false;
            this.tenMAddBtn.Click += new System.EventHandler(this.tenMAddBtn_Click);
            // 
            // panel25
            // 
            this.panel25.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel25.Controls.Add(this.tenMstepsTxtBox);
            this.panel25.Controls.Add(this.label16);
            this.panel25.ForeColor = System.Drawing.Color.Black;
            this.panel25.Location = new System.Drawing.Point(220, 28);
            this.panel25.Name = "panel25";
            this.panel25.Size = new System.Drawing.Size(170, 68);
            this.panel25.TabIndex = 2;
            // 
            // tenMstepsTxtBox
            // 
            this.tenMstepsTxtBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tenMstepsTxtBox.Font = new System.Drawing.Font("Roboto", 28F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.tenMstepsTxtBox.Location = new System.Drawing.Point(17, 16);
            this.tenMstepsTxtBox.Name = "tenMstepsTxtBox";
            this.tenMstepsTxtBox.Size = new System.Drawing.Size(74, 34);
            this.tenMstepsTxtBox.TabIndex = 0;
            this.tenMstepsTxtBox.TextChanged += new System.EventHandler(this.tenMstepsTxtBox_TextChanged);
            this.tenMstepsTxtBox.KeyUp += new System.Windows.Forms.KeyEventHandler(this.tenMstepsTxtBox_KeyUp);
            // 
            // label16
            // 
            this.label16.Font = new System.Drawing.Font("Roboto", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.label16.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(181)))), ((int)(((byte)(181)))), ((int)(((byte)(181)))));
            this.label16.Location = new System.Drawing.Point(97, 21);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(56, 29);
            this.label16.TabIndex = 1;
            this.label16.Text = "Steps";
            // 
            // panel27
            // 
            this.panel27.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel27.Controls.Add(this.tenMsecTxtBox);
            this.panel27.Controls.Add(this.label15);
            this.panel27.ForeColor = System.Drawing.Color.Black;
            this.panel27.Location = new System.Drawing.Point(40, 28);
            this.panel27.Name = "panel27";
            this.panel27.Size = new System.Drawing.Size(170, 68);
            this.panel27.TabIndex = 1;
            // 
            // tenMsecTxtBox
            // 
            this.tenMsecTxtBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tenMsecTxtBox.Font = new System.Drawing.Font("Roboto", 28F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.tenMsecTxtBox.Location = new System.Drawing.Point(22, 16);
            this.tenMsecTxtBox.Name = "tenMsecTxtBox";
            this.tenMsecTxtBox.Size = new System.Drawing.Size(74, 34);
            this.tenMsecTxtBox.TabIndex = 0;
            this.tenMsecTxtBox.TextChanged += new System.EventHandler(this.tenMsecTxtBox_TextChanged);
            this.tenMsecTxtBox.KeyUp += new System.Windows.Forms.KeyEventHandler(this.tenMsecTxtBox_KeyUp);
            // 
            // label15
            // 
            this.label15.Font = new System.Drawing.Font("Roboto", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.label15.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(181)))), ((int)(((byte)(181)))), ((int)(((byte)(181)))));
            this.label15.Location = new System.Drawing.Point(102, 21);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(56, 29);
            this.label15.TabIndex = 1;
            this.label15.Text = "Sec";
            // 
            // label14
            // 
            this.label14.Font = new System.Drawing.Font("Roboto", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.label14.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(181)))), ((int)(((byte)(181)))), ((int)(((byte)(181)))));
            this.label14.Location = new System.Drawing.Point(34, 2);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(174, 39);
            this.label14.TabIndex = 0;
            this.label14.Text = "10m Test";
            // 
            // panel26
            // 
            this.panel26.Controls.Add(this.tugAddBtn);
            this.panel26.Controls.Add(this.panel29);
            this.panel26.Controls.Add(this.label19);
            this.panel26.Location = new System.Drawing.Point(1207, 375);
            this.panel26.Name = "panel26";
            this.panel26.Size = new System.Drawing.Size(590, 103);
            this.panel26.TabIndex = 9;
            // 
            // tugAddBtn
            // 
            this.tugAddBtn.BackColor = System.Drawing.Color.White;
            this.tugAddBtn.Enabled = false;
            this.tugAddBtn.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.tugAddBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.tugAddBtn.Font = new System.Drawing.Font("Roboto", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.tugAddBtn.ForeColor = System.Drawing.Color.Black;
            this.tugAddBtn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.tugAddBtn.Location = new System.Drawing.Point(396, 28);
            this.tugAddBtn.Name = "tugAddBtn";
            this.tugAddBtn.Size = new System.Drawing.Size(170, 68);
            this.tugAddBtn.TabIndex = 2;
            this.tugAddBtn.Text = "Add";
            this.tugAddBtn.UseVisualStyleBackColor = false;
            this.tugAddBtn.Click += new System.EventHandler(this.tugAddBtn_Click);
            // 
            // panel29
            // 
            this.panel29.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel29.Controls.Add(this.tugTxtBox);
            this.panel29.Controls.Add(this.label18);
            this.panel29.ForeColor = System.Drawing.Color.Black;
            this.panel29.Location = new System.Drawing.Point(40, 28);
            this.panel29.Name = "panel29";
            this.panel29.Size = new System.Drawing.Size(170, 68);
            this.panel29.TabIndex = 1;
            // 
            // tugTxtBox
            // 
            this.tugTxtBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tugTxtBox.Font = new System.Drawing.Font("Roboto", 28F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.tugTxtBox.Location = new System.Drawing.Point(22, 17);
            this.tugTxtBox.Name = "tugTxtBox";
            this.tugTxtBox.Size = new System.Drawing.Size(74, 34);
            this.tugTxtBox.TabIndex = 0;
            this.tugTxtBox.TextChanged += new System.EventHandler(this.tugTxtBox_TextChanged);
            this.tugTxtBox.KeyUp += new System.Windows.Forms.KeyEventHandler(this.tugTxtBox_KeyUp);
            // 
            // label18
            // 
            this.label18.Font = new System.Drawing.Font("Roboto", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.label18.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(181)))), ((int)(((byte)(181)))), ((int)(((byte)(181)))));
            this.label18.Location = new System.Drawing.Point(102, 22);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(56, 29);
            this.label18.TabIndex = 1;
            this.label18.Text = "Sec";
            // 
            // label19
            // 
            this.label19.Font = new System.Drawing.Font("Roboto", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.label19.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(181)))), ((int)(((byte)(181)))), ((int)(((byte)(181)))));
            this.label19.Location = new System.Drawing.Point(37, 2);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(174, 39);
            this.label19.TabIndex = 0;
            this.label19.Text = "TUG Test";
            // 
            // panel28
            // 
            this.panel28.Controls.Add(this.dgiAddBtn);
            this.panel28.Controls.Add(this.panel31);
            this.panel28.Controls.Add(this.label21);
            this.panel28.Location = new System.Drawing.Point(1207, 489);
            this.panel28.Name = "panel28";
            this.panel28.Size = new System.Drawing.Size(590, 114);
            this.panel28.TabIndex = 10;
            // 
            // dgiAddBtn
            // 
            this.dgiAddBtn.BackColor = System.Drawing.Color.White;
            this.dgiAddBtn.Enabled = false;
            this.dgiAddBtn.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.dgiAddBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.dgiAddBtn.Font = new System.Drawing.Font("Roboto", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.dgiAddBtn.ForeColor = System.Drawing.Color.Black;
            this.dgiAddBtn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.dgiAddBtn.Location = new System.Drawing.Point(396, 28);
            this.dgiAddBtn.Name = "dgiAddBtn";
            this.dgiAddBtn.Size = new System.Drawing.Size(170, 68);
            this.dgiAddBtn.TabIndex = 2;
            this.dgiAddBtn.Text = "Add";
            this.dgiAddBtn.UseVisualStyleBackColor = false;
            this.dgiAddBtn.Click += new System.EventHandler(this.dgiAddBtn_Click);
            // 
            // panel31
            // 
            this.panel31.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel31.Controls.Add(this.dgiCombo);
            this.panel31.ForeColor = System.Drawing.Color.Black;
            this.panel31.Location = new System.Drawing.Point(40, 28);
            this.panel31.Name = "panel31";
            this.panel31.Size = new System.Drawing.Size(170, 68);
            this.panel31.TabIndex = 1;
            // 
            // dgiCombo
            // 
            this.dgiCombo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.dgiCombo.Font = new System.Drawing.Font("Roboto", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.dgiCombo.FormattingEnabled = true;
            this.dgiCombo.ItemHeight = 27;
            this.dgiCombo.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18",
            "19",
            "20",
            "21",
            "22",
            "23",
            "24",
            "25",
            "26",
            "27",
            "28",
            "29",
            "30",
            "31"});
            this.dgiCombo.Location = new System.Drawing.Point(26, 15);
            this.dgiCombo.Name = "dgiCombo";
            this.dgiCombo.Size = new System.Drawing.Size(121, 35);
            this.dgiCombo.TabIndex = 0;
            this.dgiCombo.Text = "Choose";
            this.dgiCombo.SelectedIndexChanged += new System.EventHandler(this.dgiCombo_SelectedIndexChanged);
            // 
            // label21
            // 
            this.label21.Font = new System.Drawing.Font("Roboto", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.label21.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(181)))), ((int)(((byte)(181)))), ((int)(((byte)(181)))));
            this.label21.Location = new System.Drawing.Point(36, 2);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(174, 39);
            this.label21.TabIndex = 0;
            this.label21.Text = "DGI Test";
            // 
            // panel30
            // 
            this.panel30.Controls.Add(this.mocaAddBtn);
            this.panel30.Controls.Add(this.panel32);
            this.panel30.Controls.Add(this.label17);
            this.panel30.Location = new System.Drawing.Point(1207, 615);
            this.panel30.Name = "panel30";
            this.panel30.Size = new System.Drawing.Size(590, 112);
            this.panel30.TabIndex = 11;
            // 
            // mocaAddBtn
            // 
            this.mocaAddBtn.BackColor = System.Drawing.Color.White;
            this.mocaAddBtn.Enabled = false;
            this.mocaAddBtn.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.mocaAddBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.mocaAddBtn.Font = new System.Drawing.Font("Roboto", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.mocaAddBtn.ForeColor = System.Drawing.Color.Black;
            this.mocaAddBtn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.mocaAddBtn.Location = new System.Drawing.Point(396, 28);
            this.mocaAddBtn.Name = "mocaAddBtn";
            this.mocaAddBtn.Size = new System.Drawing.Size(170, 68);
            this.mocaAddBtn.TabIndex = 2;
            this.mocaAddBtn.Text = "Add";
            this.mocaAddBtn.UseVisualStyleBackColor = false;
            this.mocaAddBtn.Click += new System.EventHandler(this.mocaAddBtn_Click);
            // 
            // panel32
            // 
            this.panel32.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel32.Controls.Add(this.mocaCombo);
            this.panel32.ForeColor = System.Drawing.Color.Black;
            this.panel32.Location = new System.Drawing.Point(40, 28);
            this.panel32.Name = "panel32";
            this.panel32.Size = new System.Drawing.Size(170, 68);
            this.panel32.TabIndex = 1;
            // 
            // mocaCombo
            // 
            this.mocaCombo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.mocaCombo.Font = new System.Drawing.Font("Roboto", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.mocaCombo.FormattingEnabled = true;
            this.mocaCombo.ItemHeight = 27;
            this.mocaCombo.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18",
            "19",
            "20",
            "21",
            "22",
            "23",
            "24",
            "25",
            "26",
            "27",
            "28",
            "29",
            "30",
            "31"});
            this.mocaCombo.Location = new System.Drawing.Point(24, 16);
            this.mocaCombo.Name = "mocaCombo";
            this.mocaCombo.Size = new System.Drawing.Size(121, 35);
            this.mocaCombo.TabIndex = 0;
            this.mocaCombo.Text = "Choose";
            this.mocaCombo.SelectedIndexChanged += new System.EventHandler(this.mocaCombo_SelectedIndexChanged);
            // 
            // label17
            // 
            this.label17.Font = new System.Drawing.Font("Roboto", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.label17.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(181)))), ((int)(((byte)(181)))), ((int)(((byte)(181)))));
            this.label17.Location = new System.Drawing.Point(36, 2);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(174, 39);
            this.label17.TabIndex = 0;
            this.label17.Text = "MoCa Test";
            // 
            // panel33
            // 
            this.panel33.Controls.Add(this.panel34);
            this.panel33.Controls.Add(this.label20);
            this.panel33.Location = new System.Drawing.Point(1209, 152);
            this.panel33.Name = "panel33";
            this.panel33.Size = new System.Drawing.Size(590, 105);
            this.panel33.TabIndex = 7;
            // 
            // panel34
            // 
            this.panel34.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel34.Controls.Add(this.userLevelCombo);
            this.panel34.ForeColor = System.Drawing.Color.Black;
            this.panel34.Location = new System.Drawing.Point(40, 28);
            this.panel34.Name = "panel34";
            this.panel34.Size = new System.Drawing.Size(170, 68);
            this.panel34.TabIndex = 1;
            // 
            // userLevelCombo
            // 
            this.userLevelCombo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.userLevelCombo.Font = new System.Drawing.Font("Roboto", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.userLevelCombo.FormattingEnabled = true;
            this.userLevelCombo.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7"});
            this.userLevelCombo.Location = new System.Drawing.Point(24, 12);
            this.userLevelCombo.Name = "userLevelCombo";
            this.userLevelCombo.Size = new System.Drawing.Size(121, 35);
            this.userLevelCombo.TabIndex = 0;
            this.userLevelCombo.Text = "Choose";
            this.userLevelCombo.SelectedIndexChanged += new System.EventHandler(this.userLevelCombo_SelectedIndexChanged);
            // 
            // label20
            // 
            this.label20.Font = new System.Drawing.Font("Roboto", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.label20.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(181)))), ((int)(((byte)(181)))), ((int)(((byte)(181)))));
            this.label20.Location = new System.Drawing.Point(36, 2);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(174, 39);
            this.label20.TabIndex = 0;
            this.label20.Text = "Patient\'s Level";
            // 
            // TestsDataGridView
            // 
            this.TestsDataGridView.AllowUserToAddRows = false;
            this.TestsDataGridView.AllowUserToDeleteRows = false;
            this.TestsDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.TestsDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Date,
            this.Test,
            this.Score,
            this.Time,
            this.Steps});
            this.TestsDataGridView.Location = new System.Drawing.Point(1211, 731);
            this.TestsDataGridView.Name = "TestsDataGridView";
            this.TestsDataGridView.ReadOnly = true;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Roboto", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.TestsDataGridView.RowHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.TestsDataGridView.RowHeadersVisible = false;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Roboto", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TestsDataGridView.RowsDefaultCellStyle = dataGridViewCellStyle7;
            this.TestsDataGridView.RowTemplate.DefaultCellStyle.Font = new System.Drawing.Font("Roboto", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TestsDataGridView.Size = new System.Drawing.Size(586, 294);
            this.TestsDataGridView.TabIndex = 13;
            // 
            // Date
            // 
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Roboto", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Date.DefaultCellStyle = dataGridViewCellStyle1;
            this.Date.HeaderText = "Date";
            this.Date.Name = "Date";
            this.Date.ReadOnly = true;
            this.Date.Width = 180;
            // 
            // Test
            // 
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Roboto", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Test.DefaultCellStyle = dataGridViewCellStyle2;
            this.Test.HeaderText = "Test";
            this.Test.Name = "Test";
            this.Test.ReadOnly = true;
            // 
            // Score
            // 
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Roboto", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Score.DefaultCellStyle = dataGridViewCellStyle3;
            this.Score.FillWeight = 70F;
            this.Score.HeaderText = "Score";
            this.Score.Name = "Score";
            this.Score.ReadOnly = true;
            this.Score.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Time
            // 
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Roboto", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Time.DefaultCellStyle = dataGridViewCellStyle4;
            this.Time.FillWeight = 70F;
            this.Time.HeaderText = "Time";
            this.Time.Name = "Time";
            this.Time.ReadOnly = true;
            this.Time.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Steps
            // 
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Roboto", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Steps.DefaultCellStyle = dataGridViewCellStyle5;
            this.Steps.FillWeight = 70F;
            this.Steps.HeaderText = "Steps";
            this.Steps.Name = "Steps";
            this.Steps.ReadOnly = true;
            this.Steps.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.fNameTxt);
            this.panel4.Controls.Add(this.label1);
            this.panel4.Location = new System.Drawing.Point(31, 154);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(574, 81);
            this.panel4.TabIndex = 0;
            // 
            // fNameTxt
            // 
            this.fNameTxt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.fNameTxt.Font = new System.Drawing.Font("Roboto", 34F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.fNameTxt.ForeColor = System.Drawing.Color.Black;
            this.fNameTxt.Location = new System.Drawing.Point(7, 28);
            this.fNameTxt.Name = "fNameTxt";
            this.fNameTxt.Size = new System.Drawing.Size(546, 48);
            this.fNameTxt.TabIndex = 1;
            this.fNameTxt.MouseClick += new System.Windows.Forms.MouseEventHandler(this.fNameTxt_MouseClick);
            this.fNameTxt.KeyUp += new System.Windows.Forms.KeyEventHandler(this.fNameTxt_KeyUp);
            this.fNameTxt.Leave += new System.EventHandler(this.fNameTxt_TextChanged_1);
            this.fNameTxt.MouseUp += new System.Windows.Forms.MouseEventHandler(this.fNameTxt_MouseUp);
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Roboto", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(181)))), ((int)(((byte)(181)))), ((int)(((byte)(181)))));
            this.label1.Location = new System.Drawing.Point(3, 2);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(139, 39);
            this.label1.TabIndex = 0;
            this.label1.Text = "First Name *";
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.lNameTxt);
            this.panel6.Controls.Add(this.label2);
            this.panel6.Location = new System.Drawing.Point(31, 261);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(574, 81);
            this.panel6.TabIndex = 1;
            // 
            // lNameTxt
            // 
            this.lNameTxt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lNameTxt.Font = new System.Drawing.Font("Roboto", 34F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.lNameTxt.ForeColor = System.Drawing.Color.Black;
            this.lNameTxt.Location = new System.Drawing.Point(7, 28);
            this.lNameTxt.Name = "lNameTxt";
            this.lNameTxt.Size = new System.Drawing.Size(546, 48);
            this.lNameTxt.TabIndex = 1;
            this.lNameTxt.MouseClick += new System.Windows.Forms.MouseEventHandler(this.lNameTxt_MouseClick);
            this.lNameTxt.KeyUp += new System.Windows.Forms.KeyEventHandler(this.lNameTxt_KeyUp);
            this.lNameTxt.Leave += new System.EventHandler(this.lNameTxt_TextChanged_1);
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Roboto", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(181)))), ((int)(((byte)(181)))), ((int)(((byte)(181)))));
            this.label2.Location = new System.Drawing.Point(3, 2);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(139, 39);
            this.label2.TabIndex = 0;
            this.label2.Text = "Last Name";
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.eMailTxt);
            this.panel7.Controls.Add(this.label3);
            this.panel7.Location = new System.Drawing.Point(31, 484);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(574, 81);
            this.panel7.TabIndex = 3;
            // 
            // eMailTxt
            // 
            this.eMailTxt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.eMailTxt.Font = new System.Drawing.Font("Roboto", 34F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.eMailTxt.ForeColor = System.Drawing.Color.Black;
            this.eMailTxt.Location = new System.Drawing.Point(7, 28);
            this.eMailTxt.Name = "eMailTxt";
            this.eMailTxt.Size = new System.Drawing.Size(546, 48);
            this.eMailTxt.TabIndex = 1;
            this.eMailTxt.MouseClick += new System.Windows.Forms.MouseEventHandler(this.eMailTxt_MouseClick);
            this.eMailTxt.KeyUp += new System.Windows.Forms.KeyEventHandler(this.eMailTxt_KeyUp);
            this.eMailTxt.Leave += new System.EventHandler(this.eMailTxt_TextChanged_1);
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Roboto", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(181)))), ((int)(((byte)(181)))), ((int)(((byte)(181)))));
            this.label3.Location = new System.Drawing.Point(3, 2);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(139, 39);
            this.label3.TabIndex = 0;
            this.label3.Text = "E-mail";
            // 
            // panel10
            // 
            this.panel10.Controls.Add(this.phoneTxt);
            this.panel10.Controls.Add(this.label5);
            this.panel10.Location = new System.Drawing.Point(31, 606);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(574, 81);
            this.panel10.TabIndex = 4;
            // 
            // phoneTxt
            // 
            this.phoneTxt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.phoneTxt.Font = new System.Drawing.Font("Roboto", 34F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.phoneTxt.ForeColor = System.Drawing.Color.Black;
            this.phoneTxt.Location = new System.Drawing.Point(7, 28);
            this.phoneTxt.Name = "phoneTxt";
            this.phoneTxt.Size = new System.Drawing.Size(546, 48);
            this.phoneTxt.TabIndex = 1;
            this.phoneTxt.MouseClick += new System.Windows.Forms.MouseEventHandler(this.phoneTxt_MouseClick);
            this.phoneTxt.KeyUp += new System.Windows.Forms.KeyEventHandler(this.phoneTxt_KeyUp);
            this.phoneTxt.Leave += new System.EventHandler(this.phoneTxt_TextChanged);
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Roboto", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(181)))), ((int)(((byte)(181)))), ((int)(((byte)(181)))));
            this.label5.Location = new System.Drawing.Point(3, 2);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(139, 39);
            this.label5.TabIndex = 0;
            this.label5.Text = "Phone Number";
            // 
            // panel9
            // 
            this.panel9.Controls.Add(this.panel12);
            this.panel9.Controls.Add(this.label8);
            this.panel9.Location = new System.Drawing.Point(3, 37);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(590, 105);
            this.panel9.TabIndex = 0;
            // 
            // panel12
            // 
            this.panel12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel12.Controls.Add(this.birthYearCombo);
            this.panel12.ForeColor = System.Drawing.Color.Black;
            this.panel12.Location = new System.Drawing.Point(35, 24);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(203, 68);
            this.panel12.TabIndex = 1;
            // 
            // birthYearCombo
            // 
            this.birthYearCombo.DropDownWidth = 140;
            this.birthYearCombo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.birthYearCombo.Font = new System.Drawing.Font("Roboto", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.birthYearCombo.FormattingEnabled = true;
            this.birthYearCombo.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.birthYearCombo.Location = new System.Drawing.Point(19, 13);
            this.birthYearCombo.Name = "birthYearCombo";
            this.birthYearCombo.Size = new System.Drawing.Size(165, 35);
            this.birthYearCombo.TabIndex = 0;
            this.birthYearCombo.UseWaitCursor = true;
            this.birthYearCombo.SelectedIndexChanged += new System.EventHandler(this.birthYearCombo_SelectedIndexChanged);
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("Roboto", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.label8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(181)))), ((int)(((byte)(181)))), ((int)(((byte)(181)))));
            this.label8.Location = new System.Drawing.Point(32, 2);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(139, 39);
            this.label8.TabIndex = 0;
            this.label8.Text = "Year of birth";
            // 
            // panel17
            // 
            this.panel17.Controls.Add(this.femaleSelectBtn);
            this.panel17.Controls.Add(this.maleSelectBtn);
            this.panel17.Controls.Add(this.label10);
            this.panel17.Location = new System.Drawing.Point(3, 150);
            this.panel17.Name = "panel17";
            this.panel17.Size = new System.Drawing.Size(590, 96);
            this.panel17.TabIndex = 1;
            // 
            // femaleSelectBtn
            // 
            this.femaleSelectBtn.BackColor = System.Drawing.Color.White;
            this.femaleSelectBtn.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.femaleSelectBtn.FlatAppearance.BorderSize = 3;
            this.femaleSelectBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.femaleSelectBtn.Font = new System.Drawing.Font("Roboto", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.femaleSelectBtn.ForeColor = System.Drawing.Color.Black;
            this.femaleSelectBtn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.femaleSelectBtn.Location = new System.Drawing.Point(212, 26);
            this.femaleSelectBtn.Name = "femaleSelectBtn";
            this.femaleSelectBtn.Size = new System.Drawing.Size(170, 68);
            this.femaleSelectBtn.TabIndex = 8;
            this.femaleSelectBtn.Text = "Female";
            this.femaleSelectBtn.UseVisualStyleBackColor = false;
            this.femaleSelectBtn.Click += new System.EventHandler(this.femaleSelectBtn_Click);
            // 
            // maleSelectBtn
            // 
            this.maleSelectBtn.BackColor = System.Drawing.Color.White;
            this.maleSelectBtn.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.maleSelectBtn.FlatAppearance.BorderSize = 3;
            this.maleSelectBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.maleSelectBtn.Font = new System.Drawing.Font("Roboto", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.maleSelectBtn.ForeColor = System.Drawing.Color.Black;
            this.maleSelectBtn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.maleSelectBtn.Location = new System.Drawing.Point(36, 26);
            this.maleSelectBtn.Name = "maleSelectBtn";
            this.maleSelectBtn.Size = new System.Drawing.Size(170, 68);
            this.maleSelectBtn.TabIndex = 1;
            this.maleSelectBtn.Text = "Male";
            this.maleSelectBtn.UseVisualStyleBackColor = false;
            this.maleSelectBtn.Click += new System.EventHandler(this.maleSelectBtn_Click);
            // 
            // label10
            // 
            this.label10.Font = new System.Drawing.Font("Roboto", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.label10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(181)))), ((int)(((byte)(181)))), ((int)(((byte)(181)))));
            this.label10.Location = new System.Drawing.Point(32, 2);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(139, 39);
            this.label10.TabIndex = 0;
            this.label10.Text = "Gender *";
            // 
            // panel18
            // 
            this.panel18.Controls.Add(this.heightCMbtn);
            this.panel18.Controls.Add(this.panel19);
            this.panel18.Controls.Add(this.heightINCHbtn);
            this.panel18.Controls.Add(this.label11);
            this.panel18.Location = new System.Drawing.Point(2, 257);
            this.panel18.Name = "panel18";
            this.panel18.Size = new System.Drawing.Size(590, 103);
            this.panel18.TabIndex = 2;
            // 
            // heightCMbtn
            // 
            this.heightCMbtn.BackColor = System.Drawing.Color.White;
            this.heightCMbtn.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(201)))), ((int)(((byte)(31)))));
            this.heightCMbtn.FlatAppearance.BorderSize = 3;
            this.heightCMbtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.heightCMbtn.Font = new System.Drawing.Font("Roboto", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.heightCMbtn.ForeColor = System.Drawing.Color.Black;
            this.heightCMbtn.Image = global::SelfitMain.Properties.Resources.Vector_2_2;
            this.heightCMbtn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.heightCMbtn.Location = new System.Drawing.Point(235, 26);
            this.heightCMbtn.Name = "heightCMbtn";
            this.heightCMbtn.Size = new System.Drawing.Size(160, 68);
            this.heightCMbtn.TabIndex = 2;
            this.heightCMbtn.Text = "cm";
            this.heightCMbtn.UseVisualStyleBackColor = false;
            this.heightCMbtn.Click += new System.EventHandler(this.heightCMbtn_Click);
            // 
            // panel19
            // 
            this.panel19.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel19.Controls.Add(this.heightCombo);
            this.panel19.ForeColor = System.Drawing.Color.Black;
            this.panel19.Location = new System.Drawing.Point(40, 26);
            this.panel19.Name = "panel19";
            this.panel19.Size = new System.Drawing.Size(170, 68);
            this.panel19.TabIndex = 1;
            // 
            // heightCombo
            // 
            this.heightCombo.DropDownWidth = 140;
            this.heightCombo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.heightCombo.Font = new System.Drawing.Font("Roboto", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.heightCombo.ForeColor = System.Drawing.Color.Black;
            this.heightCombo.FormattingEnabled = true;
            this.heightCombo.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.heightCombo.ItemHeight = 27;
            this.heightCombo.Location = new System.Drawing.Point(15, 17);
            this.heightCombo.Name = "heightCombo";
            this.heightCombo.Size = new System.Drawing.Size(132, 35);
            this.heightCombo.TabIndex = 0;
            this.heightCombo.TabStop = false;
            this.heightCombo.UseWaitCursor = true;
            this.heightCombo.DropDown += new System.EventHandler(this.heightCombo_DropDown);
            this.heightCombo.SelectedIndexChanged += new System.EventHandler(this.heightCombo_SelectedIndexChanged);
            this.heightCombo.DropDownClosed += new System.EventHandler(this.heightCombo_DropDownClosed);
            // 
            // heightINCHbtn
            // 
            this.heightINCHbtn.BackColor = System.Drawing.Color.White;
            this.heightINCHbtn.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.heightINCHbtn.FlatAppearance.BorderSize = 3;
            this.heightINCHbtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.heightINCHbtn.Font = new System.Drawing.Font("Roboto", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.heightINCHbtn.ForeColor = System.Drawing.Color.Black;
            this.heightINCHbtn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.heightINCHbtn.Location = new System.Drawing.Point(408, 26);
            this.heightINCHbtn.Name = "heightINCHbtn";
            this.heightINCHbtn.Size = new System.Drawing.Size(160, 68);
            this.heightINCHbtn.TabIndex = 3;
            this.heightINCHbtn.Text = "Inch";
            this.heightINCHbtn.UseVisualStyleBackColor = false;
            this.heightINCHbtn.Click += new System.EventHandler(this.heightINCHbtn_Click);
            // 
            // label11
            // 
            this.label11.Font = new System.Drawing.Font("Roboto", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.label11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(181)))), ((int)(((byte)(181)))), ((int)(((byte)(181)))));
            this.label11.Location = new System.Drawing.Point(32, 2);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(139, 39);
            this.label11.TabIndex = 0;
            this.label11.Text = "Height";
            // 
            // panel20
            // 
            this.panel20.Controls.Add(this.weightKGbtn);
            this.panel20.Controls.Add(this.panel21);
            this.panel20.Controls.Add(this.weightPOUNDbtn);
            this.panel20.Controls.Add(this.label12);
            this.panel20.Location = new System.Drawing.Point(3, 371);
            this.panel20.Name = "panel20";
            this.panel20.Size = new System.Drawing.Size(590, 114);
            this.panel20.TabIndex = 3;
            // 
            // weightKGbtn
            // 
            this.weightKGbtn.BackColor = System.Drawing.Color.White;
            this.weightKGbtn.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(201)))), ((int)(((byte)(31)))));
            this.weightKGbtn.FlatAppearance.BorderSize = 3;
            this.weightKGbtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.weightKGbtn.Font = new System.Drawing.Font("Roboto", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.weightKGbtn.ForeColor = System.Drawing.Color.Black;
            this.weightKGbtn.Image = global::SelfitMain.Properties.Resources.Vector_2_2;
            this.weightKGbtn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.weightKGbtn.Location = new System.Drawing.Point(234, 26);
            this.weightKGbtn.Name = "weightKGbtn";
            this.weightKGbtn.Size = new System.Drawing.Size(160, 68);
            this.weightKGbtn.TabIndex = 2;
            this.weightKGbtn.Text = "Kg";
            this.weightKGbtn.UseVisualStyleBackColor = false;
            this.weightKGbtn.Click += new System.EventHandler(this.weightKGbtn_Click);
            // 
            // panel21
            // 
            this.panel21.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel21.Controls.Add(this.weightCombo);
            this.panel21.ForeColor = System.Drawing.Color.Black;
            this.panel21.Location = new System.Drawing.Point(40, 26);
            this.panel21.Name = "panel21";
            this.panel21.Size = new System.Drawing.Size(170, 68);
            this.panel21.TabIndex = 1;
            // 
            // weightCombo
            // 
            this.weightCombo.DropDownWidth = 140;
            this.weightCombo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.weightCombo.Font = new System.Drawing.Font("Roboto", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.weightCombo.FormattingEnabled = true;
            this.weightCombo.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.weightCombo.ItemHeight = 27;
            this.weightCombo.Location = new System.Drawing.Point(14, 16);
            this.weightCombo.Name = "weightCombo";
            this.weightCombo.Size = new System.Drawing.Size(132, 35);
            this.weightCombo.TabIndex = 0;
            this.weightCombo.UseWaitCursor = true;
            this.weightCombo.DropDown += new System.EventHandler(this.weightCombo_DropDown);
            this.weightCombo.SelectedIndexChanged += new System.EventHandler(this.weightCombo_SelectedIndexChanged);
            this.weightCombo.SelectionChangeCommitted += new System.EventHandler(this.weightCombo_SelectionChangeCommitted);
            this.weightCombo.DropDownClosed += new System.EventHandler(this.weightCombo_DropDownClosed);
            // 
            // weightPOUNDbtn
            // 
            this.weightPOUNDbtn.BackColor = System.Drawing.Color.White;
            this.weightPOUNDbtn.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.weightPOUNDbtn.FlatAppearance.BorderSize = 3;
            this.weightPOUNDbtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.weightPOUNDbtn.Font = new System.Drawing.Font("Roboto", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.weightPOUNDbtn.ForeColor = System.Drawing.Color.Black;
            this.weightPOUNDbtn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.weightPOUNDbtn.Location = new System.Drawing.Point(408, 26);
            this.weightPOUNDbtn.Name = "weightPOUNDbtn";
            this.weightPOUNDbtn.Size = new System.Drawing.Size(160, 68);
            this.weightPOUNDbtn.TabIndex = 3;
            this.weightPOUNDbtn.Text = "Pound";
            this.weightPOUNDbtn.UseVisualStyleBackColor = false;
            this.weightPOUNDbtn.Click += new System.EventHandler(this.weightPOUNDbtn_Click);
            // 
            // label12
            // 
            this.label12.Font = new System.Drawing.Font("Roboto", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.label12.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(181)))), ((int)(((byte)(181)))), ((int)(((byte)(181)))));
            this.label12.Location = new System.Drawing.Point(32, 2);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(139, 39);
            this.label12.TabIndex = 0;
            this.label12.Text = "Weight";
            // 
            // panel22
            // 
            this.panel22.Controls.Add(this.shoeEUbtn);
            this.panel22.Controls.Add(this.panel23);
            this.panel22.Controls.Add(this.shoeUSbtn);
            this.panel22.Controls.Add(this.label13);
            this.panel22.Location = new System.Drawing.Point(3, 496);
            this.panel22.Name = "panel22";
            this.panel22.Size = new System.Drawing.Size(590, 112);
            this.panel22.TabIndex = 4;
            // 
            // shoeEUbtn
            // 
            this.shoeEUbtn.BackColor = System.Drawing.Color.White;
            this.shoeEUbtn.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(201)))), ((int)(((byte)(31)))));
            this.shoeEUbtn.FlatAppearance.BorderSize = 3;
            this.shoeEUbtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.shoeEUbtn.Font = new System.Drawing.Font("Roboto", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.shoeEUbtn.ForeColor = System.Drawing.Color.Black;
            this.shoeEUbtn.Image = global::SelfitMain.Properties.Resources.Vector_2_2;
            this.shoeEUbtn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.shoeEUbtn.Location = new System.Drawing.Point(234, 27);
            this.shoeEUbtn.Name = "shoeEUbtn";
            this.shoeEUbtn.Size = new System.Drawing.Size(160, 68);
            this.shoeEUbtn.TabIndex = 2;
            this.shoeEUbtn.Text = "EU";
            this.shoeEUbtn.UseVisualStyleBackColor = false;
            this.shoeEUbtn.Click += new System.EventHandler(this.shoeEUbtn_Click);
            // 
            // panel23
            // 
            this.panel23.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel23.Controls.Add(this.shoeCombo);
            this.panel23.ForeColor = System.Drawing.Color.Black;
            this.panel23.Location = new System.Drawing.Point(40, 26);
            this.panel23.Name = "panel23";
            this.panel23.Size = new System.Drawing.Size(170, 68);
            this.panel23.TabIndex = 1;
            // 
            // shoeCombo
            // 
            this.shoeCombo.DropDownWidth = 140;
            this.shoeCombo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.shoeCombo.Font = new System.Drawing.Font("Roboto", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.shoeCombo.FormattingEnabled = true;
            this.shoeCombo.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.shoeCombo.ItemHeight = 27;
            this.shoeCombo.Location = new System.Drawing.Point(14, 17);
            this.shoeCombo.Name = "shoeCombo";
            this.shoeCombo.Size = new System.Drawing.Size(132, 35);
            this.shoeCombo.TabIndex = 0;
            this.shoeCombo.UseWaitCursor = true;
            this.shoeCombo.DropDown += new System.EventHandler(this.shoeCombo_DropDown);
            this.shoeCombo.SelectedIndexChanged += new System.EventHandler(this.shoeCombo_SelectedIndexChanged_1);
            this.shoeCombo.DropDownClosed += new System.EventHandler(this.shoeCombo_DropDownClosed);
            // 
            // shoeUSbtn
            // 
            this.shoeUSbtn.BackColor = System.Drawing.Color.White;
            this.shoeUSbtn.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.shoeUSbtn.FlatAppearance.BorderSize = 3;
            this.shoeUSbtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.shoeUSbtn.Font = new System.Drawing.Font("Roboto", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.shoeUSbtn.ForeColor = System.Drawing.Color.Black;
            this.shoeUSbtn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.shoeUSbtn.Location = new System.Drawing.Point(408, 26);
            this.shoeUSbtn.Name = "shoeUSbtn";
            this.shoeUSbtn.Size = new System.Drawing.Size(160, 68);
            this.shoeUSbtn.TabIndex = 3;
            this.shoeUSbtn.Text = "US";
            this.shoeUSbtn.UseVisualStyleBackColor = false;
            this.shoeUSbtn.Click += new System.EventHandler(this.shoeUSbtn_Click);
            // 
            // label13
            // 
            this.label13.Font = new System.Drawing.Font("Roboto", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.label13.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(181)))), ((int)(((byte)(181)))), ((int)(((byte)(181)))));
            this.label13.Location = new System.Drawing.Point(32, 2);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(139, 39);
            this.label13.TabIndex = 0;
            this.label13.Text = "Shoe Size";
            // 
            // panel11
            // 
            this.panel11.Controls.Add(this.panel36);
            this.panel11.Controls.Add(this.label22);
            this.panel11.Location = new System.Drawing.Point(3, 619);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(590, 112);
            this.panel11.TabIndex = 57;
            // 
            // panel36
            // 
            this.panel36.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel36.Controls.Add(this.LangSelectionCBX);
            this.panel36.ForeColor = System.Drawing.Color.Black;
            this.panel36.Location = new System.Drawing.Point(44, 35);
            this.panel36.Name = "panel36";
            this.panel36.Size = new System.Drawing.Size(170, 68);
            this.panel36.TabIndex = 58;
            // 
            // LangSelectionCBX
            // 
            this.LangSelectionCBX.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.LangSelectionCBX.Font = new System.Drawing.Font("Roboto", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.LangSelectionCBX.FormattingEnabled = true;
            this.LangSelectionCBX.Items.AddRange(new object[] {
            "English",
            "Hebrew"});
            this.LangSelectionCBX.Location = new System.Drawing.Point(13, 15);
            this.LangSelectionCBX.Name = "LangSelectionCBX";
            this.LangSelectionCBX.Size = new System.Drawing.Size(132, 35);
            this.LangSelectionCBX.TabIndex = 13;
            this.LangSelectionCBX.Text = "Hebrew";
            this.LangSelectionCBX.SelectedIndexChanged += new System.EventHandler(this.LangSelectionCBX_SelectedIndexChanged_1);
            // 
            // label22
            // 
            this.label22.Font = new System.Drawing.Font("Roboto", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.label22.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(181)))), ((int)(((byte)(181)))), ((int)(((byte)(181)))));
            this.label22.Location = new System.Drawing.Point(40, 9);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(174, 39);
            this.label22.TabIndex = 57;
            this.label22.Text = "Spoken Language";
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.panel11);
            this.panel2.Controls.Add(this.panel22);
            this.panel2.Controls.Add(this.panel20);
            this.panel2.Controls.Add(this.panel18);
            this.panel2.Controls.Add(this.panel17);
            this.panel2.Controls.Add(this.panel9);
            this.panel2.Location = new System.Drawing.Point(607, 119);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(598, 927);
            this.panel2.TabIndex = 6;
            // 
            // EditPatient
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1204, 1100);
            this.Controls.Add(this.SelectGenderTxt);
            this.Controls.Add(this.panel10);
            this.Controls.Add(this.panel7);
            this.Controls.Add(this.panel6);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.TestsDataGridView);
            this.Controls.Add(this.panel33);
            this.Controls.Add(this.panel30);
            this.Controls.Add(this.panel28);
            this.Controls.Add(this.panel26);
            this.Controls.Add(this.panel24);
            this.Controls.Add(this.saveDataBtn);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel8);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "EditPatient";
            this.Text = "1";
            this.TopMost = true;
            this.panel1.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel8.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel24.ResumeLayout(false);
            this.panel25.ResumeLayout(false);
            this.panel25.PerformLayout();
            this.panel27.ResumeLayout(false);
            this.panel27.PerformLayout();
            this.panel26.ResumeLayout(false);
            this.panel29.ResumeLayout(false);
            this.panel29.PerformLayout();
            this.panel28.ResumeLayout(false);
            this.panel31.ResumeLayout(false);
            this.panel30.ResumeLayout(false);
            this.panel32.ResumeLayout(false);
            this.panel33.ResumeLayout(false);
            this.panel34.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.TestsDataGridView)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.panel10.ResumeLayout(false);
            this.panel10.PerformLayout();
            this.panel9.ResumeLayout(false);
            this.panel12.ResumeLayout(false);
            this.panel17.ResumeLayout(false);
            this.panel18.ResumeLayout(false);
            this.panel19.ResumeLayout(false);
            this.panel20.ResumeLayout(false);
            this.panel21.ResumeLayout(false);
            this.panel22.ResumeLayout(false);
            this.panel23.ResumeLayout(false);
            this.panel11.ResumeLayout(false);
            this.panel36.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button closeBtn;
        private System.Windows.Forms.Label editPatientCaption;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.TextBox pIdTxt;
        private System.Windows.Forms.Label patientIDlbl;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button saveDataBtn;
        private System.Windows.Forms.Panel panel24;
        private System.Windows.Forms.Panel panel27;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Panel panel25;
        private System.Windows.Forms.TextBox tenMstepsTxtBox;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox tenMsecTxtBox;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Panel panel26;
        private System.Windows.Forms.Panel panel29;
        private System.Windows.Forms.TextBox tugTxtBox;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Panel panel28;
        private System.Windows.Forms.Panel panel31;
        private System.Windows.Forms.ComboBox dgiCombo;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Panel panel30;
        private System.Windows.Forms.Panel panel32;
        private System.Windows.Forms.ComboBox mocaCombo;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Panel panel33;
        private System.Windows.Forms.Panel panel34;
        private System.Windows.Forms.ComboBox userLevelCombo;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Button tenMAddBtn;
        private System.Windows.Forms.Button tugAddBtn;
        private System.Windows.Forms.Button dgiAddBtn;
        private System.Windows.Forms.Button mocaAddBtn;
        private System.Windows.Forms.DataGridView TestsDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn Date;
        private System.Windows.Forms.DataGridViewTextBoxColumn Test;
        private System.Windows.Forms.DataGridViewTextBoxColumn Score;
        private System.Windows.Forms.DataGridViewTextBoxColumn Time;
        private System.Windows.Forms.DataGridViewTextBoxColumn Steps;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.RichTextBox notesTxt;
        private System.Windows.Forms.Label SelectGenderTxt;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.TextBox fNameTxt;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.TextBox lNameTxt;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.TextBox eMailTxt;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.TextBox phoneTxt;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button autoGenIDBtn;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.ComboBox birthYearCombo;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Panel panel17;
        private System.Windows.Forms.Button femaleSelectBtn;
        private System.Windows.Forms.Button maleSelectBtn;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Panel panel18;
        private System.Windows.Forms.Button heightCMbtn;
        private System.Windows.Forms.Panel panel19;
        private System.Windows.Forms.ComboBox heightCombo;
        private System.Windows.Forms.Button heightINCHbtn;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Panel panel20;
        private System.Windows.Forms.Button weightKGbtn;
        private System.Windows.Forms.Panel panel21;
        private System.Windows.Forms.ComboBox weightCombo;
        private System.Windows.Forms.Button weightPOUNDbtn;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Panel panel22;
        private System.Windows.Forms.Button shoeEUbtn;
        private System.Windows.Forms.Panel panel23;
        private System.Windows.Forms.ComboBox shoeCombo;
        private System.Windows.Forms.Button shoeUSbtn;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Panel panel36;
        private System.Windows.Forms.ComboBox LangSelectionCBX;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Panel panel2;
    }
}
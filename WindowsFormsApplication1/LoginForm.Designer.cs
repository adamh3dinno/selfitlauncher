﻿using System;
using System.Windows.Forms;

namespace SelfitMain
{
    partial class LogInForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.Label label36;
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint1 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(0D, 10D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint2 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(1D, 15D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint3 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(2D, 22D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint4 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(3D, 13D);
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint5 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(0D, 10D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint6 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(1D, 15D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint7 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(2D, 22D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint8 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(3D, 13D);
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea3 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series3 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint9 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(0D, 10D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint10 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(1D, 15D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint11 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(2D, 22D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint12 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(3D, 13D);
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea4 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series4 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint13 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(0D, 10D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint14 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(1D, 15D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint15 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(2D, 22D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint16 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(3D, 13D);
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea5 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series5 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint17 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(0D, 10D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint18 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(1D, 15D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint19 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(2D, 22D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint20 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(3D, 13D);
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea6 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series6 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint21 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(0D, 10D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint22 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(1D, 15D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint23 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(2D, 22D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint24 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(3D, 13D);
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea7 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series7 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint25 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(0D, 10D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint26 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(1D, 15D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint27 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(2D, 22D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint28 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(3D, 13D);
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LogInForm));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            this.UserPasswordTxtBox = new System.Windows.Forms.TextBox();
            this.lastNamePatientEdit = new System.Windows.Forms.TextBox();
            this.mailPatientEdit = new System.Windows.Forms.TextBox();
            this.phonePatientEdit = new System.Windows.Forms.TextBox();
            this.notesPatientEdit = new System.Windows.Forms.TextBox();
            this.MainTabControl = new System.Windows.Forms.TabControl();
            this.homePage = new System.Windows.Forms.TabPage();
            this.startBtn = new System.Windows.Forms.Button();
            this.panel9 = new System.Windows.Forms.Panel();
            this.SignOutNbtn = new System.Windows.Forms.Button();
            this.settingsBtn = new System.Windows.Forms.Button();
            this.reportsBtn = new System.Windows.Forms.Button();
            this.letsBeginLbl = new System.Windows.Forms.Label();
            this.patientTabControl = new System.Windows.Forms.TabControl();
            this.MainPatient = new System.Windows.Forms.TabPage();
            this.noPatientSelectedLbl = new System.Windows.Forms.Label();
            this.ShowDetails = new System.Windows.Forms.TabPage();
            this.patientDetailsEditBtn = new System.Windows.Forms.Button();
            this.label21 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.patientInfoIDLbl = new System.Windows.Forms.Label();
            this.patientInfoNameLbl = new System.Windows.Forms.Label();
            this.patientInfoNotesTxt = new System.Windows.Forms.TextBox();
            this.patientInfoPhoneTxt = new System.Windows.Forms.TextBox();
            this.patientInfoEmailTxt = new System.Windows.Forms.TextBox();
            this.NotesLbl = new System.Windows.Forms.Label();
            this.PhoneNumberLbl = new System.Windows.Forms.Label();
            this.EmailLbl = new System.Windows.Forms.Label();
            this.IDlabel = new System.Windows.Forms.Label();
            this.NameLbl = new System.Windows.Forms.Label();
            this.EditDetails = new System.Windows.Forms.TabPage();
            this.fastEditDetailsBtn = new System.Windows.Forms.Button();
            this.panel28 = new System.Windows.Forms.Panel();
            this.panel27 = new System.Windows.Forms.Panel();
            this.panel25 = new System.Windows.Forms.Panel();
            this.panel24 = new System.Windows.Forms.Panel();
            this.IdPatientEdit = new System.Windows.Forms.TextBox();
            this.panel23 = new System.Windows.Forms.Panel();
            this.panel22 = new System.Windows.Forms.Panel();
            this.firstNamePatientEdit = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.patientListArrowDownBtn = new System.Windows.Forms.Button();
            this.PatientsListBox = new System.Windows.Forms.ListBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.patientSearchMagBtn = new System.Windows.Forms.Button();
            this.patientSearchTxt = new System.Windows.Forms.TextBox();
            this.AddPatientBtn = new System.Windows.Forms.Button();
            this.selectPatientBtn = new System.Windows.Forms.Button();
            this.patientCountLbl = new System.Windows.Forms.Button();
            this.pNameMsgLbl = new System.Windows.Forms.Label();
            this.button10 = new System.Windows.Forms.Button();
            this.PlanPage = new System.Windows.Forms.TabPage();
            this.buttonDown = new System.Windows.Forms.Button();
            this.buttonUp = new System.Windows.Forms.Button();
            this.addExPicPbx = new System.Windows.Forms.PictureBox();
            this.button8 = new System.Windows.Forms.Button();
            this.exercisesFlowPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.panel44 = new System.Windows.Forms.Panel();
            this.practiceDnBtn = new System.Windows.Forms.Button();
            this.practiceUpBtn = new System.Windows.Forms.Button();
            this.button71 = new System.Windows.Forms.Button();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.label43 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.pictureBox11 = new System.Windows.Forms.PictureBox();
            this.panel29 = new System.Windows.Forms.Panel();
            this.button61 = new System.Windows.Forms.Button();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label42 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.pictureBox10 = new System.Windows.Forms.PictureBox();
            this.exeSaveForLaterBtn = new System.Windows.Forms.Button();
            this.panel26 = new System.Windows.Forms.Panel();
            this.filterFlowLayoutPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.label33 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.practicesLayoutPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.panel50 = new System.Windows.Forms.Panel();
            this.button63 = new System.Windows.Forms.Button();
            this.label103 = new System.Windows.Forms.Label();
            this.panel51 = new System.Windows.Forms.Panel();
            this.button64 = new System.Windows.Forms.Button();
            this.label101 = new System.Windows.Forms.Label();
            this.panel52 = new System.Windows.Forms.Panel();
            this.button65 = new System.Windows.Forms.Button();
            this.label102 = new System.Windows.Forms.Label();
            this.panel53 = new System.Windows.Forms.Panel();
            this.button66 = new System.Windows.Forms.Button();
            this.label104 = new System.Windows.Forms.Label();
            this.panel54 = new System.Windows.Forms.Panel();
            this.button67 = new System.Windows.Forms.Button();
            this.label105 = new System.Windows.Forms.Label();
            this.panel55 = new System.Windows.Forms.Panel();
            this.button68 = new System.Windows.Forms.Button();
            this.label106 = new System.Windows.Forms.Label();
            this.panel56 = new System.Windows.Forms.Panel();
            this.button69 = new System.Windows.Forms.Button();
            this.label107 = new System.Windows.Forms.Label();
            this.panel57 = new System.Windows.Forms.Panel();
            this.button70 = new System.Windows.Forms.Button();
            this.label108 = new System.Windows.Forms.Label();
            this.exeSaveAndStartBtn = new System.Windows.Forms.Button();
            this.exercisesTotalsTxt = new System.Windows.Forms.Label();
            this.label124 = new System.Windows.Forms.Label();
            this.label125 = new System.Windows.Forms.Label();
            this.EditPlanNameBtn = new System.Windows.Forms.Button();
            this.panel49 = new System.Windows.Forms.Panel();
            this.exercisesGReportsBtn = new System.Windows.Forms.Button();
            this.patientHistoryBtnLbl = new System.Windows.Forms.Label();
            this.exercisesPlanBtn = new System.Windows.Forms.Button();
            this.PlanPanel = new System.Windows.Forms.Panel();
            this.plansExitBtn = new System.Windows.Forms.Button();
            this.panel71 = new System.Windows.Forms.Panel();
            this.plansViewEditBtn = new System.Windows.Forms.Button();
            this.planPatientNameLbl = new System.Windows.Forms.Label();
            this.label99 = new System.Windows.Forms.Label();
            this.panel47 = new System.Windows.Forms.Panel();
            this.createPlanBtnLbl = new System.Windows.Forms.Button();
            this.panel48 = new System.Windows.Forms.Panel();
            this.exercisesQplanBtn = new System.Windows.Forms.Button();
            this.SavePlanNameBtn = new System.Windows.Forms.Button();
            this.planNameLbl = new System.Windows.Forms.TextBox();
            this.HistoryTab = new System.Windows.Forms.TabPage();
            this.parkinsonSessionDataPanel = new System.Windows.Forms.Panel();
            this.ParkinsontabControl = new System.Windows.Forms.TabControl();
            this.summaryDataTab = new System.Windows.Forms.TabPage();
            this.label31 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.ParkinsonSessionDataLayoutPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.panel98 = new System.Windows.Forms.Panel();
            this.DataViewTab = new System.Windows.Forms.TabPage();
            this.allDataBtn = new System.Windows.Forms.Button();
            this.stdDevBtn = new System.Windows.Forms.Button();
            this.validStepsBtn = new System.Windows.Forms.Button();
            this.ParkinsonDataGridView = new System.Windows.Forms.DataGridView();
            this.stepType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.stepLength = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.stepTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.stage = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel92 = new System.Windows.Forms.Panel();
            this.historyQuickPlanBtn = new System.Windows.Forms.Button();
            this.createNewPlanBtn = new System.Windows.Forms.Button();
            this.exTitleHeader = new System.Windows.Forms.Panel();
            this.label69 = new System.Windows.Forms.Label();
            this.label68 = new System.Windows.Forms.Label();
            this.label70 = new System.Windows.Forms.Label();
            this.startPlanBtn = new System.Windows.Forms.Button();
            this.selectAndEditPlanBtn = new System.Windows.Forms.Button();
            this.historyTotalExeTxt = new System.Windows.Forms.Label();
            this.exerciesLayout = new System.Windows.Forms.FlowLayoutPanel();
            this.panel37 = new System.Windows.Forms.Panel();
            this.label73 = new System.Windows.Forms.Label();
            this.label72 = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.label71 = new System.Windows.Forms.Label();
            this.panel38 = new System.Windows.Forms.Panel();
            this.label74 = new System.Windows.Forms.Label();
            this.label75 = new System.Windows.Forms.Label();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.label76 = new System.Windows.Forms.Label();
            this.panel39 = new System.Windows.Forms.Panel();
            this.label77 = new System.Windows.Forms.Label();
            this.label78 = new System.Windows.Forms.Label();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.label79 = new System.Windows.Forms.Label();
            this.panel40 = new System.Windows.Forms.Panel();
            this.label80 = new System.Windows.Forms.Label();
            this.label81 = new System.Windows.Forms.Label();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.label82 = new System.Windows.Forms.Label();
            this.panel41 = new System.Windows.Forms.Panel();
            this.label83 = new System.Windows.Forms.Label();
            this.label84 = new System.Windows.Forms.Label();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.label85 = new System.Windows.Forms.Label();
            this.panel42 = new System.Windows.Forms.Panel();
            this.label86 = new System.Windows.Forms.Label();
            this.label87 = new System.Windows.Forms.Label();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.label88 = new System.Windows.Forms.Label();
            this.panel43 = new System.Windows.Forms.Panel();
            this.label89 = new System.Windows.Forms.Label();
            this.label90 = new System.Windows.Forms.Label();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.label91 = new System.Windows.Forms.Label();
            this.sessionDatainfoPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.panel30 = new System.Windows.Forms.Panel();
            this.histTimeDataLbl = new System.Windows.Forms.Label();
            this.timeChart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.histTimeRateLbl = new System.Windows.Forms.Label();
            this.histTimeLbl = new System.Windows.Forms.Label();
            this.panel31 = new System.Windows.Forms.Panel();
            this.histDistanceDataLbl = new System.Windows.Forms.Label();
            this.DistanceChart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.histDistanceRateLbl = new System.Windows.Forms.Label();
            this.histDistanceLbl = new System.Windows.Forms.Label();
            this.panel32 = new System.Windows.Forms.Panel();
            this.histStepCountDataLbl = new System.Windows.Forms.Label();
            this.stepNumChart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.histStepCountRateLbl = new System.Windows.Forms.Label();
            this.histStepCountLbl = new System.Windows.Forms.Label();
            this.panel33 = new System.Windows.Forms.Panel();
            this.histSpeedDataLbl = new System.Windows.Forms.Label();
            this.histSpeedLbl = new System.Windows.Forms.Label();
            this.speedChart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.histSpeedRateLbl = new System.Windows.Forms.Label();
            this.panel34 = new System.Windows.Forms.Panel();
            this.histHRightDataLbl = new System.Windows.Forms.Label();
            this.stepHRchart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.histHRightRateLbl = new System.Windows.Forms.Label();
            this.histHRightLbl = new System.Windows.Forms.Label();
            this.panel35 = new System.Windows.Forms.Panel();
            this.histHLeftDataLbl = new System.Windows.Forms.Label();
            this.stepHLchart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.histHLeftRateLbl = new System.Windows.Forms.Label();
            this.histHLeftLbl = new System.Windows.Forms.Label();
            this.panel36 = new System.Windows.Forms.Panel();
            this.histMoveTimeDataLbl = new System.Windows.Forms.Label();
            this.moveTimeChart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.histMoveTimeRateLbl = new System.Windows.Forms.Label();
            this.histMoveTimeLbl = new System.Windows.Forms.Label();
            this.PlanNameTxt = new System.Windows.Forms.Label();
            this.PlanDateTxt = new System.Windows.Forms.Label();
            this.panel19 = new System.Windows.Forms.Panel();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.panel17 = new System.Windows.Forms.Panel();
            this.historyGeneralReportsBtn = new System.Windows.Forms.Button();
            this.historyPlansBtn = new System.Windows.Forms.Button();
            this.panel15 = new System.Windows.Forms.Panel();
            this.historyReportBtn = new System.Windows.Forms.Button();
            this.historyExitBtn = new System.Windows.Forms.Button();
            this.historyViewEditBtn = new System.Windows.Forms.Button();
            this.historyPatientNameLbl = new System.Windows.Forms.Label();
            this.panel16 = new System.Windows.Forms.Panel();
            this.label11 = new System.Windows.Forms.Label();
            this.panel18 = new System.Windows.Forms.Panel();
            this.noPlansAvailableTxt = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.label12 = new System.Windows.Forms.Label();
            this.SessionsFlowLayoutPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.panel21 = new System.Windows.Forms.Panel();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.panel20 = new System.Windows.Forms.Panel();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.createPlanBtn = new System.Windows.Forms.Button();
            this.LoginPage = new System.Windows.Forms.TabPage();
            this.panel8 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel7 = new System.Windows.Forms.Panel();
            this.userNameTxtLbl = new System.Windows.Forms.Label();
            this.passwordTxtLbl = new System.Windows.Forms.Label();
            this.incorrectUserTxt = new System.Windows.Forms.Label();
            this.keyBoardBtn = new System.Windows.Forms.Button();
            this.LoginExitBtn = new System.Windows.Forms.Button();
            this.WelcomeBackLbl = new System.Windows.Forms.Label();
            this.OkBtn = new System.Windows.Forms.Button();
            this.UserNameTxtBox = new System.Windows.Forms.TextBox();
            this.SignInLbl = new System.Windows.Forms.Label();
            this.PlanView = new System.Windows.Forms.TabPage();
            this.panel61 = new System.Windows.Forms.Panel();
            this.pViewReportsBtn = new System.Windows.Forms.Button();
            this.panel62 = new System.Windows.Forms.Panel();
            this.button26 = new System.Windows.Forms.Button();
            this.panel60 = new System.Windows.Forms.Panel();
            this.pHistPViewLblBtn = new System.Windows.Forms.Label();
            this.pViewQplanBtn = new System.Windows.Forms.Button();
            this.loadPlansPviewBtn = new System.Windows.Forms.Button();
            this.panel63 = new System.Windows.Forms.Panel();
            this.label55 = new System.Windows.Forms.Label();
            this.label54 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.panel65 = new System.Windows.Forms.Panel();
            this.EXviewGroupBox = new System.Windows.Forms.GroupBox();
            this.label112 = new System.Windows.Forms.Label();
            this.label111 = new System.Windows.Forms.Label();
            this.viewPracticeNameLbl = new System.Windows.Forms.Label();
            this.label93 = new System.Windows.Forms.Label();
            this.label128 = new System.Windows.Forms.Label();
            this.label129 = new System.Windows.Forms.Label();
            this.planViewStartPlanBtn = new System.Windows.Forms.Button();
            this.planViewSelectEditBtn = new System.Windows.Forms.Button();
            this.planViewTotalexeLbl = new System.Windows.Forms.Label();
            this.PlanDetailsFlowPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.panel68 = new System.Windows.Forms.Panel();
            this.label94 = new System.Windows.Forms.Label();
            this.label96 = new System.Windows.Forms.Label();
            this.pictureBox9 = new System.Windows.Forms.PictureBox();
            this.label98 = new System.Windows.Forms.Label();
            this.panel69 = new System.Windows.Forms.Panel();
            this.label100 = new System.Windows.Forms.Label();
            this.label109 = new System.Windows.Forms.Label();
            this.pictureBox12 = new System.Windows.Forms.PictureBox();
            this.label110 = new System.Windows.Forms.Label();
            this.panel70 = new System.Windows.Forms.Panel();
            this.label113 = new System.Windows.Forms.Label();
            this.label114 = new System.Windows.Forms.Label();
            this.pictureBox13 = new System.Windows.Forms.PictureBox();
            this.label115 = new System.Windows.Forms.Label();
            this.planViewCreatePlanBtn = new System.Windows.Forms.Button();
            this.panel64 = new System.Windows.Forms.Panel();
            this.label47 = new System.Windows.Forms.Label();
            this.searchNameTxt = new System.Windows.Forms.TextBox();
            this.button11 = new System.Windows.Forms.Button();
            this.panel59 = new System.Windows.Forms.Panel();
            this.pViewExitBtn = new System.Windows.Forms.Button();
            this.planVviewAndEditBtn = new System.Windows.Forms.Button();
            this.planViewPatientNameLbl = new System.Windows.Forms.Label();
            this.planViewLayoutPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.panel67 = new System.Windows.Forms.Panel();
            this.label63 = new System.Windows.Forms.Label();
            this.label64 = new System.Windows.Forms.Label();
            this.label66 = new System.Windows.Forms.Label();
            this.label67 = new System.Windows.Forms.Label();
            this.panel66 = new System.Windows.Forms.Panel();
            this.label57 = new System.Windows.Forms.Label();
            this.label58 = new System.Windows.Forms.Label();
            this.label60 = new System.Windows.Forms.Label();
            this.label61 = new System.Windows.Forms.Label();
            this.Settings = new System.Windows.Forms.TabPage();
            this.panel1 = new System.Windows.Forms.Panel();
            this.button3 = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.parkinsonModeCBX = new System.Windows.Forms.CheckBox();
            this.autoGenIDcbx = new System.Windows.Forms.CheckBox();
            this.button2 = new System.Windows.Forms.Button();
            this.selfitPathTxt = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.useHebSpeachCBX = new System.Windows.Forms.CheckBox();
            this.button46 = new System.Windows.Forms.Button();
            this.button45 = new System.Windows.Forms.Button();
            this.reports = new System.Windows.Forms.TabPage();
            this.reportsFlowLayoutPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.panel11 = new System.Windows.Forms.Panel();
            this.label32 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.panel58 = new System.Windows.Forms.Panel();
            this.pictureBox14 = new System.Windows.Forms.PictureBox();
            this.label38 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.reportsSelectExercisesBtn = new System.Windows.Forms.Button();
            this.reportsSelectPlansBtn = new System.Windows.Forms.Button();
            this.panel10 = new System.Windows.Forms.Panel();
            this.reportsPlansBtn = new System.Windows.Forms.Button();
            this.reportsPHistoryBtn = new System.Windows.Forms.Label();
            this.reportsQplanBtn = new System.Windows.Forms.Button();
            this.reportsExercisesBtn = new System.Windows.Forms.Button();
            this.panel6 = new System.Windows.Forms.Panel();
            this.button18 = new System.Windows.Forms.Button();
            this.panel5 = new System.Windows.Forms.Panel();
            this.label118 = new System.Windows.Forms.Label();
            this.reportsBackBtn = new System.Windows.Forms.Button();
            this.reportsCloseBtn = new System.Windows.Forms.Button();
            this.reportsPatientEditBtn = new System.Windows.Forms.Button();
            this.reportsPatientNameLbl = new System.Windows.Forms.Label();
            this.reportsTabControl = new System.Windows.Forms.TabControl();
            this.ReportsPlansTabPage = new System.Windows.Forms.TabPage();
            this.plansDataGridView = new System.Windows.Forms.DataGridView();
            this.Session = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sessionDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Total_Distance = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Step_Count = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Session_Time_gross = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Speed = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Step_hight_Right = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Step_hight_Left = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Step_Length_Right = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Step_Length_Left = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Movement_Time = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ReportsExercisesTabPage = new System.Windows.Forms.TabPage();
            this.selectedPracticeLbl = new System.Windows.Forms.Label();
            this.parametersDataflowLayoutPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.MeasuresDataGridView = new System.Windows.Forms.DataGridView();
            this.selectedParametersFlowLayoutPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.panel12 = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.panel14 = new System.Windows.Forms.Panel();
            this.generalReports = new System.Windows.Forms.TabPage();
            this.panel80 = new System.Windows.Forms.Panel();
            this.panel86 = new System.Windows.Forms.Panel();
            this.sumRectExercisesCountTxt = new System.Windows.Forms.Label();
            this.panel87 = new System.Windows.Forms.Panel();
            this.sumRectTotalExercisesTxt = new System.Windows.Forms.Label();
            this.label130 = new System.Windows.Forms.Label();
            this.label131 = new System.Windows.Forms.Label();
            this.panel90 = new System.Windows.Forms.Panel();
            this.sumRectTimeCountTxt = new System.Windows.Forms.Label();
            this.panel91 = new System.Windows.Forms.Panel();
            this.sumRectTotalTimeTxt = new System.Windows.Forms.Label();
            this.label144 = new System.Windows.Forms.Label();
            this.label145 = new System.Windows.Forms.Label();
            this.panel88 = new System.Windows.Forms.Panel();
            this.sumRectSessionsCountTxt = new System.Windows.Forms.Label();
            this.panel89 = new System.Windows.Forms.Panel();
            this.sumRectTotalSessionsTxt = new System.Windows.Forms.Label();
            this.label140 = new System.Windows.Forms.Label();
            this.label141 = new System.Windows.Forms.Label();
            this.panel84 = new System.Windows.Forms.Panel();
            this.sumRectPatientsCountTxt = new System.Windows.Forms.Label();
            this.panel85 = new System.Windows.Forms.Panel();
            this.sumRectTotPatientsTxt = new System.Windows.Forms.Label();
            this.label132 = new System.Windows.Forms.Label();
            this.label133 = new System.Windows.Forms.Label();
            this.panel82 = new System.Windows.Forms.Panel();
            this.sumRectMonthsTxt = new System.Windows.Forms.Label();
            this.panel83 = new System.Windows.Forms.Panel();
            this.sumRectDurationTxt = new System.Windows.Forms.Label();
            this.label122 = new System.Windows.Forms.Label();
            this.label123 = new System.Windows.Forms.Label();
            this.generalReportsListView = new System.Windows.Forms.DataGridView();
            this.No = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Date = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PatientNum = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Exercises = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PracticeTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.genRepShowBtn = new System.Windows.Forms.Button();
            this.panel81 = new System.Windows.Forms.Panel();
            this.label121 = new System.Windows.Forms.Label();
            this.genRepPnameFilterText = new System.Windows.Forms.TextBox();
            this.button7 = new System.Windows.Forms.Button();
            this.panel77 = new System.Windows.Forms.Panel();
            this.panel78 = new System.Windows.Forms.Panel();
            this.genRepToYearCombo = new System.Windows.Forms.ComboBox();
            this.panel79 = new System.Windows.Forms.Panel();
            this.genRepToMonthCombo = new System.Windows.Forms.ComboBox();
            this.label120 = new System.Windows.Forms.Label();
            this.panel74 = new System.Windows.Forms.Panel();
            this.panel75 = new System.Windows.Forms.Panel();
            this.genRepFromYearCombo = new System.Windows.Forms.ComboBox();
            this.panel76 = new System.Windows.Forms.Panel();
            this.genRepFromMonthCombo = new System.Windows.Forms.ComboBox();
            this.label119 = new System.Windows.Forms.Label();
            this.panel46 = new System.Windows.Forms.Panel();
            this.button4 = new System.Windows.Forms.Button();
            this.label117 = new System.Windows.Forms.Label();
            this.QuickPlan = new System.Windows.Forms.TabPage();
            this.qpSelectNoneBtn = new System.Windows.Forms.Button();
            this.qpSelectAllBtn = new System.Windows.Forms.Button();
            this.button25 = new System.Windows.Forms.Button();
            this.button14 = new System.Windows.Forms.Button();
            this.qpStartPlanBtn = new System.Windows.Forms.Button();
            this.sittingBtn = new System.Windows.Forms.Button();
            this.duration30mBtn = new System.Windows.Forms.Button();
            this.duration25mBtn = new System.Windows.Forms.Button();
            this.duration20mBtn = new System.Windows.Forms.Button();
            this.duration15mBtn = new System.Windows.Forms.Button();
            this.duration10mBtn = new System.Windows.Forms.Button();
            this.duration5mBtn = new System.Windows.Forms.Button();
            this.button13 = new System.Windows.Forms.Button();
            this.button12 = new System.Windows.Forms.Button();
            this.maleSelectBtn = new System.Windows.Forms.Button();
            this.panel97 = new System.Windows.Forms.Panel();
            this.qPlanReportsBtn = new System.Windows.Forms.Button();
            this.qPlanHistoryBtn = new System.Windows.Forms.Label();
            this.qPlanPlanBtn = new System.Windows.Forms.Button();
            this.panel96 = new System.Windows.Forms.Panel();
            this.qPlanExercisesBtn = new System.Windows.Forms.Button();
            this.panel95 = new System.Windows.Forms.Panel();
            this.button9 = new System.Windows.Forms.Button();
            this.panel93 = new System.Windows.Forms.Panel();
            this.button5 = new System.Windows.Forms.Button();
            this.panel94 = new System.Windows.Forms.Panel();
            this.qpEditBtn = new System.Windows.Forms.Button();
            this.qpPatientNameLbl = new System.Windows.Forms.Label();
            this.qpAerobicBtn = new System.Windows.Forms.Button();
            this.qpAgilityBtn = new System.Windows.Forms.Button();
            this.qpCoordinationBtn = new System.Windows.Forms.Button();
            this.qpGaitBtn = new System.Windows.Forms.Button();
            this.qpReactiontimeBtn = new System.Windows.Forms.Button();
            this.qpMultitaskingBtn = new System.Windows.Forms.Button();
            this.qpMovementplanningBtn = new System.Windows.Forms.Button();
            this.qpBalanceBtn = new System.Windows.Forms.Button();
            this.standingBtn = new System.Windows.Forms.Button();
            this.BottomToolStripPanel = new System.Windows.Forms.ToolStripPanel();
            this.TopToolStripPanel = new System.Windows.Forms.ToolStripPanel();
            this.RightToolStripPanel = new System.Windows.Forms.ToolStripPanel();
            this.LeftToolStripPanel = new System.Windows.Forms.ToolStripPanel();
            this.ContentPanel = new System.Windows.Forms.ToolStripContentPanel();
            label36 = new System.Windows.Forms.Label();
            this.MainTabControl.SuspendLayout();
            this.homePage.SuspendLayout();
            this.panel9.SuspendLayout();
            this.patientTabControl.SuspendLayout();
            this.MainPatient.SuspendLayout();
            this.ShowDetails.SuspendLayout();
            this.EditDetails.SuspendLayout();
            this.panel28.SuspendLayout();
            this.panel27.SuspendLayout();
            this.panel25.SuspendLayout();
            this.panel24.SuspendLayout();
            this.panel23.SuspendLayout();
            this.panel22.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel3.SuspendLayout();
            this.PlanPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.addExPicPbx)).BeginInit();
            this.exercisesFlowPanel.SuspendLayout();
            this.panel44.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).BeginInit();
            this.panel29.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).BeginInit();
            this.panel26.SuspendLayout();
            this.filterFlowLayoutPanel.SuspendLayout();
            this.practicesLayoutPanel.SuspendLayout();
            this.panel50.SuspendLayout();
            this.panel51.SuspendLayout();
            this.panel52.SuspendLayout();
            this.panel53.SuspendLayout();
            this.panel54.SuspendLayout();
            this.panel55.SuspendLayout();
            this.panel56.SuspendLayout();
            this.panel57.SuspendLayout();
            this.panel49.SuspendLayout();
            this.PlanPanel.SuspendLayout();
            this.panel47.SuspendLayout();
            this.panel48.SuspendLayout();
            this.HistoryTab.SuspendLayout();
            this.parkinsonSessionDataPanel.SuspendLayout();
            this.ParkinsontabControl.SuspendLayout();
            this.summaryDataTab.SuspendLayout();
            this.ParkinsonSessionDataLayoutPanel.SuspendLayout();
            this.DataViewTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ParkinsonDataGridView)).BeginInit();
            this.panel92.SuspendLayout();
            this.exTitleHeader.SuspendLayout();
            this.exerciesLayout.SuspendLayout();
            this.panel37.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.panel38.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.panel39.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            this.panel40.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            this.panel41.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            this.panel42.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            this.panel43.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            this.sessionDatainfoPanel.SuspendLayout();
            this.panel30.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.timeChart)).BeginInit();
            this.panel31.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DistanceChart)).BeginInit();
            this.panel32.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.stepNumChart)).BeginInit();
            this.panel33.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.speedChart)).BeginInit();
            this.panel34.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.stepHRchart)).BeginInit();
            this.panel35.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.stepHLchart)).BeginInit();
            this.panel36.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.moveTimeChart)).BeginInit();
            this.panel19.SuspendLayout();
            this.panel17.SuspendLayout();
            this.panel15.SuspendLayout();
            this.panel16.SuspendLayout();
            this.panel18.SuspendLayout();
            this.SessionsFlowLayoutPanel.SuspendLayout();
            this.panel21.SuspendLayout();
            this.panel20.SuspendLayout();
            this.LoginPage.SuspendLayout();
            this.panel8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel7.SuspendLayout();
            this.PlanView.SuspendLayout();
            this.panel61.SuspendLayout();
            this.panel62.SuspendLayout();
            this.panel60.SuspendLayout();
            this.panel65.SuspendLayout();
            this.EXviewGroupBox.SuspendLayout();
            this.PlanDetailsFlowPanel.SuspendLayout();
            this.panel68.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).BeginInit();
            this.panel69.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).BeginInit();
            this.panel70.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).BeginInit();
            this.panel64.SuspendLayout();
            this.panel59.SuspendLayout();
            this.planViewLayoutPanel.SuspendLayout();
            this.panel67.SuspendLayout();
            this.panel66.SuspendLayout();
            this.Settings.SuspendLayout();
            this.panel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.reports.SuspendLayout();
            this.reportsFlowLayoutPanel.SuspendLayout();
            this.panel11.SuspendLayout();
            this.panel58.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).BeginInit();
            this.panel10.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel5.SuspendLayout();
            this.reportsTabControl.SuspendLayout();
            this.ReportsPlansTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.plansDataGridView)).BeginInit();
            this.ReportsExercisesTabPage.SuspendLayout();
            this.parametersDataflowLayoutPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MeasuresDataGridView)).BeginInit();
            this.selectedParametersFlowLayoutPanel.SuspendLayout();
            this.panel12.SuspendLayout();
            this.generalReports.SuspendLayout();
            this.panel80.SuspendLayout();
            this.panel86.SuspendLayout();
            this.panel87.SuspendLayout();
            this.panel90.SuspendLayout();
            this.panel91.SuspendLayout();
            this.panel88.SuspendLayout();
            this.panel89.SuspendLayout();
            this.panel84.SuspendLayout();
            this.panel85.SuspendLayout();
            this.panel82.SuspendLayout();
            this.panel83.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.generalReportsListView)).BeginInit();
            this.panel81.SuspendLayout();
            this.panel77.SuspendLayout();
            this.panel78.SuspendLayout();
            this.panel79.SuspendLayout();
            this.panel74.SuspendLayout();
            this.panel75.SuspendLayout();
            this.panel76.SuspendLayout();
            this.panel46.SuspendLayout();
            this.QuickPlan.SuspendLayout();
            this.panel97.SuspendLayout();
            this.panel96.SuspendLayout();
            this.panel95.SuspendLayout();
            this.panel93.SuspendLayout();
            this.SuspendLayout();
            // 
            // label36
            // 
            label36.BackColor = System.Drawing.Color.Transparent;
            label36.Font = new System.Drawing.Font("Roboto", 22F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            label36.ForeColor = System.Drawing.Color.Black;
            label36.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            label36.Location = new System.Drawing.Point(13, 6);
            label36.Name = "label36";
            label36.Size = new System.Drawing.Size(265, 41);
            label36.TabIndex = 3;
            label36.Text = "Time";
            label36.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // UserPasswordTxtBox
            // 
            this.UserPasswordTxtBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.UserPasswordTxtBox.Font = new System.Drawing.Font("Roboto", 42F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.UserPasswordTxtBox.Location = new System.Drawing.Point(198, 742);
            this.UserPasswordTxtBox.Name = "UserPasswordTxtBox";
            this.UserPasswordTxtBox.PasswordChar = '*';
            this.UserPasswordTxtBox.Size = new System.Drawing.Size(592, 58);
            this.UserPasswordTxtBox.TabIndex = 1;
            this.UserPasswordTxtBox.TextChanged += new System.EventHandler(this.UserPasswordTxtBox_TextChanged);
            this.UserPasswordTxtBox.DoubleClick += new System.EventHandler(this.UserPasswordTxtBox_DoubleClick);
            this.UserPasswordTxtBox.KeyUp += new System.Windows.Forms.KeyEventHandler(this.UserPasswordTxtBox_KeyUp);
            // 
            // lastNamePatientEdit
            // 
            this.lastNamePatientEdit.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lastNamePatientEdit.Font = new System.Drawing.Font("Roboto", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.lastNamePatientEdit.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(56)))), ((int)(((byte)(64)))));
            this.lastNamePatientEdit.Location = new System.Drawing.Point(26, 19);
            this.lastNamePatientEdit.Name = "lastNamePatientEdit";
            this.lastNamePatientEdit.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.lastNamePatientEdit.Size = new System.Drawing.Size(535, 29);
            this.lastNamePatientEdit.TabIndex = 30;
            this.lastNamePatientEdit.Text = "Kenig";
            this.lastNamePatientEdit.TextChanged += new System.EventHandler(this.lastNamePatientEdit_TextChanged);
            // 
            // mailPatientEdit
            // 
            this.mailPatientEdit.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.mailPatientEdit.Font = new System.Drawing.Font("Roboto", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.mailPatientEdit.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(56)))), ((int)(((byte)(64)))));
            this.mailPatientEdit.Location = new System.Drawing.Point(26, 19);
            this.mailPatientEdit.Name = "mailPatientEdit";
            this.mailPatientEdit.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.mailPatientEdit.Size = new System.Drawing.Size(535, 29);
            this.mailPatientEdit.TabIndex = 30;
            this.mailPatientEdit.Text = "lindkenig@gmail.com";
            this.mailPatientEdit.TextChanged += new System.EventHandler(this.mailPatientEdit_TextChanged);
            // 
            // phonePatientEdit
            // 
            this.phonePatientEdit.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.phonePatientEdit.Font = new System.Drawing.Font("Roboto", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.phonePatientEdit.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(56)))), ((int)(((byte)(64)))));
            this.phonePatientEdit.Location = new System.Drawing.Point(26, 19);
            this.phonePatientEdit.Name = "phonePatientEdit";
            this.phonePatientEdit.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.phonePatientEdit.Size = new System.Drawing.Size(535, 29);
            this.phonePatientEdit.TabIndex = 30;
            this.phonePatientEdit.Text = "+972-52-866662";
            this.phonePatientEdit.TextChanged += new System.EventHandler(this.phonePatientEdit_TextChanged);
            // 
            // notesPatientEdit
            // 
            this.notesPatientEdit.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.notesPatientEdit.Font = new System.Drawing.Font("Roboto", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.notesPatientEdit.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(56)))), ((int)(((byte)(64)))));
            this.notesPatientEdit.Location = new System.Drawing.Point(26, 19);
            this.notesPatientEdit.Multiline = true;
            this.notesPatientEdit.Name = "notesPatientEdit";
            this.notesPatientEdit.Size = new System.Drawing.Size(535, 133);
            this.notesPatientEdit.TabIndex = 30;
            this.notesPatientEdit.Text = "Linda had a double hip...";
            this.notesPatientEdit.TextChanged += new System.EventHandler(this.notesPatientEdit_TextChanged);
            // 
            // MainTabControl
            // 
            this.MainTabControl.Appearance = System.Windows.Forms.TabAppearance.Buttons;
            this.MainTabControl.Controls.Add(this.homePage);
            this.MainTabControl.Controls.Add(this.PlanPage);
            this.MainTabControl.Controls.Add(this.HistoryTab);
            this.MainTabControl.Controls.Add(this.LoginPage);
            this.MainTabControl.Controls.Add(this.PlanView);
            this.MainTabControl.Controls.Add(this.Settings);
            this.MainTabControl.Controls.Add(this.reports);
            this.MainTabControl.Controls.Add(this.generalReports);
            this.MainTabControl.Controls.Add(this.QuickPlan);
            this.MainTabControl.Location = new System.Drawing.Point(0, 0);
            this.MainTabControl.Margin = new System.Windows.Forms.Padding(0);
            this.MainTabControl.Name = "MainTabControl";
            this.MainTabControl.Padding = new System.Drawing.Point(3, 1);
            this.MainTabControl.SelectedIndex = 0;
            this.MainTabControl.Size = new System.Drawing.Size(1918, 1199);
            this.MainTabControl.TabIndex = 5;
            this.MainTabControl.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MainTabControl_KeyDown);
            this.MainTabControl.KeyUp += new System.Windows.Forms.KeyEventHandler(this.MainTabControl_KeyUp);
            // 
            // homePage
            // 
            this.homePage.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.homePage.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.homePage.Controls.Add(this.startBtn);
            this.homePage.Controls.Add(this.panel9);
            this.homePage.Controls.Add(this.patientTabControl);
            this.homePage.Controls.Add(this.panel4);
            this.homePage.Controls.Add(this.pNameMsgLbl);
            this.homePage.Controls.Add(this.button10);
            this.homePage.Location = new System.Drawing.Point(4, 23);
            this.homePage.Margin = new System.Windows.Forms.Padding(0);
            this.homePage.Name = "homePage";
            this.homePage.Padding = new System.Windows.Forms.Padding(3);
            this.homePage.Size = new System.Drawing.Size(1910, 1172);
            this.homePage.TabIndex = 1;
            this.homePage.Text = "home";
            this.homePage.Click += new System.EventHandler(this.homePage_Click);
            // 
            // startBtn
            // 
            this.startBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(201)))), ((int)(((byte)(31)))));
            this.startBtn.Enabled = false;
            this.startBtn.FlatAppearance.BorderSize = 0;
            this.startBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.startBtn.Font = new System.Drawing.Font("Roboto", 50F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.startBtn.Location = new System.Drawing.Point(0, 1103);
            this.startBtn.Name = "startBtn";
            this.startBtn.Size = new System.Drawing.Size(1914, 69);
            this.startBtn.TabIndex = 27;
            this.startBtn.Text = "Start";
            this.startBtn.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.startBtn.UseVisualStyleBackColor = false;
            this.startBtn.Click += new System.EventHandler(this.startBtn_Click);
            // 
            // panel9
            // 
            this.panel9.BackColor = System.Drawing.Color.Black;
            this.panel9.Controls.Add(this.SignOutNbtn);
            this.panel9.Controls.Add(this.settingsBtn);
            this.panel9.Controls.Add(this.reportsBtn);
            this.panel9.Controls.Add(this.letsBeginLbl);
            this.panel9.Location = new System.Drawing.Point(0, 0);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(1937, 96);
            this.panel9.TabIndex = 17;
            // 
            // SignOutNbtn
            // 
            this.SignOutNbtn.BackColor = System.Drawing.Color.Black;
            this.SignOutNbtn.BackgroundImage = global::SelfitMain.Properties.Resources.SignOut;
            this.SignOutNbtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.SignOutNbtn.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.SignOutNbtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.SignOutNbtn.Font = new System.Drawing.Font("Roboto", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.SignOutNbtn.ForeColor = System.Drawing.Color.White;
            this.SignOutNbtn.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.SignOutNbtn.Location = new System.Drawing.Point(1770, 9);
            this.SignOutNbtn.Name = "SignOutNbtn";
            this.SignOutNbtn.Size = new System.Drawing.Size(119, 79);
            this.SignOutNbtn.TabIndex = 20;
            this.SignOutNbtn.Text = "Sign Out";
            this.SignOutNbtn.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.SignOutNbtn.UseVisualStyleBackColor = false;
            this.SignOutNbtn.Click += new System.EventHandler(this.SignOutNbtn_Click);
            // 
            // settingsBtn
            // 
            this.settingsBtn.BackColor = System.Drawing.Color.Black;
            this.settingsBtn.BackgroundImage = global::SelfitMain.Properties.Resources.SettingsDisabled2;
            this.settingsBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.settingsBtn.Enabled = false;
            this.settingsBtn.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.settingsBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.settingsBtn.Font = new System.Drawing.Font("Roboto", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.settingsBtn.ForeColor = System.Drawing.Color.DimGray;
            this.settingsBtn.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.settingsBtn.Location = new System.Drawing.Point(1630, 1);
            this.settingsBtn.Name = "settingsBtn";
            this.settingsBtn.Size = new System.Drawing.Size(119, 88);
            this.settingsBtn.TabIndex = 19;
            this.settingsBtn.Text = "Settings";
            this.settingsBtn.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.settingsBtn.UseVisualStyleBackColor = false;
            // 
            // reportsBtn
            // 
            this.reportsBtn.BackColor = System.Drawing.Color.Black;
            this.reportsBtn.BackgroundImage = global::SelfitMain.Properties.Resources.Reports2;
            this.reportsBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.reportsBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.reportsBtn.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.reportsBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.reportsBtn.Font = new System.Drawing.Font("Roboto", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.reportsBtn.ForeColor = System.Drawing.Color.White;
            this.reportsBtn.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.reportsBtn.Location = new System.Drawing.Point(1490, 8);
            this.reportsBtn.Name = "reportsBtn";
            this.reportsBtn.Size = new System.Drawing.Size(119, 79);
            this.reportsBtn.TabIndex = 18;
            this.reportsBtn.Text = "Reports";
            this.reportsBtn.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.reportsBtn.UseVisualStyleBackColor = false;
            this.reportsBtn.Click += new System.EventHandler(this.reportsBtn_Click);
            // 
            // letsBeginLbl
            // 
            this.letsBeginLbl.Font = new System.Drawing.Font("Roboto", 50F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.letsBeginLbl.ForeColor = System.Drawing.Color.White;
            this.letsBeginLbl.Location = new System.Drawing.Point(70, 15);
            this.letsBeginLbl.Name = "letsBeginLbl";
            this.letsBeginLbl.Size = new System.Drawing.Size(426, 64);
            this.letsBeginLbl.TabIndex = 0;
            this.letsBeginLbl.Text = "Hi, let\'s begin :-)";
            // 
            // patientTabControl
            // 
            this.patientTabControl.Appearance = System.Windows.Forms.TabAppearance.FlatButtons;
            this.patientTabControl.Controls.Add(this.MainPatient);
            this.patientTabControl.Controls.Add(this.ShowDetails);
            this.patientTabControl.Controls.Add(this.EditDetails);
            this.patientTabControl.ItemSize = new System.Drawing.Size(68, 23);
            this.patientTabControl.Location = new System.Drawing.Point(1050, 92);
            this.patientTabControl.Name = "patientTabControl";
            this.patientTabControl.SelectedIndex = 0;
            this.patientTabControl.Size = new System.Drawing.Size(848, 1029);
            this.patientTabControl.TabIndex = 36;
            // 
            // MainPatient
            // 
            this.MainPatient.Controls.Add(this.noPatientSelectedLbl);
            this.MainPatient.Location = new System.Drawing.Point(4, 27);
            this.MainPatient.Name = "MainPatient";
            this.MainPatient.Padding = new System.Windows.Forms.Padding(3);
            this.MainPatient.Size = new System.Drawing.Size(840, 998);
            this.MainPatient.TabIndex = 0;
            this.MainPatient.Text = "MainPatient";
            // 
            // noPatientSelectedLbl
            // 
            this.noPatientSelectedLbl.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.noPatientSelectedLbl.Font = new System.Drawing.Font("Roboto", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.noPatientSelectedLbl.Location = new System.Drawing.Point(176, 428);
            this.noPatientSelectedLbl.Name = "noPatientSelectedLbl";
            this.noPatientSelectedLbl.Size = new System.Drawing.Size(431, 106);
            this.noPatientSelectedLbl.TabIndex = 36;
            this.noPatientSelectedLbl.Text = "No patient selected";
            this.noPatientSelectedLbl.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // ShowDetails
            // 
            this.ShowDetails.Controls.Add(this.patientDetailsEditBtn);
            this.ShowDetails.Controls.Add(this.label21);
            this.ShowDetails.Controls.Add(this.label10);
            this.ShowDetails.Controls.Add(this.label9);
            this.ShowDetails.Controls.Add(this.label8);
            this.ShowDetails.Controls.Add(this.patientInfoIDLbl);
            this.ShowDetails.Controls.Add(this.patientInfoNameLbl);
            this.ShowDetails.Controls.Add(this.patientInfoNotesTxt);
            this.ShowDetails.Controls.Add(this.patientInfoPhoneTxt);
            this.ShowDetails.Controls.Add(this.patientInfoEmailTxt);
            this.ShowDetails.Controls.Add(this.NotesLbl);
            this.ShowDetails.Controls.Add(this.PhoneNumberLbl);
            this.ShowDetails.Controls.Add(this.EmailLbl);
            this.ShowDetails.Controls.Add(this.IDlabel);
            this.ShowDetails.Controls.Add(this.NameLbl);
            this.ShowDetails.Location = new System.Drawing.Point(4, 27);
            this.ShowDetails.Name = "ShowDetails";
            this.ShowDetails.Padding = new System.Windows.Forms.Padding(3);
            this.ShowDetails.Size = new System.Drawing.Size(840, 998);
            this.ShowDetails.TabIndex = 2;
            this.ShowDetails.Text = "ShowDetails";
            this.ShowDetails.UseVisualStyleBackColor = true;
            // 
            // patientDetailsEditBtn
            // 
            this.patientDetailsEditBtn.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.patientDetailsEditBtn.BackColor = System.Drawing.Color.Transparent;
            this.patientDetailsEditBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.patientDetailsEditBtn.FlatAppearance.BorderSize = 0;
            this.patientDetailsEditBtn.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.patientDetailsEditBtn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.patientDetailsEditBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.patientDetailsEditBtn.Font = new System.Drawing.Font("Roboto", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.patientDetailsEditBtn.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.patientDetailsEditBtn.Image = global::SelfitMain.Properties.Resources.btnEdit;
            this.patientDetailsEditBtn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.patientDetailsEditBtn.Location = new System.Drawing.Point(354, 873);
            this.patientDetailsEditBtn.Name = "patientDetailsEditBtn";
            this.patientDetailsEditBtn.Size = new System.Drawing.Size(208, 75);
            this.patientDetailsEditBtn.TabIndex = 37;
            this.patientDetailsEditBtn.Text = "    Edit details";
            this.patientDetailsEditBtn.UseVisualStyleBackColor = false;
            this.patientDetailsEditBtn.Click += new System.EventHandler(this.patientDetailsEditBtn_Click);
            // 
            // label21
            // 
            this.label21.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label21.Image = global::SelfitMain.Properties.Resources.pDetailsHlineG;
            this.label21.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.label21.Location = new System.Drawing.Point(25, 602);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(739, 23);
            this.label21.TabIndex = 36;
            // 
            // label10
            // 
            this.label10.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label10.Image = global::SelfitMain.Properties.Resources.pDetailsHlineG;
            this.label10.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.label10.Location = new System.Drawing.Point(25, 477);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(739, 23);
            this.label10.TabIndex = 35;
            // 
            // label9
            // 
            this.label9.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label9.Image = global::SelfitMain.Properties.Resources.pDetailsHlineG;
            this.label9.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.label9.Location = new System.Drawing.Point(25, 344);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(739, 23);
            this.label9.TabIndex = 34;
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label8.Image = global::SelfitMain.Properties.Resources.pDetailsHlineG;
            this.label8.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.label8.Location = new System.Drawing.Point(25, 211);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(739, 23);
            this.label8.TabIndex = 33;
            // 
            // patientInfoIDLbl
            // 
            this.patientInfoIDLbl.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.patientInfoIDLbl.Font = new System.Drawing.Font("Roboto", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.patientInfoIDLbl.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(64)))), ((int)(((byte)(71)))));
            this.patientInfoIDLbl.Location = new System.Drawing.Point(221, 250);
            this.patientInfoIDLbl.Name = "patientInfoIDLbl";
            this.patientInfoIDLbl.Size = new System.Drawing.Size(172, 37);
            this.patientInfoIDLbl.TabIndex = 32;
            this.patientInfoIDLbl.Text = "ID";
            this.patientInfoIDLbl.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // patientInfoNameLbl
            // 
            this.patientInfoNameLbl.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.patientInfoNameLbl.Font = new System.Drawing.Font("Roboto", 40F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.patientInfoNameLbl.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(64)))), ((int)(((byte)(71)))));
            this.patientInfoNameLbl.Location = new System.Drawing.Point(221, 127);
            this.patientInfoNameLbl.Name = "patientInfoNameLbl";
            this.patientInfoNameLbl.Size = new System.Drawing.Size(291, 49);
            this.patientInfoNameLbl.TabIndex = 31;
            this.patientInfoNameLbl.Text = "Name";
            this.patientInfoNameLbl.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // patientInfoNotesTxt
            // 
            this.patientInfoNotesTxt.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.patientInfoNotesTxt.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.patientInfoNotesTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.patientInfoNotesTxt.Enabled = false;
            this.patientInfoNotesTxt.Font = new System.Drawing.Font("Roboto", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.patientInfoNotesTxt.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(64)))), ((int)(((byte)(71)))));
            this.patientInfoNotesTxt.Location = new System.Drawing.Point(221, 653);
            this.patientInfoNotesTxt.Multiline = true;
            this.patientInfoNotesTxt.Name = "patientInfoNotesTxt";
            this.patientInfoNotesTxt.Size = new System.Drawing.Size(469, 110);
            this.patientInfoNotesTxt.TabIndex = 30;
            this.patientInfoNotesTxt.Text = "???";
            // 
            // patientInfoPhoneTxt
            // 
            this.patientInfoPhoneTxt.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.patientInfoPhoneTxt.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.patientInfoPhoneTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.patientInfoPhoneTxt.Enabled = false;
            this.patientInfoPhoneTxt.Font = new System.Drawing.Font("Roboto", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.patientInfoPhoneTxt.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(64)))), ((int)(((byte)(71)))));
            this.patientInfoPhoneTxt.Location = new System.Drawing.Point(221, 516);
            this.patientInfoPhoneTxt.Name = "patientInfoPhoneTxt";
            this.patientInfoPhoneTxt.Size = new System.Drawing.Size(291, 37);
            this.patientInfoPhoneTxt.TabIndex = 29;
            this.patientInfoPhoneTxt.Text = "???";
            // 
            // patientInfoEmailTxt
            // 
            this.patientInfoEmailTxt.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.patientInfoEmailTxt.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.patientInfoEmailTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.patientInfoEmailTxt.Enabled = false;
            this.patientInfoEmailTxt.Font = new System.Drawing.Font("Roboto", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.patientInfoEmailTxt.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(64)))), ((int)(((byte)(71)))));
            this.patientInfoEmailTxt.Location = new System.Drawing.Point(221, 379);
            this.patientInfoEmailTxt.Name = "patientInfoEmailTxt";
            this.patientInfoEmailTxt.Size = new System.Drawing.Size(329, 37);
            this.patientInfoEmailTxt.TabIndex = 28;
            this.patientInfoEmailTxt.Text = "???";
            // 
            // NotesLbl
            // 
            this.NotesLbl.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.NotesLbl.Font = new System.Drawing.Font("Roboto", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.NotesLbl.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(119)))), ((int)(((byte)(119)))), ((int)(((byte)(119)))));
            this.NotesLbl.Location = new System.Drawing.Point(24, 648);
            this.NotesLbl.Name = "NotesLbl";
            this.NotesLbl.Size = new System.Drawing.Size(181, 48);
            this.NotesLbl.TabIndex = 27;
            this.NotesLbl.Text = "Notes";
            this.NotesLbl.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // PhoneNumberLbl
            // 
            this.PhoneNumberLbl.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PhoneNumberLbl.Font = new System.Drawing.Font("Roboto", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.PhoneNumberLbl.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(119)))), ((int)(((byte)(119)))), ((int)(((byte)(119)))));
            this.PhoneNumberLbl.Location = new System.Drawing.Point(24, 516);
            this.PhoneNumberLbl.Name = "PhoneNumberLbl";
            this.PhoneNumberLbl.Size = new System.Drawing.Size(139, 39);
            this.PhoneNumberLbl.TabIndex = 26;
            this.PhoneNumberLbl.Text = "Phone Number";
            this.PhoneNumberLbl.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // EmailLbl
            // 
            this.EmailLbl.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.EmailLbl.Font = new System.Drawing.Font("Roboto", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.EmailLbl.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(119)))), ((int)(((byte)(119)))), ((int)(((byte)(119)))));
            this.EmailLbl.Location = new System.Drawing.Point(24, 384);
            this.EmailLbl.Name = "EmailLbl";
            this.EmailLbl.Size = new System.Drawing.Size(139, 39);
            this.EmailLbl.TabIndex = 25;
            this.EmailLbl.Text = "Email";
            this.EmailLbl.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // IDlabel
            // 
            this.IDlabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.IDlabel.Font = new System.Drawing.Font("Roboto", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.IDlabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(119)))), ((int)(((byte)(119)))), ((int)(((byte)(119)))));
            this.IDlabel.Location = new System.Drawing.Point(24, 252);
            this.IDlabel.Name = "IDlabel";
            this.IDlabel.Size = new System.Drawing.Size(139, 39);
            this.IDlabel.TabIndex = 24;
            this.IDlabel.Text = "ID";
            this.IDlabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // NameLbl
            // 
            this.NameLbl.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.NameLbl.Font = new System.Drawing.Font("Roboto", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.NameLbl.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(119)))), ((int)(((byte)(119)))), ((int)(((byte)(119)))));
            this.NameLbl.Location = new System.Drawing.Point(24, 120);
            this.NameLbl.Name = "NameLbl";
            this.NameLbl.Size = new System.Drawing.Size(139, 39);
            this.NameLbl.TabIndex = 23;
            this.NameLbl.Text = "Name";
            this.NameLbl.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // EditDetails
            // 
            this.EditDetails.Controls.Add(this.fastEditDetailsBtn);
            this.EditDetails.Controls.Add(this.panel28);
            this.EditDetails.Controls.Add(this.panel27);
            this.EditDetails.Controls.Add(this.panel25);
            this.EditDetails.Controls.Add(this.panel24);
            this.EditDetails.Controls.Add(this.panel23);
            this.EditDetails.Controls.Add(this.panel22);
            this.EditDetails.Controls.Add(this.label27);
            this.EditDetails.Controls.Add(this.label26);
            this.EditDetails.Controls.Add(this.label25);
            this.EditDetails.Controls.Add(this.label24);
            this.EditDetails.Controls.Add(this.label23);
            this.EditDetails.Controls.Add(this.label22);
            this.EditDetails.Location = new System.Drawing.Point(4, 27);
            this.EditDetails.Name = "EditDetails";
            this.EditDetails.Padding = new System.Windows.Forms.Padding(3);
            this.EditDetails.Size = new System.Drawing.Size(840, 998);
            this.EditDetails.TabIndex = 3;
            this.EditDetails.Text = "EditDetails";
            this.EditDetails.UseVisualStyleBackColor = true;
            // 
            // fastEditDetailsBtn
            // 
            this.fastEditDetailsBtn.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.fastEditDetailsBtn.BackColor = System.Drawing.Color.Transparent;
            this.fastEditDetailsBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.fastEditDetailsBtn.FlatAppearance.BorderColor = System.Drawing.SystemColors.Control;
            this.fastEditDetailsBtn.FlatAppearance.BorderSize = 0;
            this.fastEditDetailsBtn.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.fastEditDetailsBtn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.fastEditDetailsBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.fastEditDetailsBtn.Font = new System.Drawing.Font("Roboto", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.fastEditDetailsBtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(56)))), ((int)(((byte)(64)))));
            this.fastEditDetailsBtn.Image = global::SelfitMain.Properties.Resources.patientEditDetails;
            this.fastEditDetailsBtn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.fastEditDetailsBtn.Location = new System.Drawing.Point(528, 64);
            this.fastEditDetailsBtn.Name = "fastEditDetailsBtn";
            this.fastEditDetailsBtn.Size = new System.Drawing.Size(208, 39);
            this.fastEditDetailsBtn.TabIndex = 38;
            this.fastEditDetailsBtn.Text = " Edit details";
            this.fastEditDetailsBtn.UseVisualStyleBackColor = false;
            this.fastEditDetailsBtn.Click += new System.EventHandler(this.fastEditDetailsBtn_Click);
            // 
            // panel28
            // 
            this.panel28.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel28.BackColor = System.Drawing.Color.White;
            this.panel28.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel28.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel28.Controls.Add(this.notesPatientEdit);
            this.panel28.Location = new System.Drawing.Point(124, 780);
            this.panel28.Name = "panel28";
            this.panel28.Size = new System.Drawing.Size(580, 171);
            this.panel28.TabIndex = 36;
            // 
            // panel27
            // 
            this.panel27.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel27.BackColor = System.Drawing.Color.White;
            this.panel27.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel27.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel27.Controls.Add(this.phonePatientEdit);
            this.panel27.Location = new System.Drawing.Point(126, 649);
            this.panel27.Name = "panel27";
            this.panel27.Size = new System.Drawing.Size(580, 68);
            this.panel27.TabIndex = 35;
            // 
            // panel25
            // 
            this.panel25.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel25.BackColor = System.Drawing.Color.White;
            this.panel25.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel25.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel25.Controls.Add(this.mailPatientEdit);
            this.panel25.Location = new System.Drawing.Point(126, 518);
            this.panel25.Name = "panel25";
            this.panel25.Size = new System.Drawing.Size(580, 68);
            this.panel25.TabIndex = 34;
            // 
            // panel24
            // 
            this.panel24.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel24.BackColor = System.Drawing.Color.White;
            this.panel24.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel24.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel24.Controls.Add(this.IdPatientEdit);
            this.panel24.Location = new System.Drawing.Point(126, 387);
            this.panel24.Name = "panel24";
            this.panel24.Size = new System.Drawing.Size(580, 68);
            this.panel24.TabIndex = 33;
            // 
            // IdPatientEdit
            // 
            this.IdPatientEdit.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.IdPatientEdit.Enabled = false;
            this.IdPatientEdit.Font = new System.Drawing.Font("Roboto", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.IdPatientEdit.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(56)))), ((int)(((byte)(64)))));
            this.IdPatientEdit.Location = new System.Drawing.Point(26, 19);
            this.IdPatientEdit.Name = "IdPatientEdit";
            this.IdPatientEdit.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.IdPatientEdit.Size = new System.Drawing.Size(535, 29);
            this.IdPatientEdit.TabIndex = 30;
            this.IdPatientEdit.Text = "024123123";
            // 
            // panel23
            // 
            this.panel23.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel23.BackColor = System.Drawing.Color.White;
            this.panel23.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel23.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel23.Controls.Add(this.lastNamePatientEdit);
            this.panel23.Location = new System.Drawing.Point(126, 256);
            this.panel23.Name = "panel23";
            this.panel23.Size = new System.Drawing.Size(580, 68);
            this.panel23.TabIndex = 32;
            // 
            // panel22
            // 
            this.panel22.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel22.BackColor = System.Drawing.Color.White;
            this.panel22.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel22.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel22.Controls.Add(this.firstNamePatientEdit);
            this.panel22.Location = new System.Drawing.Point(124, 123);
            this.panel22.Name = "panel22";
            this.panel22.Size = new System.Drawing.Size(580, 68);
            this.panel22.TabIndex = 31;
            // 
            // firstNamePatientEdit
            // 
            this.firstNamePatientEdit.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.firstNamePatientEdit.Font = new System.Drawing.Font("Roboto", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.firstNamePatientEdit.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(56)))), ((int)(((byte)(64)))));
            this.firstNamePatientEdit.Location = new System.Drawing.Point(26, 19);
            this.firstNamePatientEdit.Name = "firstNamePatientEdit";
            this.firstNamePatientEdit.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.firstNamePatientEdit.Size = new System.Drawing.Size(535, 29);
            this.firstNamePatientEdit.TabIndex = 30;
            this.firstNamePatientEdit.Text = "Linda";
            this.firstNamePatientEdit.TextChanged += new System.EventHandler(this.firstNamePatientEdit_TextChanged);
            // 
            // label27
            // 
            this.label27.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label27.Font = new System.Drawing.Font("Roboto", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.label27.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(119)))), ((int)(((byte)(119)))), ((int)(((byte)(119)))));
            this.label27.Location = new System.Drawing.Point(122, 742);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(139, 39);
            this.label27.TabIndex = 29;
            this.label27.Text = "Notes";
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label26
            // 
            this.label26.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label26.Font = new System.Drawing.Font("Roboto", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(119)))), ((int)(((byte)(119)))), ((int)(((byte)(119)))));
            this.label26.Location = new System.Drawing.Point(122, 609);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(139, 39);
            this.label26.TabIndex = 28;
            this.label26.Text = "Phone number";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label25
            // 
            this.label25.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label25.Font = new System.Drawing.Font("Roboto", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(119)))), ((int)(((byte)(119)))), ((int)(((byte)(119)))));
            this.label25.Location = new System.Drawing.Point(122, 481);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(139, 39);
            this.label25.TabIndex = 27;
            this.label25.Text = "Email";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label24
            // 
            this.label24.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label24.Font = new System.Drawing.Font("Roboto", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(119)))), ((int)(((byte)(119)))), ((int)(((byte)(119)))));
            this.label24.Location = new System.Drawing.Point(122, 351);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(139, 39);
            this.label24.TabIndex = 26;
            this.label24.Text = "Patient ID";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label23
            // 
            this.label23.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label23.Font = new System.Drawing.Font("Roboto", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(119)))), ((int)(((byte)(119)))), ((int)(((byte)(119)))));
            this.label23.Location = new System.Drawing.Point(122, 218);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(139, 39);
            this.label23.TabIndex = 25;
            this.label23.Text = "Last name";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label22
            // 
            this.label22.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label22.Font = new System.Drawing.Font("Roboto", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(119)))), ((int)(((byte)(119)))), ((int)(((byte)(119)))));
            this.label22.Location = new System.Drawing.Point(122, 89);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(139, 39);
            this.label22.TabIndex = 24;
            this.label22.Text = "First name";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.White;
            this.panel4.Controls.Add(this.patientListArrowDownBtn);
            this.panel4.Controls.Add(this.PatientsListBox);
            this.panel4.Controls.Add(this.panel3);
            this.panel4.Controls.Add(this.AddPatientBtn);
            this.panel4.Controls.Add(this.selectPatientBtn);
            this.panel4.Controls.Add(this.patientCountLbl);
            this.panel4.Location = new System.Drawing.Point(3, 99);
            this.panel4.Margin = new System.Windows.Forms.Padding(0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(962, 1004);
            this.panel4.TabIndex = 33;
            // 
            // patientListArrowDownBtn
            // 
            this.patientListArrowDownBtn.BackgroundImage = global::SelfitMain.Properties.Resources.down;
            this.patientListArrowDownBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.patientListArrowDownBtn.FlatAppearance.BorderSize = 0;
            this.patientListArrowDownBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.patientListArrowDownBtn.Location = new System.Drawing.Point(97, 951);
            this.patientListArrowDownBtn.Name = "patientListArrowDownBtn";
            this.patientListArrowDownBtn.Size = new System.Drawing.Size(768, 40);
            this.patientListArrowDownBtn.TabIndex = 31;
            this.patientListArrowDownBtn.UseVisualStyleBackColor = true;
            this.patientListArrowDownBtn.Visible = false;
            // 
            // PatientsListBox
            // 
            this.PatientsListBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.PatientsListBox.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.PatientsListBox.Font = new System.Drawing.Font("Roboto", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.PatientsListBox.ForeColor = System.Drawing.Color.Black;
            this.PatientsListBox.FormattingEnabled = true;
            this.PatientsListBox.ItemHeight = 37;
            this.PatientsListBox.Items.AddRange(new object[] {
            "\t01\tTest",
            "\t02\tDavid Guetta",
            "\t03\tJulie Cohen",
            "\t04\tArik Davidson",
            "\t05\tSheela Blum",
            "\t06\tJhon Smith",
            "\t08\tSheela Green",
            "09",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18",
            "19",
            "20",
            "21",
            "22",
            "23",
            "24",
            "25",
            "26",
            "27",
            "28"});
            this.PatientsListBox.Location = new System.Drawing.Point(97, 205);
            this.PatientsListBox.Name = "PatientsListBox";
            this.PatientsListBox.Size = new System.Drawing.Size(765, 740);
            this.PatientsListBox.TabIndex = 30;
            this.PatientsListBox.DrawItem += new System.Windows.Forms.DrawItemEventHandler(this.PatientsListBox_DrawItem);
            this.PatientsListBox.SelectedIndexChanged += new System.EventHandler(this.PatientsListBox_SelectedIndexChanged);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.White;
            this.panel3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.patientSearchMagBtn);
            this.panel3.Controls.Add(this.patientSearchTxt);
            this.panel3.Location = new System.Drawing.Point(78, 126);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(803, 68);
            this.panel3.TabIndex = 6;
            // 
            // patientSearchMagBtn
            // 
            this.patientSearchMagBtn.BackgroundImage = global::SelfitMain.Properties.Resources.Search;
            this.patientSearchMagBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.patientSearchMagBtn.FlatAppearance.BorderSize = 0;
            this.patientSearchMagBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.patientSearchMagBtn.Location = new System.Drawing.Point(738, 3);
            this.patientSearchMagBtn.Name = "patientSearchMagBtn";
            this.patientSearchMagBtn.Size = new System.Drawing.Size(60, 60);
            this.patientSearchMagBtn.TabIndex = 1;
            this.patientSearchMagBtn.UseVisualStyleBackColor = true;
            this.patientSearchMagBtn.Click += new System.EventHandler(this.patientSearchMagBtn_Click);
            // 
            // patientSearchTxt
            // 
            this.patientSearchTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.patientSearchTxt.Font = new System.Drawing.Font("Roboto", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.patientSearchTxt.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(56)))), ((int)(((byte)(64)))));
            this.patientSearchTxt.Location = new System.Drawing.Point(33, 23);
            this.patientSearchTxt.Name = "patientSearchTxt";
            this.patientSearchTxt.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.patientSearchTxt.Size = new System.Drawing.Size(702, 29);
            this.patientSearchTxt.TabIndex = 0;
            this.patientSearchTxt.Text = "Enter patient name OR patient number";
            this.patientSearchTxt.TextChanged += new System.EventHandler(this.patientSearchTxt_TextChanged);
            this.patientSearchTxt.KeyUp += new System.Windows.Forms.KeyEventHandler(this.patientSearchTxt_KeyUp);
            this.patientSearchTxt.Leave += new System.EventHandler(this.patientSearchTxt_Leave);
            this.patientSearchTxt.MouseDown += new System.Windows.Forms.MouseEventHandler(this.patientSearchTxt_MouseDown);
            this.patientSearchTxt.MouseUp += new System.Windows.Forms.MouseEventHandler(this.patientSearchTxt_MouseUp);
            // 
            // AddPatientBtn
            // 
            this.AddPatientBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.AddPatientBtn.BackColor = System.Drawing.Color.Transparent;
            this.AddPatientBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.AddPatientBtn.FlatAppearance.BorderSize = 0;
            this.AddPatientBtn.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.AddPatientBtn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.AddPatientBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.AddPatientBtn.Font = new System.Drawing.Font("Roboto", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.AddPatientBtn.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.AddPatientBtn.Image = global::SelfitMain.Properties.Resources.add_patient1;
            this.AddPatientBtn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.AddPatientBtn.Location = new System.Drawing.Point(692, 57);
            this.AddPatientBtn.Name = "AddPatientBtn";
            this.AddPatientBtn.Size = new System.Drawing.Size(208, 39);
            this.AddPatientBtn.TabIndex = 14;
            this.AddPatientBtn.Text = "    Add a Patient";
            this.AddPatientBtn.UseVisualStyleBackColor = false;
            this.AddPatientBtn.Click += new System.EventHandler(this.AddPatientBtn_Click_1);
            // 
            // selectPatientBtn
            // 
            this.selectPatientBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.selectPatientBtn.BackColor = System.Drawing.Color.Transparent;
            this.selectPatientBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.selectPatientBtn.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.selectPatientBtn.FlatAppearance.BorderSize = 0;
            this.selectPatientBtn.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.selectPatientBtn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.selectPatientBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.selectPatientBtn.Font = new System.Drawing.Font("Roboto", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.selectPatientBtn.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.selectPatientBtn.Location = new System.Drawing.Point(78, 57);
            this.selectPatientBtn.Name = "selectPatientBtn";
            this.selectPatientBtn.Size = new System.Drawing.Size(211, 46);
            this.selectPatientBtn.TabIndex = 12;
            this.selectPatientBtn.Text = "Select patient";
            this.selectPatientBtn.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.selectPatientBtn.UseVisualStyleBackColor = false;
            // 
            // patientCountLbl
            // 
            this.patientCountLbl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.patientCountLbl.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.patientCountLbl.BackColor = System.Drawing.Color.Transparent;
            this.patientCountLbl.Cursor = System.Windows.Forms.Cursors.Hand;
            this.patientCountLbl.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.patientCountLbl.FlatAppearance.BorderSize = 0;
            this.patientCountLbl.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.patientCountLbl.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.patientCountLbl.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.patientCountLbl.Font = new System.Drawing.Font("Roboto", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.patientCountLbl.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.patientCountLbl.Location = new System.Drawing.Point(280, 61);
            this.patientCountLbl.Name = "patientCountLbl";
            this.patientCountLbl.Size = new System.Drawing.Size(202, 39);
            this.patientCountLbl.TabIndex = 29;
            this.patientCountLbl.Text = " (0 available)";
            this.patientCountLbl.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.patientCountLbl.UseVisualStyleBackColor = false;
            // 
            // pNameMsgLbl
            // 
            this.pNameMsgLbl.Font = new System.Drawing.Font("Roboto", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.pNameMsgLbl.ForeColor = System.Drawing.Color.Red;
            this.pNameMsgLbl.Location = new System.Drawing.Point(603, 446);
            this.pNameMsgLbl.Name = "pNameMsgLbl";
            this.pNameMsgLbl.Size = new System.Drawing.Size(265, 39);
            this.pNameMsgLbl.TabIndex = 31;
            this.pNameMsgLbl.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // button10
            // 
            this.button10.BackColor = System.Drawing.Color.Transparent;
            this.button10.BackgroundImage = global::SelfitMain.Properties.Resources.Polygon2;
            this.button10.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button10.FlatAppearance.BorderSize = 0;
            this.button10.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button10.Location = new System.Drawing.Point(951, 452);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(66, 132);
            this.button10.TabIndex = 30;
            this.button10.UseVisualStyleBackColor = false;
            // 
            // PlanPage
            // 
            this.PlanPage.BackColor = System.Drawing.Color.White;
            this.PlanPage.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PlanPage.Controls.Add(this.buttonDown);
            this.PlanPage.Controls.Add(this.buttonUp);
            this.PlanPage.Controls.Add(this.addExPicPbx);
            this.PlanPage.Controls.Add(this.button8);
            this.PlanPage.Controls.Add(this.exercisesFlowPanel);
            this.PlanPage.Controls.Add(this.exeSaveForLaterBtn);
            this.PlanPage.Controls.Add(this.panel26);
            this.PlanPage.Controls.Add(this.practicesLayoutPanel);
            this.PlanPage.Controls.Add(this.exeSaveAndStartBtn);
            this.PlanPage.Controls.Add(this.exercisesTotalsTxt);
            this.PlanPage.Controls.Add(this.label124);
            this.PlanPage.Controls.Add(this.label125);
            this.PlanPage.Controls.Add(this.EditPlanNameBtn);
            this.PlanPage.Controls.Add(this.panel49);
            this.PlanPage.Controls.Add(this.PlanPanel);
            this.PlanPage.Controls.Add(this.label99);
            this.PlanPage.Controls.Add(this.panel47);
            this.PlanPage.Controls.Add(this.panel48);
            this.PlanPage.Controls.Add(this.SavePlanNameBtn);
            this.PlanPage.Controls.Add(this.planNameLbl);
            this.PlanPage.Location = new System.Drawing.Point(4, 23);
            this.PlanPage.Name = "PlanPage";
            this.PlanPage.Size = new System.Drawing.Size(1910, 1172);
            this.PlanPage.TabIndex = 5;
            this.PlanPage.Text = "Plans";
            // 
            // buttonDown
            // 
            this.buttonDown.BackgroundImage = global::SelfitMain.Properties.Resources.down1;
            this.buttonDown.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buttonDown.FlatAppearance.BorderSize = 0;
            this.buttonDown.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonDown.Location = new System.Drawing.Point(1160, 1109);
            this.buttonDown.Name = "buttonDown";
            this.buttonDown.Size = new System.Drawing.Size(164, 60);
            this.buttonDown.TabIndex = 28;
            this.buttonDown.UseVisualStyleBackColor = true;
            this.buttonDown.Visible = false;
            this.buttonDown.Click += new System.EventHandler(this.buttonDown_Click);
            // 
            // buttonUp
            // 
            this.buttonUp.BackgroundImage = global::SelfitMain.Properties.Resources.up1;
            this.buttonUp.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buttonUp.FlatAppearance.BorderSize = 0;
            this.buttonUp.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonUp.Location = new System.Drawing.Point(1160, 98);
            this.buttonUp.Name = "buttonUp";
            this.buttonUp.Size = new System.Drawing.Size(164, 60);
            this.buttonUp.TabIndex = 27;
            this.buttonUp.UseVisualStyleBackColor = true;
            this.buttonUp.Visible = false;
            this.buttonUp.Click += new System.EventHandler(this.buttonUp_Click);
            // 
            // addExPicPbx
            // 
            this.addExPicPbx.Image = global::SelfitMain.Properties.Resources.addExToList;
            this.addExPicPbx.Location = new System.Drawing.Point(1422, 387);
            this.addExPicPbx.Name = "addExPicPbx";
            this.addExPicPbx.Size = new System.Drawing.Size(432, 289);
            this.addExPicPbx.TabIndex = 26;
            this.addExPicPbx.TabStop = false;
            this.addExPicPbx.Visible = false;
            // 
            // button8
            // 
            this.button8.Enabled = false;
            this.button8.FlatAppearance.BorderSize = 0;
            this.button8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button8.Location = new System.Drawing.Point(1325, 160);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(35, 1012);
            this.button8.TabIndex = 25;
            this.button8.UseVisualStyleBackColor = true;
            // 
            // exercisesFlowPanel
            // 
            this.exercisesFlowPanel.AutoScroll = true;
            this.exercisesFlowPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(119)))), ((int)(((byte)(119)))), ((int)(((byte)(119)))));
            this.exercisesFlowPanel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.exercisesFlowPanel.Controls.Add(this.panel44);
            this.exercisesFlowPanel.Controls.Add(this.panel29);
            this.exercisesFlowPanel.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.exercisesFlowPanel.Location = new System.Drawing.Point(1360, 160);
            this.exercisesFlowPanel.Margin = new System.Windows.Forms.Padding(3, 1, 3, 1);
            this.exercisesFlowPanel.Name = "exercisesFlowPanel";
            this.exercisesFlowPanel.Size = new System.Drawing.Size(590, 197);
            this.exercisesFlowPanel.TabIndex = 14;
            this.exercisesFlowPanel.WrapContents = false;
            this.exercisesFlowPanel.MouseDown += new System.Windows.Forms.MouseEventHandler(this.exercisesFlowPanel_MouseDown);
            this.exercisesFlowPanel.MouseMove += new System.Windows.Forms.MouseEventHandler(this.exercisesFlowPanel_MouseMove);
            this.exercisesFlowPanel.MouseWheel += new System.Windows.Forms.MouseEventHandler(this.exercisesFlowPanel_MouseWheel);
            // 
            // panel44
            // 
            this.panel44.AllowDrop = true;
            this.panel44.BackColor = System.Drawing.Color.White;
            this.panel44.Controls.Add(this.practiceDnBtn);
            this.panel44.Controls.Add(this.practiceUpBtn);
            this.panel44.Controls.Add(this.button71);
            this.panel44.Controls.Add(this.comboBox2);
            this.panel44.Controls.Add(this.label43);
            this.panel44.Controls.Add(this.label44);
            this.panel44.Controls.Add(this.pictureBox11);
            this.panel44.ForeColor = System.Drawing.Color.Black;
            this.panel44.Location = new System.Drawing.Point(0, 2);
            this.panel44.Margin = new System.Windows.Forms.Padding(0, 2, 0, 0);
            this.panel44.Name = "panel44";
            this.panel44.Size = new System.Drawing.Size(554, 95);
            this.panel44.TabIndex = 1;
            // 
            // practiceDnBtn
            // 
            this.practiceDnBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.practiceDnBtn.FlatAppearance.BorderSize = 0;
            this.practiceDnBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.practiceDnBtn.Image = global::SelfitMain.Properties.Resources.UP_ICNB;
            this.practiceDnBtn.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.practiceDnBtn.Location = new System.Drawing.Point(3, 3);
            this.practiceDnBtn.Name = "practiceDnBtn";
            this.practiceDnBtn.Size = new System.Drawing.Size(48, 38);
            this.practiceDnBtn.TabIndex = 7;
            this.practiceDnBtn.UseVisualStyleBackColor = true;
            // 
            // practiceUpBtn
            // 
            this.practiceUpBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.practiceUpBtn.FlatAppearance.BorderSize = 0;
            this.practiceUpBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.practiceUpBtn.Image = global::SelfitMain.Properties.Resources.DN_ICNB;
            this.practiceUpBtn.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.practiceUpBtn.Location = new System.Drawing.Point(3, 54);
            this.practiceUpBtn.Name = "practiceUpBtn";
            this.practiceUpBtn.Size = new System.Drawing.Size(48, 38);
            this.practiceUpBtn.TabIndex = 6;
            this.practiceUpBtn.UseVisualStyleBackColor = true;
            // 
            // button71
            // 
            this.button71.BackgroundImage = global::SelfitMain.Properties.Resources.X_ICNB;
            this.button71.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.button71.FlatAppearance.BorderSize = 0;
            this.button71.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button71.Location = new System.Drawing.Point(471, 11);
            this.button71.Name = "button71";
            this.button71.Size = new System.Drawing.Size(48, 76);
            this.button71.TabIndex = 5;
            this.button71.UseVisualStyleBackColor = true;
            // 
            // comboBox2
            // 
            this.comboBox2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox2.DropDownWidth = 55;
            this.comboBox2.Font = new System.Drawing.Font("Roboto", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.ItemHeight = 37;
            this.comboBox2.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10"});
            this.comboBox2.Location = new System.Drawing.Point(370, 27);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(70, 45);
            this.comboBox2.TabIndex = 4;
            // 
            // label43
            // 
            this.label43.Font = new System.Drawing.Font("Roboto", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.label43.Location = new System.Drawing.Point(168, 34);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(181, 30);
            this.label43.TabIndex = 3;
            this.label43.Text = "Walk The square";
            this.label43.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label44
            // 
            this.label44.Font = new System.Drawing.Font("Roboto", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.label44.Location = new System.Drawing.Point(65, 38);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(36, 23);
            this.label44.TabIndex = 2;
            this.label44.Text = "01";
            this.label44.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pictureBox11
            // 
            this.pictureBox11.Location = new System.Drawing.Point(114, 29);
            this.pictureBox11.Name = "pictureBox11";
            this.pictureBox11.Size = new System.Drawing.Size(40, 40);
            this.pictureBox11.TabIndex = 1;
            this.pictureBox11.TabStop = false;
            // 
            // panel29
            // 
            this.panel29.AllowDrop = true;
            this.panel29.BackColor = System.Drawing.Color.White;
            this.panel29.Controls.Add(this.button61);
            this.panel29.Controls.Add(this.comboBox1);
            this.panel29.Controls.Add(this.label42);
            this.panel29.Controls.Add(this.label35);
            this.panel29.Controls.Add(this.pictureBox10);
            this.panel29.ForeColor = System.Drawing.Color.Black;
            this.panel29.Location = new System.Drawing.Point(0, 99);
            this.panel29.Margin = new System.Windows.Forms.Padding(0, 2, 0, 0);
            this.panel29.Name = "panel29";
            this.panel29.Size = new System.Drawing.Size(527, 80);
            this.panel29.TabIndex = 0;
            // 
            // button61
            // 
            this.button61.BackgroundImage = global::SelfitMain.Properties.Resources.X_ICN;
            this.button61.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.button61.FlatAppearance.BorderSize = 0;
            this.button61.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button61.Location = new System.Drawing.Point(415, 32);
            this.button61.Name = "button61";
            this.button61.Size = new System.Drawing.Size(17, 17);
            this.button61.TabIndex = 5;
            this.button61.UseVisualStyleBackColor = true;
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10"});
            this.comboBox1.Location = new System.Drawing.Point(342, 30);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(44, 21);
            this.comboBox1.TabIndex = 4;
            // 
            // label42
            // 
            this.label42.Font = new System.Drawing.Font("Roboto", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.label42.Location = new System.Drawing.Point(142, 26);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(174, 30);
            this.label42.TabIndex = 3;
            this.label42.Text = "Walk The square";
            this.label42.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Roboto", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.label35.Location = new System.Drawing.Point(32, 26);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(32, 23);
            this.label35.TabIndex = 2;
            this.label35.Text = "01";
            this.label35.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pictureBox10
            // 
            this.pictureBox10.Location = new System.Drawing.Point(83, 20);
            this.pictureBox10.Name = "pictureBox10";
            this.pictureBox10.Size = new System.Drawing.Size(40, 40);
            this.pictureBox10.TabIndex = 1;
            this.pictureBox10.TabStop = false;
            // 
            // exeSaveForLaterBtn
            // 
            this.exeSaveForLaterBtn.BackColor = System.Drawing.Color.LightGray;
            this.exeSaveForLaterBtn.Enabled = false;
            this.exeSaveForLaterBtn.FlatAppearance.BorderSize = 0;
            this.exeSaveForLaterBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.exeSaveForLaterBtn.Font = new System.Drawing.Font("Roboto", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.exeSaveForLaterBtn.ForeColor = System.Drawing.Color.Gray;
            this.exeSaveForLaterBtn.Location = new System.Drawing.Point(1461, 879);
            this.exeSaveForLaterBtn.Name = "exeSaveForLaterBtn";
            this.exeSaveForLaterBtn.Size = new System.Drawing.Size(360, 60);
            this.exeSaveForLaterBtn.TabIndex = 24;
            this.exeSaveForLaterBtn.Text = "  Save for Later";
            this.exeSaveForLaterBtn.UseVisualStyleBackColor = false;
            this.exeSaveForLaterBtn.Click += new System.EventHandler(this.exeSaveForLaterBtn_Click);
            // 
            // panel26
            // 
            this.panel26.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.panel26.Controls.Add(this.filterFlowLayoutPanel);
            this.panel26.Location = new System.Drawing.Point(146, 96);
            this.panel26.Name = "panel26";
            this.panel26.Size = new System.Drawing.Size(286, 1076);
            this.panel26.TabIndex = 21;
            // 
            // filterFlowLayoutPanel
            // 
            this.filterFlowLayoutPanel.AutoScroll = true;
            this.filterFlowLayoutPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(211)))), ((int)(((byte)(209)))), ((int)(((byte)(212)))));
            this.filterFlowLayoutPanel.Controls.Add(this.label33);
            this.filterFlowLayoutPanel.Controls.Add(this.label45);
            this.filterFlowLayoutPanel.Controls.Add(this.label46);
            this.filterFlowLayoutPanel.Location = new System.Drawing.Point(4, 0);
            this.filterFlowLayoutPanel.Name = "filterFlowLayoutPanel";
            this.filterFlowLayoutPanel.Size = new System.Drawing.Size(312, 1078);
            this.filterFlowLayoutPanel.TabIndex = 4;
            this.filterFlowLayoutPanel.MouseDown += new System.Windows.Forms.MouseEventHandler(this.filterFlowLayoutPanel_MouseDown);
            this.filterFlowLayoutPanel.MouseMove += new System.Windows.Forms.MouseEventHandler(this.filterFlowLayoutPanel_MouseMove);
            this.filterFlowLayoutPanel.MouseWheel += new System.Windows.Forms.MouseEventHandler(this.filterFlowLayoutPanel_MouseWheel);
            // 
            // label33
            // 
            this.label33.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(211)))), ((int)(((byte)(209)))), ((int)(((byte)(212)))));
            this.label33.Font = new System.Drawing.Font("Roboto", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.label33.ForeColor = System.Drawing.Color.Black;
            this.label33.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label33.Location = new System.Drawing.Point(0, 0);
            this.label33.Margin = new System.Windows.Forms.Padding(0, 0, 0, 4);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(280, 63);
            this.label33.TabIndex = 2;
            this.label33.Text = "    Standing";
            this.label33.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label45
            // 
            this.label45.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.label45.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label45.Font = new System.Drawing.Font("Roboto", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.label45.ForeColor = System.Drawing.Color.Black;
            this.label45.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label45.Location = new System.Drawing.Point(0, 67);
            this.label45.Margin = new System.Windows.Forms.Padding(0, 0, 0, 4);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(316, 63);
            this.label45.TabIndex = 3;
            this.label45.Text = "Lower extremity strength ";
            this.label45.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label46
            // 
            this.label46.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(211)))), ((int)(((byte)(209)))), ((int)(((byte)(212)))));
            this.label46.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label46.Font = new System.Drawing.Font("Roboto", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.label46.ForeColor = System.Drawing.Color.Black;
            this.label46.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label46.Location = new System.Drawing.Point(0, 134);
            this.label46.Margin = new System.Windows.Forms.Padding(0);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(280, 63);
            this.label46.TabIndex = 4;
            this.label46.Text = "    Sitting";
            this.label46.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // practicesLayoutPanel
            // 
            this.practicesLayoutPanel.AutoScroll = true;
            this.practicesLayoutPanel.Controls.Add(this.panel50);
            this.practicesLayoutPanel.Controls.Add(this.panel51);
            this.practicesLayoutPanel.Controls.Add(this.panel52);
            this.practicesLayoutPanel.Controls.Add(this.panel53);
            this.practicesLayoutPanel.Controls.Add(this.panel54);
            this.practicesLayoutPanel.Controls.Add(this.panel55);
            this.practicesLayoutPanel.Controls.Add(this.panel56);
            this.practicesLayoutPanel.Controls.Add(this.panel57);
            this.practicesLayoutPanel.Location = new System.Drawing.Point(434, 160);
            this.practicesLayoutPanel.Name = "practicesLayoutPanel";
            this.practicesLayoutPanel.Size = new System.Drawing.Size(908, 948);
            this.practicesLayoutPanel.TabIndex = 19;
            this.practicesLayoutPanel.MouseDown += new System.Windows.Forms.MouseEventHandler(this.practicesLayoutPanel_MouseDown);
            this.practicesLayoutPanel.MouseEnter += new System.EventHandler(this.practicesLayoutPanel_MouseEnter);
            this.practicesLayoutPanel.MouseMove += new System.Windows.Forms.MouseEventHandler(this.practicesLayoutPanel_MouseMove);
            this.practicesLayoutPanel.MouseWheel += new System.Windows.Forms.MouseEventHandler(this.practicesLayoutPanel_MouseWheel);
            // 
            // panel50
            // 
            this.panel50.BackgroundImage = global::SelfitMain.Properties.Resources.WTS;
            this.panel50.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.panel50.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel50.Controls.Add(this.button63);
            this.panel50.Controls.Add(this.label103);
            this.panel50.Location = new System.Drawing.Point(12, 12);
            this.panel50.Margin = new System.Windows.Forms.Padding(12);
            this.panel50.Name = "panel50";
            this.panel50.Size = new System.Drawing.Size(276, 276);
            this.panel50.TabIndex = 0;
            // 
            // button63
            // 
            this.button63.BackgroundImage = global::SelfitMain.Properties.Resources.planAddBtn;
            this.button63.FlatAppearance.BorderSize = 0;
            this.button63.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button63.Location = new System.Drawing.Point(213, 214);
            this.button63.Name = "button63";
            this.button63.Size = new System.Drawing.Size(40, 40);
            this.button63.TabIndex = 3;
            this.button63.UseVisualStyleBackColor = true;
            this.button63.Click += new System.EventHandler(this.button63_Click);
            // 
            // label103
            // 
            this.label103.BackColor = System.Drawing.Color.Transparent;
            this.label103.Font = new System.Drawing.Font("Roboto", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.label103.ForeColor = System.Drawing.Color.Black;
            this.label103.Location = new System.Drawing.Point(14, 5);
            this.label103.Name = "label103";
            this.label103.Size = new System.Drawing.Size(184, 31);
            this.label103.TabIndex = 2;
            this.label103.Text = "Walk the square";
            this.label103.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel51
            // 
            this.panel51.BackgroundImage = global::SelfitMain.Properties.Resources.inAndOutBtn;
            this.panel51.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.panel51.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel51.Controls.Add(this.button64);
            this.panel51.Controls.Add(this.label101);
            this.panel51.Location = new System.Drawing.Point(312, 12);
            this.panel51.Margin = new System.Windows.Forms.Padding(12);
            this.panel51.Name = "panel51";
            this.panel51.Size = new System.Drawing.Size(276, 276);
            this.panel51.TabIndex = 1;
            // 
            // button64
            // 
            this.button64.BackgroundImage = global::SelfitMain.Properties.Resources.btn_add_disabled;
            this.button64.FlatAppearance.BorderSize = 0;
            this.button64.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button64.Location = new System.Drawing.Point(213, 214);
            this.button64.Name = "button64";
            this.button64.Size = new System.Drawing.Size(40, 40);
            this.button64.TabIndex = 3;
            this.button64.UseVisualStyleBackColor = true;
            // 
            // label101
            // 
            this.label101.BackColor = System.Drawing.Color.Transparent;
            this.label101.Font = new System.Drawing.Font("Roboto", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.label101.ForeColor = System.Drawing.Color.Black;
            this.label101.Location = new System.Drawing.Point(14, 5);
            this.label101.Name = "label101";
            this.label101.Size = new System.Drawing.Size(184, 31);
            this.label101.TabIndex = 2;
            this.label101.Text = "In && out";
            this.label101.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel52
            // 
            this.panel52.BackgroundImage = global::SelfitMain.Properties.Resources.ThroughPath;
            this.panel52.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.panel52.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel52.Controls.Add(this.button65);
            this.panel52.Controls.Add(this.label102);
            this.panel52.Location = new System.Drawing.Point(612, 12);
            this.panel52.Margin = new System.Windows.Forms.Padding(12);
            this.panel52.Name = "panel52";
            this.panel52.Size = new System.Drawing.Size(276, 276);
            this.panel52.TabIndex = 2;
            // 
            // button65
            // 
            this.button65.BackColor = System.Drawing.Color.Transparent;
            this.button65.BackgroundImage = global::SelfitMain.Properties.Resources.planAddBtn;
            this.button65.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.button65.FlatAppearance.BorderSize = 0;
            this.button65.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button65.Location = new System.Drawing.Point(196, 196);
            this.button65.Name = "button65";
            this.button65.Size = new System.Drawing.Size(76, 76);
            this.button65.TabIndex = 3;
            this.button65.UseVisualStyleBackColor = false;
            // 
            // label102
            // 
            this.label102.BackColor = System.Drawing.Color.Transparent;
            this.label102.Font = new System.Drawing.Font("Roboto", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.label102.ForeColor = System.Drawing.Color.Black;
            this.label102.Location = new System.Drawing.Point(14, 5);
            this.label102.Name = "label102";
            this.label102.Size = new System.Drawing.Size(184, 31);
            this.label102.TabIndex = 2;
            this.label102.Text = "Through a path";
            this.label102.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel53
            // 
            this.panel53.BackgroundImage = global::SelfitMain.Properties.Resources.EightPath;
            this.panel53.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.panel53.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel53.Controls.Add(this.button66);
            this.panel53.Controls.Add(this.label104);
            this.panel53.Location = new System.Drawing.Point(12, 312);
            this.panel53.Margin = new System.Windows.Forms.Padding(12);
            this.panel53.Name = "panel53";
            this.panel53.Size = new System.Drawing.Size(276, 276);
            this.panel53.TabIndex = 3;
            // 
            // button66
            // 
            this.button66.BackgroundImage = global::SelfitMain.Properties.Resources.planAddBtn;
            this.button66.FlatAppearance.BorderSize = 0;
            this.button66.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button66.Location = new System.Drawing.Point(213, 214);
            this.button66.Name = "button66";
            this.button66.Size = new System.Drawing.Size(40, 40);
            this.button66.TabIndex = 3;
            this.button66.UseVisualStyleBackColor = true;
            // 
            // label104
            // 
            this.label104.BackColor = System.Drawing.Color.Transparent;
            this.label104.Font = new System.Drawing.Font("Roboto", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.label104.ForeColor = System.Drawing.Color.Black;
            this.label104.Location = new System.Drawing.Point(14, 5);
            this.label104.Name = "label104";
            this.label104.Size = new System.Drawing.Size(257, 31);
            this.label104.TabIndex = 2;
            this.label104.Text = "Eight path";
            this.label104.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel54
            // 
            this.panel54.BackgroundImage = global::SelfitMain.Properties.Resources.WTS;
            this.panel54.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.panel54.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel54.Controls.Add(this.button67);
            this.panel54.Controls.Add(this.label105);
            this.panel54.Location = new System.Drawing.Point(312, 312);
            this.panel54.Margin = new System.Windows.Forms.Padding(12);
            this.panel54.Name = "panel54";
            this.panel54.Size = new System.Drawing.Size(276, 276);
            this.panel54.TabIndex = 4;
            // 
            // button67
            // 
            this.button67.BackgroundImage = global::SelfitMain.Properties.Resources.planAddBtn;
            this.button67.FlatAppearance.BorderSize = 0;
            this.button67.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button67.Location = new System.Drawing.Point(213, 214);
            this.button67.Name = "button67";
            this.button67.Size = new System.Drawing.Size(40, 40);
            this.button67.TabIndex = 3;
            this.button67.UseVisualStyleBackColor = true;
            // 
            // label105
            // 
            this.label105.BackColor = System.Drawing.Color.Transparent;
            this.label105.Font = new System.Drawing.Font("Roboto", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.label105.ForeColor = System.Drawing.Color.Black;
            this.label105.Location = new System.Drawing.Point(14, 5);
            this.label105.Name = "label105";
            this.label105.Size = new System.Drawing.Size(184, 31);
            this.label105.TabIndex = 2;
            this.label105.Text = "Walk the square";
            this.label105.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel55
            // 
            this.panel55.BackgroundImage = global::SelfitMain.Properties.Resources.WTS;
            this.panel55.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.panel55.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel55.Controls.Add(this.button68);
            this.panel55.Controls.Add(this.label106);
            this.panel55.Location = new System.Drawing.Point(612, 312);
            this.panel55.Margin = new System.Windows.Forms.Padding(12);
            this.panel55.Name = "panel55";
            this.panel55.Size = new System.Drawing.Size(276, 276);
            this.panel55.TabIndex = 5;
            // 
            // button68
            // 
            this.button68.BackgroundImage = global::SelfitMain.Properties.Resources.planAddBtn;
            this.button68.FlatAppearance.BorderSize = 0;
            this.button68.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button68.Location = new System.Drawing.Point(213, 214);
            this.button68.Name = "button68";
            this.button68.Size = new System.Drawing.Size(40, 40);
            this.button68.TabIndex = 3;
            this.button68.UseVisualStyleBackColor = true;
            // 
            // label106
            // 
            this.label106.BackColor = System.Drawing.Color.Transparent;
            this.label106.Font = new System.Drawing.Font("Roboto", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.label106.ForeColor = System.Drawing.Color.Black;
            this.label106.Location = new System.Drawing.Point(14, 5);
            this.label106.Name = "label106";
            this.label106.Size = new System.Drawing.Size(184, 31);
            this.label106.TabIndex = 2;
            this.label106.Text = "Walk the square";
            this.label106.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel56
            // 
            this.panel56.BackgroundImage = global::SelfitMain.Properties.Resources.WTS;
            this.panel56.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.panel56.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel56.Controls.Add(this.button69);
            this.panel56.Controls.Add(this.label107);
            this.panel56.Location = new System.Drawing.Point(12, 612);
            this.panel56.Margin = new System.Windows.Forms.Padding(12);
            this.panel56.Name = "panel56";
            this.panel56.Size = new System.Drawing.Size(276, 276);
            this.panel56.TabIndex = 6;
            // 
            // button69
            // 
            this.button69.BackgroundImage = global::SelfitMain.Properties.Resources.planAddBtn;
            this.button69.FlatAppearance.BorderSize = 0;
            this.button69.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button69.Location = new System.Drawing.Point(213, 214);
            this.button69.Name = "button69";
            this.button69.Size = new System.Drawing.Size(40, 40);
            this.button69.TabIndex = 3;
            this.button69.UseVisualStyleBackColor = true;
            // 
            // label107
            // 
            this.label107.BackColor = System.Drawing.Color.Transparent;
            this.label107.Font = new System.Drawing.Font("Roboto", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.label107.ForeColor = System.Drawing.Color.Black;
            this.label107.Location = new System.Drawing.Point(14, 5);
            this.label107.Name = "label107";
            this.label107.Size = new System.Drawing.Size(184, 31);
            this.label107.TabIndex = 2;
            this.label107.Text = "Walk the square";
            this.label107.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel57
            // 
            this.panel57.BackgroundImage = global::SelfitMain.Properties.Resources.WTS;
            this.panel57.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.panel57.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel57.Controls.Add(this.button70);
            this.panel57.Controls.Add(this.label108);
            this.panel57.Location = new System.Drawing.Point(312, 612);
            this.panel57.Margin = new System.Windows.Forms.Padding(12);
            this.panel57.Name = "panel57";
            this.panel57.Size = new System.Drawing.Size(276, 276);
            this.panel57.TabIndex = 7;
            // 
            // button70
            // 
            this.button70.BackgroundImage = global::SelfitMain.Properties.Resources.planAddBtn;
            this.button70.FlatAppearance.BorderSize = 0;
            this.button70.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button70.Location = new System.Drawing.Point(213, 214);
            this.button70.Name = "button70";
            this.button70.Size = new System.Drawing.Size(40, 40);
            this.button70.TabIndex = 3;
            this.button70.UseVisualStyleBackColor = true;
            // 
            // label108
            // 
            this.label108.BackColor = System.Drawing.Color.Transparent;
            this.label108.Font = new System.Drawing.Font("Roboto", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.label108.ForeColor = System.Drawing.Color.Black;
            this.label108.Location = new System.Drawing.Point(14, 5);
            this.label108.Name = "label108";
            this.label108.Size = new System.Drawing.Size(184, 31);
            this.label108.TabIndex = 2;
            this.label108.Text = "Walk the square";
            this.label108.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // exeSaveAndStartBtn
            // 
            this.exeSaveAndStartBtn.BackColor = System.Drawing.Color.LightGray;
            this.exeSaveAndStartBtn.Enabled = false;
            this.exeSaveAndStartBtn.FlatAppearance.BorderSize = 0;
            this.exeSaveAndStartBtn.Font = new System.Drawing.Font("Roboto", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.exeSaveAndStartBtn.ForeColor = System.Drawing.Color.Gray;
            this.exeSaveAndStartBtn.Location = new System.Drawing.Point(1461, 967);
            this.exeSaveAndStartBtn.Name = "exeSaveAndStartBtn";
            this.exeSaveAndStartBtn.Size = new System.Drawing.Size(360, 60);
            this.exeSaveAndStartBtn.TabIndex = 18;
            this.exeSaveAndStartBtn.Text = "Save && Start";
            this.exeSaveAndStartBtn.UseVisualStyleBackColor = false;
            this.exeSaveAndStartBtn.Click += new System.EventHandler(this.exeSaveAndStartBtn_Click);
            // 
            // exercisesTotalsTxt
            // 
            this.exercisesTotalsTxt.Font = new System.Drawing.Font("Roboto", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.exercisesTotalsTxt.Location = new System.Drawing.Point(1459, 830);
            this.exercisesTotalsTxt.Name = "exercisesTotalsTxt";
            this.exercisesTotalsTxt.Size = new System.Drawing.Size(362, 33);
            this.exercisesTotalsTxt.TabIndex = 17;
            this.exercisesTotalsTxt.Text = "00 Exercises | 00 Reps.";
            this.exercisesTotalsTxt.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.exercisesTotalsTxt.Click += new System.EventHandler(this.exercisesTotalsTxt_Click);
            // 
            // label124
            // 
            this.label124.BackColor = System.Drawing.Color.White;
            this.label124.Font = new System.Drawing.Font("Roboto", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.label124.ForeColor = System.Drawing.Color.Black;
            this.label124.Location = new System.Drawing.Point(1531, 126);
            this.label124.Name = "label124";
            this.label124.Size = new System.Drawing.Size(103, 26);
            this.label124.TabIndex = 13;
            this.label124.Text = "Exercises";
            this.label124.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label125
            // 
            this.label125.BackColor = System.Drawing.Color.White;
            this.label125.Font = new System.Drawing.Font("Roboto", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.label125.ForeColor = System.Drawing.Color.Black;
            this.label125.Location = new System.Drawing.Point(1415, 126);
            this.label125.Name = "label125";
            this.label125.Size = new System.Drawing.Size(52, 26);
            this.label125.TabIndex = 12;
            this.label125.Text = "No.";
            this.label125.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // EditPlanNameBtn
            // 
            this.EditPlanNameBtn.BackColor = System.Drawing.Color.White;
            this.EditPlanNameBtn.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.EditPlanNameBtn.FlatAppearance.BorderSize = 0;
            this.EditPlanNameBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.EditPlanNameBtn.Font = new System.Drawing.Font("Roboto", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.EditPlanNameBtn.Location = new System.Drawing.Point(835, 115);
            this.EditPlanNameBtn.Name = "EditPlanNameBtn";
            this.EditPlanNameBtn.Size = new System.Drawing.Size(96, 37);
            this.EditPlanNameBtn.TabIndex = 7;
            this.EditPlanNameBtn.Text = "(Edit)";
            this.EditPlanNameBtn.UseVisualStyleBackColor = false;
            this.EditPlanNameBtn.Click += new System.EventHandler(this.EditPlanNameBtn_Click);
            // 
            // panel49
            // 
            this.panel49.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(56)))), ((int)(((byte)(64)))));
            this.panel49.Controls.Add(this.exercisesGReportsBtn);
            this.panel49.Controls.Add(this.patientHistoryBtnLbl);
            this.panel49.Controls.Add(this.exercisesPlanBtn);
            this.panel49.Location = new System.Drawing.Point(0, 528);
            this.panel49.Name = "panel49";
            this.panel49.Size = new System.Drawing.Size(150, 663);
            this.panel49.TabIndex = 5;
            // 
            // exercisesGReportsBtn
            // 
            this.exercisesGReportsBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.exercisesGReportsBtn.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(56)))), ((int)(((byte)(64)))));
            this.exercisesGReportsBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.exercisesGReportsBtn.Font = new System.Drawing.Font("Roboto", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.exercisesGReportsBtn.ForeColor = System.Drawing.Color.White;
            this.exercisesGReportsBtn.Image = global::SelfitMain.Properties.Resources.reportsBtn;
            this.exercisesGReportsBtn.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.exercisesGReportsBtn.Location = new System.Drawing.Point(0, 426);
            this.exercisesGReportsBtn.Name = "exercisesGReportsBtn";
            this.exercisesGReportsBtn.Size = new System.Drawing.Size(150, 94);
            this.exercisesGReportsBtn.TabIndex = 3;
            this.exercisesGReportsBtn.Text = "Reports";
            this.exercisesGReportsBtn.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.exercisesGReportsBtn.UseVisualStyleBackColor = true;
            this.exercisesGReportsBtn.Click += new System.EventHandler(this.exercisesGReportsBtn_Click);
            // 
            // patientHistoryBtnLbl
            // 
            this.patientHistoryBtnLbl.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(56)))), ((int)(((byte)(64)))));
            this.patientHistoryBtnLbl.Font = new System.Drawing.Font("Roboto", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.patientHistoryBtnLbl.ForeColor = System.Drawing.Color.White;
            this.patientHistoryBtnLbl.Image = global::SelfitMain.Properties.Resources.Group1;
            this.patientHistoryBtnLbl.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.patientHistoryBtnLbl.Location = new System.Drawing.Point(-4, 45);
            this.patientHistoryBtnLbl.Name = "patientHistoryBtnLbl";
            this.patientHistoryBtnLbl.Size = new System.Drawing.Size(151, 110);
            this.patientHistoryBtnLbl.TabIndex = 0;
            this.patientHistoryBtnLbl.Text = "Patient History";
            this.patientHistoryBtnLbl.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.patientHistoryBtnLbl.Click += new System.EventHandler(this.patientHistoryBtnLbl_Click);
            // 
            // exercisesPlanBtn
            // 
            this.exercisesPlanBtn.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(56)))), ((int)(((byte)(64)))));
            this.exercisesPlanBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.exercisesPlanBtn.Font = new System.Drawing.Font("Roboto", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.exercisesPlanBtn.ForeColor = System.Drawing.Color.White;
            this.exercisesPlanBtn.Image = global::SelfitMain.Properties.Resources.Star;
            this.exercisesPlanBtn.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.exercisesPlanBtn.Location = new System.Drawing.Point(0, 244);
            this.exercisesPlanBtn.Name = "exercisesPlanBtn";
            this.exercisesPlanBtn.Size = new System.Drawing.Size(147, 138);
            this.exercisesPlanBtn.TabIndex = 1;
            this.exercisesPlanBtn.Text = "Saved Plans";
            this.exercisesPlanBtn.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.exercisesPlanBtn.UseVisualStyleBackColor = true;
            this.exercisesPlanBtn.Click += new System.EventHandler(this.button35_Click_1);
            // 
            // PlanPanel
            // 
            this.PlanPanel.BackColor = System.Drawing.Color.Black;
            this.PlanPanel.Controls.Add(this.plansExitBtn);
            this.PlanPanel.Controls.Add(this.panel71);
            this.PlanPanel.Controls.Add(this.plansViewEditBtn);
            this.PlanPanel.Controls.Add(this.planPatientNameLbl);
            this.PlanPanel.Location = new System.Drawing.Point(0, 0);
            this.PlanPanel.Name = "PlanPanel";
            this.PlanPanel.Size = new System.Drawing.Size(1907, 96);
            this.PlanPanel.TabIndex = 1;
            // 
            // plansExitBtn
            // 
            this.plansExitBtn.FlatAppearance.BorderSize = 0;
            this.plansExitBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.plansExitBtn.Image = global::SelfitMain.Properties.Resources.exitYellow;
            this.plansExitBtn.Location = new System.Drawing.Point(1815, 22);
            this.plansExitBtn.Name = "plansExitBtn";
            this.plansExitBtn.Size = new System.Drawing.Size(60, 60);
            this.plansExitBtn.TabIndex = 4;
            this.plansExitBtn.UseVisualStyleBackColor = true;
            this.plansExitBtn.Click += new System.EventHandler(this.plansExitBtn_Click);
            // 
            // panel71
            // 
            this.panel71.ForeColor = System.Drawing.Color.Black;
            this.panel71.Location = new System.Drawing.Point(129, 94);
            this.panel71.Name = "panel71";
            this.panel71.Size = new System.Drawing.Size(21, 592);
            this.panel71.TabIndex = 1;
            // 
            // plansViewEditBtn
            // 
            this.plansViewEditBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(201)))), ((int)(((byte)(31)))));
            this.plansViewEditBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.plansViewEditBtn.Font = new System.Drawing.Font("Roboto", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.plansViewEditBtn.Location = new System.Drawing.Point(297, 29);
            this.plansViewEditBtn.Name = "plansViewEditBtn";
            this.plansViewEditBtn.Size = new System.Drawing.Size(170, 37);
            this.plansViewEditBtn.TabIndex = 2;
            this.plansViewEditBtn.Text = "View && Edit";
            this.plansViewEditBtn.UseVisualStyleBackColor = false;
            this.plansViewEditBtn.Click += new System.EventHandler(this.plansViewEditBtn_Click);
            // 
            // planPatientNameLbl
            // 
            this.planPatientNameLbl.Font = new System.Drawing.Font("Roboto", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.planPatientNameLbl.ForeColor = System.Drawing.Color.White;
            this.planPatientNameLbl.Location = new System.Drawing.Point(39, 29);
            this.planPatientNameLbl.Name = "planPatientNameLbl";
            this.planPatientNameLbl.Size = new System.Drawing.Size(295, 34);
            this.planPatientNameLbl.TabIndex = 1;
            this.planPatientNameLbl.Text = "Mr. Jhon Adams";
            this.planPatientNameLbl.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label99
            // 
            this.label99.BackColor = System.Drawing.Color.White;
            this.label99.Font = new System.Drawing.Font("Roboto", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.label99.ForeColor = System.Drawing.Color.Black;
            this.label99.Location = new System.Drawing.Point(1729, 126);
            this.label99.Name = "label99";
            this.label99.Size = new System.Drawing.Size(64, 26);
            this.label99.TabIndex = 15;
            this.label99.Text = "Reps.";
            this.label99.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel47
            // 
            this.panel47.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(201)))), ((int)(((byte)(31)))));
            this.panel47.Controls.Add(this.createPlanBtnLbl);
            this.panel47.Location = new System.Drawing.Point(0, 306);
            this.panel47.Name = "panel47";
            this.panel47.Size = new System.Drawing.Size(150, 222);
            this.panel47.TabIndex = 4;
            // 
            // createPlanBtnLbl
            // 
            this.createPlanBtnLbl.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(201)))), ((int)(((byte)(31)))));
            this.createPlanBtnLbl.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.createPlanBtnLbl.Font = new System.Drawing.Font("Roboto", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.createPlanBtnLbl.ForeColor = System.Drawing.Color.Black;
            this.createPlanBtnLbl.Image = global::SelfitMain.Properties.Resources.Group_3_2;
            this.createPlanBtnLbl.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.createPlanBtnLbl.Location = new System.Drawing.Point(0, 46);
            this.createPlanBtnLbl.Name = "createPlanBtnLbl";
            this.createPlanBtnLbl.Size = new System.Drawing.Size(147, 116);
            this.createPlanBtnLbl.TabIndex = 0;
            this.createPlanBtnLbl.Text = "Exercises";
            this.createPlanBtnLbl.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.createPlanBtnLbl.UseVisualStyleBackColor = true;
            // 
            // panel48
            // 
            this.panel48.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(56)))), ((int)(((byte)(64)))));
            this.panel48.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.panel48.Controls.Add(this.exercisesQplanBtn);
            this.panel48.Location = new System.Drawing.Point(0, 83);
            this.panel48.Name = "panel48";
            this.panel48.Size = new System.Drawing.Size(147, 223);
            this.panel48.TabIndex = 3;
            // 
            // exercisesQplanBtn
            // 
            this.exercisesQplanBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.exercisesQplanBtn.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(56)))), ((int)(((byte)(64)))));
            this.exercisesQplanBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.exercisesQplanBtn.Font = new System.Drawing.Font("Roboto", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.exercisesQplanBtn.ForeColor = System.Drawing.Color.White;
            this.exercisesQplanBtn.Image = global::SelfitMain.Properties.Resources.quickplan_bright;
            this.exercisesQplanBtn.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.exercisesQplanBtn.Location = new System.Drawing.Point(11, 46);
            this.exercisesQplanBtn.Name = "exercisesQplanBtn";
            this.exercisesQplanBtn.Size = new System.Drawing.Size(125, 128);
            this.exercisesQplanBtn.TabIndex = 2;
            this.exercisesQplanBtn.Text = "Quick Plan";
            this.exercisesQplanBtn.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.exercisesQplanBtn.UseVisualStyleBackColor = true;
            this.exercisesQplanBtn.Click += new System.EventHandler(this.exercisesQplanBtn_Click);
            // 
            // SavePlanNameBtn
            // 
            this.SavePlanNameBtn.BackColor = System.Drawing.Color.White;
            this.SavePlanNameBtn.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.SavePlanNameBtn.FlatAppearance.BorderSize = 0;
            this.SavePlanNameBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.SavePlanNameBtn.Font = new System.Drawing.Font("Roboto", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.SavePlanNameBtn.Location = new System.Drawing.Point(836, 115);
            this.SavePlanNameBtn.Name = "SavePlanNameBtn";
            this.SavePlanNameBtn.Size = new System.Drawing.Size(96, 37);
            this.SavePlanNameBtn.TabIndex = 23;
            this.SavePlanNameBtn.Text = "(Save)";
            this.SavePlanNameBtn.UseVisualStyleBackColor = false;
            this.SavePlanNameBtn.Visible = false;
            this.SavePlanNameBtn.Click += new System.EventHandler(this.SavePlanNameBtn_Click);
            // 
            // planNameLbl
            // 
            this.planNameLbl.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.planNameLbl.Enabled = false;
            this.planNameLbl.Font = new System.Drawing.Font("Roboto", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.planNameLbl.Location = new System.Drawing.Point(444, 115);
            this.planNameLbl.Multiline = true;
            this.planNameLbl.Name = "planNameLbl";
            this.planNameLbl.Size = new System.Drawing.Size(385, 37);
            this.planNameLbl.TabIndex = 22;
            this.planNameLbl.Text = "New Plan #5, 28.05.19";
            this.planNameLbl.KeyUp += new System.Windows.Forms.KeyEventHandler(this.planNameEditTxt_KeyUp);
            this.planNameLbl.MouseUp += new System.Windows.Forms.MouseEventHandler(this.planNameLbl_MouseUp);
            // 
            // HistoryTab
            // 
            this.HistoryTab.BackColor = System.Drawing.Color.White;
            this.HistoryTab.Controls.Add(this.parkinsonSessionDataPanel);
            this.HistoryTab.Controls.Add(this.panel92);
            this.HistoryTab.Controls.Add(this.exTitleHeader);
            this.HistoryTab.Controls.Add(this.startPlanBtn);
            this.HistoryTab.Controls.Add(this.selectAndEditPlanBtn);
            this.HistoryTab.Controls.Add(this.historyTotalExeTxt);
            this.HistoryTab.Controls.Add(this.exerciesLayout);
            this.HistoryTab.Controls.Add(this.sessionDatainfoPanel);
            this.HistoryTab.Controls.Add(this.PlanNameTxt);
            this.HistoryTab.Controls.Add(this.PlanDateTxt);
            this.HistoryTab.Controls.Add(this.panel19);
            this.HistoryTab.Controls.Add(this.panel17);
            this.HistoryTab.Controls.Add(this.panel15);
            this.HistoryTab.Controls.Add(this.panel16);
            this.HistoryTab.Controls.Add(this.panel18);
            this.HistoryTab.Controls.Add(this.createPlanBtn);
            this.HistoryTab.ForeColor = System.Drawing.Color.White;
            this.HistoryTab.Location = new System.Drawing.Point(4, 23);
            this.HistoryTab.Name = "HistoryTab";
            this.HistoryTab.Padding = new System.Windows.Forms.Padding(3);
            this.HistoryTab.Size = new System.Drawing.Size(1910, 1172);
            this.HistoryTab.TabIndex = 8;
            this.HistoryTab.Text = "History";
            // 
            // parkinsonSessionDataPanel
            // 
            this.parkinsonSessionDataPanel.Controls.Add(this.ParkinsontabControl);
            this.parkinsonSessionDataPanel.Location = new System.Drawing.Point(510, 172);
            this.parkinsonSessionDataPanel.Name = "parkinsonSessionDataPanel";
            this.parkinsonSessionDataPanel.Size = new System.Drawing.Size(979, 903);
            this.parkinsonSessionDataPanel.TabIndex = 19;
            // 
            // ParkinsontabControl
            // 
            this.ParkinsontabControl.Appearance = System.Windows.Forms.TabAppearance.FlatButtons;
            this.ParkinsontabControl.Controls.Add(this.summaryDataTab);
            this.ParkinsontabControl.Controls.Add(this.DataViewTab);
            this.ParkinsontabControl.Font = new System.Drawing.Font("Roboto", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.ParkinsontabControl.Location = new System.Drawing.Point(3, 3);
            this.ParkinsontabControl.Name = "ParkinsontabControl";
            this.ParkinsontabControl.SelectedIndex = 0;
            this.ParkinsontabControl.Size = new System.Drawing.Size(973, 900);
            this.ParkinsontabControl.TabIndex = 0;
            // 
            // summaryDataTab
            // 
            this.summaryDataTab.Controls.Add(this.label31);
            this.summaryDataTab.Controls.Add(this.label30);
            this.summaryDataTab.Controls.Add(this.label29);
            this.summaryDataTab.Controls.Add(this.label28);
            this.summaryDataTab.Controls.Add(this.label2);
            this.summaryDataTab.Controls.Add(this.ParkinsonSessionDataLayoutPanel);
            this.summaryDataTab.Location = new System.Drawing.Point(4, 39);
            this.summaryDataTab.Name = "summaryDataTab";
            this.summaryDataTab.Padding = new System.Windows.Forms.Padding(3);
            this.summaryDataTab.Size = new System.Drawing.Size(965, 857);
            this.summaryDataTab.TabIndex = 0;
            this.summaryDataTab.Text = "Summary";
            this.summaryDataTab.UseVisualStyleBackColor = true;
            // 
            // label31
            // 
            this.label31.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.label31.Font = new System.Drawing.Font("Roboto", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.label31.ForeColor = System.Drawing.Color.Black;
            this.label31.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label31.Location = new System.Drawing.Point(733, 24);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(178, 28);
            this.label31.TabIndex = 5;
            this.label31.Text = "Step time (net)";
            this.label31.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label30
            // 
            this.label30.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.label30.Font = new System.Drawing.Font("Roboto", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.label30.ForeColor = System.Drawing.Color.Black;
            this.label30.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label30.Location = new System.Drawing.Point(515, 24);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(178, 28);
            this.label30.TabIndex = 4;
            this.label30.Text = "Standard deviation";
            this.label30.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label29
            // 
            this.label29.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.label29.Font = new System.Drawing.Font("Roboto", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.label29.ForeColor = System.Drawing.Color.Black;
            this.label29.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label29.Location = new System.Drawing.Point(321, 24);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(149, 28);
            this.label29.TabIndex = 2;
            this.label29.Text = "Average step Length";
            this.label29.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label28
            // 
            this.label28.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.label28.Font = new System.Drawing.Font("Roboto", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.label28.ForeColor = System.Drawing.Color.Black;
            this.label28.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label28.Location = new System.Drawing.Point(173, 24);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(149, 28);
            this.label28.TabIndex = 3;
            this.label28.Text = "Steps count";
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.label2.Font = new System.Drawing.Font("Roboto", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label2.Location = new System.Drawing.Point(18, 24);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(116, 28);
            this.label2.TabIndex = 2;
            this.label2.Text = "Test stage";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // ParkinsonSessionDataLayoutPanel
            // 
            this.ParkinsonSessionDataLayoutPanel.Controls.Add(this.panel98);
            this.ParkinsonSessionDataLayoutPanel.Location = new System.Drawing.Point(0, 70);
            this.ParkinsonSessionDataLayoutPanel.Name = "ParkinsonSessionDataLayoutPanel";
            this.ParkinsonSessionDataLayoutPanel.Size = new System.Drawing.Size(965, 429);
            this.ParkinsonSessionDataLayoutPanel.TabIndex = 0;
            // 
            // panel98
            // 
            this.panel98.Location = new System.Drawing.Point(3, 3);
            this.panel98.Name = "panel98";
            this.panel98.Size = new System.Drawing.Size(956, 100);
            this.panel98.TabIndex = 0;
            // 
            // DataViewTab
            // 
            this.DataViewTab.Controls.Add(this.allDataBtn);
            this.DataViewTab.Controls.Add(this.stdDevBtn);
            this.DataViewTab.Controls.Add(this.validStepsBtn);
            this.DataViewTab.Controls.Add(this.ParkinsonDataGridView);
            this.DataViewTab.Location = new System.Drawing.Point(4, 39);
            this.DataViewTab.Name = "DataViewTab";
            this.DataViewTab.Padding = new System.Windows.Forms.Padding(3);
            this.DataViewTab.Size = new System.Drawing.Size(965, 857);
            this.DataViewTab.TabIndex = 1;
            this.DataViewTab.Text = "Data";
            this.DataViewTab.UseVisualStyleBackColor = true;
            // 
            // allDataBtn
            // 
            this.allDataBtn.BackColor = System.Drawing.Color.White;
            this.allDataBtn.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.allDataBtn.FlatAppearance.BorderSize = 3;
            this.allDataBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.allDataBtn.Font = new System.Drawing.Font("Roboto", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.allDataBtn.ForeColor = System.Drawing.Color.Black;
            this.allDataBtn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.allDataBtn.Location = new System.Drawing.Point(608, 9);
            this.allDataBtn.Name = "allDataBtn";
            this.allDataBtn.Size = new System.Drawing.Size(228, 41);
            this.allDataBtn.TabIndex = 20;
            this.allDataBtn.Text = "All data";
            this.allDataBtn.UseVisualStyleBackColor = false;
            this.allDataBtn.Click += new System.EventHandler(this.allDataBtn_Click);
            // 
            // stdDevBtn
            // 
            this.stdDevBtn.BackColor = System.Drawing.Color.White;
            this.stdDevBtn.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.stdDevBtn.FlatAppearance.BorderSize = 3;
            this.stdDevBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.stdDevBtn.Font = new System.Drawing.Font("Roboto", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.stdDevBtn.ForeColor = System.Drawing.Color.Black;
            this.stdDevBtn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.stdDevBtn.Location = new System.Drawing.Point(318, 9);
            this.stdDevBtn.Name = "stdDevBtn";
            this.stdDevBtn.Size = new System.Drawing.Size(228, 41);
            this.stdDevBtn.TabIndex = 19;
            this.stdDevBtn.Text = "STD dev";
            this.stdDevBtn.UseVisualStyleBackColor = false;
            this.stdDevBtn.Click += new System.EventHandler(this.stdDevBtn_Click);
            // 
            // validStepsBtn
            // 
            this.validStepsBtn.BackColor = System.Drawing.Color.White;
            this.validStepsBtn.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(201)))), ((int)(((byte)(31)))));
            this.validStepsBtn.FlatAppearance.BorderSize = 3;
            this.validStepsBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.validStepsBtn.Font = new System.Drawing.Font("Roboto", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.validStepsBtn.ForeColor = System.Drawing.Color.Black;
            this.validStepsBtn.Image = global::SelfitMain.Properties.Resources.Vector_2_2;
            this.validStepsBtn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.validStepsBtn.Location = new System.Drawing.Point(77, 9);
            this.validStepsBtn.Name = "validStepsBtn";
            this.validStepsBtn.Size = new System.Drawing.Size(195, 41);
            this.validStepsBtn.TabIndex = 18;
            this.validStepsBtn.Text = "Valid steps";
            this.validStepsBtn.UseVisualStyleBackColor = false;
            this.validStepsBtn.Click += new System.EventHandler(this.validStepsBtn_Click);
            // 
            // ParkinsonDataGridView
            // 
            this.ParkinsonDataGridView.AllowUserToAddRows = false;
            this.ParkinsonDataGridView.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black;
            this.ParkinsonDataGridView.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.ParkinsonDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ParkinsonDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.stepType,
            this.stepLength,
            this.stepTime,
            this.stage});
            this.ParkinsonDataGridView.Location = new System.Drawing.Point(3, 59);
            this.ParkinsonDataGridView.Name = "ParkinsonDataGridView";
            this.ParkinsonDataGridView.Size = new System.Drawing.Size(956, 795);
            this.ParkinsonDataGridView.TabIndex = 0;
            this.ParkinsonDataGridView.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // stepType
            // 
            this.stepType.HeaderText = "Step Type";
            this.stepType.Name = "stepType";
            this.stepType.Width = 200;
            // 
            // stepLength
            // 
            this.stepLength.HeaderText = "Length (CM)";
            this.stepLength.Name = "stepLength";
            this.stepLength.Width = 250;
            // 
            // stepTime
            // 
            this.stepTime.HeaderText = "Time (SEC)";
            this.stepTime.Name = "stepTime";
            this.stepTime.Width = 250;
            // 
            // stage
            // 
            this.stage.HeaderText = "Stage";
            this.stage.Name = "stage";
            this.stage.Width = 200;
            // 
            // panel92
            // 
            this.panel92.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(56)))), ((int)(((byte)(64)))));
            this.panel92.Controls.Add(this.historyQuickPlanBtn);
            this.panel92.Controls.Add(this.createNewPlanBtn);
            this.panel92.Location = new System.Drawing.Point(-1, 96);
            this.panel92.Name = "panel92";
            this.panel92.Size = new System.Drawing.Size(150, 445);
            this.panel92.TabIndex = 18;
            // 
            // historyQuickPlanBtn
            // 
            this.historyQuickPlanBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.historyQuickPlanBtn.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(56)))), ((int)(((byte)(64)))));
            this.historyQuickPlanBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.historyQuickPlanBtn.Font = new System.Drawing.Font("Roboto", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.historyQuickPlanBtn.ForeColor = System.Drawing.Color.White;
            this.historyQuickPlanBtn.Image = global::SelfitMain.Properties.Resources.quickplan_bright;
            this.historyQuickPlanBtn.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.historyQuickPlanBtn.Location = new System.Drawing.Point(12, 46);
            this.historyQuickPlanBtn.Name = "historyQuickPlanBtn";
            this.historyQuickPlanBtn.Size = new System.Drawing.Size(126, 127);
            this.historyQuickPlanBtn.TabIndex = 1;
            this.historyQuickPlanBtn.Text = "Quick Plan";
            this.historyQuickPlanBtn.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.historyQuickPlanBtn.UseVisualStyleBackColor = true;
            this.historyQuickPlanBtn.Click += new System.EventHandler(this.historyQuickPlanBtn_Click);
            // 
            // createNewPlanBtn
            // 
            this.createNewPlanBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.createNewPlanBtn.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(56)))), ((int)(((byte)(64)))));
            this.createNewPlanBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.createNewPlanBtn.Font = new System.Drawing.Font("Roboto", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.createNewPlanBtn.ForeColor = System.Drawing.Color.White;
            this.createNewPlanBtn.Image = global::SelfitMain.Properties.Resources.HistoryaddPatient;
            this.createNewPlanBtn.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.createNewPlanBtn.Location = new System.Drawing.Point(1, 268);
            this.createNewPlanBtn.Name = "createNewPlanBtn";
            this.createNewPlanBtn.Size = new System.Drawing.Size(150, 113);
            this.createNewPlanBtn.TabIndex = 0;
            this.createNewPlanBtn.Text = "Exercises";
            this.createNewPlanBtn.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.createNewPlanBtn.UseVisualStyleBackColor = true;
            this.createNewPlanBtn.Click += new System.EventHandler(this.createNewPlanBtn_Click);
            // 
            // exTitleHeader
            // 
            this.exTitleHeader.Controls.Add(this.label69);
            this.exTitleHeader.Controls.Add(this.label68);
            this.exTitleHeader.Controls.Add(this.label70);
            this.exTitleHeader.Location = new System.Drawing.Point(1509, 102);
            this.exTitleHeader.Name = "exTitleHeader";
            this.exTitleHeader.Size = new System.Drawing.Size(395, 59);
            this.exTitleHeader.TabIndex = 17;
            // 
            // label69
            // 
            this.label69.BackColor = System.Drawing.Color.White;
            this.label69.Font = new System.Drawing.Font("Roboto", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.label69.ForeColor = System.Drawing.Color.Black;
            this.label69.Location = new System.Drawing.Point(7, 25);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(52, 28);
            this.label69.TabIndex = 8;
            this.label69.Text = "No.";
            this.label69.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label68
            // 
            this.label68.BackColor = System.Drawing.Color.White;
            this.label68.Font = new System.Drawing.Font("Roboto", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.label68.ForeColor = System.Drawing.Color.Black;
            this.label68.Location = new System.Drawing.Point(137, 30);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(86, 28);
            this.label68.TabIndex = 9;
            this.label68.Text = "Exercises";
            this.label68.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label70
            // 
            this.label70.BackColor = System.Drawing.Color.White;
            this.label70.Font = new System.Drawing.Font("Roboto", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.label70.ForeColor = System.Drawing.Color.Black;
            this.label70.Location = new System.Drawing.Point(306, 30);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(86, 28);
            this.label70.TabIndex = 11;
            this.label70.Text = "Reps.";
            this.label70.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // startPlanBtn
            // 
            this.startPlanBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(201)))), ((int)(((byte)(31)))));
            this.startPlanBtn.FlatAppearance.BorderSize = 0;
            this.startPlanBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.startPlanBtn.Font = new System.Drawing.Font("Roboto", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.startPlanBtn.ForeColor = System.Drawing.Color.Black;
            this.startPlanBtn.Location = new System.Drawing.Point(1519, 1015);
            this.startPlanBtn.Name = "startPlanBtn";
            this.startPlanBtn.Size = new System.Drawing.Size(360, 60);
            this.startPlanBtn.TabIndex = 14;
            this.startPlanBtn.Text = "Start Plan";
            this.startPlanBtn.UseVisualStyleBackColor = false;
            this.startPlanBtn.Click += new System.EventHandler(this.startPlanBtn_Click);
            // 
            // selectAndEditPlanBtn
            // 
            this.selectAndEditPlanBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(56)))), ((int)(((byte)(64)))));
            this.selectAndEditPlanBtn.FlatAppearance.BorderSize = 0;
            this.selectAndEditPlanBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.selectAndEditPlanBtn.Font = new System.Drawing.Font("Roboto", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.selectAndEditPlanBtn.Location = new System.Drawing.Point(1519, 924);
            this.selectAndEditPlanBtn.Name = "selectAndEditPlanBtn";
            this.selectAndEditPlanBtn.Size = new System.Drawing.Size(360, 60);
            this.selectAndEditPlanBtn.TabIndex = 13;
            this.selectAndEditPlanBtn.Text = "Select && Edit";
            this.selectAndEditPlanBtn.UseVisualStyleBackColor = false;
            this.selectAndEditPlanBtn.Click += new System.EventHandler(this.selectAndEditPlanBtn_Click);
            // 
            // historyTotalExeTxt
            // 
            this.historyTotalExeTxt.BackColor = System.Drawing.Color.White;
            this.historyTotalExeTxt.Font = new System.Drawing.Font("Roboto", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.historyTotalExeTxt.ForeColor = System.Drawing.Color.Black;
            this.historyTotalExeTxt.Location = new System.Drawing.Point(1517, 844);
            this.historyTotalExeTxt.Name = "historyTotalExeTxt";
            this.historyTotalExeTxt.Size = new System.Drawing.Size(362, 33);
            this.historyTotalExeTxt.TabIndex = 12;
            this.historyTotalExeTxt.Text = "Completed 8/10 Exercises";
            this.historyTotalExeTxt.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // exerciesLayout
            // 
            this.exerciesLayout.AutoScroll = true;
            this.exerciesLayout.BackColor = System.Drawing.Color.White;
            this.exerciesLayout.Controls.Add(this.panel37);
            this.exerciesLayout.Controls.Add(this.panel38);
            this.exerciesLayout.Controls.Add(this.panel39);
            this.exerciesLayout.Controls.Add(this.panel40);
            this.exerciesLayout.Controls.Add(this.panel41);
            this.exerciesLayout.Controls.Add(this.panel42);
            this.exerciesLayout.Controls.Add(this.panel43);
            this.exerciesLayout.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.exerciesLayout.Location = new System.Drawing.Point(1509, 160);
            this.exerciesLayout.Margin = new System.Windows.Forms.Padding(3, 1, 3, 1);
            this.exerciesLayout.Name = "exerciesLayout";
            this.exerciesLayout.Size = new System.Drawing.Size(396, 607);
            this.exerciesLayout.TabIndex = 10;
            this.exerciesLayout.WrapContents = false;
            this.exerciesLayout.MouseDown += new System.Windows.Forms.MouseEventHandler(this.exerciesLayout_MouseDown);
            this.exerciesLayout.MouseMove += new System.Windows.Forms.MouseEventHandler(this.exerciesLayout_MouseMove);
            this.exerciesLayout.MouseWheel += new System.Windows.Forms.MouseEventHandler(this.exerciesLayout_MouseWheel);
            // 
            // panel37
            // 
            this.panel37.BackColor = System.Drawing.Color.White;
            this.panel37.Controls.Add(this.label73);
            this.panel37.Controls.Add(this.label72);
            this.panel37.Controls.Add(this.pictureBox2);
            this.panel37.Controls.Add(this.label71);
            this.panel37.Location = new System.Drawing.Point(0, 1);
            this.panel37.Margin = new System.Windows.Forms.Padding(0, 1, 0, 0);
            this.panel37.Name = "panel37";
            this.panel37.Size = new System.Drawing.Size(396, 74);
            this.panel37.TabIndex = 0;
            // 
            // label73
            // 
            this.label73.BackColor = System.Drawing.Color.White;
            this.label73.Font = new System.Drawing.Font("Roboto", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.label73.ForeColor = System.Drawing.Color.Black;
            this.label73.Location = new System.Drawing.Point(306, 23);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(72, 29);
            this.label73.TabIndex = 13;
            this.label73.Text = "04/07";
            this.label73.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label72
            // 
            this.label72.BackColor = System.Drawing.Color.White;
            this.label72.Font = new System.Drawing.Font("Roboto", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.label72.ForeColor = System.Drawing.Color.Black;
            this.label72.Location = new System.Drawing.Point(115, 23);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(174, 29);
            this.label72.TabIndex = 12;
            this.label72.Text = "Walk the square";
            this.label72.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pictureBox2.Image = global::SelfitMain.Properties.Resources.wtsImage;
            this.pictureBox2.Location = new System.Drawing.Point(65, 21);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(33, 33);
            this.pictureBox2.TabIndex = 11;
            this.pictureBox2.TabStop = false;
            // 
            // label71
            // 
            this.label71.BackColor = System.Drawing.Color.White;
            this.label71.Font = new System.Drawing.Font("Roboto", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.label71.ForeColor = System.Drawing.Color.Black;
            this.label71.Location = new System.Drawing.Point(8, 23);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(40, 29);
            this.label71.TabIndex = 10;
            this.label71.Text = "01";
            this.label71.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel38
            // 
            this.panel38.BackColor = System.Drawing.Color.White;
            this.panel38.Controls.Add(this.label74);
            this.panel38.Controls.Add(this.label75);
            this.panel38.Controls.Add(this.pictureBox3);
            this.panel38.Controls.Add(this.label76);
            this.panel38.Location = new System.Drawing.Point(0, 76);
            this.panel38.Margin = new System.Windows.Forms.Padding(0, 1, 0, 0);
            this.panel38.Name = "panel38";
            this.panel38.Size = new System.Drawing.Size(396, 74);
            this.panel38.TabIndex = 1;
            // 
            // label74
            // 
            this.label74.BackColor = System.Drawing.Color.White;
            this.label74.Font = new System.Drawing.Font("Roboto", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.label74.ForeColor = System.Drawing.Color.Black;
            this.label74.Location = new System.Drawing.Point(306, 23);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(86, 29);
            this.label74.TabIndex = 13;
            this.label74.Text = "04/07";
            this.label74.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label75
            // 
            this.label75.BackColor = System.Drawing.Color.White;
            this.label75.Font = new System.Drawing.Font("Roboto", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.label75.ForeColor = System.Drawing.Color.Black;
            this.label75.Location = new System.Drawing.Point(115, 23);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(174, 29);
            this.label75.TabIndex = 12;
            this.label75.Text = "Walk the square";
            this.label75.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pictureBox3.Image = global::SelfitMain.Properties.Resources.wtsImage;
            this.pictureBox3.Location = new System.Drawing.Point(65, 21);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(33, 33);
            this.pictureBox3.TabIndex = 11;
            this.pictureBox3.TabStop = false;
            // 
            // label76
            // 
            this.label76.BackColor = System.Drawing.Color.White;
            this.label76.Font = new System.Drawing.Font("Roboto", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.label76.ForeColor = System.Drawing.Color.Black;
            this.label76.Location = new System.Drawing.Point(8, 23);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(40, 29);
            this.label76.TabIndex = 10;
            this.label76.Text = "01";
            this.label76.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel39
            // 
            this.panel39.BackColor = System.Drawing.Color.White;
            this.panel39.Controls.Add(this.label77);
            this.panel39.Controls.Add(this.label78);
            this.panel39.Controls.Add(this.pictureBox4);
            this.panel39.Controls.Add(this.label79);
            this.panel39.Location = new System.Drawing.Point(0, 151);
            this.panel39.Margin = new System.Windows.Forms.Padding(0, 1, 0, 0);
            this.panel39.Name = "panel39";
            this.panel39.Size = new System.Drawing.Size(396, 74);
            this.panel39.TabIndex = 2;
            // 
            // label77
            // 
            this.label77.BackColor = System.Drawing.Color.White;
            this.label77.Font = new System.Drawing.Font("Roboto", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.label77.ForeColor = System.Drawing.Color.Black;
            this.label77.Location = new System.Drawing.Point(306, 23);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(86, 29);
            this.label77.TabIndex = 13;
            this.label77.Text = "04/07";
            this.label77.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label78
            // 
            this.label78.BackColor = System.Drawing.Color.White;
            this.label78.Font = new System.Drawing.Font("Roboto", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.label78.ForeColor = System.Drawing.Color.Black;
            this.label78.Location = new System.Drawing.Point(115, 23);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(174, 29);
            this.label78.TabIndex = 12;
            this.label78.Text = "Walk the square";
            this.label78.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pictureBox4.Image = global::SelfitMain.Properties.Resources.wtsImage;
            this.pictureBox4.Location = new System.Drawing.Point(65, 21);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(33, 33);
            this.pictureBox4.TabIndex = 11;
            this.pictureBox4.TabStop = false;
            // 
            // label79
            // 
            this.label79.BackColor = System.Drawing.Color.White;
            this.label79.Font = new System.Drawing.Font("Roboto", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.label79.ForeColor = System.Drawing.Color.Black;
            this.label79.Location = new System.Drawing.Point(8, 23);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(40, 29);
            this.label79.TabIndex = 10;
            this.label79.Text = "01";
            this.label79.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel40
            // 
            this.panel40.BackColor = System.Drawing.Color.White;
            this.panel40.Controls.Add(this.label80);
            this.panel40.Controls.Add(this.label81);
            this.panel40.Controls.Add(this.pictureBox5);
            this.panel40.Controls.Add(this.label82);
            this.panel40.Location = new System.Drawing.Point(0, 226);
            this.panel40.Margin = new System.Windows.Forms.Padding(0, 1, 0, 0);
            this.panel40.Name = "panel40";
            this.panel40.Size = new System.Drawing.Size(396, 74);
            this.panel40.TabIndex = 3;
            // 
            // label80
            // 
            this.label80.BackColor = System.Drawing.Color.White;
            this.label80.Font = new System.Drawing.Font("Roboto", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.label80.ForeColor = System.Drawing.Color.Black;
            this.label80.Location = new System.Drawing.Point(306, 23);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(86, 29);
            this.label80.TabIndex = 13;
            this.label80.Text = "04/07";
            this.label80.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label81
            // 
            this.label81.BackColor = System.Drawing.Color.White;
            this.label81.Font = new System.Drawing.Font("Roboto", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.label81.ForeColor = System.Drawing.Color.Black;
            this.label81.Location = new System.Drawing.Point(115, 23);
            this.label81.Name = "label81";
            this.label81.Size = new System.Drawing.Size(174, 29);
            this.label81.TabIndex = 12;
            this.label81.Text = "Walk the square";
            this.label81.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pictureBox5
            // 
            this.pictureBox5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pictureBox5.Image = global::SelfitMain.Properties.Resources.wtsImage;
            this.pictureBox5.Location = new System.Drawing.Point(65, 21);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(33, 33);
            this.pictureBox5.TabIndex = 11;
            this.pictureBox5.TabStop = false;
            // 
            // label82
            // 
            this.label82.BackColor = System.Drawing.Color.White;
            this.label82.Font = new System.Drawing.Font("Roboto", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.label82.ForeColor = System.Drawing.Color.Black;
            this.label82.Location = new System.Drawing.Point(8, 23);
            this.label82.Name = "label82";
            this.label82.Size = new System.Drawing.Size(40, 29);
            this.label82.TabIndex = 10;
            this.label82.Text = "01";
            this.label82.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel41
            // 
            this.panel41.BackColor = System.Drawing.Color.White;
            this.panel41.Controls.Add(this.label83);
            this.panel41.Controls.Add(this.label84);
            this.panel41.Controls.Add(this.pictureBox6);
            this.panel41.Controls.Add(this.label85);
            this.panel41.Location = new System.Drawing.Point(0, 301);
            this.panel41.Margin = new System.Windows.Forms.Padding(0, 1, 0, 0);
            this.panel41.Name = "panel41";
            this.panel41.Size = new System.Drawing.Size(396, 74);
            this.panel41.TabIndex = 4;
            // 
            // label83
            // 
            this.label83.BackColor = System.Drawing.Color.White;
            this.label83.Font = new System.Drawing.Font("Roboto", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.label83.ForeColor = System.Drawing.Color.Black;
            this.label83.Location = new System.Drawing.Point(306, 23);
            this.label83.Name = "label83";
            this.label83.Size = new System.Drawing.Size(86, 29);
            this.label83.TabIndex = 13;
            this.label83.Text = "04/07";
            this.label83.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label84
            // 
            this.label84.BackColor = System.Drawing.Color.White;
            this.label84.Font = new System.Drawing.Font("Roboto", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.label84.ForeColor = System.Drawing.Color.Black;
            this.label84.Location = new System.Drawing.Point(115, 23);
            this.label84.Name = "label84";
            this.label84.Size = new System.Drawing.Size(174, 29);
            this.label84.TabIndex = 12;
            this.label84.Text = "Walk the square";
            this.label84.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pictureBox6
            // 
            this.pictureBox6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pictureBox6.Image = global::SelfitMain.Properties.Resources.wtsImage;
            this.pictureBox6.Location = new System.Drawing.Point(65, 21);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(33, 33);
            this.pictureBox6.TabIndex = 11;
            this.pictureBox6.TabStop = false;
            // 
            // label85
            // 
            this.label85.BackColor = System.Drawing.Color.White;
            this.label85.Font = new System.Drawing.Font("Roboto", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.label85.ForeColor = System.Drawing.Color.Black;
            this.label85.Location = new System.Drawing.Point(8, 23);
            this.label85.Name = "label85";
            this.label85.Size = new System.Drawing.Size(40, 29);
            this.label85.TabIndex = 10;
            this.label85.Text = "01";
            this.label85.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel42
            // 
            this.panel42.BackColor = System.Drawing.Color.White;
            this.panel42.Controls.Add(this.label86);
            this.panel42.Controls.Add(this.label87);
            this.panel42.Controls.Add(this.pictureBox7);
            this.panel42.Controls.Add(this.label88);
            this.panel42.Location = new System.Drawing.Point(0, 376);
            this.panel42.Margin = new System.Windows.Forms.Padding(0, 1, 0, 0);
            this.panel42.Name = "panel42";
            this.panel42.Size = new System.Drawing.Size(396, 74);
            this.panel42.TabIndex = 5;
            // 
            // label86
            // 
            this.label86.BackColor = System.Drawing.Color.White;
            this.label86.Font = new System.Drawing.Font("Roboto", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.label86.ForeColor = System.Drawing.Color.Black;
            this.label86.Location = new System.Drawing.Point(306, 23);
            this.label86.Name = "label86";
            this.label86.Size = new System.Drawing.Size(86, 29);
            this.label86.TabIndex = 13;
            this.label86.Text = "04/07";
            this.label86.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label87
            // 
            this.label87.BackColor = System.Drawing.Color.White;
            this.label87.Font = new System.Drawing.Font("Roboto", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.label87.ForeColor = System.Drawing.Color.Black;
            this.label87.Location = new System.Drawing.Point(115, 23);
            this.label87.Name = "label87";
            this.label87.Size = new System.Drawing.Size(174, 29);
            this.label87.TabIndex = 12;
            this.label87.Text = "Walk the square";
            this.label87.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pictureBox7
            // 
            this.pictureBox7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pictureBox7.Image = global::SelfitMain.Properties.Resources.wtsImage;
            this.pictureBox7.Location = new System.Drawing.Point(65, 21);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(33, 33);
            this.pictureBox7.TabIndex = 11;
            this.pictureBox7.TabStop = false;
            // 
            // label88
            // 
            this.label88.BackColor = System.Drawing.Color.White;
            this.label88.Font = new System.Drawing.Font("Roboto", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.label88.ForeColor = System.Drawing.Color.Black;
            this.label88.Location = new System.Drawing.Point(8, 23);
            this.label88.Name = "label88";
            this.label88.Size = new System.Drawing.Size(40, 29);
            this.label88.TabIndex = 10;
            this.label88.Text = "01";
            this.label88.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel43
            // 
            this.panel43.BackColor = System.Drawing.Color.White;
            this.panel43.Controls.Add(this.label89);
            this.panel43.Controls.Add(this.label90);
            this.panel43.Controls.Add(this.pictureBox8);
            this.panel43.Controls.Add(this.label91);
            this.panel43.Location = new System.Drawing.Point(0, 451);
            this.panel43.Margin = new System.Windows.Forms.Padding(0, 1, 0, 0);
            this.panel43.Name = "panel43";
            this.panel43.Size = new System.Drawing.Size(396, 74);
            this.panel43.TabIndex = 6;
            // 
            // label89
            // 
            this.label89.BackColor = System.Drawing.Color.White;
            this.label89.Font = new System.Drawing.Font("Roboto", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.label89.ForeColor = System.Drawing.Color.Black;
            this.label89.Location = new System.Drawing.Point(306, 23);
            this.label89.Name = "label89";
            this.label89.Size = new System.Drawing.Size(86, 29);
            this.label89.TabIndex = 13;
            this.label89.Text = "04/07";
            this.label89.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label90
            // 
            this.label90.BackColor = System.Drawing.Color.White;
            this.label90.Font = new System.Drawing.Font("Roboto", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.label90.ForeColor = System.Drawing.Color.Black;
            this.label90.Location = new System.Drawing.Point(115, 23);
            this.label90.Name = "label90";
            this.label90.Size = new System.Drawing.Size(174, 29);
            this.label90.TabIndex = 12;
            this.label90.Text = "Walk the square";
            this.label90.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pictureBox8
            // 
            this.pictureBox8.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pictureBox8.Image = global::SelfitMain.Properties.Resources.wtsImage;
            this.pictureBox8.Location = new System.Drawing.Point(65, 21);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(33, 33);
            this.pictureBox8.TabIndex = 11;
            this.pictureBox8.TabStop = false;
            // 
            // label91
            // 
            this.label91.BackColor = System.Drawing.Color.White;
            this.label91.Font = new System.Drawing.Font("Roboto", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.label91.ForeColor = System.Drawing.Color.Black;
            this.label91.Location = new System.Drawing.Point(8, 23);
            this.label91.Name = "label91";
            this.label91.Size = new System.Drawing.Size(40, 29);
            this.label91.TabIndex = 10;
            this.label91.Text = "01";
            this.label91.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // sessionDatainfoPanel
            // 
            this.sessionDatainfoPanel.Controls.Add(this.panel30);
            this.sessionDatainfoPanel.Controls.Add(this.panel31);
            this.sessionDatainfoPanel.Controls.Add(this.panel32);
            this.sessionDatainfoPanel.Controls.Add(this.panel33);
            this.sessionDatainfoPanel.Controls.Add(this.panel34);
            this.sessionDatainfoPanel.Controls.Add(this.panel35);
            this.sessionDatainfoPanel.Controls.Add(this.panel36);
            this.sessionDatainfoPanel.Location = new System.Drawing.Point(510, 147);
            this.sessionDatainfoPanel.Name = "sessionDatainfoPanel";
            this.sessionDatainfoPanel.Size = new System.Drawing.Size(928, 928);
            this.sessionDatainfoPanel.TabIndex = 7;
            this.sessionDatainfoPanel.MouseDown += new System.Windows.Forms.MouseEventHandler(this.sessionDatainfoPanel_MouseDown);
            this.sessionDatainfoPanel.MouseMove += new System.Windows.Forms.MouseEventHandler(this.sessionDatainfoPanel_MouseMove);
            // 
            // panel30
            // 
            this.panel30.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.panel30.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel30.Controls.Add(this.histTimeDataLbl);
            this.panel30.Controls.Add(this.timeChart);
            this.panel30.Controls.Add(this.histTimeRateLbl);
            this.panel30.Controls.Add(this.histTimeLbl);
            this.panel30.Location = new System.Drawing.Point(15, 15);
            this.panel30.Margin = new System.Windows.Forms.Padding(15);
            this.panel30.Name = "panel30";
            this.panel30.Size = new System.Drawing.Size(276, 276);
            this.panel30.TabIndex = 0;
            // 
            // histTimeDataLbl
            // 
            this.histTimeDataLbl.BackColor = System.Drawing.Color.Transparent;
            this.histTimeDataLbl.Font = new System.Drawing.Font("Roboto", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.histTimeDataLbl.ForeColor = System.Drawing.Color.Black;
            this.histTimeDataLbl.Location = new System.Drawing.Point(19, 231);
            this.histTimeDataLbl.Name = "histTimeDataLbl";
            this.histTimeDataLbl.Size = new System.Drawing.Size(127, 36);
            this.histTimeDataLbl.TabIndex = 3;
            this.histTimeDataLbl.Text = "7.6 m/sec";
            this.histTimeDataLbl.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // timeChart
            // 
            this.timeChart.BackColor = System.Drawing.Color.Transparent;
            this.timeChart.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            chartArea1.AxisX.LabelAutoFitMaxFontSize = 5;
            chartArea1.AxisX.LabelAutoFitMinFontSize = 5;
            chartArea1.AxisX.LabelStyle.Enabled = false;
            chartArea1.AxisX.LineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.NotSet;
            chartArea1.AxisX.MajorGrid.Enabled = false;
            chartArea1.AxisX.MajorTickMark.Enabled = false;
            chartArea1.AxisX.TitleFont = new System.Drawing.Font("Microsoft Sans Serif", 4F);
            chartArea1.AxisY.Enabled = System.Windows.Forms.DataVisualization.Charting.AxisEnabled.False;
            chartArea1.AxisY.LineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.NotSet;
            chartArea1.AxisY.MajorGrid.Enabled = false;
            chartArea1.AxisY.MajorTickMark.Enabled = false;
            chartArea1.AxisY.TitleFont = new System.Drawing.Font("Microsoft Sans Serif", 4F);
            chartArea1.BackColor = System.Drawing.Color.Transparent;
            chartArea1.BorderWidth = 0;
            chartArea1.Name = "ChartArea1";
            chartArea1.Position.Auto = false;
            chartArea1.Position.Height = 100F;
            chartArea1.Position.Width = 100F;
            this.timeChart.ChartAreas.Add(chartArea1);
            this.timeChart.Location = new System.Drawing.Point(-3, 37);
            this.timeChart.Name = "timeChart";
            this.timeChart.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.Fire;
            series1.BorderWidth = 4;
            series1.ChartArea = "ChartArea1";
            series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series1.Color = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(201)))), ((int)(((byte)(31)))));
            series1.IsVisibleInLegend = false;
            series1.MarkerBorderColor = System.Drawing.Color.White;
            series1.Name = "Series1";
            series1.Points.Add(dataPoint1);
            series1.Points.Add(dataPoint2);
            series1.Points.Add(dataPoint3);
            series1.Points.Add(dataPoint4);
            series1.SmartLabelStyle.Enabled = false;
            this.timeChart.Series.Add(series1);
            this.timeChart.Size = new System.Drawing.Size(278, 180);
            this.timeChart.TabIndex = 6;
            // 
            // histTimeRateLbl
            // 
            this.histTimeRateLbl.BackColor = System.Drawing.Color.Transparent;
            this.histTimeRateLbl.Font = new System.Drawing.Font("Roboto", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.histTimeRateLbl.ForeColor = System.Drawing.Color.DarkRed;
            this.histTimeRateLbl.Location = new System.Drawing.Point(200, 247);
            this.histTimeRateLbl.Name = "histTimeRateLbl";
            this.histTimeRateLbl.Size = new System.Drawing.Size(73, 23);
            this.histTimeRateLbl.TabIndex = 4;
            this.histTimeRateLbl.Text = "+10%";
            this.histTimeRateLbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // histTimeLbl
            // 
            this.histTimeLbl.BackColor = System.Drawing.Color.Transparent;
            this.histTimeLbl.Font = new System.Drawing.Font("Roboto", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.histTimeLbl.ForeColor = System.Drawing.Color.Black;
            this.histTimeLbl.Location = new System.Drawing.Point(19, 9);
            this.histTimeLbl.Name = "histTimeLbl";
            this.histTimeLbl.Size = new System.Drawing.Size(190, 48);
            this.histTimeLbl.TabIndex = 2;
            this.histTimeLbl.Text = "Time";
            // 
            // panel31
            // 
            this.panel31.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.panel31.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel31.Controls.Add(this.histDistanceDataLbl);
            this.panel31.Controls.Add(this.DistanceChart);
            this.panel31.Controls.Add(this.histDistanceRateLbl);
            this.panel31.Controls.Add(this.histDistanceLbl);
            this.panel31.Location = new System.Drawing.Point(321, 15);
            this.panel31.Margin = new System.Windows.Forms.Padding(15);
            this.panel31.Name = "panel31";
            this.panel31.Size = new System.Drawing.Size(276, 276);
            this.panel31.TabIndex = 1;
            // 
            // histDistanceDataLbl
            // 
            this.histDistanceDataLbl.BackColor = System.Drawing.Color.Transparent;
            this.histDistanceDataLbl.Font = new System.Drawing.Font("Roboto", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.histDistanceDataLbl.ForeColor = System.Drawing.Color.Black;
            this.histDistanceDataLbl.Location = new System.Drawing.Point(19, 231);
            this.histDistanceDataLbl.Name = "histDistanceDataLbl";
            this.histDistanceDataLbl.Size = new System.Drawing.Size(127, 36);
            this.histDistanceDataLbl.TabIndex = 3;
            this.histDistanceDataLbl.Text = "7.6 m/sec";
            this.histDistanceDataLbl.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // DistanceChart
            // 
            this.DistanceChart.BackColor = System.Drawing.Color.Transparent;
            this.DistanceChart.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            chartArea2.AxisX.LabelAutoFitMaxFontSize = 5;
            chartArea2.AxisX.LabelAutoFitMinFontSize = 5;
            chartArea2.AxisX.LabelStyle.Enabled = false;
            chartArea2.AxisX.LineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.NotSet;
            chartArea2.AxisX.MajorGrid.Enabled = false;
            chartArea2.AxisX.MajorTickMark.Enabled = false;
            chartArea2.AxisX.TitleFont = new System.Drawing.Font("Microsoft Sans Serif", 4F);
            chartArea2.AxisY.Enabled = System.Windows.Forms.DataVisualization.Charting.AxisEnabled.False;
            chartArea2.AxisY.LineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.NotSet;
            chartArea2.AxisY.MajorGrid.Enabled = false;
            chartArea2.AxisY.MajorTickMark.Enabled = false;
            chartArea2.AxisY.TitleFont = new System.Drawing.Font("Microsoft Sans Serif", 4F);
            chartArea2.BackColor = System.Drawing.Color.Transparent;
            chartArea2.BorderWidth = 0;
            chartArea2.Name = "ChartArea1";
            chartArea2.Position.Auto = false;
            chartArea2.Position.Height = 100F;
            chartArea2.Position.Width = 100F;
            this.DistanceChart.ChartAreas.Add(chartArea2);
            this.DistanceChart.Location = new System.Drawing.Point(-1, 37);
            this.DistanceChart.Name = "DistanceChart";
            this.DistanceChart.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.Fire;
            series2.BorderWidth = 4;
            series2.ChartArea = "ChartArea1";
            series2.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series2.Color = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(201)))), ((int)(((byte)(31)))));
            series2.IsVisibleInLegend = false;
            series2.MarkerBorderColor = System.Drawing.Color.White;
            series2.Name = "Series1";
            series2.Points.Add(dataPoint5);
            series2.Points.Add(dataPoint6);
            series2.Points.Add(dataPoint7);
            series2.Points.Add(dataPoint8);
            series2.SmartLabelStyle.Enabled = false;
            this.DistanceChart.Series.Add(series2);
            this.DistanceChart.Size = new System.Drawing.Size(276, 180);
            this.DistanceChart.TabIndex = 7;
            // 
            // histDistanceRateLbl
            // 
            this.histDistanceRateLbl.BackColor = System.Drawing.Color.Transparent;
            this.histDistanceRateLbl.Font = new System.Drawing.Font("Roboto", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.histDistanceRateLbl.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(167)))), ((int)(((byte)(0)))));
            this.histDistanceRateLbl.Location = new System.Drawing.Point(200, 247);
            this.histDistanceRateLbl.Name = "histDistanceRateLbl";
            this.histDistanceRateLbl.Size = new System.Drawing.Size(73, 23);
            this.histDistanceRateLbl.TabIndex = 4;
            this.histDistanceRateLbl.Text = "+10%";
            this.histDistanceRateLbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // histDistanceLbl
            // 
            this.histDistanceLbl.BackColor = System.Drawing.Color.Transparent;
            this.histDistanceLbl.Font = new System.Drawing.Font("Roboto", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.histDistanceLbl.ForeColor = System.Drawing.Color.Black;
            this.histDistanceLbl.Location = new System.Drawing.Point(19, 9);
            this.histDistanceLbl.Name = "histDistanceLbl";
            this.histDistanceLbl.Size = new System.Drawing.Size(190, 48);
            this.histDistanceLbl.TabIndex = 2;
            this.histDistanceLbl.Text = "Distance";
            // 
            // panel32
            // 
            this.panel32.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.panel32.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel32.Controls.Add(this.histStepCountDataLbl);
            this.panel32.Controls.Add(this.stepNumChart);
            this.panel32.Controls.Add(this.histStepCountRateLbl);
            this.panel32.Controls.Add(this.histStepCountLbl);
            this.panel32.Location = new System.Drawing.Point(627, 15);
            this.panel32.Margin = new System.Windows.Forms.Padding(15);
            this.panel32.Name = "panel32";
            this.panel32.Size = new System.Drawing.Size(276, 276);
            this.panel32.TabIndex = 2;
            // 
            // histStepCountDataLbl
            // 
            this.histStepCountDataLbl.BackColor = System.Drawing.Color.Transparent;
            this.histStepCountDataLbl.Font = new System.Drawing.Font("Roboto", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.histStepCountDataLbl.ForeColor = System.Drawing.Color.Black;
            this.histStepCountDataLbl.Location = new System.Drawing.Point(19, 231);
            this.histStepCountDataLbl.Name = "histStepCountDataLbl";
            this.histStepCountDataLbl.Size = new System.Drawing.Size(127, 36);
            this.histStepCountDataLbl.TabIndex = 3;
            this.histStepCountDataLbl.Text = "7.6 m/sec";
            this.histStepCountDataLbl.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // stepNumChart
            // 
            this.stepNumChart.BackColor = System.Drawing.Color.Transparent;
            this.stepNumChart.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            chartArea3.AxisX.LabelAutoFitMaxFontSize = 5;
            chartArea3.AxisX.LabelAutoFitMinFontSize = 5;
            chartArea3.AxisX.LabelStyle.Enabled = false;
            chartArea3.AxisX.LineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.NotSet;
            chartArea3.AxisX.MajorGrid.Enabled = false;
            chartArea3.AxisX.MajorTickMark.Enabled = false;
            chartArea3.AxisX.TitleFont = new System.Drawing.Font("Microsoft Sans Serif", 4F);
            chartArea3.AxisY.Enabled = System.Windows.Forms.DataVisualization.Charting.AxisEnabled.False;
            chartArea3.AxisY.LineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.NotSet;
            chartArea3.AxisY.MajorGrid.Enabled = false;
            chartArea3.AxisY.MajorTickMark.Enabled = false;
            chartArea3.AxisY.TitleFont = new System.Drawing.Font("Microsoft Sans Serif", 4F);
            chartArea3.BackColor = System.Drawing.Color.Transparent;
            chartArea3.BorderWidth = 0;
            chartArea3.Name = "ChartArea1";
            chartArea3.Position.Auto = false;
            chartArea3.Position.Height = 100F;
            chartArea3.Position.Width = 100F;
            this.stepNumChart.ChartAreas.Add(chartArea3);
            this.stepNumChart.Location = new System.Drawing.Point(-1, 37);
            this.stepNumChart.Name = "stepNumChart";
            this.stepNumChart.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.Fire;
            series3.BorderWidth = 4;
            series3.ChartArea = "ChartArea1";
            series3.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series3.Color = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(201)))), ((int)(((byte)(31)))));
            series3.IsVisibleInLegend = false;
            series3.MarkerBorderColor = System.Drawing.Color.White;
            series3.Name = "Series1";
            series3.Points.Add(dataPoint9);
            series3.Points.Add(dataPoint10);
            series3.Points.Add(dataPoint11);
            series3.Points.Add(dataPoint12);
            series3.SmartLabelStyle.Enabled = false;
            this.stepNumChart.Series.Add(series3);
            this.stepNumChart.Size = new System.Drawing.Size(276, 180);
            this.stepNumChart.TabIndex = 8;
            // 
            // histStepCountRateLbl
            // 
            this.histStepCountRateLbl.BackColor = System.Drawing.Color.Transparent;
            this.histStepCountRateLbl.Font = new System.Drawing.Font("Roboto", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.histStepCountRateLbl.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(167)))), ((int)(((byte)(0)))));
            this.histStepCountRateLbl.Location = new System.Drawing.Point(200, 247);
            this.histStepCountRateLbl.Name = "histStepCountRateLbl";
            this.histStepCountRateLbl.Size = new System.Drawing.Size(73, 23);
            this.histStepCountRateLbl.TabIndex = 4;
            this.histStepCountRateLbl.Text = "+10%";
            this.histStepCountRateLbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // histStepCountLbl
            // 
            this.histStepCountLbl.BackColor = System.Drawing.Color.Transparent;
            this.histStepCountLbl.Font = new System.Drawing.Font("Roboto", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.histStepCountLbl.ForeColor = System.Drawing.Color.Black;
            this.histStepCountLbl.Location = new System.Drawing.Point(19, 9);
            this.histStepCountLbl.Name = "histStepCountLbl";
            this.histStepCountLbl.Size = new System.Drawing.Size(190, 48);
            this.histStepCountLbl.TabIndex = 2;
            this.histStepCountLbl.Text = "No. of Steps";
            // 
            // panel33
            // 
            this.panel33.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.panel33.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel33.Controls.Add(this.histSpeedDataLbl);
            this.panel33.Controls.Add(this.histSpeedLbl);
            this.panel33.Controls.Add(this.speedChart);
            this.panel33.Controls.Add(this.histSpeedRateLbl);
            this.panel33.Location = new System.Drawing.Point(15, 321);
            this.panel33.Margin = new System.Windows.Forms.Padding(15);
            this.panel33.Name = "panel33";
            this.panel33.Size = new System.Drawing.Size(276, 276);
            this.panel33.TabIndex = 3;
            // 
            // histSpeedDataLbl
            // 
            this.histSpeedDataLbl.BackColor = System.Drawing.Color.Transparent;
            this.histSpeedDataLbl.Font = new System.Drawing.Font("Roboto", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.histSpeedDataLbl.ForeColor = System.Drawing.Color.Black;
            this.histSpeedDataLbl.Location = new System.Drawing.Point(19, 231);
            this.histSpeedDataLbl.Name = "histSpeedDataLbl";
            this.histSpeedDataLbl.Size = new System.Drawing.Size(127, 36);
            this.histSpeedDataLbl.TabIndex = 3;
            this.histSpeedDataLbl.Text = "7.6 m/sec";
            this.histSpeedDataLbl.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // histSpeedLbl
            // 
            this.histSpeedLbl.BackColor = System.Drawing.Color.Transparent;
            this.histSpeedLbl.Font = new System.Drawing.Font("Roboto", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.histSpeedLbl.ForeColor = System.Drawing.Color.Black;
            this.histSpeedLbl.Location = new System.Drawing.Point(19, 9);
            this.histSpeedLbl.Name = "histSpeedLbl";
            this.histSpeedLbl.Size = new System.Drawing.Size(190, 48);
            this.histSpeedLbl.TabIndex = 2;
            this.histSpeedLbl.Text = "Speed";
            // 
            // speedChart
            // 
            this.speedChart.BackColor = System.Drawing.Color.Transparent;
            this.speedChart.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            chartArea4.AxisX.LabelAutoFitMaxFontSize = 5;
            chartArea4.AxisX.LabelAutoFitMinFontSize = 5;
            chartArea4.AxisX.LabelStyle.Enabled = false;
            chartArea4.AxisX.LineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.NotSet;
            chartArea4.AxisX.MajorGrid.Enabled = false;
            chartArea4.AxisX.MajorTickMark.Enabled = false;
            chartArea4.AxisX.TitleFont = new System.Drawing.Font("Microsoft Sans Serif", 4F);
            chartArea4.AxisY.Enabled = System.Windows.Forms.DataVisualization.Charting.AxisEnabled.False;
            chartArea4.AxisY.LineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.NotSet;
            chartArea4.AxisY.MajorGrid.Enabled = false;
            chartArea4.AxisY.MajorTickMark.Enabled = false;
            chartArea4.AxisY.TitleFont = new System.Drawing.Font("Microsoft Sans Serif", 4F);
            chartArea4.BackColor = System.Drawing.Color.Transparent;
            chartArea4.BorderWidth = 0;
            chartArea4.Name = "ChartArea1";
            chartArea4.Position.Auto = false;
            chartArea4.Position.Height = 100F;
            chartArea4.Position.Width = 100F;
            this.speedChart.ChartAreas.Add(chartArea4);
            this.speedChart.Location = new System.Drawing.Point(-3, 42);
            this.speedChart.Name = "speedChart";
            this.speedChart.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.Fire;
            series4.BorderWidth = 4;
            series4.ChartArea = "ChartArea1";
            series4.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series4.Color = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(201)))), ((int)(((byte)(31)))));
            series4.IsVisibleInLegend = false;
            series4.MarkerBorderColor = System.Drawing.Color.White;
            series4.Name = "Series1";
            series4.Points.Add(dataPoint13);
            series4.Points.Add(dataPoint14);
            series4.Points.Add(dataPoint15);
            series4.Points.Add(dataPoint16);
            series4.SmartLabelStyle.Enabled = false;
            this.speedChart.Series.Add(series4);
            this.speedChart.Size = new System.Drawing.Size(278, 180);
            this.speedChart.TabIndex = 5;
            // 
            // histSpeedRateLbl
            // 
            this.histSpeedRateLbl.BackColor = System.Drawing.Color.Transparent;
            this.histSpeedRateLbl.Font = new System.Drawing.Font("Roboto", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.histSpeedRateLbl.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(167)))), ((int)(((byte)(0)))));
            this.histSpeedRateLbl.Location = new System.Drawing.Point(200, 247);
            this.histSpeedRateLbl.Name = "histSpeedRateLbl";
            this.histSpeedRateLbl.Size = new System.Drawing.Size(73, 23);
            this.histSpeedRateLbl.TabIndex = 4;
            this.histSpeedRateLbl.Text = "+10%";
            this.histSpeedRateLbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel34
            // 
            this.panel34.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.panel34.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel34.Controls.Add(this.histHRightDataLbl);
            this.panel34.Controls.Add(this.stepHRchart);
            this.panel34.Controls.Add(this.histHRightRateLbl);
            this.panel34.Controls.Add(this.histHRightLbl);
            this.panel34.Location = new System.Drawing.Point(321, 321);
            this.panel34.Margin = new System.Windows.Forms.Padding(15);
            this.panel34.Name = "panel34";
            this.panel34.Size = new System.Drawing.Size(276, 276);
            this.panel34.TabIndex = 4;
            // 
            // histHRightDataLbl
            // 
            this.histHRightDataLbl.BackColor = System.Drawing.Color.Transparent;
            this.histHRightDataLbl.Font = new System.Drawing.Font("Roboto", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.histHRightDataLbl.ForeColor = System.Drawing.Color.Black;
            this.histHRightDataLbl.Location = new System.Drawing.Point(19, 231);
            this.histHRightDataLbl.Name = "histHRightDataLbl";
            this.histHRightDataLbl.Size = new System.Drawing.Size(127, 36);
            this.histHRightDataLbl.TabIndex = 3;
            this.histHRightDataLbl.Text = "7.6 m/sec";
            this.histHRightDataLbl.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // stepHRchart
            // 
            this.stepHRchart.BackColor = System.Drawing.Color.Transparent;
            this.stepHRchart.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            chartArea5.AxisX.LabelAutoFitMaxFontSize = 5;
            chartArea5.AxisX.LabelAutoFitMinFontSize = 5;
            chartArea5.AxisX.LabelStyle.Enabled = false;
            chartArea5.AxisX.LineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.NotSet;
            chartArea5.AxisX.MajorGrid.Enabled = false;
            chartArea5.AxisX.MajorTickMark.Enabled = false;
            chartArea5.AxisX.TitleFont = new System.Drawing.Font("Microsoft Sans Serif", 4F);
            chartArea5.AxisY.Enabled = System.Windows.Forms.DataVisualization.Charting.AxisEnabled.False;
            chartArea5.AxisY.LineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.NotSet;
            chartArea5.AxisY.MajorGrid.Enabled = false;
            chartArea5.AxisY.MajorTickMark.Enabled = false;
            chartArea5.AxisY.TitleFont = new System.Drawing.Font("Microsoft Sans Serif", 4F);
            chartArea5.BackColor = System.Drawing.Color.Transparent;
            chartArea5.BorderWidth = 0;
            chartArea5.Name = "ChartArea1";
            chartArea5.Position.Auto = false;
            chartArea5.Position.Height = 100F;
            chartArea5.Position.Width = 100F;
            this.stepHRchart.ChartAreas.Add(chartArea5);
            this.stepHRchart.Location = new System.Drawing.Point(-1, 42);
            this.stepHRchart.Name = "stepHRchart";
            this.stepHRchart.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.Fire;
            series5.BorderWidth = 4;
            series5.ChartArea = "ChartArea1";
            series5.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series5.Color = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(201)))), ((int)(((byte)(31)))));
            series5.IsVisibleInLegend = false;
            series5.MarkerBorderColor = System.Drawing.Color.White;
            series5.Name = "Series1";
            series5.Points.Add(dataPoint17);
            series5.Points.Add(dataPoint18);
            series5.Points.Add(dataPoint19);
            series5.Points.Add(dataPoint20);
            series5.SmartLabelStyle.Enabled = false;
            this.stepHRchart.Series.Add(series5);
            this.stepHRchart.Size = new System.Drawing.Size(276, 180);
            this.stepHRchart.TabIndex = 8;
            // 
            // histHRightRateLbl
            // 
            this.histHRightRateLbl.BackColor = System.Drawing.Color.Transparent;
            this.histHRightRateLbl.Font = new System.Drawing.Font("Roboto", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.histHRightRateLbl.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(167)))), ((int)(((byte)(0)))));
            this.histHRightRateLbl.Location = new System.Drawing.Point(200, 247);
            this.histHRightRateLbl.Name = "histHRightRateLbl";
            this.histHRightRateLbl.Size = new System.Drawing.Size(73, 23);
            this.histHRightRateLbl.TabIndex = 4;
            this.histHRightRateLbl.Text = "+10%";
            this.histHRightRateLbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // histHRightLbl
            // 
            this.histHRightLbl.BackColor = System.Drawing.Color.Transparent;
            this.histHRightLbl.Font = new System.Drawing.Font("Roboto", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.histHRightLbl.ForeColor = System.Drawing.Color.Black;
            this.histHRightLbl.Location = new System.Drawing.Point(19, 9);
            this.histHRightLbl.Name = "histHRightLbl";
            this.histHRightLbl.Size = new System.Drawing.Size(207, 48);
            this.histHRightLbl.TabIndex = 2;
            this.histHRightLbl.Text = "Step Height Right";
            // 
            // panel35
            // 
            this.panel35.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.panel35.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel35.Controls.Add(this.histHLeftDataLbl);
            this.panel35.Controls.Add(this.stepHLchart);
            this.panel35.Controls.Add(this.histHLeftRateLbl);
            this.panel35.Controls.Add(this.histHLeftLbl);
            this.panel35.Location = new System.Drawing.Point(627, 321);
            this.panel35.Margin = new System.Windows.Forms.Padding(15);
            this.panel35.Name = "panel35";
            this.panel35.Size = new System.Drawing.Size(276, 276);
            this.panel35.TabIndex = 5;
            // 
            // histHLeftDataLbl
            // 
            this.histHLeftDataLbl.BackColor = System.Drawing.Color.Transparent;
            this.histHLeftDataLbl.Font = new System.Drawing.Font("Roboto", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.histHLeftDataLbl.ForeColor = System.Drawing.Color.Black;
            this.histHLeftDataLbl.Location = new System.Drawing.Point(19, 231);
            this.histHLeftDataLbl.Name = "histHLeftDataLbl";
            this.histHLeftDataLbl.Size = new System.Drawing.Size(127, 36);
            this.histHLeftDataLbl.TabIndex = 3;
            this.histHLeftDataLbl.Text = "7.6 m/sec";
            this.histHLeftDataLbl.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // stepHLchart
            // 
            this.stepHLchart.BackColor = System.Drawing.Color.Transparent;
            this.stepHLchart.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            chartArea6.AxisX.LabelAutoFitMaxFontSize = 5;
            chartArea6.AxisX.LabelAutoFitMinFontSize = 5;
            chartArea6.AxisX.LabelStyle.Enabled = false;
            chartArea6.AxisX.LineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.NotSet;
            chartArea6.AxisX.MajorGrid.Enabled = false;
            chartArea6.AxisX.MajorTickMark.Enabled = false;
            chartArea6.AxisX.TitleFont = new System.Drawing.Font("Microsoft Sans Serif", 4F);
            chartArea6.AxisY.Enabled = System.Windows.Forms.DataVisualization.Charting.AxisEnabled.False;
            chartArea6.AxisY.LineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.NotSet;
            chartArea6.AxisY.MajorGrid.Enabled = false;
            chartArea6.AxisY.MajorTickMark.Enabled = false;
            chartArea6.AxisY.TitleFont = new System.Drawing.Font("Microsoft Sans Serif", 4F);
            chartArea6.BackColor = System.Drawing.Color.Transparent;
            chartArea6.BorderWidth = 0;
            chartArea6.Name = "ChartArea1";
            chartArea6.Position.Auto = false;
            chartArea6.Position.Height = 100F;
            chartArea6.Position.Width = 100F;
            this.stepHLchart.ChartAreas.Add(chartArea6);
            this.stepHLchart.Location = new System.Drawing.Point(-1, 42);
            this.stepHLchart.Name = "stepHLchart";
            this.stepHLchart.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.Fire;
            series6.BorderWidth = 4;
            series6.ChartArea = "ChartArea1";
            series6.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series6.Color = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(201)))), ((int)(((byte)(31)))));
            series6.IsVisibleInLegend = false;
            series6.MarkerBorderColor = System.Drawing.Color.White;
            series6.Name = "Series1";
            series6.Points.Add(dataPoint21);
            series6.Points.Add(dataPoint22);
            series6.Points.Add(dataPoint23);
            series6.Points.Add(dataPoint24);
            series6.SmartLabelStyle.Enabled = false;
            this.stepHLchart.Series.Add(series6);
            this.stepHLchart.Size = new System.Drawing.Size(276, 180);
            this.stepHLchart.TabIndex = 8;
            // 
            // histHLeftRateLbl
            // 
            this.histHLeftRateLbl.BackColor = System.Drawing.Color.Transparent;
            this.histHLeftRateLbl.Font = new System.Drawing.Font("Roboto", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.histHLeftRateLbl.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(167)))), ((int)(((byte)(0)))));
            this.histHLeftRateLbl.Location = new System.Drawing.Point(200, 247);
            this.histHLeftRateLbl.Name = "histHLeftRateLbl";
            this.histHLeftRateLbl.Size = new System.Drawing.Size(73, 23);
            this.histHLeftRateLbl.TabIndex = 4;
            this.histHLeftRateLbl.Text = "+10%";
            this.histHLeftRateLbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // histHLeftLbl
            // 
            this.histHLeftLbl.BackColor = System.Drawing.Color.Transparent;
            this.histHLeftLbl.Font = new System.Drawing.Font("Roboto", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.histHLeftLbl.ForeColor = System.Drawing.Color.Black;
            this.histHLeftLbl.Location = new System.Drawing.Point(19, 9);
            this.histHLeftLbl.Name = "histHLeftLbl";
            this.histHLeftLbl.Size = new System.Drawing.Size(190, 48);
            this.histHLeftLbl.TabIndex = 2;
            this.histHLeftLbl.Text = "Step Height Left";
            // 
            // panel36
            // 
            this.panel36.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.panel36.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel36.Controls.Add(this.histMoveTimeDataLbl);
            this.panel36.Controls.Add(this.moveTimeChart);
            this.panel36.Controls.Add(this.histMoveTimeRateLbl);
            this.panel36.Controls.Add(this.histMoveTimeLbl);
            this.panel36.Location = new System.Drawing.Point(15, 627);
            this.panel36.Margin = new System.Windows.Forms.Padding(15);
            this.panel36.Name = "panel36";
            this.panel36.Size = new System.Drawing.Size(276, 276);
            this.panel36.TabIndex = 6;
            // 
            // histMoveTimeDataLbl
            // 
            this.histMoveTimeDataLbl.BackColor = System.Drawing.Color.Transparent;
            this.histMoveTimeDataLbl.Font = new System.Drawing.Font("Roboto", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.histMoveTimeDataLbl.ForeColor = System.Drawing.Color.Black;
            this.histMoveTimeDataLbl.Location = new System.Drawing.Point(19, 231);
            this.histMoveTimeDataLbl.Name = "histMoveTimeDataLbl";
            this.histMoveTimeDataLbl.Size = new System.Drawing.Size(127, 36);
            this.histMoveTimeDataLbl.TabIndex = 3;
            this.histMoveTimeDataLbl.Text = "7.6 m/sec";
            this.histMoveTimeDataLbl.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // moveTimeChart
            // 
            this.moveTimeChart.BackColor = System.Drawing.Color.Transparent;
            this.moveTimeChart.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            chartArea7.AxisX.LabelAutoFitMaxFontSize = 5;
            chartArea7.AxisX.LabelAutoFitMinFontSize = 5;
            chartArea7.AxisX.LabelStyle.Enabled = false;
            chartArea7.AxisX.LineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.NotSet;
            chartArea7.AxisX.MajorGrid.Enabled = false;
            chartArea7.AxisX.MajorTickMark.Enabled = false;
            chartArea7.AxisX.TitleFont = new System.Drawing.Font("Microsoft Sans Serif", 4F);
            chartArea7.AxisY.Enabled = System.Windows.Forms.DataVisualization.Charting.AxisEnabled.False;
            chartArea7.AxisY.LineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.NotSet;
            chartArea7.AxisY.MajorGrid.Enabled = false;
            chartArea7.AxisY.MajorTickMark.Enabled = false;
            chartArea7.AxisY.TitleFont = new System.Drawing.Font("Microsoft Sans Serif", 4F);
            chartArea7.BackColor = System.Drawing.Color.Transparent;
            chartArea7.BorderWidth = 0;
            chartArea7.Name = "ChartArea1";
            chartArea7.Position.Auto = false;
            chartArea7.Position.Height = 100F;
            chartArea7.Position.Width = 100F;
            this.moveTimeChart.ChartAreas.Add(chartArea7);
            this.moveTimeChart.Location = new System.Drawing.Point(-51, 37);
            this.moveTimeChart.Name = "moveTimeChart";
            this.moveTimeChart.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.Fire;
            series7.BorderWidth = 4;
            series7.ChartArea = "ChartArea1";
            series7.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series7.Color = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(201)))), ((int)(((byte)(31)))));
            series7.IsVisibleInLegend = false;
            series7.MarkerBorderColor = System.Drawing.Color.White;
            series7.Name = "Series1";
            series7.Points.Add(dataPoint25);
            series7.Points.Add(dataPoint26);
            series7.Points.Add(dataPoint27);
            series7.Points.Add(dataPoint28);
            series7.SmartLabelStyle.Enabled = false;
            this.moveTimeChart.Series.Add(series7);
            this.moveTimeChart.Size = new System.Drawing.Size(377, 180);
            this.moveTimeChart.TabIndex = 8;
            // 
            // histMoveTimeRateLbl
            // 
            this.histMoveTimeRateLbl.BackColor = System.Drawing.Color.Transparent;
            this.histMoveTimeRateLbl.Font = new System.Drawing.Font("Roboto", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.histMoveTimeRateLbl.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(167)))), ((int)(((byte)(0)))));
            this.histMoveTimeRateLbl.Location = new System.Drawing.Point(200, 247);
            this.histMoveTimeRateLbl.Name = "histMoveTimeRateLbl";
            this.histMoveTimeRateLbl.Size = new System.Drawing.Size(73, 23);
            this.histMoveTimeRateLbl.TabIndex = 4;
            this.histMoveTimeRateLbl.Text = "+10%";
            this.histMoveTimeRateLbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // histMoveTimeLbl
            // 
            this.histMoveTimeLbl.BackColor = System.Drawing.Color.Transparent;
            this.histMoveTimeLbl.Font = new System.Drawing.Font("Roboto", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.histMoveTimeLbl.ForeColor = System.Drawing.Color.Black;
            this.histMoveTimeLbl.Location = new System.Drawing.Point(19, 9);
            this.histMoveTimeLbl.Name = "histMoveTimeLbl";
            this.histMoveTimeLbl.Size = new System.Drawing.Size(206, 48);
            this.histMoveTimeLbl.TabIndex = 2;
            this.histMoveTimeLbl.Text = "Net. Moving time";
            // 
            // PlanNameTxt
            // 
            this.PlanNameTxt.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(201)))), ((int)(((byte)(31)))));
            this.PlanNameTxt.Font = new System.Drawing.Font("Roboto", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.PlanNameTxt.ForeColor = System.Drawing.Color.Black;
            this.PlanNameTxt.Location = new System.Drawing.Point(795, 114);
            this.PlanNameTxt.Name = "PlanNameTxt";
            this.PlanNameTxt.Size = new System.Drawing.Size(208, 28);
            this.PlanNameTxt.TabIndex = 6;
            this.PlanNameTxt.Text = "Cognitive + Cardio";
            this.PlanNameTxt.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.PlanNameTxt.Visible = false;
            // 
            // PlanDateTxt
            // 
            this.PlanDateTxt.BackColor = System.Drawing.Color.White;
            this.PlanDateTxt.Font = new System.Drawing.Font("Roboto", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.PlanDateTxt.ForeColor = System.Drawing.Color.Black;
            this.PlanDateTxt.Location = new System.Drawing.Point(539, 111);
            this.PlanDateTxt.Name = "PlanDateTxt";
            this.PlanDateTxt.Size = new System.Drawing.Size(250, 31);
            this.PlanDateTxt.TabIndex = 5;
            this.PlanDateTxt.Text = "Plan 24.05.19";
            this.PlanDateTxt.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel19
            // 
            this.panel19.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.panel19.Controls.Add(this.label13);
            this.panel19.Controls.Add(this.label14);
            this.panel19.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.panel19.Location = new System.Drawing.Point(150, 202);
            this.panel19.Name = "panel19";
            this.panel19.Size = new System.Drawing.Size(348, 35);
            this.panel19.TabIndex = 4;
            // 
            // label13
            // 
            this.label13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.label13.Font = new System.Drawing.Font("Roboto", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.label13.ForeColor = System.Drawing.Color.Black;
            this.label13.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label13.Location = new System.Drawing.Point(35, 1);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(149, 28);
            this.label13.TabIndex = 1;
            this.label13.Text = "Date";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label14
            // 
            this.label14.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.label14.Font = new System.Drawing.Font("Roboto", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.label14.ForeColor = System.Drawing.Color.Black;
            this.label14.Location = new System.Drawing.Point(182, 1);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(124, 28);
            this.label14.TabIndex = 2;
            this.label14.Text = "Exercises";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel17
            // 
            this.panel17.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(56)))), ((int)(((byte)(64)))));
            this.panel17.Controls.Add(this.historyGeneralReportsBtn);
            this.panel17.Controls.Add(this.historyPlansBtn);
            this.panel17.Location = new System.Drawing.Point(0, 764);
            this.panel17.Name = "panel17";
            this.panel17.Size = new System.Drawing.Size(150, 443);
            this.panel17.TabIndex = 2;
            // 
            // historyGeneralReportsBtn
            // 
            this.historyGeneralReportsBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.historyGeneralReportsBtn.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(56)))), ((int)(((byte)(64)))));
            this.historyGeneralReportsBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.historyGeneralReportsBtn.Font = new System.Drawing.Font("Roboto", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.historyGeneralReportsBtn.ForeColor = System.Drawing.Color.DimGray;
            this.historyGeneralReportsBtn.Image = global::SelfitMain.Properties.Resources.reportsBtn;
            this.historyGeneralReportsBtn.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.historyGeneralReportsBtn.Location = new System.Drawing.Point(0, 242);
            this.historyGeneralReportsBtn.Name = "historyGeneralReportsBtn";
            this.historyGeneralReportsBtn.Size = new System.Drawing.Size(150, 94);
            this.historyGeneralReportsBtn.TabIndex = 2;
            this.historyGeneralReportsBtn.Text = "Reports";
            this.historyGeneralReportsBtn.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.historyGeneralReportsBtn.UseVisualStyleBackColor = true;
            this.historyGeneralReportsBtn.Click += new System.EventHandler(this.historyGeneralReportsBtn_Click);
            // 
            // historyPlansBtn
            // 
            this.historyPlansBtn.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(56)))), ((int)(((byte)(64)))));
            this.historyPlansBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.historyPlansBtn.Font = new System.Drawing.Font("Roboto", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.historyPlansBtn.ForeColor = System.Drawing.Color.White;
            this.historyPlansBtn.Image = global::SelfitMain.Properties.Resources.Star;
            this.historyPlansBtn.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.historyPlansBtn.Location = new System.Drawing.Point(0, 53);
            this.historyPlansBtn.Name = "historyPlansBtn";
            this.historyPlansBtn.Size = new System.Drawing.Size(150, 138);
            this.historyPlansBtn.TabIndex = 1;
            this.historyPlansBtn.Text = "Saved Plans";
            this.historyPlansBtn.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.historyPlansBtn.UseVisualStyleBackColor = true;
            this.historyPlansBtn.Click += new System.EventHandler(this.button13_Click_1);
            // 
            // panel15
            // 
            this.panel15.BackColor = System.Drawing.Color.Black;
            this.panel15.Controls.Add(this.historyReportBtn);
            this.panel15.Controls.Add(this.historyExitBtn);
            this.panel15.Controls.Add(this.historyViewEditBtn);
            this.panel15.Controls.Add(this.historyPatientNameLbl);
            this.panel15.Location = new System.Drawing.Point(0, 0);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(1937, 96);
            this.panel15.TabIndex = 0;
            // 
            // historyReportBtn
            // 
            this.historyReportBtn.BackColor = System.Drawing.Color.Black;
            this.historyReportBtn.BackgroundImage = global::SelfitMain.Properties.Resources.Reports2;
            this.historyReportBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.historyReportBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.historyReportBtn.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.historyReportBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.historyReportBtn.Font = new System.Drawing.Font("Roboto", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.historyReportBtn.ForeColor = System.Drawing.Color.White;
            this.historyReportBtn.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.historyReportBtn.Location = new System.Drawing.Point(1650, 9);
            this.historyReportBtn.Name = "historyReportBtn";
            this.historyReportBtn.Size = new System.Drawing.Size(119, 79);
            this.historyReportBtn.TabIndex = 19;
            this.historyReportBtn.Text = "Reports";
            this.historyReportBtn.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.historyReportBtn.UseVisualStyleBackColor = false;
            this.historyReportBtn.Visible = false;
            this.historyReportBtn.Click += new System.EventHandler(this.historyReportBtn_Click);
            // 
            // historyExitBtn
            // 
            this.historyExitBtn.FlatAppearance.BorderSize = 0;
            this.historyExitBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.historyExitBtn.Image = global::SelfitMain.Properties.Resources.exitYellow;
            this.historyExitBtn.Location = new System.Drawing.Point(1815, 22);
            this.historyExitBtn.Name = "historyExitBtn";
            this.historyExitBtn.Size = new System.Drawing.Size(60, 60);
            this.historyExitBtn.TabIndex = 3;
            this.historyExitBtn.UseVisualStyleBackColor = true;
            this.historyExitBtn.Click += new System.EventHandler(this.historyExitBtn_Click);
            // 
            // historyViewEditBtn
            // 
            this.historyViewEditBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(201)))), ((int)(((byte)(31)))));
            this.historyViewEditBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.historyViewEditBtn.Font = new System.Drawing.Font("Roboto", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.historyViewEditBtn.ForeColor = System.Drawing.Color.Black;
            this.historyViewEditBtn.Location = new System.Drawing.Point(297, 29);
            this.historyViewEditBtn.Name = "historyViewEditBtn";
            this.historyViewEditBtn.Size = new System.Drawing.Size(170, 37);
            this.historyViewEditBtn.TabIndex = 2;
            this.historyViewEditBtn.Text = "View && Edit";
            this.historyViewEditBtn.UseVisualStyleBackColor = false;
            this.historyViewEditBtn.Click += new System.EventHandler(this.historyViewEditBtn_Click);
            // 
            // historyPatientNameLbl
            // 
            this.historyPatientNameLbl.Font = new System.Drawing.Font("Roboto", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.historyPatientNameLbl.ForeColor = System.Drawing.Color.White;
            this.historyPatientNameLbl.Location = new System.Drawing.Point(39, 29);
            this.historyPatientNameLbl.Name = "historyPatientNameLbl";
            this.historyPatientNameLbl.Size = new System.Drawing.Size(295, 34);
            this.historyPatientNameLbl.TabIndex = 1;
            this.historyPatientNameLbl.Text = "Mr. Jhon Adams";
            this.historyPatientNameLbl.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel16
            // 
            this.panel16.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(201)))), ((int)(((byte)(31)))));
            this.panel16.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.panel16.Controls.Add(this.label11);
            this.panel16.Location = new System.Drawing.Point(0, 541);
            this.panel16.Name = "panel16";
            this.panel16.Size = new System.Drawing.Size(149, 223);
            this.panel16.TabIndex = 1;
            // 
            // label11
            // 
            this.label11.Font = new System.Drawing.Font("Roboto", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Image = global::SelfitMain.Properties.Resources.Group;
            this.label11.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.label11.Location = new System.Drawing.Point(0, 57);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(149, 109);
            this.label11.TabIndex = 1;
            this.label11.Text = "Patient History";
            this.label11.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.label11.Click += new System.EventHandler(this.label11_Click);
            // 
            // panel18
            // 
            this.panel18.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.panel18.Controls.Add(this.noPlansAvailableTxt);
            this.panel18.Controls.Add(this.panel2);
            this.panel18.Controls.Add(this.dateTimePicker1);
            this.panel18.Controls.Add(this.label12);
            this.panel18.Controls.Add(this.SessionsFlowLayoutPanel);
            this.panel18.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.panel18.Location = new System.Drawing.Point(150, 90);
            this.panel18.Name = "panel18";
            this.panel18.Size = new System.Drawing.Size(392, 1110);
            this.panel18.TabIndex = 3;
            // 
            // noPlansAvailableTxt
            // 
            this.noPlansAvailableTxt.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.noPlansAvailableTxt.Font = new System.Drawing.Font("Roboto", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.noPlansAvailableTxt.ForeColor = System.Drawing.Color.Black;
            this.noPlansAvailableTxt.Location = new System.Drawing.Point(3, 425);
            this.noPlansAvailableTxt.Name = "noPlansAvailableTxt";
            this.noPlansAvailableTxt.Size = new System.Drawing.Size(325, 51);
            this.noPlansAvailableTxt.TabIndex = 7;
            this.noPlansAvailableTxt.Text = "No plans available";
            this.noPlansAvailableTxt.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.noPlansAvailableTxt.Visible = false;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.White;
            this.panel2.Location = new System.Drawing.Point(354, 2);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(200, 1078);
            this.panel2.TabIndex = 18;
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Checked = false;
            this.dateTimePicker1.CustomFormat = "MM/yyyy";
            this.dateTimePicker1.Font = new System.Drawing.Font("Roboto", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.dateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePicker1.Location = new System.Drawing.Point(35, 49);
            this.dateTimePicker1.MaxDate = new System.DateTime(2019, 12, 3, 0, 0, 0, 0);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(282, 44);
            this.dateTimePicker1.TabIndex = 2;
            this.dateTimePicker1.Value = new System.DateTime(2019, 11, 6, 0, 0, 0, 0);
            this.dateTimePicker1.ValueChanged += new System.EventHandler(this.dateTimePicker1_ValueChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Roboto", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(43, 23);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(52, 23);
            this.label12.TabIndex = 0;
            this.label12.Text = "Date";
            // 
            // SessionsFlowLayoutPanel
            // 
            this.SessionsFlowLayoutPanel.AutoScroll = true;
            this.SessionsFlowLayoutPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.SessionsFlowLayoutPanel.Controls.Add(this.panel21);
            this.SessionsFlowLayoutPanel.Controls.Add(this.panel20);
            this.SessionsFlowLayoutPanel.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.SessionsFlowLayoutPanel.Location = new System.Drawing.Point(0, 148);
            this.SessionsFlowLayoutPanel.Margin = new System.Windows.Forms.Padding(3, 1, 3, 1);
            this.SessionsFlowLayoutPanel.MaximumSize = new System.Drawing.Size(367, 829);
            this.SessionsFlowLayoutPanel.Name = "SessionsFlowLayoutPanel";
            this.SessionsFlowLayoutPanel.Size = new System.Drawing.Size(367, 807);
            this.SessionsFlowLayoutPanel.TabIndex = 3;
            this.SessionsFlowLayoutPanel.WrapContents = false;
            this.SessionsFlowLayoutPanel.MouseDown += new System.Windows.Forms.MouseEventHandler(this.SessionsFlowLayoutPanel_MouseDown);
            this.SessionsFlowLayoutPanel.MouseMove += new System.Windows.Forms.MouseEventHandler(this.SessionsFlowLayoutPanel_MouseMove);
            this.SessionsFlowLayoutPanel.MouseWheel += new System.Windows.Forms.MouseEventHandler(this.SessionsFlowLayoutPanel_MouseWheel);
            // 
            // panel21
            // 
            this.panel21.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.panel21.Controls.Add(this.label18);
            this.panel21.Controls.Add(this.label19);
            this.panel21.Controls.Add(this.label20);
            this.panel21.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.panel21.Location = new System.Drawing.Point(3, 1);
            this.panel21.Margin = new System.Windows.Forms.Padding(3, 1, 3, 0);
            this.panel21.Name = "panel21";
            this.panel21.Size = new System.Drawing.Size(357, 79);
            this.panel21.TabIndex = 1;
            // 
            // label18
            // 
            this.label18.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.label18.Font = new System.Drawing.Font("Roboto", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.label18.ForeColor = System.Drawing.Color.Black;
            this.label18.Location = new System.Drawing.Point(5, 45);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(208, 28);
            this.label18.TabIndex = 4;
            this.label18.Text = "Cognitive + Cardio";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label19
            // 
            this.label19.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.label19.Font = new System.Drawing.Font("Roboto", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.label19.ForeColor = System.Drawing.Color.Black;
            this.label19.Location = new System.Drawing.Point(219, 20);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(98, 28);
            this.label19.TabIndex = 3;
            this.label19.Text = "10";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label20
            // 
            this.label20.BackColor = System.Drawing.Color.Transparent;
            this.label20.Font = new System.Drawing.Font("Roboto", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.label20.ForeColor = System.Drawing.Color.Black;
            this.label20.Location = new System.Drawing.Point(5, 11);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(179, 28);
            this.label20.TabIndex = 2;
            this.label20.Text = "24.05.19";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel20
            // 
            this.panel20.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.panel20.Controls.Add(this.label17);
            this.panel20.Controls.Add(this.label16);
            this.panel20.Controls.Add(this.label15);
            this.panel20.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.panel20.Location = new System.Drawing.Point(3, 81);
            this.panel20.Margin = new System.Windows.Forms.Padding(3, 1, 3, 0);
            this.panel20.Name = "panel20";
            this.panel20.Size = new System.Drawing.Size(357, 79);
            this.panel20.TabIndex = 2;
            // 
            // label17
            // 
            this.label17.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.label17.Font = new System.Drawing.Font("Roboto", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.label17.ForeColor = System.Drawing.Color.Black;
            this.label17.Location = new System.Drawing.Point(5, 45);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(218, 28);
            this.label17.TabIndex = 4;
            this.label17.Text = "Cognitive + Cardio";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label16
            // 
            this.label16.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.label16.Font = new System.Drawing.Font("Roboto", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.label16.ForeColor = System.Drawing.Color.Black;
            this.label16.Location = new System.Drawing.Point(219, 20);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(98, 28);
            this.label16.TabIndex = 3;
            this.label16.Text = "10";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label15
            // 
            this.label15.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.label15.Font = new System.Drawing.Font("Roboto", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.label15.ForeColor = System.Drawing.Color.Black;
            this.label15.Location = new System.Drawing.Point(5, 11);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(179, 28);
            this.label15.TabIndex = 2;
            this.label15.Text = "21.05.19";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // createPlanBtn
            // 
            this.createPlanBtn.FlatAppearance.BorderSize = 0;
            this.createPlanBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.createPlanBtn.Font = new System.Drawing.Font("Roboto", 40F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.createPlanBtn.ForeColor = System.Drawing.Color.Black;
            this.createPlanBtn.Image = global::SelfitMain.Properties.Resources.Lg_ICN;
            this.createPlanBtn.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.createPlanBtn.Location = new System.Drawing.Point(969, 379);
            this.createPlanBtn.Name = "createPlanBtn";
            this.createPlanBtn.Size = new System.Drawing.Size(491, 481);
            this.createPlanBtn.TabIndex = 15;
            this.createPlanBtn.Text = "Create your first plan";
            this.createPlanBtn.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.createPlanBtn.UseVisualStyleBackColor = true;
            this.createPlanBtn.Click += new System.EventHandler(this.createPlanBtn_Click);
            // 
            // LoginPage
            // 
            this.LoginPage.BackColor = System.Drawing.Color.White;
            this.LoginPage.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.LoginPage.Controls.Add(this.panel8);
            this.LoginPage.Controls.Add(this.panel7);
            this.LoginPage.Location = new System.Drawing.Point(4, 23);
            this.LoginPage.Name = "LoginPage";
            this.LoginPage.Padding = new System.Windows.Forms.Padding(3);
            this.LoginPage.Size = new System.Drawing.Size(1910, 1172);
            this.LoginPage.TabIndex = 0;
            this.LoginPage.Text = "Login";
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.panel8.Controls.Add(this.pictureBox1);
            this.panel8.Location = new System.Drawing.Point(0, 0);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(960, 1200);
            this.panel8.TabIndex = 9;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = global::SelfitMain.Properties.Resources.selfitLogo_signIn;
            this.pictureBox1.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.InitialImage")));
            this.pictureBox1.Location = new System.Drawing.Point(200, 510);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(500, 150);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(201)))), ((int)(((byte)(31)))));
            this.panel7.Controls.Add(this.userNameTxtLbl);
            this.panel7.Controls.Add(this.passwordTxtLbl);
            this.panel7.Controls.Add(this.incorrectUserTxt);
            this.panel7.Controls.Add(this.UserPasswordTxtBox);
            this.panel7.Controls.Add(this.keyBoardBtn);
            this.panel7.Controls.Add(this.LoginExitBtn);
            this.panel7.Controls.Add(this.WelcomeBackLbl);
            this.panel7.Controls.Add(this.OkBtn);
            this.panel7.Controls.Add(this.UserNameTxtBox);
            this.panel7.Controls.Add(this.SignInLbl);
            this.panel7.Location = new System.Drawing.Point(960, 0);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(960, 1200);
            this.panel7.TabIndex = 8;
            // 
            // userNameTxtLbl
            // 
            this.userNameTxtLbl.BackColor = System.Drawing.Color.White;
            this.userNameTxtLbl.Font = new System.Drawing.Font("Roboto", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.userNameTxtLbl.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(64)))), ((int)(((byte)(71)))));
            this.userNameTxtLbl.Location = new System.Drawing.Point(212, 670);
            this.userNameTxtLbl.Name = "userNameTxtLbl";
            this.userNameTxtLbl.Size = new System.Drawing.Size(191, 41);
            this.userNameTxtLbl.TabIndex = 25;
            this.userNameTxtLbl.Text = "User name";
            this.userNameTxtLbl.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.userNameTxtLbl.Click += new System.EventHandler(this.userNameTxtLbl_Click);
            // 
            // passwordTxtLbl
            // 
            this.passwordTxtLbl.BackColor = System.Drawing.Color.White;
            this.passwordTxtLbl.Font = new System.Drawing.Font("Roboto", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.passwordTxtLbl.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(64)))), ((int)(((byte)(71)))));
            this.passwordTxtLbl.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.passwordTxtLbl.Location = new System.Drawing.Point(216, 751);
            this.passwordTxtLbl.Name = "passwordTxtLbl";
            this.passwordTxtLbl.Size = new System.Drawing.Size(191, 41);
            this.passwordTxtLbl.TabIndex = 24;
            this.passwordTxtLbl.Text = "Password";
            this.passwordTxtLbl.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.passwordTxtLbl.Click += new System.EventHandler(this.passwordTxtLbl_Click);
            // 
            // incorrectUserTxt
            // 
            this.incorrectUserTxt.BackColor = System.Drawing.Color.Transparent;
            this.incorrectUserTxt.Font = new System.Drawing.Font("Roboto", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.incorrectUserTxt.ForeColor = System.Drawing.Color.Red;
            this.incorrectUserTxt.Location = new System.Drawing.Point(198, 813);
            this.incorrectUserTxt.Name = "incorrectUserTxt";
            this.incorrectUserTxt.Size = new System.Drawing.Size(592, 25);
            this.incorrectUserTxt.TabIndex = 23;
            this.incorrectUserTxt.Text = "Incorrect password or user not exists!";
            this.incorrectUserTxt.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.incorrectUserTxt.Visible = false;
            // 
            // keyBoardBtn
            // 
            this.keyBoardBtn.BackgroundImage = global::SelfitMain.Properties.Resources._62829_keyboard_icon;
            this.keyBoardBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.keyBoardBtn.FlatAppearance.BorderSize = 0;
            this.keyBoardBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.keyBoardBtn.Location = new System.Drawing.Point(813, 650);
            this.keyBoardBtn.Name = "keyBoardBtn";
            this.keyBoardBtn.Size = new System.Drawing.Size(116, 73);
            this.keyBoardBtn.TabIndex = 22;
            this.keyBoardBtn.UseVisualStyleBackColor = true;
            this.keyBoardBtn.Visible = false;
            this.keyBoardBtn.Click += new System.EventHandler(this.keyBoardBtn_Click);
            // 
            // LoginExitBtn
            // 
            this.LoginExitBtn.FlatAppearance.BorderSize = 0;
            this.LoginExitBtn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Gold;
            this.LoginExitBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.LoginExitBtn.Image = global::SelfitMain.Properties.Resources.exitYellow;
            this.LoginExitBtn.Location = new System.Drawing.Point(869, 16);
            this.LoginExitBtn.Name = "LoginExitBtn";
            this.LoginExitBtn.Size = new System.Drawing.Size(60, 60);
            this.LoginExitBtn.TabIndex = 21;
            this.LoginExitBtn.UseVisualStyleBackColor = true;
            this.LoginExitBtn.Click += new System.EventHandler(this.LoginExitBtn_Click);
            // 
            // WelcomeBackLbl
            // 
            this.WelcomeBackLbl.AutoSize = true;
            this.WelcomeBackLbl.Font = new System.Drawing.Font("Roboto", 80F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.WelcomeBackLbl.Location = new System.Drawing.Point(185, 239);
            this.WelcomeBackLbl.Name = "WelcomeBackLbl";
            this.WelcomeBackLbl.Size = new System.Drawing.Size(591, 96);
            this.WelcomeBackLbl.TabIndex = 20;
            this.WelcomeBackLbl.Text = "Welcome Back,";
            this.WelcomeBackLbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // OkBtn
            // 
            this.OkBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.OkBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(56)))), ((int)(((byte)(64)))));
            this.OkBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.OkBtn.FlatAppearance.BorderSize = 0;
            this.OkBtn.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Gray;
            this.OkBtn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DimGray;
            this.OkBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.OkBtn.Font = new System.Drawing.Font("Roboto", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.OkBtn.ForeColor = System.Drawing.Color.White;
            this.OkBtn.Location = new System.Drawing.Point(198, 852);
            this.OkBtn.Name = "OkBtn";
            this.OkBtn.Size = new System.Drawing.Size(592, 69);
            this.OkBtn.TabIndex = 2;
            this.OkBtn.Text = "Sign In";
            this.OkBtn.UseMnemonic = false;
            this.OkBtn.UseVisualStyleBackColor = false;
            this.OkBtn.Click += new System.EventHandler(this.OkBtn_Click);
            // 
            // UserNameTxtBox
            // 
            this.UserNameTxtBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.UserNameTxtBox.Font = new System.Drawing.Font("Roboto", 42F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.UserNameTxtBox.Location = new System.Drawing.Point(198, 661);
            this.UserNameTxtBox.Name = "UserNameTxtBox";
            this.UserNameTxtBox.Size = new System.Drawing.Size(592, 58);
            this.UserNameTxtBox.TabIndex = 0;
            this.UserNameTxtBox.TextChanged += new System.EventHandler(this.UserNameTxtBox_TextChanged);
            this.UserNameTxtBox.KeyUp += new System.Windows.Forms.KeyEventHandler(this.UserNameTxtBox_KeyUp_1);
            this.UserNameTxtBox.MouseUp += new System.Windows.Forms.MouseEventHandler(this.UserNameTxtBox_MouseUp);
            // 
            // SignInLbl
            // 
            this.SignInLbl.BackColor = System.Drawing.Color.Transparent;
            this.SignInLbl.Font = new System.Drawing.Font("Roboto", 80F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.SignInLbl.ForeColor = System.Drawing.Color.Black;
            this.SignInLbl.Location = new System.Drawing.Point(185, 531);
            this.SignInLbl.Name = "SignInLbl";
            this.SignInLbl.Size = new System.Drawing.Size(592, 93);
            this.SignInLbl.TabIndex = 18;
            this.SignInLbl.Text = "Sign In";
            // 
            // PlanView
            // 
            this.PlanView.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.PlanView.Controls.Add(this.panel61);
            this.PlanView.Controls.Add(this.panel62);
            this.PlanView.Controls.Add(this.panel60);
            this.PlanView.Controls.Add(this.label55);
            this.PlanView.Controls.Add(this.label54);
            this.PlanView.Controls.Add(this.label52);
            this.PlanView.Controls.Add(this.label51);
            this.PlanView.Controls.Add(this.panel65);
            this.PlanView.Controls.Add(this.panel64);
            this.PlanView.Controls.Add(this.panel59);
            this.PlanView.Controls.Add(this.planViewLayoutPanel);
            this.PlanView.Location = new System.Drawing.Point(4, 23);
            this.PlanView.Name = "PlanView";
            this.PlanView.Padding = new System.Windows.Forms.Padding(3);
            this.PlanView.Size = new System.Drawing.Size(1910, 1172);
            this.PlanView.TabIndex = 9;
            this.PlanView.Text = "PlanView";
            // 
            // panel61
            // 
            this.panel61.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(56)))), ((int)(((byte)(64)))));
            this.panel61.Controls.Add(this.pViewReportsBtn);
            this.panel61.Location = new System.Drawing.Point(0, 989);
            this.panel61.Name = "panel61";
            this.panel61.Size = new System.Drawing.Size(150, 222);
            this.panel61.TabIndex = 16;
            // 
            // pViewReportsBtn
            // 
            this.pViewReportsBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pViewReportsBtn.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(56)))), ((int)(((byte)(64)))));
            this.pViewReportsBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.pViewReportsBtn.Font = new System.Drawing.Font("Roboto", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.pViewReportsBtn.ForeColor = System.Drawing.Color.White;
            this.pViewReportsBtn.Image = global::SelfitMain.Properties.Resources.reportsBtn;
            this.pViewReportsBtn.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.pViewReportsBtn.Location = new System.Drawing.Point(0, 48);
            this.pViewReportsBtn.Name = "pViewReportsBtn";
            this.pViewReportsBtn.Size = new System.Drawing.Size(150, 94);
            this.pViewReportsBtn.TabIndex = 3;
            this.pViewReportsBtn.Text = "Reports";
            this.pViewReportsBtn.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.pViewReportsBtn.UseVisualStyleBackColor = true;
            this.pViewReportsBtn.Click += new System.EventHandler(this.pViewReportsBtn_Click);
            // 
            // panel62
            // 
            this.panel62.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(201)))), ((int)(((byte)(31)))));
            this.panel62.Controls.Add(this.button26);
            this.panel62.Location = new System.Drawing.Point(0, 767);
            this.panel62.Name = "panel62";
            this.panel62.Size = new System.Drawing.Size(150, 222);
            this.panel62.TabIndex = 6;
            // 
            // button26
            // 
            this.button26.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(201)))), ((int)(((byte)(31)))));
            this.button26.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button26.Font = new System.Drawing.Font("Roboto", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.button26.ForeColor = System.Drawing.Color.Black;
            this.button26.Image = global::SelfitMain.Properties.Resources.StarBlack;
            this.button26.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.button26.Location = new System.Drawing.Point(0, 58);
            this.button26.Name = "button26";
            this.button26.Size = new System.Drawing.Size(150, 138);
            this.button26.TabIndex = 1;
            this.button26.Text = "Saved Plans";
            this.button26.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.button26.UseVisualStyleBackColor = true;
            // 
            // panel60
            // 
            this.panel60.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(56)))), ((int)(((byte)(64)))));
            this.panel60.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.panel60.Controls.Add(this.pHistPViewLblBtn);
            this.panel60.Controls.Add(this.pViewQplanBtn);
            this.panel60.Controls.Add(this.loadPlansPviewBtn);
            this.panel60.Controls.Add(this.panel63);
            this.panel60.Location = new System.Drawing.Point(0, 97);
            this.panel60.Name = "panel60";
            this.panel60.Size = new System.Drawing.Size(149, 670);
            this.panel60.TabIndex = 4;
            // 
            // pHistPViewLblBtn
            // 
            this.pHistPViewLblBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(56)))), ((int)(((byte)(64)))));
            this.pHistPViewLblBtn.Font = new System.Drawing.Font("Roboto", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.pHistPViewLblBtn.ForeColor = System.Drawing.Color.White;
            this.pHistPViewLblBtn.Image = global::SelfitMain.Properties.Resources.Group1;
            this.pHistPViewLblBtn.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.pHistPViewLblBtn.Location = new System.Drawing.Point(3, 470);
            this.pHistPViewLblBtn.Name = "pHistPViewLblBtn";
            this.pHistPViewLblBtn.Size = new System.Drawing.Size(136, 110);
            this.pHistPViewLblBtn.TabIndex = 9;
            this.pHistPViewLblBtn.Text = "Patient History";
            this.pHistPViewLblBtn.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.pHistPViewLblBtn.Click += new System.EventHandler(this.pHistPViewLblBtn_Click_1);
            // 
            // pViewQplanBtn
            // 
            this.pViewQplanBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pViewQplanBtn.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(56)))), ((int)(((byte)(64)))));
            this.pViewQplanBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.pViewQplanBtn.Font = new System.Drawing.Font("Roboto", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.pViewQplanBtn.ForeColor = System.Drawing.Color.White;
            this.pViewQplanBtn.Image = global::SelfitMain.Properties.Resources.quickplan_bright;
            this.pViewQplanBtn.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.pViewQplanBtn.Location = new System.Drawing.Point(11, 46);
            this.pViewQplanBtn.Name = "pViewQplanBtn";
            this.pViewQplanBtn.Size = new System.Drawing.Size(128, 127);
            this.pViewQplanBtn.TabIndex = 8;
            this.pViewQplanBtn.Text = "Quick Plan";
            this.pViewQplanBtn.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.pViewQplanBtn.UseVisualStyleBackColor = true;
            this.pViewQplanBtn.Click += new System.EventHandler(this.pViewQplanBtn_Click);
            // 
            // loadPlansPviewBtn
            // 
            this.loadPlansPviewBtn.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(56)))), ((int)(((byte)(64)))));
            this.loadPlansPviewBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.loadPlansPviewBtn.Font = new System.Drawing.Font("Roboto", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.loadPlansPviewBtn.ForeColor = System.Drawing.Color.White;
            this.loadPlansPviewBtn.Image = global::SelfitMain.Properties.Resources.BigPlusWhite;
            this.loadPlansPviewBtn.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.loadPlansPviewBtn.Location = new System.Drawing.Point(0, 268);
            this.loadPlansPviewBtn.Name = "loadPlansPviewBtn";
            this.loadPlansPviewBtn.Size = new System.Drawing.Size(146, 114);
            this.loadPlansPviewBtn.TabIndex = 0;
            this.loadPlansPviewBtn.Text = "Exercises";
            this.loadPlansPviewBtn.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.loadPlansPviewBtn.UseVisualStyleBackColor = true;
            this.loadPlansPviewBtn.Click += new System.EventHandler(this.loadPlansPviewBtn_Click);
            // 
            // panel63
            // 
            this.panel63.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.panel63.Location = new System.Drawing.Point(151, 16);
            this.panel63.Name = "panel63";
            this.panel63.Size = new System.Drawing.Size(885, 1110);
            this.panel63.TabIndex = 7;
            // 
            // label55
            // 
            this.label55.BackColor = System.Drawing.Color.Transparent;
            this.label55.Font = new System.Drawing.Font("Roboto", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.label55.ForeColor = System.Drawing.Color.Black;
            this.label55.Location = new System.Drawing.Point(791, 204);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(75, 26);
            this.label55.TabIndex = 14;
            this.label55.Text = "Time";
            this.label55.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label54
            // 
            this.label54.BackColor = System.Drawing.Color.Transparent;
            this.label54.Font = new System.Drawing.Font("Roboto", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.label54.ForeColor = System.Drawing.Color.Black;
            this.label54.Location = new System.Drawing.Point(598, 201);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(100, 26);
            this.label54.TabIndex = 15;
            this.label54.Text = "Exercises";
            this.label54.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label52
            // 
            this.label52.BackColor = System.Drawing.Color.Transparent;
            this.label52.Font = new System.Drawing.Font("Roboto", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.label52.ForeColor = System.Drawing.Color.Black;
            this.label52.Location = new System.Drawing.Point(301, 204);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(84, 26);
            this.label52.TabIndex = 14;
            this.label52.Text = "Name";
            this.label52.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label51
            // 
            this.label51.BackColor = System.Drawing.Color.Transparent;
            this.label51.Font = new System.Drawing.Font("Roboto", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.label51.ForeColor = System.Drawing.Color.Black;
            this.label51.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label51.Location = new System.Drawing.Point(180, 204);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(52, 26);
            this.label51.TabIndex = 13;
            this.label51.Text = "No.";
            this.label51.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel65
            // 
            this.panel65.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panel65.Controls.Add(this.EXviewGroupBox);
            this.panel65.Controls.Add(this.planViewCreatePlanBtn);
            this.panel65.Location = new System.Drawing.Point(1036, 96);
            this.panel65.Name = "panel65";
            this.panel65.Size = new System.Drawing.Size(874, 1077);
            this.panel65.TabIndex = 10;
            // 
            // EXviewGroupBox
            // 
            this.EXviewGroupBox.Controls.Add(this.label112);
            this.EXviewGroupBox.Controls.Add(this.label111);
            this.EXviewGroupBox.Controls.Add(this.viewPracticeNameLbl);
            this.EXviewGroupBox.Controls.Add(this.label93);
            this.EXviewGroupBox.Controls.Add(this.label128);
            this.EXviewGroupBox.Controls.Add(this.label129);
            this.EXviewGroupBox.Controls.Add(this.planViewStartPlanBtn);
            this.EXviewGroupBox.Controls.Add(this.planViewSelectEditBtn);
            this.EXviewGroupBox.Controls.Add(this.planViewTotalexeLbl);
            this.EXviewGroupBox.Controls.Add(this.PlanDetailsFlowPanel);
            this.EXviewGroupBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.EXviewGroupBox.Location = new System.Drawing.Point(6, 0);
            this.EXviewGroupBox.Name = "EXviewGroupBox";
            this.EXviewGroupBox.Size = new System.Drawing.Size(862, 981);
            this.EXviewGroupBox.TabIndex = 26;
            this.EXviewGroupBox.TabStop = false;
            // 
            // label112
            // 
            this.label112.Location = new System.Drawing.Point(683, 123);
            this.label112.Name = "label112";
            this.label112.Size = new System.Drawing.Size(19, 620);
            this.label112.TabIndex = 36;
            // 
            // label111
            // 
            this.label111.Location = new System.Drawing.Point(166, 117);
            this.label111.Name = "label111";
            this.label111.Size = new System.Drawing.Size(19, 620);
            this.label111.TabIndex = 35;
            // 
            // viewPracticeNameLbl
            // 
            this.viewPracticeNameLbl.BackColor = System.Drawing.Color.Transparent;
            this.viewPracticeNameLbl.Font = new System.Drawing.Font("Roboto", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.viewPracticeNameLbl.ForeColor = System.Drawing.Color.Black;
            this.viewPracticeNameLbl.Location = new System.Drawing.Point(197, 24);
            this.viewPracticeNameLbl.Name = "viewPracticeNameLbl";
            this.viewPracticeNameLbl.Size = new System.Drawing.Size(389, 41);
            this.viewPracticeNameLbl.TabIndex = 34;
            this.viewPracticeNameLbl.Text = "No.";
            this.viewPracticeNameLbl.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label93
            // 
            this.label93.BackColor = System.Drawing.Color.Transparent;
            this.label93.Font = new System.Drawing.Font("Roboto", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.label93.ForeColor = System.Drawing.Color.Black;
            this.label93.Location = new System.Drawing.Point(557, 87);
            this.label93.Name = "label93";
            this.label93.Size = new System.Drawing.Size(86, 28);
            this.label93.TabIndex = 33;
            this.label93.Text = "Reps.";
            this.label93.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label128
            // 
            this.label128.BackColor = System.Drawing.Color.Transparent;
            this.label128.Font = new System.Drawing.Font("Roboto", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.label128.ForeColor = System.Drawing.Color.Black;
            this.label128.Location = new System.Drawing.Point(369, 87);
            this.label128.Name = "label128";
            this.label128.Size = new System.Drawing.Size(86, 28);
            this.label128.TabIndex = 32;
            this.label128.Text = "Exercises";
            this.label128.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label129
            // 
            this.label129.BackColor = System.Drawing.Color.Transparent;
            this.label129.Font = new System.Drawing.Font("Roboto", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.label129.ForeColor = System.Drawing.Color.Black;
            this.label129.Location = new System.Drawing.Point(195, 87);
            this.label129.Name = "label129";
            this.label129.Size = new System.Drawing.Size(52, 28);
            this.label129.TabIndex = 31;
            this.label129.Text = "No.";
            this.label129.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // planViewStartPlanBtn
            // 
            this.planViewStartPlanBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(201)))), ((int)(((byte)(31)))));
            this.planViewStartPlanBtn.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(201)))), ((int)(((byte)(31)))));
            this.planViewStartPlanBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.planViewStartPlanBtn.Font = new System.Drawing.Font("Roboto", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.planViewStartPlanBtn.ForeColor = System.Drawing.Color.Black;
            this.planViewStartPlanBtn.Location = new System.Drawing.Point(254, 920);
            this.planViewStartPlanBtn.Name = "planViewStartPlanBtn";
            this.planViewStartPlanBtn.Size = new System.Drawing.Size(360, 60);
            this.planViewStartPlanBtn.TabIndex = 29;
            this.planViewStartPlanBtn.Text = "Start Plan";
            this.planViewStartPlanBtn.UseVisualStyleBackColor = false;
            this.planViewStartPlanBtn.Click += new System.EventHandler(this.planViewStartPlanBtn_Click);
            // 
            // planViewSelectEditBtn
            // 
            this.planViewSelectEditBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(56)))), ((int)(((byte)(64)))));
            this.planViewSelectEditBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.planViewSelectEditBtn.Font = new System.Drawing.Font("Roboto", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.planViewSelectEditBtn.ForeColor = System.Drawing.Color.White;
            this.planViewSelectEditBtn.Location = new System.Drawing.Point(254, 829);
            this.planViewSelectEditBtn.Name = "planViewSelectEditBtn";
            this.planViewSelectEditBtn.Size = new System.Drawing.Size(360, 60);
            this.planViewSelectEditBtn.TabIndex = 28;
            this.planViewSelectEditBtn.Text = "Select && Edit";
            this.planViewSelectEditBtn.UseVisualStyleBackColor = false;
            this.planViewSelectEditBtn.Click += new System.EventHandler(this.planViewSelectEditBtn_Click);
            // 
            // planViewTotalexeLbl
            // 
            this.planViewTotalexeLbl.BackColor = System.Drawing.Color.WhiteSmoke;
            this.planViewTotalexeLbl.Font = new System.Drawing.Font("Roboto", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.planViewTotalexeLbl.ForeColor = System.Drawing.Color.Black;
            this.planViewTotalexeLbl.Location = new System.Drawing.Point(253, 764);
            this.planViewTotalexeLbl.Name = "planViewTotalexeLbl";
            this.planViewTotalexeLbl.Size = new System.Drawing.Size(362, 33);
            this.planViewTotalexeLbl.TabIndex = 27;
            this.planViewTotalexeLbl.Text = "10 Exercises";
            this.planViewTotalexeLbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // PlanDetailsFlowPanel
            // 
            this.PlanDetailsFlowPanel.AutoScroll = true;
            this.PlanDetailsFlowPanel.BackColor = System.Drawing.Color.WhiteSmoke;
            this.PlanDetailsFlowPanel.Controls.Add(this.panel68);
            this.PlanDetailsFlowPanel.Controls.Add(this.panel69);
            this.PlanDetailsFlowPanel.Controls.Add(this.panel70);
            this.PlanDetailsFlowPanel.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.PlanDetailsFlowPanel.Location = new System.Drawing.Point(184, 132);
            this.PlanDetailsFlowPanel.Margin = new System.Windows.Forms.Padding(3, 1, 3, 1);
            this.PlanDetailsFlowPanel.Name = "PlanDetailsFlowPanel";
            this.PlanDetailsFlowPanel.Size = new System.Drawing.Size(500, 607);
            this.PlanDetailsFlowPanel.TabIndex = 26;
            this.PlanDetailsFlowPanel.WrapContents = false;
            // 
            // panel68
            // 
            this.panel68.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panel68.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel68.Controls.Add(this.label94);
            this.panel68.Controls.Add(this.label96);
            this.panel68.Controls.Add(this.pictureBox9);
            this.panel68.Controls.Add(this.label98);
            this.panel68.Location = new System.Drawing.Point(0, 0);
            this.panel68.Margin = new System.Windows.Forms.Padding(0);
            this.panel68.Name = "panel68";
            this.panel68.Size = new System.Drawing.Size(500, 74);
            this.panel68.TabIndex = 0;
            // 
            // label94
            // 
            this.label94.BackColor = System.Drawing.Color.Transparent;
            this.label94.Font = new System.Drawing.Font("Roboto", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.label94.ForeColor = System.Drawing.Color.Black;
            this.label94.Location = new System.Drawing.Point(381, 23);
            this.label94.Name = "label94";
            this.label94.Size = new System.Drawing.Size(72, 29);
            this.label94.TabIndex = 13;
            this.label94.Text = "04/07";
            this.label94.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label96
            // 
            this.label96.BackColor = System.Drawing.Color.Transparent;
            this.label96.Font = new System.Drawing.Font("Roboto", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.label96.ForeColor = System.Drawing.Color.Black;
            this.label96.Location = new System.Drawing.Point(131, 23);
            this.label96.Name = "label96";
            this.label96.Size = new System.Drawing.Size(207, 29);
            this.label96.TabIndex = 12;
            this.label96.Text = "Walk the square";
            this.label96.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pictureBox9
            // 
            this.pictureBox9.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pictureBox9.Image = global::SelfitMain.Properties.Resources.wtsImage;
            this.pictureBox9.Location = new System.Drawing.Point(65, 21);
            this.pictureBox9.Name = "pictureBox9";
            this.pictureBox9.Size = new System.Drawing.Size(33, 33);
            this.pictureBox9.TabIndex = 11;
            this.pictureBox9.TabStop = false;
            // 
            // label98
            // 
            this.label98.BackColor = System.Drawing.Color.Transparent;
            this.label98.Font = new System.Drawing.Font("Roboto", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.label98.ForeColor = System.Drawing.Color.Black;
            this.label98.Location = new System.Drawing.Point(8, 23);
            this.label98.Name = "label98";
            this.label98.Size = new System.Drawing.Size(40, 29);
            this.label98.TabIndex = 10;
            this.label98.Text = "01";
            this.label98.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel69
            // 
            this.panel69.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panel69.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel69.Controls.Add(this.label100);
            this.panel69.Controls.Add(this.label109);
            this.panel69.Controls.Add(this.pictureBox12);
            this.panel69.Controls.Add(this.label110);
            this.panel69.Location = new System.Drawing.Point(0, 74);
            this.panel69.Margin = new System.Windows.Forms.Padding(0);
            this.panel69.Name = "panel69";
            this.panel69.Size = new System.Drawing.Size(500, 74);
            this.panel69.TabIndex = 1;
            // 
            // label100
            // 
            this.label100.BackColor = System.Drawing.Color.Transparent;
            this.label100.Font = new System.Drawing.Font("Roboto", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.label100.ForeColor = System.Drawing.Color.Black;
            this.label100.Location = new System.Drawing.Point(381, 23);
            this.label100.Name = "label100";
            this.label100.Size = new System.Drawing.Size(72, 29);
            this.label100.TabIndex = 13;
            this.label100.Text = "04/07";
            this.label100.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label109
            // 
            this.label109.BackColor = System.Drawing.Color.Transparent;
            this.label109.Font = new System.Drawing.Font("Roboto", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.label109.ForeColor = System.Drawing.Color.Black;
            this.label109.Location = new System.Drawing.Point(131, 23);
            this.label109.Name = "label109";
            this.label109.Size = new System.Drawing.Size(207, 29);
            this.label109.TabIndex = 12;
            this.label109.Text = "Walk the square";
            this.label109.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pictureBox12
            // 
            this.pictureBox12.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pictureBox12.Image = global::SelfitMain.Properties.Resources.wtsImage;
            this.pictureBox12.Location = new System.Drawing.Point(65, 21);
            this.pictureBox12.Name = "pictureBox12";
            this.pictureBox12.Size = new System.Drawing.Size(33, 33);
            this.pictureBox12.TabIndex = 11;
            this.pictureBox12.TabStop = false;
            // 
            // label110
            // 
            this.label110.BackColor = System.Drawing.Color.Transparent;
            this.label110.Font = new System.Drawing.Font("Roboto", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.label110.ForeColor = System.Drawing.Color.Black;
            this.label110.Location = new System.Drawing.Point(8, 23);
            this.label110.Name = "label110";
            this.label110.Size = new System.Drawing.Size(40, 29);
            this.label110.TabIndex = 10;
            this.label110.Text = "01";
            this.label110.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel70
            // 
            this.panel70.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panel70.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel70.Controls.Add(this.label113);
            this.panel70.Controls.Add(this.label114);
            this.panel70.Controls.Add(this.pictureBox13);
            this.panel70.Controls.Add(this.label115);
            this.panel70.Location = new System.Drawing.Point(0, 148);
            this.panel70.Margin = new System.Windows.Forms.Padding(0);
            this.panel70.Name = "panel70";
            this.panel70.Size = new System.Drawing.Size(500, 74);
            this.panel70.TabIndex = 2;
            // 
            // label113
            // 
            this.label113.BackColor = System.Drawing.Color.Transparent;
            this.label113.Font = new System.Drawing.Font("Roboto", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.label113.ForeColor = System.Drawing.Color.Black;
            this.label113.Location = new System.Drawing.Point(381, 23);
            this.label113.Name = "label113";
            this.label113.Size = new System.Drawing.Size(72, 29);
            this.label113.TabIndex = 13;
            this.label113.Text = "04/07";
            this.label113.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label114
            // 
            this.label114.BackColor = System.Drawing.Color.Transparent;
            this.label114.Font = new System.Drawing.Font("Roboto", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.label114.ForeColor = System.Drawing.Color.Black;
            this.label114.Location = new System.Drawing.Point(131, 23);
            this.label114.Name = "label114";
            this.label114.Size = new System.Drawing.Size(207, 29);
            this.label114.TabIndex = 12;
            this.label114.Text = "Walk the square";
            this.label114.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pictureBox13
            // 
            this.pictureBox13.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pictureBox13.Image = global::SelfitMain.Properties.Resources.wtsImage;
            this.pictureBox13.Location = new System.Drawing.Point(65, 21);
            this.pictureBox13.Name = "pictureBox13";
            this.pictureBox13.Size = new System.Drawing.Size(33, 33);
            this.pictureBox13.TabIndex = 11;
            this.pictureBox13.TabStop = false;
            // 
            // label115
            // 
            this.label115.BackColor = System.Drawing.Color.Transparent;
            this.label115.Font = new System.Drawing.Font("Roboto", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.label115.ForeColor = System.Drawing.Color.Black;
            this.label115.Location = new System.Drawing.Point(8, 23);
            this.label115.Name = "label115";
            this.label115.Size = new System.Drawing.Size(40, 29);
            this.label115.TabIndex = 10;
            this.label115.Text = "01";
            this.label115.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // planViewCreatePlanBtn
            // 
            this.planViewCreatePlanBtn.FlatAppearance.BorderSize = 0;
            this.planViewCreatePlanBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.planViewCreatePlanBtn.Font = new System.Drawing.Font("Roboto", 40F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.planViewCreatePlanBtn.ForeColor = System.Drawing.Color.Black;
            this.planViewCreatePlanBtn.Image = global::SelfitMain.Properties.Resources.Lg_ICN;
            this.planViewCreatePlanBtn.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.planViewCreatePlanBtn.Location = new System.Drawing.Point(180, 180);
            this.planViewCreatePlanBtn.Name = "planViewCreatePlanBtn";
            this.planViewCreatePlanBtn.Size = new System.Drawing.Size(491, 481);
            this.planViewCreatePlanBtn.TabIndex = 24;
            this.planViewCreatePlanBtn.Text = "Create a plan";
            this.planViewCreatePlanBtn.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.planViewCreatePlanBtn.UseVisualStyleBackColor = true;
            this.planViewCreatePlanBtn.Visible = false;
            this.planViewCreatePlanBtn.Click += new System.EventHandler(this.planViewCreatePlanBtn_Click);
            // 
            // panel64
            // 
            this.panel64.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.panel64.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.panel64.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel64.Controls.Add(this.label47);
            this.panel64.Controls.Add(this.searchNameTxt);
            this.panel64.Controls.Add(this.button11);
            this.panel64.Location = new System.Drawing.Point(170, 129);
            this.panel64.Name = "panel64";
            this.panel64.Size = new System.Drawing.Size(849, 50);
            this.panel64.TabIndex = 7;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Font = new System.Drawing.Font("Roboto", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.label47.ForeColor = System.Drawing.Color.DimGray;
            this.label47.Location = new System.Drawing.Point(20, 10);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(158, 27);
            this.label47.TabIndex = 3;
            this.label47.Text = "Search Name";
            // 
            // searchNameTxt
            // 
            this.searchNameTxt.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.searchNameTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.searchNameTxt.Font = new System.Drawing.Font("Roboto", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.searchNameTxt.Location = new System.Drawing.Point(184, 12);
            this.searchNameTxt.Name = "searchNameTxt";
            this.searchNameTxt.Size = new System.Drawing.Size(583, 25);
            this.searchNameTxt.TabIndex = 2;
            this.searchNameTxt.Text = "?";
            this.searchNameTxt.TextChanged += new System.EventHandler(this.searchNameTxt_TextChanged);
            // 
            // button11
            // 
            this.button11.BackgroundImage = global::SelfitMain.Properties.Resources.Search;
            this.button11.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.button11.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button11.ForeColor = System.Drawing.Color.White;
            this.button11.Location = new System.Drawing.Point(800, 9);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(28, 28);
            this.button11.TabIndex = 1;
            this.button11.UseVisualStyleBackColor = true;
            // 
            // panel59
            // 
            this.panel59.BackColor = System.Drawing.Color.Black;
            this.panel59.Controls.Add(this.pViewExitBtn);
            this.panel59.Controls.Add(this.planVviewAndEditBtn);
            this.panel59.Controls.Add(this.planViewPatientNameLbl);
            this.panel59.Location = new System.Drawing.Point(0, 0);
            this.panel59.Name = "panel59";
            this.panel59.Size = new System.Drawing.Size(1907, 96);
            this.panel59.TabIndex = 2;
            // 
            // pViewExitBtn
            // 
            this.pViewExitBtn.FlatAppearance.BorderSize = 0;
            this.pViewExitBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.pViewExitBtn.Image = global::SelfitMain.Properties.Resources.exitYellow;
            this.pViewExitBtn.Location = new System.Drawing.Point(1815, 22);
            this.pViewExitBtn.Name = "pViewExitBtn";
            this.pViewExitBtn.Size = new System.Drawing.Size(60, 60);
            this.pViewExitBtn.TabIndex = 5;
            this.pViewExitBtn.UseVisualStyleBackColor = true;
            this.pViewExitBtn.Click += new System.EventHandler(this.pViewExitBtn_Click);
            // 
            // planVviewAndEditBtn
            // 
            this.planVviewAndEditBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(201)))), ((int)(((byte)(31)))));
            this.planVviewAndEditBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.planVviewAndEditBtn.Font = new System.Drawing.Font("Roboto", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.planVviewAndEditBtn.Location = new System.Drawing.Point(297, 29);
            this.planVviewAndEditBtn.Name = "planVviewAndEditBtn";
            this.planVviewAndEditBtn.Size = new System.Drawing.Size(170, 37);
            this.planVviewAndEditBtn.TabIndex = 2;
            this.planVviewAndEditBtn.Text = "View && Edit";
            this.planVviewAndEditBtn.UseVisualStyleBackColor = false;
            this.planVviewAndEditBtn.Click += new System.EventHandler(this.planVviewAndEditBtn_Click);
            // 
            // planViewPatientNameLbl
            // 
            this.planViewPatientNameLbl.Font = new System.Drawing.Font("Roboto", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.planViewPatientNameLbl.ForeColor = System.Drawing.Color.White;
            this.planViewPatientNameLbl.Location = new System.Drawing.Point(39, 29);
            this.planViewPatientNameLbl.Name = "planViewPatientNameLbl";
            this.planViewPatientNameLbl.Size = new System.Drawing.Size(295, 34);
            this.planViewPatientNameLbl.TabIndex = 1;
            this.planViewPatientNameLbl.Text = "Mr. Jhon Adams";
            this.planViewPatientNameLbl.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // planViewLayoutPanel
            // 
            this.planViewLayoutPanel.AutoScroll = true;
            this.planViewLayoutPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.planViewLayoutPanel.Controls.Add(this.panel67);
            this.planViewLayoutPanel.Controls.Add(this.panel66);
            this.planViewLayoutPanel.Location = new System.Drawing.Point(145, 233);
            this.planViewLayoutPanel.Name = "planViewLayoutPanel";
            this.planViewLayoutPanel.Size = new System.Drawing.Size(931, 940);
            this.planViewLayoutPanel.TabIndex = 11;
            this.planViewLayoutPanel.MouseDown += new System.Windows.Forms.MouseEventHandler(this.planViewLayoutPanel_MouseDown);
            this.planViewLayoutPanel.MouseMove += new System.Windows.Forms.MouseEventHandler(this.planViewLayoutPanel_MouseMove);
            this.planViewLayoutPanel.MouseWheel += new System.Windows.Forms.MouseEventHandler(this.planViewLayoutPanel_MouseWheel);
            // 
            // panel67
            // 
            this.panel67.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.panel67.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel67.Controls.Add(this.label63);
            this.panel67.Controls.Add(this.label64);
            this.panel67.Controls.Add(this.label66);
            this.panel67.Controls.Add(this.label67);
            this.panel67.Location = new System.Drawing.Point(0, 0);
            this.panel67.Margin = new System.Windows.Forms.Padding(0);
            this.panel67.Name = "panel67";
            this.panel67.Size = new System.Drawing.Size(893, 80);
            this.panel67.TabIndex = 1;
            // 
            // label63
            // 
            this.label63.BackColor = System.Drawing.Color.Transparent;
            this.label63.Font = new System.Drawing.Font("Roboto", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.label63.ForeColor = System.Drawing.Color.Black;
            this.label63.Location = new System.Drawing.Point(642, 25);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(168, 36);
            this.label63.TabIndex = 17;
            this.label63.Text = "02.34 Min";
            this.label63.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label64
            // 
            this.label64.BackColor = System.Drawing.Color.Transparent;
            this.label64.Font = new System.Drawing.Font("Roboto", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.label64.ForeColor = System.Drawing.Color.Black;
            this.label64.Location = new System.Drawing.Point(449, 25);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(52, 36);
            this.label64.TabIndex = 16;
            this.label64.Text = "01";
            this.label64.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label66
            // 
            this.label66.BackColor = System.Drawing.Color.Transparent;
            this.label66.Font = new System.Drawing.Font("Roboto", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.label66.ForeColor = System.Drawing.Color.Black;
            this.label66.Location = new System.Drawing.Point(152, 25);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(273, 36);
            this.label66.TabIndex = 15;
            this.label66.Text = "Cognitive 1";
            this.label66.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label67
            // 
            this.label67.BackColor = System.Drawing.Color.Transparent;
            this.label67.Font = new System.Drawing.Font("Roboto", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.label67.ForeColor = System.Drawing.Color.Black;
            this.label67.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label67.Location = new System.Drawing.Point(31, 25);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(89, 36);
            this.label67.TabIndex = 14;
            this.label67.Text = "01";
            this.label67.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel66
            // 
            this.panel66.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.panel66.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel66.Controls.Add(this.label57);
            this.panel66.Controls.Add(this.label58);
            this.panel66.Controls.Add(this.label60);
            this.panel66.Controls.Add(this.label61);
            this.panel66.Location = new System.Drawing.Point(0, 80);
            this.panel66.Margin = new System.Windows.Forms.Padding(0);
            this.panel66.Name = "panel66";
            this.panel66.Size = new System.Drawing.Size(893, 80);
            this.panel66.TabIndex = 2;
            // 
            // label57
            // 
            this.label57.BackColor = System.Drawing.Color.Transparent;
            this.label57.Font = new System.Drawing.Font("Roboto", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.label57.ForeColor = System.Drawing.Color.Black;
            this.label57.Location = new System.Drawing.Point(642, 25);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(168, 36);
            this.label57.TabIndex = 17;
            this.label57.Text = "02.34 Min";
            this.label57.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label58
            // 
            this.label58.BackColor = System.Drawing.Color.Transparent;
            this.label58.Font = new System.Drawing.Font("Roboto", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.label58.ForeColor = System.Drawing.Color.Black;
            this.label58.Location = new System.Drawing.Point(449, 25);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(52, 36);
            this.label58.TabIndex = 16;
            this.label58.Text = "01";
            this.label58.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label60
            // 
            this.label60.BackColor = System.Drawing.Color.Transparent;
            this.label60.Font = new System.Drawing.Font("Roboto", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.label60.ForeColor = System.Drawing.Color.Black;
            this.label60.Location = new System.Drawing.Point(152, 25);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(253, 36);
            this.label60.TabIndex = 15;
            this.label60.Text = "Cognitive 1";
            this.label60.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label61
            // 
            this.label61.BackColor = System.Drawing.Color.Transparent;
            this.label61.Font = new System.Drawing.Font("Roboto", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.label61.ForeColor = System.Drawing.Color.Black;
            this.label61.Location = new System.Drawing.Point(31, 25);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(52, 36);
            this.label61.TabIndex = 14;
            this.label61.Text = "01";
            this.label61.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Settings
            // 
            this.Settings.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Settings.Controls.Add(this.panel1);
            this.Settings.Controls.Add(this.groupBox1);
            this.Settings.Controls.Add(this.button46);
            this.Settings.Controls.Add(this.button45);
            this.Settings.Location = new System.Drawing.Point(4, 23);
            this.Settings.Name = "Settings";
            this.Settings.Padding = new System.Windows.Forms.Padding(3);
            this.Settings.Size = new System.Drawing.Size(1910, 1172);
            this.Settings.TabIndex = 6;
            this.Settings.Text = "Settings";
            this.Settings.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Black;
            this.panel1.Controls.Add(this.button3);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Location = new System.Drawing.Point(3, 6);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1937, 96);
            this.panel1.TabIndex = 25;
            // 
            // button3
            // 
            this.button3.FlatAppearance.BorderSize = 0;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.Image = global::SelfitMain.Properties.Resources.exitYellow;
            this.button3.Location = new System.Drawing.Point(1815, 22);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(60, 60);
            this.button3.TabIndex = 3;
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click_1);
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Roboto", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(39, 29);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(295, 34);
            this.label4.TabIndex = 1;
            this.label4.Text = "System Settings";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.parkinsonModeCBX);
            this.groupBox1.Controls.Add(this.autoGenIDcbx);
            this.groupBox1.Controls.Add(this.button2);
            this.groupBox1.Controls.Add(this.selfitPathTxt);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.checkBox1);
            this.groupBox1.Controls.Add(this.useHebSpeachCBX);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.groupBox1.Location = new System.Drawing.Point(22, 144);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(952, 349);
            this.groupBox1.TabIndex = 24;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Settings: ";
            // 
            // parkinsonModeCBX
            // 
            this.parkinsonModeCBX.AutoSize = true;
            this.parkinsonModeCBX.Font = new System.Drawing.Font("Roboto", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.parkinsonModeCBX.Location = new System.Drawing.Point(26, 282);
            this.parkinsonModeCBX.Name = "parkinsonModeCBX";
            this.parkinsonModeCBX.Size = new System.Drawing.Size(261, 33);
            this.parkinsonModeCBX.TabIndex = 6;
            this.parkinsonModeCBX.Text = "Parkinson Pilot Mode";
            this.parkinsonModeCBX.UseVisualStyleBackColor = true;
            this.parkinsonModeCBX.CheckedChanged += new System.EventHandler(this.parkinsonModeCBX_CheckedChanged);
            // 
            // autoGenIDcbx
            // 
            this.autoGenIDcbx.AutoSize = true;
            this.autoGenIDcbx.Font = new System.Drawing.Font("Roboto", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.autoGenIDcbx.Location = new System.Drawing.Point(26, 224);
            this.autoGenIDcbx.Name = "autoGenIDcbx";
            this.autoGenIDcbx.Size = new System.Drawing.Size(299, 33);
            this.autoGenIDcbx.TabIndex = 5;
            this.autoGenIDcbx.Text = "Auto generate ID number";
            this.autoGenIDcbx.UseVisualStyleBackColor = true;
            this.autoGenIDcbx.CheckedChanged += new System.EventHandler(this.autoGenIDcbx_CheckedChanged);
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Roboto", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.button2.Location = new System.Drawing.Point(808, 158);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(128, 43);
            this.button2.TabIndex = 4;
            this.button2.Text = "Save";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // selfitPathTxt
            // 
            this.selfitPathTxt.Font = new System.Drawing.Font("Roboto", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.selfitPathTxt.Location = new System.Drawing.Point(205, 162);
            this.selfitPathTxt.Name = "selfitPathTxt";
            this.selfitPathTxt.Size = new System.Drawing.Size(597, 36);
            this.selfitPathTxt.TabIndex = 3;
            this.selfitPathTxt.TextChanged += new System.EventHandler(this.selfitPathTxt_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Roboto", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.label3.Location = new System.Drawing.Point(21, 165);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(181, 29);
            this.label3.TabIndex = 2;
            this.label3.Text = "Selfit EXE path: ";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Font = new System.Drawing.Font("Roboto", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.checkBox1.Location = new System.Drawing.Point(26, 104);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(231, 33);
            this.checkBox1.TabIndex = 1;
            this.checkBox1.Text = "Show Rate Control";
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // useHebSpeachCBX
            // 
            this.useHebSpeachCBX.AutoSize = true;
            this.useHebSpeachCBX.Font = new System.Drawing.Font("Roboto", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.useHebSpeachCBX.Location = new System.Drawing.Point(26, 47);
            this.useHebSpeachCBX.Name = "useHebSpeachCBX";
            this.useHebSpeachCBX.Size = new System.Drawing.Size(376, 33);
            this.useHebSpeachCBX.TabIndex = 0;
            this.useHebSpeachCBX.Text = "Use Hebrew instructions speach";
            this.useHebSpeachCBX.UseVisualStyleBackColor = true;
            this.useHebSpeachCBX.CheckedChanged += new System.EventHandler(this.useHebSpeachCBX_CheckedChanged);
            // 
            // button46
            // 
            this.button46.BackColor = System.Drawing.Color.Transparent;
            this.button46.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button46.FlatAppearance.BorderSize = 0;
            this.button46.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.button46.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.button46.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button46.Location = new System.Drawing.Point(83, 267);
            this.button46.Name = "button46";
            this.button46.Size = new System.Drawing.Size(104, 31);
            this.button46.TabIndex = 23;
            this.button46.UseVisualStyleBackColor = false;
            this.button46.Click += new System.EventHandler(this.button46_Click);
            // 
            // button45
            // 
            this.button45.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button45.BackColor = System.Drawing.Color.Transparent;
            this.button45.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button45.FlatAppearance.BorderSize = 0;
            this.button45.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.button45.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.button45.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button45.Location = new System.Drawing.Point(195, 986);
            this.button45.Name = "button45";
            this.button45.Size = new System.Drawing.Size(40, 172);
            this.button45.TabIndex = 22;
            this.button45.UseVisualStyleBackColor = false;
            this.button45.Click += new System.EventHandler(this.button45_Click);
            // 
            // reports
            // 
            this.reports.BackColor = System.Drawing.Color.White;
            this.reports.Controls.Add(this.reportsFlowLayoutPanel);
            this.reports.Controls.Add(this.label5);
            this.reports.Controls.Add(this.label6);
            this.reports.Controls.Add(this.reportsSelectExercisesBtn);
            this.reports.Controls.Add(this.reportsSelectPlansBtn);
            this.reports.Controls.Add(this.panel10);
            this.reports.Controls.Add(this.panel6);
            this.reports.Controls.Add(this.panel5);
            this.reports.Controls.Add(this.reportsTabControl);
            this.reports.Location = new System.Drawing.Point(4, 23);
            this.reports.Name = "reports";
            this.reports.Padding = new System.Windows.Forms.Padding(3);
            this.reports.Size = new System.Drawing.Size(1910, 1172);
            this.reports.TabIndex = 10;
            this.reports.Text = "reports";
            // 
            // reportsFlowLayoutPanel
            // 
            this.reportsFlowLayoutPanel.BackColor = System.Drawing.Color.White;
            this.reportsFlowLayoutPanel.Controls.Add(this.panel11);
            this.reportsFlowLayoutPanel.Controls.Add(this.panel58);
            this.reportsFlowLayoutPanel.Location = new System.Drawing.Point(452, 228);
            this.reportsFlowLayoutPanel.Margin = new System.Windows.Forms.Padding(10);
            this.reportsFlowLayoutPanel.Name = "reportsFlowLayoutPanel";
            this.reportsFlowLayoutPanel.Size = new System.Drawing.Size(364, 943);
            this.reportsFlowLayoutPanel.TabIndex = 5;
            // 
            // panel11
            // 
            this.panel11.BackColor = System.Drawing.Color.White;
            this.panel11.Controls.Add(this.label32);
            this.panel11.Controls.Add(this.label34);
            this.panel11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.panel11.Location = new System.Drawing.Point(3, 1);
            this.panel11.Margin = new System.Windows.Forms.Padding(3, 1, 3, 0);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(357, 54);
            this.panel11.TabIndex = 2;
            // 
            // label32
            // 
            this.label32.BackColor = System.Drawing.Color.White;
            this.label32.Font = new System.Drawing.Font("Roboto", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.label32.ForeColor = System.Drawing.Color.Black;
            this.label32.Location = new System.Drawing.Point(265, 11);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(75, 28);
            this.label32.TabIndex = 3;
            this.label32.Text = "10";
            this.label32.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label34
            // 
            this.label34.BackColor = System.Drawing.Color.Transparent;
            this.label34.Font = new System.Drawing.Font("Roboto", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.label34.ForeColor = System.Drawing.Color.Black;
            this.label34.Location = new System.Drawing.Point(5, 11);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(247, 28);
            this.label34.TabIndex = 2;
            this.label34.Text = "24.05.19";
            this.label34.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel58
            // 
            this.panel58.BackColor = System.Drawing.Color.White;
            this.panel58.Controls.Add(this.pictureBox14);
            this.panel58.Controls.Add(this.label38);
            this.panel58.Controls.Add(this.label49);
            this.panel58.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.panel58.Location = new System.Drawing.Point(3, 56);
            this.panel58.Margin = new System.Windows.Forms.Padding(3, 1, 3, 0);
            this.panel58.Name = "panel58";
            this.panel58.Size = new System.Drawing.Size(357, 54);
            this.panel58.TabIndex = 3;
            // 
            // pictureBox14
            // 
            this.pictureBox14.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pictureBox14.Image = global::SelfitMain.Properties.Resources.wtsImage;
            this.pictureBox14.Location = new System.Drawing.Point(5, 9);
            this.pictureBox14.Name = "pictureBox14";
            this.pictureBox14.Size = new System.Drawing.Size(33, 33);
            this.pictureBox14.TabIndex = 12;
            this.pictureBox14.TabStop = false;
            // 
            // label38
            // 
            this.label38.BackColor = System.Drawing.Color.White;
            this.label38.Font = new System.Drawing.Font("Roboto", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.label38.ForeColor = System.Drawing.Color.Black;
            this.label38.Location = new System.Drawing.Point(265, 11);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(78, 28);
            this.label38.TabIndex = 3;
            this.label38.Text = "10";
            this.label38.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label49
            // 
            this.label49.BackColor = System.Drawing.Color.Transparent;
            this.label49.Font = new System.Drawing.Font("Roboto", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.label49.ForeColor = System.Drawing.Color.Black;
            this.label49.Location = new System.Drawing.Point(47, 11);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(218, 28);
            this.label49.TabIndex = 2;
            this.label49.Text = "24.05.19";
            this.label49.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label5
            // 
            this.label5.BackColor = System.Drawing.Color.White;
            this.label5.Font = new System.Drawing.Font("Roboto", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label5.Location = new System.Drawing.Point(498, 141);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(149, 28);
            this.label5.TabIndex = 20;
            this.label5.Text = "Date";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label6
            // 
            this.label6.BackColor = System.Drawing.Color.White;
            this.label6.Font = new System.Drawing.Font("Roboto", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(658, 141);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(124, 28);
            this.label6.TabIndex = 21;
            this.label6.Text = "Exercises";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // reportsSelectExercisesBtn
            // 
            this.reportsSelectExercisesBtn.BackColor = System.Drawing.Color.White;
            this.reportsSelectExercisesBtn.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(201)))), ((int)(((byte)(31)))));
            this.reportsSelectExercisesBtn.FlatAppearance.BorderSize = 3;
            this.reportsSelectExercisesBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.reportsSelectExercisesBtn.Font = new System.Drawing.Font("Roboto", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.reportsSelectExercisesBtn.ForeColor = System.Drawing.Color.Black;
            this.reportsSelectExercisesBtn.Image = global::SelfitMain.Properties.Resources.Vector_2_2;
            this.reportsSelectExercisesBtn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.reportsSelectExercisesBtn.Location = new System.Drawing.Point(179, 143);
            this.reportsSelectExercisesBtn.Name = "reportsSelectExercisesBtn";
            this.reportsSelectExercisesBtn.Size = new System.Drawing.Size(245, 58);
            this.reportsSelectExercisesBtn.TabIndex = 19;
            this.reportsSelectExercisesBtn.Text = "Exercises";
            this.reportsSelectExercisesBtn.UseVisualStyleBackColor = false;
            this.reportsSelectExercisesBtn.Click += new System.EventHandler(this.reportsSelectExercisesBtn_Click);
            // 
            // reportsSelectPlansBtn
            // 
            this.reportsSelectPlansBtn.BackColor = System.Drawing.Color.White;
            this.reportsSelectPlansBtn.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.reportsSelectPlansBtn.FlatAppearance.BorderSize = 3;
            this.reportsSelectPlansBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.reportsSelectPlansBtn.Font = new System.Drawing.Font("Roboto", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.reportsSelectPlansBtn.ForeColor = System.Drawing.Color.Black;
            this.reportsSelectPlansBtn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.reportsSelectPlansBtn.Location = new System.Drawing.Point(179, 225);
            this.reportsSelectPlansBtn.Name = "reportsSelectPlansBtn";
            this.reportsSelectPlansBtn.Size = new System.Drawing.Size(245, 58);
            this.reportsSelectPlansBtn.TabIndex = 18;
            this.reportsSelectPlansBtn.Text = "Plans";
            this.reportsSelectPlansBtn.UseVisualStyleBackColor = false;
            this.reportsSelectPlansBtn.Click += new System.EventHandler(this.reportsSelectPlansBtn_Click);
            // 
            // panel10
            // 
            this.panel10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(56)))), ((int)(((byte)(64)))));
            this.panel10.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.panel10.Controls.Add(this.reportsPlansBtn);
            this.panel10.Controls.Add(this.reportsPHistoryBtn);
            this.panel10.Controls.Add(this.reportsQplanBtn);
            this.panel10.Controls.Add(this.reportsExercisesBtn);
            this.panel10.Location = new System.Drawing.Point(0, 95);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(150, 850);
            this.panel10.TabIndex = 4;
            // 
            // reportsPlansBtn
            // 
            this.reportsPlansBtn.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(56)))), ((int)(((byte)(64)))));
            this.reportsPlansBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.reportsPlansBtn.Font = new System.Drawing.Font("Roboto", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.reportsPlansBtn.ForeColor = System.Drawing.Color.White;
            this.reportsPlansBtn.Image = global::SelfitMain.Properties.Resources.Star;
            this.reportsPlansBtn.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.reportsPlansBtn.Location = new System.Drawing.Point(1, 673);
            this.reportsPlansBtn.Name = "reportsPlansBtn";
            this.reportsPlansBtn.Size = new System.Drawing.Size(148, 138);
            this.reportsPlansBtn.TabIndex = 11;
            this.reportsPlansBtn.Text = "Saved Plans";
            this.reportsPlansBtn.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.reportsPlansBtn.UseVisualStyleBackColor = true;
            this.reportsPlansBtn.Click += new System.EventHandler(this.reportsPlansBtn_Click);
            // 
            // reportsPHistoryBtn
            // 
            this.reportsPHistoryBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(56)))), ((int)(((byte)(64)))));
            this.reportsPHistoryBtn.Font = new System.Drawing.Font("Roboto", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.reportsPHistoryBtn.ForeColor = System.Drawing.Color.White;
            this.reportsPHistoryBtn.Image = global::SelfitMain.Properties.Resources.Group1;
            this.reportsPHistoryBtn.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.reportsPHistoryBtn.Location = new System.Drawing.Point(-3, 470);
            this.reportsPHistoryBtn.Name = "reportsPHistoryBtn";
            this.reportsPHistoryBtn.Size = new System.Drawing.Size(150, 110);
            this.reportsPHistoryBtn.TabIndex = 10;
            this.reportsPHistoryBtn.Text = "Patient History";
            this.reportsPHistoryBtn.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.reportsPHistoryBtn.Click += new System.EventHandler(this.reportsPHistoryBtn_Click);
            // 
            // reportsQplanBtn
            // 
            this.reportsQplanBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.reportsQplanBtn.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(56)))), ((int)(((byte)(64)))));
            this.reportsQplanBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.reportsQplanBtn.Font = new System.Drawing.Font("Roboto", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.reportsQplanBtn.ForeColor = System.Drawing.Color.White;
            this.reportsQplanBtn.Image = global::SelfitMain.Properties.Resources.quickplan_bright;
            this.reportsQplanBtn.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.reportsQplanBtn.Location = new System.Drawing.Point(0, 46);
            this.reportsQplanBtn.Name = "reportsQplanBtn";
            this.reportsQplanBtn.Size = new System.Drawing.Size(147, 127);
            this.reportsQplanBtn.TabIndex = 9;
            this.reportsQplanBtn.Text = "Quick Plan";
            this.reportsQplanBtn.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.reportsQplanBtn.UseVisualStyleBackColor = true;
            this.reportsQplanBtn.Click += new System.EventHandler(this.reportsQplanBtn_Click);
            // 
            // reportsExercisesBtn
            // 
            this.reportsExercisesBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.reportsExercisesBtn.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(56)))), ((int)(((byte)(64)))));
            this.reportsExercisesBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.reportsExercisesBtn.Font = new System.Drawing.Font("Roboto", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.reportsExercisesBtn.ForeColor = System.Drawing.Color.White;
            this.reportsExercisesBtn.Image = global::SelfitMain.Properties.Resources.HistoryaddPatient;
            this.reportsExercisesBtn.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.reportsExercisesBtn.Location = new System.Drawing.Point(0, 268);
            this.reportsExercisesBtn.Name = "reportsExercisesBtn";
            this.reportsExercisesBtn.Size = new System.Drawing.Size(148, 118);
            this.reportsExercisesBtn.TabIndex = 0;
            this.reportsExercisesBtn.Text = "Exercises";
            this.reportsExercisesBtn.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.reportsExercisesBtn.UseVisualStyleBackColor = true;
            this.reportsExercisesBtn.Click += new System.EventHandler(this.reportsExercisesBtn_Click);
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(201)))), ((int)(((byte)(31)))));
            this.panel6.Controls.Add(this.button18);
            this.panel6.Location = new System.Drawing.Point(1, 945);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(149, 226);
            this.panel6.TabIndex = 4;
            // 
            // button18
            // 
            this.button18.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(56)))), ((int)(((byte)(64)))));
            this.button18.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button18.Font = new System.Drawing.Font("Roboto", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.button18.ForeColor = System.Drawing.Color.Black;
            this.button18.Image = global::SelfitMain.Properties.Resources.reportBtnBlack;
            this.button18.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.button18.Location = new System.Drawing.Point(0, 86);
            this.button18.Name = "button18";
            this.button18.Size = new System.Drawing.Size(150, 94);
            this.button18.TabIndex = 4;
            this.button18.Text = "Reports";
            this.button18.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.button18.UseVisualStyleBackColor = true;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.Black;
            this.panel5.Controls.Add(this.label118);
            this.panel5.Controls.Add(this.reportsBackBtn);
            this.panel5.Controls.Add(this.reportsCloseBtn);
            this.panel5.Controls.Add(this.reportsPatientEditBtn);
            this.panel5.Controls.Add(this.reportsPatientNameLbl);
            this.panel5.Location = new System.Drawing.Point(0, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(1937, 96);
            this.panel5.TabIndex = 1;
            // 
            // label118
            // 
            this.label118.Font = new System.Drawing.Font("Roboto", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.label118.ForeColor = System.Drawing.Color.White;
            this.label118.Location = new System.Drawing.Point(144, 14);
            this.label118.Name = "label118";
            this.label118.Size = new System.Drawing.Size(135, 60);
            this.label118.TabIndex = 5;
            this.label118.Text = "Reports";
            this.label118.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // reportsBackBtn
            // 
            this.reportsBackBtn.FlatAppearance.BorderSize = 0;
            this.reportsBackBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.reportsBackBtn.Image = global::SelfitMain.Properties.Resources.reportsBack;
            this.reportsBackBtn.Location = new System.Drawing.Point(32, 14);
            this.reportsBackBtn.Name = "reportsBackBtn";
            this.reportsBackBtn.Size = new System.Drawing.Size(60, 60);
            this.reportsBackBtn.TabIndex = 4;
            this.reportsBackBtn.UseVisualStyleBackColor = true;
            this.reportsBackBtn.Click += new System.EventHandler(this.reportsBackBtn_Click);
            // 
            // reportsCloseBtn
            // 
            this.reportsCloseBtn.FlatAppearance.BorderSize = 0;
            this.reportsCloseBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.reportsCloseBtn.Image = global::SelfitMain.Properties.Resources.exitYellow;
            this.reportsCloseBtn.Location = new System.Drawing.Point(1815, 22);
            this.reportsCloseBtn.Name = "reportsCloseBtn";
            this.reportsCloseBtn.Size = new System.Drawing.Size(60, 60);
            this.reportsCloseBtn.TabIndex = 3;
            this.reportsCloseBtn.UseVisualStyleBackColor = true;
            this.reportsCloseBtn.Click += new System.EventHandler(this.reportsCloseBtn_Click);
            // 
            // reportsPatientEditBtn
            // 
            this.reportsPatientEditBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(201)))), ((int)(((byte)(31)))));
            this.reportsPatientEditBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.reportsPatientEditBtn.Font = new System.Drawing.Font("Roboto", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.reportsPatientEditBtn.ForeColor = System.Drawing.Color.Black;
            this.reportsPatientEditBtn.Location = new System.Drawing.Point(786, 27);
            this.reportsPatientEditBtn.Name = "reportsPatientEditBtn";
            this.reportsPatientEditBtn.Size = new System.Drawing.Size(170, 37);
            this.reportsPatientEditBtn.TabIndex = 2;
            this.reportsPatientEditBtn.Text = "View && Edit";
            this.reportsPatientEditBtn.UseVisualStyleBackColor = false;
            this.reportsPatientEditBtn.Click += new System.EventHandler(this.reportsPatientEditBtn_Click);
            // 
            // reportsPatientNameLbl
            // 
            this.reportsPatientNameLbl.Font = new System.Drawing.Font("Roboto", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.reportsPatientNameLbl.ForeColor = System.Drawing.Color.White;
            this.reportsPatientNameLbl.Location = new System.Drawing.Point(485, 27);
            this.reportsPatientNameLbl.Name = "reportsPatientNameLbl";
            this.reportsPatientNameLbl.Size = new System.Drawing.Size(295, 34);
            this.reportsPatientNameLbl.TabIndex = 1;
            this.reportsPatientNameLbl.Text = "Mr. Jhon Adams";
            this.reportsPatientNameLbl.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // reportsTabControl
            // 
            this.reportsTabControl.Appearance = System.Windows.Forms.TabAppearance.FlatButtons;
            this.reportsTabControl.Controls.Add(this.ReportsPlansTabPage);
            this.reportsTabControl.Controls.Add(this.ReportsExercisesTabPage);
            this.reportsTabControl.ItemSize = new System.Drawing.Size(58, 20);
            this.reportsTabControl.Location = new System.Drawing.Point(816, 94);
            this.reportsTabControl.Name = "reportsTabControl";
            this.reportsTabControl.SelectedIndex = 0;
            this.reportsTabControl.Size = new System.Drawing.Size(1129, 1074);
            this.reportsTabControl.TabIndex = 9;
            // 
            // ReportsPlansTabPage
            // 
            this.ReportsPlansTabPage.Controls.Add(this.plansDataGridView);
            this.ReportsPlansTabPage.Location = new System.Drawing.Point(4, 24);
            this.ReportsPlansTabPage.Name = "ReportsPlansTabPage";
            this.ReportsPlansTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.ReportsPlansTabPage.Size = new System.Drawing.Size(1121, 1046);
            this.ReportsPlansTabPage.TabIndex = 0;
            this.ReportsPlansTabPage.Text = "reportsPlansTabPage";
            this.ReportsPlansTabPage.UseVisualStyleBackColor = true;
            // 
            // plansDataGridView
            // 
            this.plansDataGridView.AllowUserToAddRows = false;
            this.plansDataGridView.AllowUserToDeleteRows = false;
            this.plansDataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.plansDataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.plansDataGridView.ColumnHeadersHeight = 50;
            this.plansDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.plansDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Session,
            this.sessionDate,
            this.Total_Distance,
            this.Step_Count,
            this.Session_Time_gross,
            this.Speed,
            this.Step_hight_Right,
            this.Step_hight_Left,
            this.Step_Length_Right,
            this.Step_Length_Left,
            this.Movement_Time});
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.plansDataGridView.DefaultCellStyle = dataGridViewCellStyle3;
            this.plansDataGridView.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.plansDataGridView.Location = new System.Drawing.Point(3, 3);
            this.plansDataGridView.Name = "plansDataGridView";
            this.plansDataGridView.ReadOnly = true;
            this.plansDataGridView.RowHeadersVisible = false;
            this.plansDataGridView.RowHeadersWidth = 31;
            this.plansDataGridView.Size = new System.Drawing.Size(1081, 952);
            this.plansDataGridView.TabIndex = 1;
            // 
            // Session
            // 
            this.Session.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.Session.HeaderText = "Session";
            this.Session.Name = "Session";
            this.Session.ReadOnly = true;
            this.Session.Width = 80;
            // 
            // sessionDate
            // 
            this.sessionDate.HeaderText = "Date";
            this.sessionDate.Name = "sessionDate";
            this.sessionDate.ReadOnly = true;
            this.sessionDate.Width = 62;
            // 
            // Total_Distance
            // 
            this.Total_Distance.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.Total_Distance.HeaderText = "Total Distance";
            this.Total_Distance.Name = "Total_Distance";
            this.Total_Distance.ReadOnly = true;
            this.Total_Distance.Width = 111;
            // 
            // Step_Count
            // 
            this.Step_Count.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.Step_Count.HeaderText = "Step Count";
            this.Step_Count.Name = "Step_Count";
            this.Step_Count.ReadOnly = true;
            this.Step_Count.Width = 94;
            // 
            // Session_Time_gross
            // 
            this.Session_Time_gross.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.Session_Time_gross.HeaderText = "Session Time gross";
            this.Session_Time_gross.Name = "Session_Time_gross";
            this.Session_Time_gross.ReadOnly = true;
            this.Session_Time_gross.Width = 107;
            // 
            // Speed
            // 
            this.Speed.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.Speed.HeaderText = "Avg Speed";
            this.Speed.Name = "Speed";
            this.Speed.ReadOnly = true;
            this.Speed.Width = 80;
            // 
            // Step_hight_Right
            // 
            this.Step_hight_Right.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.Step_hight_Right.HeaderText = "Step hight Right";
            this.Step_hight_Right.Name = "Step_hight_Right";
            this.Step_hight_Right.ReadOnly = true;
            this.Step_hight_Right.Width = 110;
            // 
            // Step_hight_Left
            // 
            this.Step_hight_Left.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.Step_hight_Left.HeaderText = "Step hight Left";
            this.Step_hight_Left.Name = "Step_hight_Left";
            this.Step_hight_Left.ReadOnly = true;
            this.Step_hight_Left.Width = 110;
            // 
            // Step_Length_Right
            // 
            this.Step_Length_Right.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.Step_Length_Right.HeaderText = "Step Length Right";
            this.Step_Length_Right.Name = "Step_Length_Right";
            this.Step_Length_Right.ReadOnly = true;
            this.Step_Length_Right.Width = 110;
            // 
            // Step_Length_Left
            // 
            this.Step_Length_Left.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.Step_Length_Left.HeaderText = "Step Length Left";
            this.Step_Length_Left.Name = "Step_Length_Left";
            this.Step_Length_Left.ReadOnly = true;
            this.Step_Length_Left.Width = 124;
            // 
            // Movement_Time
            // 
            this.Movement_Time.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.Movement_Time.HeaderText = "Movement Time";
            this.Movement_Time.Name = "Movement_Time";
            this.Movement_Time.ReadOnly = true;
            // 
            // ReportsExercisesTabPage
            // 
            this.ReportsExercisesTabPage.Controls.Add(this.selectedPracticeLbl);
            this.ReportsExercisesTabPage.Controls.Add(this.parametersDataflowLayoutPanel);
            this.ReportsExercisesTabPage.Controls.Add(this.selectedParametersFlowLayoutPanel);
            this.ReportsExercisesTabPage.Controls.Add(this.label7);
            this.ReportsExercisesTabPage.Controls.Add(this.panel14);
            this.ReportsExercisesTabPage.Location = new System.Drawing.Point(4, 24);
            this.ReportsExercisesTabPage.Name = "ReportsExercisesTabPage";
            this.ReportsExercisesTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.ReportsExercisesTabPage.Size = new System.Drawing.Size(1121, 1046);
            this.ReportsExercisesTabPage.TabIndex = 1;
            this.ReportsExercisesTabPage.Text = "tabPage2";
            this.ReportsExercisesTabPage.UseVisualStyleBackColor = true;
            // 
            // selectedPracticeLbl
            // 
            this.selectedPracticeLbl.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.selectedPracticeLbl.Font = new System.Drawing.Font("Roboto", 26F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.selectedPracticeLbl.ForeColor = System.Drawing.Color.Black;
            this.selectedPracticeLbl.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.selectedPracticeLbl.Location = new System.Drawing.Point(327, 11);
            this.selectedPracticeLbl.Name = "selectedPracticeLbl";
            this.selectedPracticeLbl.Size = new System.Drawing.Size(728, 40);
            this.selectedPracticeLbl.TabIndex = 25;
            this.selectedPracticeLbl.Text = "Practice Name";
            this.selectedPracticeLbl.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.selectedPracticeLbl.Visible = false;
            // 
            // parametersDataflowLayoutPanel
            // 
            this.parametersDataflowLayoutPanel.Controls.Add(this.MeasuresDataGridView);
            this.parametersDataflowLayoutPanel.Location = new System.Drawing.Point(312, 64);
            this.parametersDataflowLayoutPanel.Name = "parametersDataflowLayoutPanel";
            this.parametersDataflowLayoutPanel.Size = new System.Drawing.Size(775, 979);
            this.parametersDataflowLayoutPanel.TabIndex = 24;
            // 
            // MeasuresDataGridView
            // 
            this.MeasuresDataGridView.AllowUserToAddRows = false;
            this.MeasuresDataGridView.AllowUserToDeleteRows = false;
            this.MeasuresDataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Roboto", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.MeasuresDataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.MeasuresDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Roboto", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.MeasuresDataGridView.DefaultCellStyle = dataGridViewCellStyle5;
            this.MeasuresDataGridView.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.MeasuresDataGridView.Location = new System.Drawing.Point(3, 3);
            this.MeasuresDataGridView.Name = "MeasuresDataGridView";
            this.MeasuresDataGridView.ReadOnly = true;
            this.MeasuresDataGridView.Size = new System.Drawing.Size(772, 973);
            this.MeasuresDataGridView.TabIndex = 0;
            // 
            // selectedParametersFlowLayoutPanel
            // 
            this.selectedParametersFlowLayoutPanel.BackColor = System.Drawing.Color.WhiteSmoke;
            this.selectedParametersFlowLayoutPanel.Controls.Add(this.panel12);
            this.selectedParametersFlowLayoutPanel.Location = new System.Drawing.Point(0, 64);
            this.selectedParametersFlowLayoutPanel.Name = "selectedParametersFlowLayoutPanel";
            this.selectedParametersFlowLayoutPanel.Size = new System.Drawing.Size(310, 979);
            this.selectedParametersFlowLayoutPanel.TabIndex = 23;
            // 
            // panel12
            // 
            this.panel12.Controls.Add(label36);
            this.panel12.Location = new System.Drawing.Point(3, 3);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(305, 54);
            this.panel12.TabIndex = 0;
            // 
            // label7
            // 
            this.label7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.label7.Font = new System.Drawing.Font("Roboto", 26F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label7.Location = new System.Drawing.Point(12, 11);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(168, 40);
            this.label7.TabIndex = 22;
            this.label7.Text = "Parameters";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel14
            // 
            this.panel14.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.panel14.Location = new System.Drawing.Point(297, 64);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(46, 979);
            this.panel14.TabIndex = 26;
            // 
            // generalReports
            // 
            this.generalReports.Controls.Add(this.panel80);
            this.generalReports.Controls.Add(this.generalReportsListView);
            this.generalReports.Controls.Add(this.genRepShowBtn);
            this.generalReports.Controls.Add(this.panel81);
            this.generalReports.Controls.Add(this.panel77);
            this.generalReports.Controls.Add(this.panel74);
            this.generalReports.Controls.Add(this.panel46);
            this.generalReports.Location = new System.Drawing.Point(4, 23);
            this.generalReports.Name = "generalReports";
            this.generalReports.Size = new System.Drawing.Size(1910, 1172);
            this.generalReports.TabIndex = 11;
            this.generalReports.Text = "generalReports";
            this.generalReports.UseVisualStyleBackColor = true;
            // 
            // panel80
            // 
            this.panel80.BackColor = System.Drawing.Color.White;
            this.panel80.Controls.Add(this.panel86);
            this.panel80.Controls.Add(this.panel90);
            this.panel80.Controls.Add(this.panel88);
            this.panel80.Controls.Add(this.panel84);
            this.panel80.Controls.Add(this.panel82);
            this.panel80.Location = new System.Drawing.Point(1280, 96);
            this.panel80.Name = "panel80";
            this.panel80.Size = new System.Drawing.Size(627, 1080);
            this.panel80.TabIndex = 12;
            // 
            // panel86
            // 
            this.panel86.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(201)))), ((int)(((byte)(31)))));
            this.panel86.Controls.Add(this.sumRectExercisesCountTxt);
            this.panel86.Controls.Add(this.panel87);
            this.panel86.Controls.Add(this.label131);
            this.panel86.Location = new System.Drawing.Point(21, 659);
            this.panel86.Margin = new System.Windows.Forms.Padding(80, 40, 80, 40);
            this.panel86.Name = "panel86";
            this.panel86.Size = new System.Drawing.Size(591, 202);
            this.panel86.TabIndex = 6;
            // 
            // sumRectExercisesCountTxt
            // 
            this.sumRectExercisesCountTxt.Font = new System.Drawing.Font("Roboto", 100F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.sumRectExercisesCountTxt.ForeColor = System.Drawing.Color.Black;
            this.sumRectExercisesCountTxt.Location = new System.Drawing.Point(400, 16);
            this.sumRectExercisesCountTxt.Name = "sumRectExercisesCountTxt";
            this.sumRectExercisesCountTxt.Size = new System.Drawing.Size(188, 130);
            this.sumRectExercisesCountTxt.TabIndex = 7;
            this.sumRectExercisesCountTxt.Text = "31";
            this.sumRectExercisesCountTxt.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel87
            // 
            this.panel87.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(56)))), ((int)(((byte)(64)))));
            this.panel87.Controls.Add(this.sumRectTotalExercisesTxt);
            this.panel87.Controls.Add(this.label130);
            this.panel87.Location = new System.Drawing.Point(0, 0);
            this.panel87.Name = "panel87";
            this.panel87.Size = new System.Drawing.Size(394, 200);
            this.panel87.TabIndex = 6;
            // 
            // sumRectTotalExercisesTxt
            // 
            this.sumRectTotalExercisesTxt.Font = new System.Drawing.Font("Roboto", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.sumRectTotalExercisesTxt.ForeColor = System.Drawing.Color.White;
            this.sumRectTotalExercisesTxt.Location = new System.Drawing.Point(31, 54);
            this.sumRectTotalExercisesTxt.Name = "sumRectTotalExercisesTxt";
            this.sumRectTotalExercisesTxt.Size = new System.Drawing.Size(241, 126);
            this.sumRectTotalExercisesTxt.TabIndex = 5;
            this.sumRectTotalExercisesTxt.Text = "Total 31 Exercises";
            this.sumRectTotalExercisesTxt.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label130
            // 
            this.label130.Font = new System.Drawing.Font("Roboto", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.label130.ForeColor = System.Drawing.Color.White;
            this.label130.Location = new System.Drawing.Point(31, 13);
            this.label130.Name = "label130";
            this.label130.Size = new System.Drawing.Size(308, 41);
            this.label130.TabIndex = 4;
            this.label130.Text = "Exercises";
            this.label130.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label131
            // 
            this.label131.Font = new System.Drawing.Font("Roboto", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.label131.ForeColor = System.Drawing.Color.Black;
            this.label131.Location = new System.Drawing.Point(400, 146);
            this.label131.Name = "label131";
            this.label131.Size = new System.Drawing.Size(188, 34);
            this.label131.TabIndex = 2;
            this.label131.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel90
            // 
            this.panel90.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(201)))), ((int)(((byte)(31)))));
            this.panel90.Controls.Add(this.sumRectTimeCountTxt);
            this.panel90.Controls.Add(this.panel91);
            this.panel90.Controls.Add(this.label145);
            this.panel90.Location = new System.Drawing.Point(21, 868);
            this.panel90.Margin = new System.Windows.Forms.Padding(80, 40, 80, 40);
            this.panel90.Name = "panel90";
            this.panel90.Size = new System.Drawing.Size(591, 202);
            this.panel90.TabIndex = 5;
            // 
            // sumRectTimeCountTxt
            // 
            this.sumRectTimeCountTxt.Font = new System.Drawing.Font("Roboto", 100F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.sumRectTimeCountTxt.ForeColor = System.Drawing.Color.Black;
            this.sumRectTimeCountTxt.Location = new System.Drawing.Point(400, 16);
            this.sumRectTimeCountTxt.Name = "sumRectTimeCountTxt";
            this.sumRectTimeCountTxt.Size = new System.Drawing.Size(188, 130);
            this.sumRectTimeCountTxt.TabIndex = 7;
            this.sumRectTimeCountTxt.Text = "2.3";
            this.sumRectTimeCountTxt.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel91
            // 
            this.panel91.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(56)))), ((int)(((byte)(64)))));
            this.panel91.Controls.Add(this.sumRectTotalTimeTxt);
            this.panel91.Controls.Add(this.label144);
            this.panel91.Location = new System.Drawing.Point(0, 0);
            this.panel91.Name = "panel91";
            this.panel91.Size = new System.Drawing.Size(394, 200);
            this.panel91.TabIndex = 6;
            // 
            // sumRectTotalTimeTxt
            // 
            this.sumRectTotalTimeTxt.Font = new System.Drawing.Font("Roboto", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.sumRectTotalTimeTxt.ForeColor = System.Drawing.Color.White;
            this.sumRectTotalTimeTxt.Location = new System.Drawing.Point(31, 54);
            this.sumRectTotalTimeTxt.Name = "sumRectTotalTimeTxt";
            this.sumRectTotalTimeTxt.Size = new System.Drawing.Size(241, 126);
            this.sumRectTotalTimeTxt.TabIndex = 5;
            this.sumRectTotalTimeTxt.Text = "Total Time \r\n2.3 Hr / 149 Min";
            this.sumRectTotalTimeTxt.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label144
            // 
            this.label144.Font = new System.Drawing.Font("Roboto", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.label144.ForeColor = System.Drawing.Color.White;
            this.label144.Location = new System.Drawing.Point(31, 13);
            this.label144.Name = "label144";
            this.label144.Size = new System.Drawing.Size(145, 41);
            this.label144.TabIndex = 4;
            this.label144.Text = "Time";
            this.label144.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label145
            // 
            this.label145.Font = new System.Drawing.Font("Roboto", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.label145.ForeColor = System.Drawing.Color.Black;
            this.label145.Location = new System.Drawing.Point(400, 146);
            this.label145.Name = "label145";
            this.label145.Size = new System.Drawing.Size(188, 34);
            this.label145.TabIndex = 2;
            this.label145.Text = "Hours";
            this.label145.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel88
            // 
            this.panel88.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(201)))), ((int)(((byte)(31)))));
            this.panel88.Controls.Add(this.sumRectSessionsCountTxt);
            this.panel88.Controls.Add(this.panel89);
            this.panel88.Controls.Add(this.label141);
            this.panel88.Location = new System.Drawing.Point(21, 449);
            this.panel88.Margin = new System.Windows.Forms.Padding(80, 40, 80, 40);
            this.panel88.Name = "panel88";
            this.panel88.Size = new System.Drawing.Size(591, 202);
            this.panel88.TabIndex = 4;
            // 
            // sumRectSessionsCountTxt
            // 
            this.sumRectSessionsCountTxt.Font = new System.Drawing.Font("Roboto", 100F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.sumRectSessionsCountTxt.ForeColor = System.Drawing.Color.Black;
            this.sumRectSessionsCountTxt.Location = new System.Drawing.Point(400, 16);
            this.sumRectSessionsCountTxt.Name = "sumRectSessionsCountTxt";
            this.sumRectSessionsCountTxt.Size = new System.Drawing.Size(188, 130);
            this.sumRectSessionsCountTxt.TabIndex = 7;
            this.sumRectSessionsCountTxt.Text = "31";
            this.sumRectSessionsCountTxt.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel89
            // 
            this.panel89.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(56)))), ((int)(((byte)(64)))));
            this.panel89.Controls.Add(this.sumRectTotalSessionsTxt);
            this.panel89.Controls.Add(this.label140);
            this.panel89.Location = new System.Drawing.Point(0, 0);
            this.panel89.Name = "panel89";
            this.panel89.Size = new System.Drawing.Size(394, 200);
            this.panel89.TabIndex = 6;
            // 
            // sumRectTotalSessionsTxt
            // 
            this.sumRectTotalSessionsTxt.Font = new System.Drawing.Font("Roboto", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.sumRectTotalSessionsTxt.ForeColor = System.Drawing.Color.White;
            this.sumRectTotalSessionsTxt.Location = new System.Drawing.Point(31, 54);
            this.sumRectTotalSessionsTxt.Name = "sumRectTotalSessionsTxt";
            this.sumRectTotalSessionsTxt.Size = new System.Drawing.Size(241, 126);
            this.sumRectTotalSessionsTxt.TabIndex = 5;
            this.sumRectTotalSessionsTxt.Text = "Total 31 sessions";
            this.sumRectTotalSessionsTxt.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label140
            // 
            this.label140.Font = new System.Drawing.Font("Roboto", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.label140.ForeColor = System.Drawing.Color.White;
            this.label140.Location = new System.Drawing.Point(31, 13);
            this.label140.Name = "label140";
            this.label140.Size = new System.Drawing.Size(308, 41);
            this.label140.TabIndex = 4;
            this.label140.Text = "Sessions";
            this.label140.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label141
            // 
            this.label141.Font = new System.Drawing.Font("Roboto", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.label141.ForeColor = System.Drawing.Color.Black;
            this.label141.Location = new System.Drawing.Point(400, 146);
            this.label141.Name = "label141";
            this.label141.Size = new System.Drawing.Size(188, 34);
            this.label141.TabIndex = 2;
            this.label141.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel84
            // 
            this.panel84.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(201)))), ((int)(((byte)(31)))));
            this.panel84.Controls.Add(this.sumRectPatientsCountTxt);
            this.panel84.Controls.Add(this.panel85);
            this.panel84.Controls.Add(this.label133);
            this.panel84.Location = new System.Drawing.Point(21, 229);
            this.panel84.Margin = new System.Windows.Forms.Padding(80, 40, 80, 40);
            this.panel84.Name = "panel84";
            this.panel84.Size = new System.Drawing.Size(591, 202);
            this.panel84.TabIndex = 2;
            // 
            // sumRectPatientsCountTxt
            // 
            this.sumRectPatientsCountTxt.Font = new System.Drawing.Font("Roboto", 100F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.sumRectPatientsCountTxt.ForeColor = System.Drawing.Color.Black;
            this.sumRectPatientsCountTxt.Location = new System.Drawing.Point(400, 16);
            this.sumRectPatientsCountTxt.Name = "sumRectPatientsCountTxt";
            this.sumRectPatientsCountTxt.Size = new System.Drawing.Size(188, 130);
            this.sumRectPatientsCountTxt.TabIndex = 7;
            this.sumRectPatientsCountTxt.Text = "34";
            this.sumRectPatientsCountTxt.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel85
            // 
            this.panel85.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(56)))), ((int)(((byte)(64)))));
            this.panel85.Controls.Add(this.sumRectTotPatientsTxt);
            this.panel85.Controls.Add(this.label132);
            this.panel85.Location = new System.Drawing.Point(0, 0);
            this.panel85.Name = "panel85";
            this.panel85.Size = new System.Drawing.Size(394, 200);
            this.panel85.TabIndex = 6;
            // 
            // sumRectTotPatientsTxt
            // 
            this.sumRectTotPatientsTxt.Font = new System.Drawing.Font("Roboto", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.sumRectTotPatientsTxt.ForeColor = System.Drawing.Color.White;
            this.sumRectTotPatientsTxt.Location = new System.Drawing.Point(31, 54);
            this.sumRectTotPatientsTxt.Name = "sumRectTotPatientsTxt";
            this.sumRectTotPatientsTxt.Size = new System.Drawing.Size(257, 126);
            this.sumRectTotPatientsTxt.TabIndex = 5;
            this.sumRectTotPatientsTxt.Text = "Total 34 Patients";
            this.sumRectTotPatientsTxt.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label132
            // 
            this.label132.Font = new System.Drawing.Font("Roboto", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.label132.ForeColor = System.Drawing.Color.White;
            this.label132.Location = new System.Drawing.Point(31, 13);
            this.label132.Name = "label132";
            this.label132.Size = new System.Drawing.Size(308, 41);
            this.label132.TabIndex = 4;
            this.label132.Text = "Patients";
            this.label132.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label133
            // 
            this.label133.Font = new System.Drawing.Font("Roboto", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.label133.ForeColor = System.Drawing.Color.Black;
            this.label133.Location = new System.Drawing.Point(400, 146);
            this.label133.Name = "label133";
            this.label133.Size = new System.Drawing.Size(188, 34);
            this.label133.TabIndex = 2;
            this.label133.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel82
            // 
            this.panel82.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(201)))), ((int)(((byte)(31)))));
            this.panel82.Controls.Add(this.sumRectMonthsTxt);
            this.panel82.Controls.Add(this.panel83);
            this.panel82.Controls.Add(this.label123);
            this.panel82.Location = new System.Drawing.Point(21, 9);
            this.panel82.Margin = new System.Windows.Forms.Padding(80, 40, 80, 40);
            this.panel82.Name = "panel82";
            this.panel82.Size = new System.Drawing.Size(591, 202);
            this.panel82.TabIndex = 1;
            // 
            // sumRectMonthsTxt
            // 
            this.sumRectMonthsTxt.Font = new System.Drawing.Font("Roboto", 100F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.sumRectMonthsTxt.ForeColor = System.Drawing.Color.Black;
            this.sumRectMonthsTxt.Location = new System.Drawing.Point(400, 16);
            this.sumRectMonthsTxt.Name = "sumRectMonthsTxt";
            this.sumRectMonthsTxt.Size = new System.Drawing.Size(188, 130);
            this.sumRectMonthsTxt.TabIndex = 7;
            this.sumRectMonthsTxt.Text = "01";
            this.sumRectMonthsTxt.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel83
            // 
            this.panel83.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(56)))), ((int)(((byte)(64)))));
            this.panel83.Controls.Add(this.sumRectDurationTxt);
            this.panel83.Controls.Add(this.label122);
            this.panel83.Location = new System.Drawing.Point(0, 0);
            this.panel83.Name = "panel83";
            this.panel83.Size = new System.Drawing.Size(394, 200);
            this.panel83.TabIndex = 6;
            // 
            // sumRectDurationTxt
            // 
            this.sumRectDurationTxt.Font = new System.Drawing.Font("Roboto", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.sumRectDurationTxt.ForeColor = System.Drawing.Color.White;
            this.sumRectDurationTxt.Location = new System.Drawing.Point(31, 54);
            this.sumRectDurationTxt.Name = "sumRectDurationTxt";
            this.sumRectDurationTxt.Size = new System.Drawing.Size(330, 126);
            this.sumRectDurationTxt.TabIndex = 5;
            this.sumRectDurationTxt.Text = "From 04, 2019 \r\nTo 04, 2019";
            this.sumRectDurationTxt.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label122
            // 
            this.label122.Font = new System.Drawing.Font("Roboto", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.label122.ForeColor = System.Drawing.Color.White;
            this.label122.Location = new System.Drawing.Point(31, 13);
            this.label122.Name = "label122";
            this.label122.Size = new System.Drawing.Size(308, 41);
            this.label122.TabIndex = 4;
            this.label122.Text = "Duration";
            this.label122.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label123
            // 
            this.label123.Font = new System.Drawing.Font("Roboto", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.label123.ForeColor = System.Drawing.Color.Black;
            this.label123.Location = new System.Drawing.Point(400, 146);
            this.label123.Name = "label123";
            this.label123.Size = new System.Drawing.Size(188, 34);
            this.label123.TabIndex = 2;
            this.label123.Text = "Months";
            this.label123.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // generalReportsListView
            // 
            this.generalReportsListView.AllowUserToAddRows = false;
            this.generalReportsListView.AllowUserToDeleteRows = false;
            this.generalReportsListView.AllowUserToResizeColumns = false;
            this.generalReportsListView.AllowUserToResizeRows = false;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control;
            this.generalReportsListView.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle6;
            this.generalReportsListView.BackgroundColor = System.Drawing.SystemColors.Control;
            this.generalReportsListView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.generalReportsListView.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal;
            this.generalReportsListView.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Roboto", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.generalReportsListView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.generalReportsListView.ColumnHeadersHeight = 65;
            this.generalReportsListView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.generalReportsListView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.No,
            this.Date,
            this.PatientNum,
            this.Exercises,
            this.PracticeTime});
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle13.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle13.Font = new System.Drawing.Font("Roboto", 28F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            dataGridViewCellStyle13.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle13.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle13.SelectionForeColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle13.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.generalReportsListView.DefaultCellStyle = dataGridViewCellStyle13;
            this.generalReportsListView.GridColor = System.Drawing.SystemColors.Control;
            this.generalReportsListView.Location = new System.Drawing.Point(32, 319);
            this.generalReportsListView.Name = "generalReportsListView";
            this.generalReportsListView.ReadOnly = true;
            this.generalReportsListView.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle14.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle14.Font = new System.Drawing.Font("Roboto", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle14.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle14.SelectionBackColor = System.Drawing.SystemColors.ControlLight;
            dataGridViewCellStyle14.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.generalReportsListView.RowHeadersDefaultCellStyle = dataGridViewCellStyle14;
            this.generalReportsListView.RowHeadersVisible = false;
            dataGridViewCellStyle15.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle15.Font = new System.Drawing.Font("Roboto", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle15.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle15.SelectionBackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle15.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.generalReportsListView.RowsDefaultCellStyle = dataGridViewCellStyle15;
            this.generalReportsListView.RowTemplate.DefaultCellStyle.BackColor = System.Drawing.SystemColors.Control;
            this.generalReportsListView.RowTemplate.DividerHeight = 1;
            this.generalReportsListView.RowTemplate.Height = 55;
            this.generalReportsListView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.generalReportsListView.Size = new System.Drawing.Size(1273, 731);
            this.generalReportsListView.TabIndex = 15;
            // 
            // No
            // 
            dataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.Control;
            this.No.DefaultCellStyle = dataGridViewCellStyle8;
            this.No.HeaderText = "No.";
            this.No.Name = "No";
            this.No.ReadOnly = true;
            this.No.Width = 150;
            // 
            // Date
            // 
            dataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle9.Format = "d";
            dataGridViewCellStyle9.NullValue = null;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.Control;
            this.Date.DefaultCellStyle = dataGridViewCellStyle9;
            this.Date.HeaderText = "Date";
            this.Date.Name = "Date";
            this.Date.ReadOnly = true;
            this.Date.Width = 250;
            // 
            // PatientNum
            // 
            dataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.Control;
            this.PatientNum.DefaultCellStyle = dataGridViewCellStyle10;
            this.PatientNum.HeaderText = "Patient #";
            this.PatientNum.Name = "PatientNum";
            this.PatientNum.ReadOnly = true;
            this.PatientNum.Width = 300;
            // 
            // Exercises
            // 
            dataGridViewCellStyle11.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle11.ForeColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.Control;
            this.Exercises.DefaultCellStyle = dataGridViewCellStyle11;
            this.Exercises.HeaderText = "Exercises";
            this.Exercises.Name = "Exercises";
            this.Exercises.ReadOnly = true;
            this.Exercises.Width = 250;
            // 
            // PracticeTime
            // 
            dataGridViewCellStyle12.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle12.ForeColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.Control;
            this.PracticeTime.DefaultCellStyle = dataGridViewCellStyle12;
            this.PracticeTime.HeaderText = "Time";
            this.PracticeTime.Name = "PracticeTime";
            this.PracticeTime.ReadOnly = true;
            this.PracticeTime.Width = 250;
            // 
            // genRepShowBtn
            // 
            this.genRepShowBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(201)))), ((int)(((byte)(31)))));
            this.genRepShowBtn.Enabled = false;
            this.genRepShowBtn.FlatAppearance.BorderSize = 0;
            this.genRepShowBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.genRepShowBtn.Font = new System.Drawing.Font("Roboto", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.genRepShowBtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.genRepShowBtn.Location = new System.Drawing.Point(955, 221);
            this.genRepShowBtn.Name = "genRepShowBtn";
            this.genRepShowBtn.Size = new System.Drawing.Size(303, 48);
            this.genRepShowBtn.TabIndex = 14;
            this.genRepShowBtn.Text = "Show";
            this.genRepShowBtn.UseVisualStyleBackColor = false;
            this.genRepShowBtn.Click += new System.EventHandler(this.genRepShowBtn_Click);
            // 
            // panel81
            // 
            this.panel81.BackColor = System.Drawing.Color.White;
            this.panel81.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.panel81.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel81.Controls.Add(this.label121);
            this.panel81.Controls.Add(this.genRepPnameFilterText);
            this.panel81.Controls.Add(this.button7);
            this.panel81.Location = new System.Drawing.Point(36, 115);
            this.panel81.Name = "panel81";
            this.panel81.Size = new System.Drawing.Size(855, 50);
            this.panel81.TabIndex = 13;
            // 
            // label121
            // 
            this.label121.AutoSize = true;
            this.label121.Font = new System.Drawing.Font("Roboto", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.label121.ForeColor = System.Drawing.Color.Black;
            this.label121.Location = new System.Drawing.Point(10, 10);
            this.label121.Name = "label121";
            this.label121.Size = new System.Drawing.Size(131, 27);
            this.label121.TabIndex = 3;
            this.label121.Text = "All patients";
            // 
            // genRepPnameFilterText
            // 
            this.genRepPnameFilterText.BackColor = System.Drawing.Color.White;
            this.genRepPnameFilterText.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.genRepPnameFilterText.Font = new System.Drawing.Font("Roboto", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.genRepPnameFilterText.Location = new System.Drawing.Point(184, 12);
            this.genRepPnameFilterText.Name = "genRepPnameFilterText";
            this.genRepPnameFilterText.Size = new System.Drawing.Size(583, 25);
            this.genRepPnameFilterText.TabIndex = 2;
            this.genRepPnameFilterText.Text = "?";
            this.genRepPnameFilterText.TextChanged += new System.EventHandler(this.genRepPnameFilterText_TextChanged);
            // 
            // button7
            // 
            this.button7.BackgroundImage = global::SelfitMain.Properties.Resources.Search;
            this.button7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.button7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button7.ForeColor = System.Drawing.Color.White;
            this.button7.Location = new System.Drawing.Point(800, 9);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(28, 28);
            this.button7.TabIndex = 1;
            this.button7.UseVisualStyleBackColor = true;
            // 
            // panel77
            // 
            this.panel77.Controls.Add(this.panel78);
            this.panel77.Controls.Add(this.panel79);
            this.panel77.Controls.Add(this.label120);
            this.panel77.Location = new System.Drawing.Point(517, 193);
            this.panel77.Name = "panel77";
            this.panel77.Size = new System.Drawing.Size(374, 81);
            this.panel77.TabIndex = 11;
            // 
            // panel78
            // 
            this.panel78.BackColor = System.Drawing.Color.White;
            this.panel78.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel78.Controls.Add(this.genRepToYearCombo);
            this.panel78.Location = new System.Drawing.Point(197, 28);
            this.panel78.Name = "panel78";
            this.panel78.Size = new System.Drawing.Size(159, 48);
            this.panel78.TabIndex = 3;
            // 
            // genRepToYearCombo
            // 
            this.genRepToYearCombo.BackColor = System.Drawing.Color.White;
            this.genRepToYearCombo.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.genRepToYearCombo.Font = new System.Drawing.Font("Roboto", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.genRepToYearCombo.FormattingEnabled = true;
            this.genRepToYearCombo.Items.AddRange(new object[] {
            "2021",
            "2020",
            "2019",
            "2018"});
            this.genRepToYearCombo.Location = new System.Drawing.Point(24, 6);
            this.genRepToYearCombo.Name = "genRepToYearCombo";
            this.genRepToYearCombo.Size = new System.Drawing.Size(121, 35);
            this.genRepToYearCombo.TabIndex = 2;
            this.genRepToYearCombo.Text = "2020";
            // 
            // panel79
            // 
            this.panel79.BackColor = System.Drawing.Color.White;
            this.panel79.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel79.Controls.Add(this.genRepToMonthCombo);
            this.panel79.Location = new System.Drawing.Point(4, 28);
            this.panel79.Name = "panel79";
            this.panel79.Size = new System.Drawing.Size(157, 48);
            this.panel79.TabIndex = 2;
            // 
            // genRepToMonthCombo
            // 
            this.genRepToMonthCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.genRepToMonthCombo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.genRepToMonthCombo.Font = new System.Drawing.Font("Roboto", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.genRepToMonthCombo.FormattingEnabled = true;
            this.genRepToMonthCombo.Items.AddRange(new object[] {
            "Jan",
            "Feb",
            "March",
            "April",
            "May",
            "June",
            "July",
            "Aug",
            "Sep",
            "Oct",
            "Nov",
            "Dec"});
            this.genRepToMonthCombo.Location = new System.Drawing.Point(24, 6);
            this.genRepToMonthCombo.Name = "genRepToMonthCombo";
            this.genRepToMonthCombo.Size = new System.Drawing.Size(121, 35);
            this.genRepToMonthCombo.TabIndex = 1;
            this.genRepToMonthCombo.SelectedIndexChanged += new System.EventHandler(this.genRepToMonthCombo_SelectedIndexChanged);
            // 
            // label120
            // 
            this.label120.Font = new System.Drawing.Font("Roboto", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.label120.ForeColor = System.Drawing.Color.Black;
            this.label120.Location = new System.Drawing.Point(0, 2);
            this.label120.Name = "label120";
            this.label120.Size = new System.Drawing.Size(139, 39);
            this.label120.TabIndex = 0;
            this.label120.Text = "To";
            // 
            // panel74
            // 
            this.panel74.Controls.Add(this.panel75);
            this.panel74.Controls.Add(this.panel76);
            this.panel74.Controls.Add(this.label119);
            this.panel74.Location = new System.Drawing.Point(32, 193);
            this.panel74.Name = "panel74";
            this.panel74.Size = new System.Drawing.Size(374, 81);
            this.panel74.TabIndex = 10;
            // 
            // panel75
            // 
            this.panel75.BackColor = System.Drawing.Color.White;
            this.panel75.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel75.Controls.Add(this.genRepFromYearCombo);
            this.panel75.Location = new System.Drawing.Point(197, 28);
            this.panel75.Name = "panel75";
            this.panel75.Size = new System.Drawing.Size(160, 48);
            this.panel75.TabIndex = 3;
            // 
            // genRepFromYearCombo
            // 
            this.genRepFromYearCombo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.genRepFromYearCombo.Font = new System.Drawing.Font("Roboto", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.genRepFromYearCombo.FormattingEnabled = true;
            this.genRepFromYearCombo.Items.AddRange(new object[] {
            "2021",
            "2020",
            "2019",
            "2018"});
            this.genRepFromYearCombo.Location = new System.Drawing.Point(24, 6);
            this.genRepFromYearCombo.Name = "genRepFromYearCombo";
            this.genRepFromYearCombo.Size = new System.Drawing.Size(121, 35);
            this.genRepFromYearCombo.TabIndex = 2;
            this.genRepFromYearCombo.Text = "2020";
            // 
            // panel76
            // 
            this.panel76.BackColor = System.Drawing.Color.White;
            this.panel76.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel76.Controls.Add(this.genRepFromMonthCombo);
            this.panel76.Location = new System.Drawing.Point(4, 28);
            this.panel76.Name = "panel76";
            this.panel76.Size = new System.Drawing.Size(162, 48);
            this.panel76.TabIndex = 2;
            // 
            // genRepFromMonthCombo
            // 
            this.genRepFromMonthCombo.BackColor = System.Drawing.Color.White;
            this.genRepFromMonthCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.genRepFromMonthCombo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.genRepFromMonthCombo.Font = new System.Drawing.Font("Roboto", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.genRepFromMonthCombo.FormattingEnabled = true;
            this.genRepFromMonthCombo.Items.AddRange(new object[] {
            "Jan",
            "Feb",
            "March",
            "April",
            "May",
            "June",
            "July",
            "Aug",
            "Sep",
            "Oct",
            "Nov",
            "Dec"});
            this.genRepFromMonthCombo.Location = new System.Drawing.Point(24, 6);
            this.genRepFromMonthCombo.Name = "genRepFromMonthCombo";
            this.genRepFromMonthCombo.Size = new System.Drawing.Size(121, 35);
            this.genRepFromMonthCombo.TabIndex = 1;
            this.genRepFromMonthCombo.SelectedIndexChanged += new System.EventHandler(this.genRepFromMonthCombo_SelectedIndexChanged);
            // 
            // label119
            // 
            this.label119.Font = new System.Drawing.Font("Roboto", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.label119.ForeColor = System.Drawing.Color.Black;
            this.label119.Location = new System.Drawing.Point(0, 2);
            this.label119.Name = "label119";
            this.label119.Size = new System.Drawing.Size(139, 39);
            this.label119.TabIndex = 0;
            this.label119.Text = "From";
            // 
            // panel46
            // 
            this.panel46.BackColor = System.Drawing.Color.Black;
            this.panel46.Controls.Add(this.button4);
            this.panel46.Controls.Add(this.label117);
            this.panel46.Location = new System.Drawing.Point(0, 0);
            this.panel46.Name = "panel46";
            this.panel46.Size = new System.Drawing.Size(1937, 96);
            this.panel46.TabIndex = 2;
            // 
            // button4
            // 
            this.button4.FlatAppearance.BorderSize = 0;
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button4.Image = global::SelfitMain.Properties.Resources.reportsBack;
            this.button4.Location = new System.Drawing.Point(32, 14);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(60, 60);
            this.button4.TabIndex = 4;
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // label117
            // 
            this.label117.Font = new System.Drawing.Font("Roboto", 50F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.label117.ForeColor = System.Drawing.Color.White;
            this.label117.Location = new System.Drawing.Point(148, 14);
            this.label117.Name = "label117";
            this.label117.Size = new System.Drawing.Size(295, 60);
            this.label117.TabIndex = 1;
            this.label117.Text = "Reports";
            this.label117.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // QuickPlan
            // 
            this.QuickPlan.BackColor = System.Drawing.Color.White;
            this.QuickPlan.Controls.Add(this.qpSelectNoneBtn);
            this.QuickPlan.Controls.Add(this.qpSelectAllBtn);
            this.QuickPlan.Controls.Add(this.button25);
            this.QuickPlan.Controls.Add(this.button14);
            this.QuickPlan.Controls.Add(this.qpStartPlanBtn);
            this.QuickPlan.Controls.Add(this.sittingBtn);
            this.QuickPlan.Controls.Add(this.duration30mBtn);
            this.QuickPlan.Controls.Add(this.duration25mBtn);
            this.QuickPlan.Controls.Add(this.duration20mBtn);
            this.QuickPlan.Controls.Add(this.duration15mBtn);
            this.QuickPlan.Controls.Add(this.duration10mBtn);
            this.QuickPlan.Controls.Add(this.duration5mBtn);
            this.QuickPlan.Controls.Add(this.button13);
            this.QuickPlan.Controls.Add(this.button12);
            this.QuickPlan.Controls.Add(this.maleSelectBtn);
            this.QuickPlan.Controls.Add(this.panel97);
            this.QuickPlan.Controls.Add(this.panel96);
            this.QuickPlan.Controls.Add(this.panel95);
            this.QuickPlan.Controls.Add(this.panel93);
            this.QuickPlan.Controls.Add(this.qpAerobicBtn);
            this.QuickPlan.Controls.Add(this.qpAgilityBtn);
            this.QuickPlan.Controls.Add(this.qpCoordinationBtn);
            this.QuickPlan.Controls.Add(this.qpGaitBtn);
            this.QuickPlan.Controls.Add(this.qpReactiontimeBtn);
            this.QuickPlan.Controls.Add(this.qpMultitaskingBtn);
            this.QuickPlan.Controls.Add(this.qpMovementplanningBtn);
            this.QuickPlan.Controls.Add(this.qpBalanceBtn);
            this.QuickPlan.Controls.Add(this.standingBtn);
            this.QuickPlan.Location = new System.Drawing.Point(4, 23);
            this.QuickPlan.Name = "QuickPlan";
            this.QuickPlan.Padding = new System.Windows.Forms.Padding(3);
            this.QuickPlan.Size = new System.Drawing.Size(1910, 1172);
            this.QuickPlan.TabIndex = 12;
            this.QuickPlan.Text = "QuickPlan";
            // 
            // qpSelectNoneBtn
            // 
            this.qpSelectNoneBtn.BackColor = System.Drawing.Color.White;
            this.qpSelectNoneBtn.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.qpSelectNoneBtn.FlatAppearance.BorderSize = 2;
            this.qpSelectNoneBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.qpSelectNoneBtn.Font = new System.Drawing.Font("Roboto", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.qpSelectNoneBtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(201)))), ((int)(((byte)(31)))));
            this.qpSelectNoneBtn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.qpSelectNoneBtn.Location = new System.Drawing.Point(1686, 453);
            this.qpSelectNoneBtn.Name = "qpSelectNoneBtn";
            this.qpSelectNoneBtn.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.qpSelectNoneBtn.Size = new System.Drawing.Size(101, 68);
            this.qpSelectNoneBtn.TabIndex = 34;
            this.qpSelectNoneBtn.Text = "None";
            this.qpSelectNoneBtn.UseVisualStyleBackColor = false;
            this.qpSelectNoneBtn.Click += new System.EventHandler(this.qpSelectNoneBtn_Click);
            // 
            // qpSelectAllBtn
            // 
            this.qpSelectAllBtn.BackColor = System.Drawing.Color.White;
            this.qpSelectAllBtn.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.qpSelectAllBtn.FlatAppearance.BorderSize = 2;
            this.qpSelectAllBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.qpSelectAllBtn.Font = new System.Drawing.Font("Roboto", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.qpSelectAllBtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(201)))), ((int)(((byte)(31)))));
            this.qpSelectAllBtn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.qpSelectAllBtn.Location = new System.Drawing.Point(1568, 453);
            this.qpSelectAllBtn.Name = "qpSelectAllBtn";
            this.qpSelectAllBtn.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.qpSelectAllBtn.Size = new System.Drawing.Size(99, 68);
            this.qpSelectAllBtn.TabIndex = 33;
            this.qpSelectAllBtn.Text = "All";
            this.qpSelectAllBtn.UseVisualStyleBackColor = false;
            this.qpSelectAllBtn.Click += new System.EventHandler(this.qpSelectAllBtn_Click);
            // 
            // button25
            // 
            this.button25.BackColor = System.Drawing.Color.White;
            this.button25.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button25.FlatAppearance.BorderSize = 3;
            this.button25.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button25.Font = new System.Drawing.Font("Roboto", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.button25.ForeColor = System.Drawing.Color.Black;
            this.button25.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button25.Location = new System.Drawing.Point(1664, 454);
            this.button25.Name = "button25";
            this.button25.Size = new System.Drawing.Size(27, 68);
            this.button25.TabIndex = 32;
            this.button25.Text = "/";
            this.button25.UseVisualStyleBackColor = false;
            // 
            // button14
            // 
            this.button14.BackColor = System.Drawing.Color.White;
            this.button14.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button14.FlatAppearance.BorderSize = 3;
            this.button14.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button14.Font = new System.Drawing.Font("Roboto", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.button14.ForeColor = System.Drawing.Color.Black;
            this.button14.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button14.Location = new System.Drawing.Point(1480, 454);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(103, 68);
            this.button14.TabIndex = 31;
            this.button14.Text = "Select";
            this.button14.UseVisualStyleBackColor = false;
            // 
            // qpStartPlanBtn
            // 
            this.qpStartPlanBtn.BackColor = System.Drawing.Color.Silver;
            this.qpStartPlanBtn.Enabled = false;
            this.qpStartPlanBtn.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.qpStartPlanBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.qpStartPlanBtn.Font = new System.Drawing.Font("Roboto", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.qpStartPlanBtn.ForeColor = System.Drawing.Color.Black;
            this.qpStartPlanBtn.Location = new System.Drawing.Point(320, 1066);
            this.qpStartPlanBtn.Name = "qpStartPlanBtn";
            this.qpStartPlanBtn.Size = new System.Drawing.Size(1467, 60);
            this.qpStartPlanBtn.TabIndex = 30;
            this.qpStartPlanBtn.Text = "Start Plan";
            this.qpStartPlanBtn.UseVisualStyleBackColor = false;
            this.qpStartPlanBtn.Click += new System.EventHandler(this.qpStartPlanBtn_Click);
            // 
            // sittingBtn
            // 
            this.sittingBtn.BackColor = System.Drawing.Color.White;
            this.sittingBtn.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.sittingBtn.FlatAppearance.BorderSize = 3;
            this.sittingBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.sittingBtn.Font = new System.Drawing.Font("Roboto", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.sittingBtn.ForeColor = System.Drawing.Color.Black;
            this.sittingBtn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.sittingBtn.Location = new System.Drawing.Point(981, 307);
            this.sittingBtn.Name = "sittingBtn";
            this.sittingBtn.Size = new System.Drawing.Size(382, 68);
            this.sittingBtn.TabIndex = 17;
            this.sittingBtn.Text = "Sitting";
            this.sittingBtn.UseVisualStyleBackColor = false;
            this.sittingBtn.Click += new System.EventHandler(this.sittingBtn_Click);
            // 
            // duration30mBtn
            // 
            this.duration30mBtn.BackColor = System.Drawing.Color.White;
            this.duration30mBtn.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.duration30mBtn.FlatAppearance.BorderSize = 3;
            this.duration30mBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.duration30mBtn.Font = new System.Drawing.Font("Roboto", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.duration30mBtn.ForeColor = System.Drawing.Color.Black;
            this.duration30mBtn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.duration30mBtn.Location = new System.Drawing.Point(1617, 167);
            this.duration30mBtn.Name = "duration30mBtn";
            this.duration30mBtn.Size = new System.Drawing.Size(170, 68);
            this.duration30mBtn.TabIndex = 15;
            this.duration30mBtn.Text = "30m";
            this.duration30mBtn.UseVisualStyleBackColor = false;
            this.duration30mBtn.Click += new System.EventHandler(this.duration30mBtn_Click);
            // 
            // duration25mBtn
            // 
            this.duration25mBtn.BackColor = System.Drawing.Color.White;
            this.duration25mBtn.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.duration25mBtn.FlatAppearance.BorderSize = 3;
            this.duration25mBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.duration25mBtn.Font = new System.Drawing.Font("Roboto", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.duration25mBtn.ForeColor = System.Drawing.Color.Black;
            this.duration25mBtn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.duration25mBtn.Location = new System.Drawing.Point(1405, 167);
            this.duration25mBtn.Name = "duration25mBtn";
            this.duration25mBtn.Size = new System.Drawing.Size(170, 68);
            this.duration25mBtn.TabIndex = 14;
            this.duration25mBtn.Text = "25m";
            this.duration25mBtn.UseVisualStyleBackColor = false;
            this.duration25mBtn.Click += new System.EventHandler(this.duration25mBtn_Click);
            // 
            // duration20mBtn
            // 
            this.duration20mBtn.BackColor = System.Drawing.Color.White;
            this.duration20mBtn.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.duration20mBtn.FlatAppearance.BorderSize = 3;
            this.duration20mBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.duration20mBtn.Font = new System.Drawing.Font("Roboto", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.duration20mBtn.ForeColor = System.Drawing.Color.Black;
            this.duration20mBtn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.duration20mBtn.Location = new System.Drawing.Point(1193, 167);
            this.duration20mBtn.Name = "duration20mBtn";
            this.duration20mBtn.Size = new System.Drawing.Size(170, 68);
            this.duration20mBtn.TabIndex = 13;
            this.duration20mBtn.Text = "20m";
            this.duration20mBtn.UseVisualStyleBackColor = false;
            this.duration20mBtn.Click += new System.EventHandler(this.duration20mBtn_Click);
            // 
            // duration15mBtn
            // 
            this.duration15mBtn.BackColor = System.Drawing.Color.White;
            this.duration15mBtn.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.duration15mBtn.FlatAppearance.BorderSize = 3;
            this.duration15mBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.duration15mBtn.Font = new System.Drawing.Font("Roboto", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.duration15mBtn.ForeColor = System.Drawing.Color.Black;
            this.duration15mBtn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.duration15mBtn.Location = new System.Drawing.Point(981, 167);
            this.duration15mBtn.Name = "duration15mBtn";
            this.duration15mBtn.Size = new System.Drawing.Size(170, 68);
            this.duration15mBtn.TabIndex = 12;
            this.duration15mBtn.Text = "15m";
            this.duration15mBtn.UseVisualStyleBackColor = false;
            this.duration15mBtn.Click += new System.EventHandler(this.duration15mBtn_Click);
            // 
            // duration10mBtn
            // 
            this.duration10mBtn.BackColor = System.Drawing.Color.White;
            this.duration10mBtn.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.duration10mBtn.FlatAppearance.BorderSize = 3;
            this.duration10mBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.duration10mBtn.Font = new System.Drawing.Font("Roboto", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.duration10mBtn.ForeColor = System.Drawing.Color.Black;
            this.duration10mBtn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.duration10mBtn.Location = new System.Drawing.Point(769, 167);
            this.duration10mBtn.Name = "duration10mBtn";
            this.duration10mBtn.Size = new System.Drawing.Size(170, 68);
            this.duration10mBtn.TabIndex = 11;
            this.duration10mBtn.Text = "10m";
            this.duration10mBtn.UseVisualStyleBackColor = false;
            this.duration10mBtn.Click += new System.EventHandler(this.duration10mBtn_Click);
            // 
            // duration5mBtn
            // 
            this.duration5mBtn.BackColor = System.Drawing.Color.White;
            this.duration5mBtn.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.duration5mBtn.FlatAppearance.BorderSize = 3;
            this.duration5mBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.duration5mBtn.Font = new System.Drawing.Font("Roboto", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.duration5mBtn.ForeColor = System.Drawing.Color.Black;
            this.duration5mBtn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.duration5mBtn.Location = new System.Drawing.Point(557, 167);
            this.duration5mBtn.Name = "duration5mBtn";
            this.duration5mBtn.Size = new System.Drawing.Size(170, 68);
            this.duration5mBtn.TabIndex = 10;
            this.duration5mBtn.Text = "5m";
            this.duration5mBtn.UseVisualStyleBackColor = false;
            this.duration5mBtn.Click += new System.EventHandler(this.duration5mBtn_Click);
            // 
            // button13
            // 
            this.button13.BackColor = System.Drawing.Color.White;
            this.button13.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button13.FlatAppearance.BorderSize = 3;
            this.button13.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button13.Font = new System.Drawing.Font("Roboto", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.button13.ForeColor = System.Drawing.Color.Black;
            this.button13.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button13.Location = new System.Drawing.Point(297, 454);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(278, 68);
            this.button13.TabIndex = 9;
            this.button13.Text = "Select categories";
            this.button13.UseVisualStyleBackColor = false;
            // 
            // button12
            // 
            this.button12.BackColor = System.Drawing.Color.White;
            this.button12.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button12.FlatAppearance.BorderSize = 3;
            this.button12.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button12.Font = new System.Drawing.Font("Roboto", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.button12.ForeColor = System.Drawing.Color.Black;
            this.button12.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button12.Location = new System.Drawing.Point(297, 304);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(170, 68);
            this.button12.TabIndex = 8;
            this.button12.Text = "Posture";
            this.button12.UseVisualStyleBackColor = false;
            // 
            // maleSelectBtn
            // 
            this.maleSelectBtn.BackColor = System.Drawing.Color.White;
            this.maleSelectBtn.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.maleSelectBtn.FlatAppearance.BorderSize = 3;
            this.maleSelectBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.maleSelectBtn.Font = new System.Drawing.Font("Roboto", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.maleSelectBtn.ForeColor = System.Drawing.Color.Black;
            this.maleSelectBtn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.maleSelectBtn.Location = new System.Drawing.Point(297, 167);
            this.maleSelectBtn.Name = "maleSelectBtn";
            this.maleSelectBtn.Size = new System.Drawing.Size(170, 68);
            this.maleSelectBtn.TabIndex = 7;
            this.maleSelectBtn.Text = "Duration";
            this.maleSelectBtn.UseVisualStyleBackColor = false;
            // 
            // panel97
            // 
            this.panel97.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(56)))), ((int)(((byte)(64)))));
            this.panel97.Controls.Add(this.qPlanReportsBtn);
            this.panel97.Controls.Add(this.qPlanHistoryBtn);
            this.panel97.Controls.Add(this.qPlanPlanBtn);
            this.panel97.Location = new System.Drawing.Point(0, 527);
            this.panel97.Name = "panel97";
            this.panel97.Size = new System.Drawing.Size(150, 663);
            this.panel97.TabIndex = 6;
            // 
            // qPlanReportsBtn
            // 
            this.qPlanReportsBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.qPlanReportsBtn.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(56)))), ((int)(((byte)(64)))));
            this.qPlanReportsBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.qPlanReportsBtn.Font = new System.Drawing.Font("Roboto", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.qPlanReportsBtn.ForeColor = System.Drawing.Color.White;
            this.qPlanReportsBtn.Image = global::SelfitMain.Properties.Resources.reportsBtn;
            this.qPlanReportsBtn.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.qPlanReportsBtn.Location = new System.Drawing.Point(0, 471);
            this.qPlanReportsBtn.Name = "qPlanReportsBtn";
            this.qPlanReportsBtn.Size = new System.Drawing.Size(150, 94);
            this.qPlanReportsBtn.TabIndex = 3;
            this.qPlanReportsBtn.Text = "Reports";
            this.qPlanReportsBtn.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.qPlanReportsBtn.UseVisualStyleBackColor = true;
            this.qPlanReportsBtn.Click += new System.EventHandler(this.qPlanReportsBtn_Click);
            // 
            // qPlanHistoryBtn
            // 
            this.qPlanHistoryBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(56)))), ((int)(((byte)(64)))));
            this.qPlanHistoryBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.qPlanHistoryBtn.Font = new System.Drawing.Font("Roboto", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.qPlanHistoryBtn.ForeColor = System.Drawing.Color.White;
            this.qPlanHistoryBtn.Image = global::SelfitMain.Properties.Resources.Group1;
            this.qPlanHistoryBtn.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.qPlanHistoryBtn.Location = new System.Drawing.Point(-4, 60);
            this.qPlanHistoryBtn.Name = "qPlanHistoryBtn";
            this.qPlanHistoryBtn.Size = new System.Drawing.Size(151, 110);
            this.qPlanHistoryBtn.TabIndex = 0;
            this.qPlanHistoryBtn.Text = "Patient History";
            this.qPlanHistoryBtn.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.qPlanHistoryBtn.Click += new System.EventHandler(this.qPlanHistoryBtn_Click);
            // 
            // qPlanPlanBtn
            // 
            this.qPlanPlanBtn.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(56)))), ((int)(((byte)(64)))));
            this.qPlanPlanBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.qPlanPlanBtn.Font = new System.Drawing.Font("Roboto", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.qPlanPlanBtn.ForeColor = System.Drawing.Color.White;
            this.qPlanPlanBtn.Image = global::SelfitMain.Properties.Resources.Star;
            this.qPlanPlanBtn.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.qPlanPlanBtn.Location = new System.Drawing.Point(0, 260);
            this.qPlanPlanBtn.Name = "qPlanPlanBtn";
            this.qPlanPlanBtn.Size = new System.Drawing.Size(147, 138);
            this.qPlanPlanBtn.TabIndex = 1;
            this.qPlanPlanBtn.Text = "Saved Plans";
            this.qPlanPlanBtn.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.qPlanPlanBtn.UseVisualStyleBackColor = true;
            this.qPlanPlanBtn.Click += new System.EventHandler(this.qPlanPlanBtn_Click);
            // 
            // panel96
            // 
            this.panel96.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(56)))), ((int)(((byte)(64)))));
            this.panel96.Controls.Add(this.qPlanExercisesBtn);
            this.panel96.Location = new System.Drawing.Point(0, 306);
            this.panel96.Name = "panel96";
            this.panel96.Size = new System.Drawing.Size(150, 222);
            this.panel96.TabIndex = 5;
            // 
            // qPlanExercisesBtn
            // 
            this.qPlanExercisesBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.qPlanExercisesBtn.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(56)))), ((int)(((byte)(64)))));
            this.qPlanExercisesBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.qPlanExercisesBtn.Font = new System.Drawing.Font("Roboto", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.qPlanExercisesBtn.ForeColor = System.Drawing.Color.White;
            this.qPlanExercisesBtn.Image = global::SelfitMain.Properties.Resources.HistoryaddPatient;
            this.qPlanExercisesBtn.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.qPlanExercisesBtn.Location = new System.Drawing.Point(0, 55);
            this.qPlanExercisesBtn.Name = "qPlanExercisesBtn";
            this.qPlanExercisesBtn.Size = new System.Drawing.Size(150, 113);
            this.qPlanExercisesBtn.TabIndex = 1;
            this.qPlanExercisesBtn.Text = "Exercises";
            this.qPlanExercisesBtn.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.qPlanExercisesBtn.UseVisualStyleBackColor = true;
            this.qPlanExercisesBtn.Click += new System.EventHandler(this.qPlanExercisesBtn_Click);
            // 
            // panel95
            // 
            this.panel95.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(201)))), ((int)(((byte)(31)))));
            this.panel95.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.panel95.Controls.Add(this.button9);
            this.panel95.Location = new System.Drawing.Point(0, 96);
            this.panel95.Name = "panel95";
            this.panel95.Size = new System.Drawing.Size(150, 212);
            this.panel95.TabIndex = 4;
            // 
            // button9
            // 
            this.button9.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button9.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(201)))), ((int)(((byte)(31)))));
            this.button9.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button9.Font = new System.Drawing.Font("Roboto", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.button9.ForeColor = System.Drawing.Color.Black;
            this.button9.Image = global::SelfitMain.Properties.Resources.QuickPlanBlack;
            this.button9.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.button9.Location = new System.Drawing.Point(11, 46);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(125, 128);
            this.button9.TabIndex = 2;
            this.button9.Text = "Quick Plan";
            this.button9.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.button9.UseVisualStyleBackColor = true;
            // 
            // panel93
            // 
            this.panel93.BackColor = System.Drawing.Color.Black;
            this.panel93.Controls.Add(this.button5);
            this.panel93.Controls.Add(this.panel94);
            this.panel93.Controls.Add(this.qpEditBtn);
            this.panel93.Controls.Add(this.qpPatientNameLbl);
            this.panel93.Location = new System.Drawing.Point(0, 0);
            this.panel93.Name = "panel93";
            this.panel93.Size = new System.Drawing.Size(1907, 96);
            this.panel93.TabIndex = 2;
            // 
            // button5
            // 
            this.button5.FlatAppearance.BorderSize = 0;
            this.button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button5.Image = global::SelfitMain.Properties.Resources.exitYellow;
            this.button5.Location = new System.Drawing.Point(1815, 22);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(60, 60);
            this.button5.TabIndex = 4;
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // panel94
            // 
            this.panel94.ForeColor = System.Drawing.Color.Black;
            this.panel94.Location = new System.Drawing.Point(129, 94);
            this.panel94.Name = "panel94";
            this.panel94.Size = new System.Drawing.Size(21, 592);
            this.panel94.TabIndex = 1;
            // 
            // qpEditBtn
            // 
            this.qpEditBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(201)))), ((int)(((byte)(31)))));
            this.qpEditBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.qpEditBtn.Font = new System.Drawing.Font("Roboto", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.qpEditBtn.Location = new System.Drawing.Point(297, 29);
            this.qpEditBtn.Name = "qpEditBtn";
            this.qpEditBtn.Size = new System.Drawing.Size(170, 37);
            this.qpEditBtn.TabIndex = 2;
            this.qpEditBtn.Text = "View && Edit";
            this.qpEditBtn.UseVisualStyleBackColor = false;
            this.qpEditBtn.Click += new System.EventHandler(this.qpEditBtn_Click);
            // 
            // qpPatientNameLbl
            // 
            this.qpPatientNameLbl.Font = new System.Drawing.Font("Roboto", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.qpPatientNameLbl.ForeColor = System.Drawing.Color.White;
            this.qpPatientNameLbl.Location = new System.Drawing.Point(39, 29);
            this.qpPatientNameLbl.Name = "qpPatientNameLbl";
            this.qpPatientNameLbl.Size = new System.Drawing.Size(295, 34);
            this.qpPatientNameLbl.TabIndex = 1;
            this.qpPatientNameLbl.Text = "Mr. Jhon Adams";
            this.qpPatientNameLbl.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // qpAerobicBtn
            // 
            this.qpAerobicBtn.BackColor = System.Drawing.Color.White;
            this.qpAerobicBtn.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(182)))), ((int)(((byte)(182)))), ((int)(((byte)(182)))));
            this.qpAerobicBtn.FlatAppearance.BorderSize = 2;
            this.qpAerobicBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.qpAerobicBtn.Font = new System.Drawing.Font("Roboto", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.qpAerobicBtn.ForeColor = System.Drawing.Color.Black;
            this.qpAerobicBtn.Image = global::SelfitMain.Properties.Resources.Aerobic;
            this.qpAerobicBtn.Location = new System.Drawing.Point(1442, 797);
            this.qpAerobicBtn.Name = "qpAerobicBtn";
            this.qpAerobicBtn.Size = new System.Drawing.Size(345, 234);
            this.qpAerobicBtn.TabIndex = 25;
            this.qpAerobicBtn.Text = "Aerobic";
            this.qpAerobicBtn.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.qpAerobicBtn.UseVisualStyleBackColor = false;
            this.qpAerobicBtn.Click += new System.EventHandler(this.qpAerobicBtn_Click);
            // 
            // qpAgilityBtn
            // 
            this.qpAgilityBtn.BackColor = System.Drawing.Color.White;
            this.qpAgilityBtn.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(182)))), ((int)(((byte)(182)))), ((int)(((byte)(182)))));
            this.qpAgilityBtn.FlatAppearance.BorderSize = 2;
            this.qpAgilityBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.qpAgilityBtn.Font = new System.Drawing.Font("Roboto", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.qpAgilityBtn.ForeColor = System.Drawing.Color.Black;
            this.qpAgilityBtn.Image = global::SelfitMain.Properties.Resources.Agility;
            this.qpAgilityBtn.Location = new System.Drawing.Point(1068, 797);
            this.qpAgilityBtn.Name = "qpAgilityBtn";
            this.qpAgilityBtn.Size = new System.Drawing.Size(345, 234);
            this.qpAgilityBtn.TabIndex = 24;
            this.qpAgilityBtn.Text = "Agility";
            this.qpAgilityBtn.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.qpAgilityBtn.UseVisualStyleBackColor = false;
            this.qpAgilityBtn.Click += new System.EventHandler(this.qpAgilityBtn_Click);
            // 
            // qpCoordinationBtn
            // 
            this.qpCoordinationBtn.BackColor = System.Drawing.Color.White;
            this.qpCoordinationBtn.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(182)))), ((int)(((byte)(182)))), ((int)(((byte)(182)))));
            this.qpCoordinationBtn.FlatAppearance.BorderSize = 2;
            this.qpCoordinationBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.qpCoordinationBtn.Font = new System.Drawing.Font("Roboto", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.qpCoordinationBtn.ForeColor = System.Drawing.Color.Black;
            this.qpCoordinationBtn.Image = global::SelfitMain.Properties.Resources.Coordination2;
            this.qpCoordinationBtn.Location = new System.Drawing.Point(694, 797);
            this.qpCoordinationBtn.Name = "qpCoordinationBtn";
            this.qpCoordinationBtn.Size = new System.Drawing.Size(345, 234);
            this.qpCoordinationBtn.TabIndex = 23;
            this.qpCoordinationBtn.Text = "Coordination";
            this.qpCoordinationBtn.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.qpCoordinationBtn.UseVisualStyleBackColor = false;
            this.qpCoordinationBtn.Click += new System.EventHandler(this.qpCoordinationBtn_Click);
            // 
            // qpGaitBtn
            // 
            this.qpGaitBtn.BackColor = System.Drawing.Color.White;
            this.qpGaitBtn.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(182)))), ((int)(((byte)(182)))), ((int)(((byte)(182)))));
            this.qpGaitBtn.FlatAppearance.BorderSize = 2;
            this.qpGaitBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.qpGaitBtn.Font = new System.Drawing.Font("Roboto", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.qpGaitBtn.ForeColor = System.Drawing.Color.Black;
            this.qpGaitBtn.Image = global::SelfitMain.Properties.Resources.Gait;
            this.qpGaitBtn.Location = new System.Drawing.Point(320, 797);
            this.qpGaitBtn.Name = "qpGaitBtn";
            this.qpGaitBtn.Size = new System.Drawing.Size(345, 234);
            this.qpGaitBtn.TabIndex = 22;
            this.qpGaitBtn.Text = "Gait";
            this.qpGaitBtn.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.qpGaitBtn.UseVisualStyleBackColor = false;
            this.qpGaitBtn.Click += new System.EventHandler(this.qpGaitBtn_Click);
            // 
            // qpReactiontimeBtn
            // 
            this.qpReactiontimeBtn.BackColor = System.Drawing.Color.White;
            this.qpReactiontimeBtn.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(182)))), ((int)(((byte)(182)))), ((int)(((byte)(182)))));
            this.qpReactiontimeBtn.FlatAppearance.BorderSize = 2;
            this.qpReactiontimeBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.qpReactiontimeBtn.Font = new System.Drawing.Font("Roboto", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.qpReactiontimeBtn.ForeColor = System.Drawing.Color.Black;
            this.qpReactiontimeBtn.Image = global::SelfitMain.Properties.Resources.ReactionTime;
            this.qpReactiontimeBtn.Location = new System.Drawing.Point(1442, 529);
            this.qpReactiontimeBtn.Name = "qpReactiontimeBtn";
            this.qpReactiontimeBtn.Size = new System.Drawing.Size(345, 234);
            this.qpReactiontimeBtn.TabIndex = 21;
            this.qpReactiontimeBtn.Text = "Reaction time";
            this.qpReactiontimeBtn.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.qpReactiontimeBtn.UseVisualStyleBackColor = false;
            this.qpReactiontimeBtn.Click += new System.EventHandler(this.qpReactiontimeBtn_Click);
            // 
            // qpMultitaskingBtn
            // 
            this.qpMultitaskingBtn.BackColor = System.Drawing.Color.White;
            this.qpMultitaskingBtn.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(182)))), ((int)(((byte)(182)))), ((int)(((byte)(182)))));
            this.qpMultitaskingBtn.FlatAppearance.BorderSize = 2;
            this.qpMultitaskingBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.qpMultitaskingBtn.Font = new System.Drawing.Font("Roboto", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.qpMultitaskingBtn.ForeColor = System.Drawing.Color.Black;
            this.qpMultitaskingBtn.Image = global::SelfitMain.Properties.Resources.MultiTasking;
            this.qpMultitaskingBtn.Location = new System.Drawing.Point(1068, 529);
            this.qpMultitaskingBtn.Name = "qpMultitaskingBtn";
            this.qpMultitaskingBtn.Size = new System.Drawing.Size(345, 234);
            this.qpMultitaskingBtn.TabIndex = 20;
            this.qpMultitaskingBtn.Text = "Multi tasking";
            this.qpMultitaskingBtn.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.qpMultitaskingBtn.UseVisualStyleBackColor = false;
            this.qpMultitaskingBtn.Click += new System.EventHandler(this.qpMultitaskingBtn_Click);
            // 
            // qpMovementplanningBtn
            // 
            this.qpMovementplanningBtn.BackColor = System.Drawing.Color.White;
            this.qpMovementplanningBtn.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(182)))), ((int)(((byte)(182)))), ((int)(((byte)(182)))));
            this.qpMovementplanningBtn.FlatAppearance.BorderSize = 2;
            this.qpMovementplanningBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.qpMovementplanningBtn.Font = new System.Drawing.Font("Roboto", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.qpMovementplanningBtn.ForeColor = System.Drawing.Color.Black;
            this.qpMovementplanningBtn.Image = global::SelfitMain.Properties.Resources.mPlanning;
            this.qpMovementplanningBtn.Location = new System.Drawing.Point(694, 529);
            this.qpMovementplanningBtn.Name = "qpMovementplanningBtn";
            this.qpMovementplanningBtn.Size = new System.Drawing.Size(345, 234);
            this.qpMovementplanningBtn.TabIndex = 19;
            this.qpMovementplanningBtn.Text = "Movement planning";
            this.qpMovementplanningBtn.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.qpMovementplanningBtn.UseVisualStyleBackColor = false;
            this.qpMovementplanningBtn.Click += new System.EventHandler(this.qpMovementplanningBtn_Click);
            // 
            // qpBalanceBtn
            // 
            this.qpBalanceBtn.BackColor = System.Drawing.Color.White;
            this.qpBalanceBtn.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(182)))), ((int)(((byte)(182)))), ((int)(((byte)(182)))));
            this.qpBalanceBtn.FlatAppearance.BorderSize = 2;
            this.qpBalanceBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.qpBalanceBtn.Font = new System.Drawing.Font("Roboto", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.qpBalanceBtn.ForeColor = System.Drawing.Color.Black;
            this.qpBalanceBtn.Image = global::SelfitMain.Properties.Resources.qpBalance;
            this.qpBalanceBtn.Location = new System.Drawing.Point(320, 529);
            this.qpBalanceBtn.Name = "qpBalanceBtn";
            this.qpBalanceBtn.Size = new System.Drawing.Size(345, 234);
            this.qpBalanceBtn.TabIndex = 18;
            this.qpBalanceBtn.Text = "Balance";
            this.qpBalanceBtn.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.qpBalanceBtn.UseVisualStyleBackColor = false;
            this.qpBalanceBtn.Click += new System.EventHandler(this.qpBalanceBtn_Click);
            // 
            // standingBtn
            // 
            this.standingBtn.BackColor = System.Drawing.Color.White;
            this.standingBtn.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(201)))), ((int)(((byte)(31)))));
            this.standingBtn.FlatAppearance.BorderSize = 3;
            this.standingBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.standingBtn.Font = new System.Drawing.Font("Roboto", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.standingBtn.ForeColor = System.Drawing.Color.Black;
            this.standingBtn.Image = global::SelfitMain.Properties.Resources.Vector_2_2;
            this.standingBtn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.standingBtn.Location = new System.Drawing.Point(557, 307);
            this.standingBtn.Name = "standingBtn";
            this.standingBtn.Size = new System.Drawing.Size(382, 68);
            this.standingBtn.TabIndex = 16;
            this.standingBtn.Text = "Standing";
            this.standingBtn.UseVisualStyleBackColor = false;
            this.standingBtn.Click += new System.EventHandler(this.standingBtn_Click);
            // 
            // BottomToolStripPanel
            // 
            this.BottomToolStripPanel.Location = new System.Drawing.Point(0, 0);
            this.BottomToolStripPanel.Name = "BottomToolStripPanel";
            this.BottomToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal;
            this.BottomToolStripPanel.RowMargin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.BottomToolStripPanel.Size = new System.Drawing.Size(0, 0);
            // 
            // TopToolStripPanel
            // 
            this.TopToolStripPanel.Location = new System.Drawing.Point(0, 0);
            this.TopToolStripPanel.Name = "TopToolStripPanel";
            this.TopToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal;
            this.TopToolStripPanel.RowMargin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.TopToolStripPanel.Size = new System.Drawing.Size(0, 0);
            // 
            // RightToolStripPanel
            // 
            this.RightToolStripPanel.Location = new System.Drawing.Point(0, 0);
            this.RightToolStripPanel.Name = "RightToolStripPanel";
            this.RightToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal;
            this.RightToolStripPanel.RowMargin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.RightToolStripPanel.Size = new System.Drawing.Size(0, 0);
            // 
            // LeftToolStripPanel
            // 
            this.LeftToolStripPanel.Location = new System.Drawing.Point(0, 0);
            this.LeftToolStripPanel.Name = "LeftToolStripPanel";
            this.LeftToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal;
            this.LeftToolStripPanel.RowMargin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.LeftToolStripPanel.Size = new System.Drawing.Size(0, 0);
            // 
            // ContentPanel
            // 
            this.ContentPanel.Size = new System.Drawing.Size(290, 268);
            // 
            // LogInForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.Color.White;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(2292, 1100);
            this.Controls.Add(this.MainTabControl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "LogInForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Selfit";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.LogInForm_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LogInForm_KeyDown);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.LogInForm_KeyUp);
            this.MainTabControl.ResumeLayout(false);
            this.homePage.ResumeLayout(false);
            this.panel9.ResumeLayout(false);
            this.patientTabControl.ResumeLayout(false);
            this.MainPatient.ResumeLayout(false);
            this.ShowDetails.ResumeLayout(false);
            this.ShowDetails.PerformLayout();
            this.EditDetails.ResumeLayout(false);
            this.panel28.ResumeLayout(false);
            this.panel28.PerformLayout();
            this.panel27.ResumeLayout(false);
            this.panel27.PerformLayout();
            this.panel25.ResumeLayout(false);
            this.panel25.PerformLayout();
            this.panel24.ResumeLayout(false);
            this.panel24.PerformLayout();
            this.panel23.ResumeLayout(false);
            this.panel23.PerformLayout();
            this.panel22.ResumeLayout(false);
            this.panel22.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.PlanPage.ResumeLayout(false);
            this.PlanPage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.addExPicPbx)).EndInit();
            this.exercisesFlowPanel.ResumeLayout(false);
            this.panel44.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).EndInit();
            this.panel29.ResumeLayout(false);
            this.panel29.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).EndInit();
            this.panel26.ResumeLayout(false);
            this.filterFlowLayoutPanel.ResumeLayout(false);
            this.practicesLayoutPanel.ResumeLayout(false);
            this.panel50.ResumeLayout(false);
            this.panel51.ResumeLayout(false);
            this.panel52.ResumeLayout(false);
            this.panel53.ResumeLayout(false);
            this.panel54.ResumeLayout(false);
            this.panel55.ResumeLayout(false);
            this.panel56.ResumeLayout(false);
            this.panel57.ResumeLayout(false);
            this.panel49.ResumeLayout(false);
            this.PlanPanel.ResumeLayout(false);
            this.panel47.ResumeLayout(false);
            this.panel48.ResumeLayout(false);
            this.HistoryTab.ResumeLayout(false);
            this.parkinsonSessionDataPanel.ResumeLayout(false);
            this.ParkinsontabControl.ResumeLayout(false);
            this.summaryDataTab.ResumeLayout(false);
            this.ParkinsonSessionDataLayoutPanel.ResumeLayout(false);
            this.DataViewTab.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ParkinsonDataGridView)).EndInit();
            this.panel92.ResumeLayout(false);
            this.exTitleHeader.ResumeLayout(false);
            this.exerciesLayout.ResumeLayout(false);
            this.panel37.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.panel38.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.panel39.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            this.panel40.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            this.panel41.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            this.panel42.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            this.panel43.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            this.sessionDatainfoPanel.ResumeLayout(false);
            this.panel30.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.timeChart)).EndInit();
            this.panel31.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DistanceChart)).EndInit();
            this.panel32.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.stepNumChart)).EndInit();
            this.panel33.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.speedChart)).EndInit();
            this.panel34.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.stepHRchart)).EndInit();
            this.panel35.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.stepHLchart)).EndInit();
            this.panel36.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.moveTimeChart)).EndInit();
            this.panel19.ResumeLayout(false);
            this.panel17.ResumeLayout(false);
            this.panel15.ResumeLayout(false);
            this.panel16.ResumeLayout(false);
            this.panel18.ResumeLayout(false);
            this.panel18.PerformLayout();
            this.SessionsFlowLayoutPanel.ResumeLayout(false);
            this.panel21.ResumeLayout(false);
            this.panel20.ResumeLayout(false);
            this.LoginPage.ResumeLayout(false);
            this.panel8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.PlanView.ResumeLayout(false);
            this.panel61.ResumeLayout(false);
            this.panel62.ResumeLayout(false);
            this.panel60.ResumeLayout(false);
            this.panel65.ResumeLayout(false);
            this.EXviewGroupBox.ResumeLayout(false);
            this.PlanDetailsFlowPanel.ResumeLayout(false);
            this.panel68.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).EndInit();
            this.panel69.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).EndInit();
            this.panel70.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).EndInit();
            this.panel64.ResumeLayout(false);
            this.panel64.PerformLayout();
            this.panel59.ResumeLayout(false);
            this.planViewLayoutPanel.ResumeLayout(false);
            this.panel67.ResumeLayout(false);
            this.panel66.ResumeLayout(false);
            this.Settings.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.reports.ResumeLayout(false);
            this.reportsFlowLayoutPanel.ResumeLayout(false);
            this.panel11.ResumeLayout(false);
            this.panel58.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).EndInit();
            this.panel10.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.reportsTabControl.ResumeLayout(false);
            this.ReportsPlansTabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.plansDataGridView)).EndInit();
            this.ReportsExercisesTabPage.ResumeLayout(false);
            this.parametersDataflowLayoutPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.MeasuresDataGridView)).EndInit();
            this.selectedParametersFlowLayoutPanel.ResumeLayout(false);
            this.panel12.ResumeLayout(false);
            this.generalReports.ResumeLayout(false);
            this.panel80.ResumeLayout(false);
            this.panel86.ResumeLayout(false);
            this.panel87.ResumeLayout(false);
            this.panel90.ResumeLayout(false);
            this.panel91.ResumeLayout(false);
            this.panel88.ResumeLayout(false);
            this.panel89.ResumeLayout(false);
            this.panel84.ResumeLayout(false);
            this.panel85.ResumeLayout(false);
            this.panel82.ResumeLayout(false);
            this.panel83.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.generalReportsListView)).EndInit();
            this.panel81.ResumeLayout(false);
            this.panel81.PerformLayout();
            this.panel77.ResumeLayout(false);
            this.panel78.ResumeLayout(false);
            this.panel79.ResumeLayout(false);
            this.panel74.ResumeLayout(false);
            this.panel75.ResumeLayout(false);
            this.panel76.ResumeLayout(false);
            this.panel46.ResumeLayout(false);
            this.QuickPlan.ResumeLayout(false);
            this.panel97.ResumeLayout(false);
            this.panel96.ResumeLayout(false);
            this.panel95.ResumeLayout(false);
            this.panel93.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        

        #endregion
        private System.Windows.Forms.TabPage Settings;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.CheckBox useHebSpeachCBX;
        private System.Windows.Forms.Button button46;
        private System.Windows.Forms.Button button45;
        private System.Windows.Forms.TabPage PlanPage;
        private System.Windows.Forms.TabPage homePage;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button selectPatientBtn;
        private System.Windows.Forms.TabPage LoginPage;
        private System.Windows.Forms.TabControl MainTabControl;
        private System.Windows.Forms.TabPage HistoryTab;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Button OkBtn;
        private System.Windows.Forms.TextBox UserNameTxtBox;
        private System.Windows.Forms.Label SignInLbl;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label WelcomeBackLbl;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Label letsBeginLbl;
        private System.Windows.Forms.Button reportsBtn;
        private System.Windows.Forms.Button settingsBtn;
        private System.Windows.Forms.Button SignOutNbtn;
        private System.Windows.Forms.TextBox patientSearchTxt;
        private System.Windows.Forms.Button startBtn;
        private Panel panel15;
        private Label historyPatientNameLbl;
        private Button historyViewEditBtn;
        private Panel panel16;
        private Panel panel17;
        private Button createNewPlanBtn;
        private Button historyPlansBtn;
        private Panel panel18;
        private Label label12;
        private DateTimePicker dateTimePicker1;
        private Panel panel19;
        private Label label14;
        private Label label13;
        private FlowLayoutPanel SessionsFlowLayoutPanel;
        private Panel panel21;
        private Label label18;
        private Label label19;
        private Label label20;
        private Panel panel20;
        private Label label17;
        private Label label16;
        private Label label15;
        private FlowLayoutPanel sessionDatainfoPanel;
        private Panel panel30;
        private Label histTimeRateLbl;
        private Label histTimeDataLbl;
        private Label histTimeLbl;
        private Label PlanNameTxt;
        private Label PlanDateTxt;
        private Panel panel31;
        private Label histDistanceRateLbl;
        private Label histDistanceDataLbl;
        private Label histDistanceLbl;
        private Panel panel32;
        private Label histStepCountRateLbl;
        private Label histStepCountDataLbl;
        private Label histStepCountLbl;
        private Panel panel33;
        private Label histSpeedRateLbl;
        private Label histSpeedDataLbl;
        private Label histSpeedLbl;
        private Panel panel34;
        private Label histHRightRateLbl;
        private Label histHRightDataLbl;
        private Label histHRightLbl;
        private Panel panel35;
        private Label histHLeftRateLbl;
        private Label histHLeftDataLbl;
        private Label histHLeftLbl;
        private Panel panel36;
        private Label histMoveTimeRateLbl;
        private Label histMoveTimeDataLbl;
        private Label histMoveTimeLbl;
        private FlowLayoutPanel exerciesLayout;
        private Label label68;
        private Label label69;
        private Panel panel37;
        private Label label70;
        private PictureBox pictureBox2;
        private Label label71;
        private Label label73;
        private Label label72;
        private Panel panel38;
        private Label label74;
        private Label label75;
        private PictureBox pictureBox3;
        private Label label76;
        private Panel panel39;
        private Label label77;
        private Label label78;
        private PictureBox pictureBox4;
        private Label label79;
        private Panel panel40;
        private Label label80;
        private Label label81;
        private PictureBox pictureBox5;
        private Label label82;
        private Panel panel41;
        private Label label83;
        private Label label84;
        private PictureBox pictureBox6;
        private Label label85;
        private Panel panel42;
        private Label label86;
        private Label label87;
        private PictureBox pictureBox7;
        private Label label88;
        private Panel panel43;
        private Label label89;
        private Label label90;
        private PictureBox pictureBox8;
        private Label label91;
        private Button startPlanBtn;
        private Button selectAndEditPlanBtn;
        private Label historyTotalExeTxt;
        private Button createPlanBtn;
        private Panel PlanPanel;
        private Button plansViewEditBtn;
        private Label planPatientNameLbl;
        private Panel panel47;
        private Button createPlanBtnLbl;
        private Panel panel48;
        private Label patientHistoryBtnLbl;
        private Panel panel49;
        private Button exercisesPlanBtn;
        private FlowLayoutPanel exercisesFlowPanel;
        private Label label124;
        private Label label125;
        private Label label99;
        private Button exeSaveAndStartBtn;
        private Label exercisesTotalsTxt;
        private FlowLayoutPanel practicesLayoutPanel;
        private Panel panel50;
        private Label label103;
        private Button button63;
        private Panel panel51;
        private Button button64;
        private Label label101;
        private Panel panel52;
        private Button button65;
        private Label label102;
        private Panel panel53;
        private Button button66;
        private Label label104;
        private Panel panel54;
        private Button button67;
        private Label label105;
        private Panel panel55;
        private Button button68;
        private Label label106;
        private Panel panel56;
        private Button button69;
        private Label label107;
        private Panel panel57;
        private Button button70;
        private Label label108;
        private Button historyExitBtn;
        private ToolStripPanel BottomToolStripPanel;
        private ToolStripPanel TopToolStripPanel;
        private ToolStripPanel RightToolStripPanel;
        private ToolStripPanel LeftToolStripPanel;
        private ToolStripContentPanel ContentPanel;
        private Panel panel26;
        private Panel panel29;
        private Button button61;
        private ComboBox comboBox1;
        private Label label42;
        private Label label35;
        private PictureBox pictureBox10;
        private Panel panel44;
        private Button button71;
        private ComboBox comboBox2;
        private Label label43;
        private Label label44;
        private PictureBox pictureBox11;
        private Button practiceDnBtn;
        private Button practiceUpBtn;
        private FlowLayoutPanel filterFlowLayoutPanel;
        private Label label33;
        private Label label45;
        private Label label46;
        private TabPage PlanView;
        private Panel panel59;
        private Button planVviewAndEditBtn;
        private Label planViewPatientNameLbl;
        private Panel panel60;
        private Panel panel62;
        private Button button26;
        private Button loadPlansPviewBtn;
        private Panel panel64;
        private Label label47;
        private TextBox searchNameTxt;
        private Button button11;
        private Panel panel63;
        private Panel panel65;
        private FlowLayoutPanel planViewLayoutPanel;
        private Label label55;
        private Label label54;
        private Label label52;
        private Label label51;
        private Panel panel67;
        private Label label63;
        private Label label64;
        private Label label66;
        private Label label67;
        private Button planViewCreatePlanBtn;
        private GroupBox EXviewGroupBox;
        private Button planViewStartPlanBtn;
        private Button planViewSelectEditBtn;
        private Label planViewTotalexeLbl;
        private FlowLayoutPanel PlanDetailsFlowPanel;
        private Panel panel68;
        private Label label94;
        private Label label96;
        private PictureBox pictureBox9;
        private Label label98;
        private Label label93;
        private Label label128;
        private Label label129;
        private Panel panel66;
        private Label label57;
        private Label label58;
        private Label label60;
        private Label label61;
        private Label viewPracticeNameLbl;
        private Label label112;
        private Label label111;
        private Panel panel69;
        private Label label100;
        private Label label109;
        private PictureBox pictureBox12;
        private Label label110;
        private Panel panel70;
        private Label label113;
        private Label label114;
        private PictureBox pictureBox13;
        private Label label115;
        private Panel panel71;
        private TextBox planNameLbl;
        private Button button2;
        private TextBox selfitPathTxt;
        private Label label3;
        private Panel panel1;
        private Button button3;
        private Label label4;
        private Button plansExitBtn;
        private Button pViewExitBtn;
        private Panel exTitleHeader;
        private Panel panel2;
        private System.Windows.Forms.DataVisualization.Charting.Chart speedChart;
        private System.Windows.Forms.DataVisualization.Charting.Chart timeChart;
        private System.Windows.Forms.DataVisualization.Charting.Chart DistanceChart;
        private System.Windows.Forms.DataVisualization.Charting.Chart stepNumChart;
        private System.Windows.Forms.DataVisualization.Charting.Chart stepHRchart;
        private System.Windows.Forms.DataVisualization.Charting.Chart stepHLchart;
        private System.Windows.Forms.DataVisualization.Charting.Chart moveTimeChart;
        private Button exeSaveForLaterBtn;
        private Button LoginExitBtn;
        private Button AddPatientBtn;
        private Button patientCountLbl;
        private Label pNameMsgLbl;
        private TabPage reports;
        private Panel panel6;
        private Button reportsExercisesBtn;
        private Panel panel5;
        private Button reportsCloseBtn;
        private Button reportsPatientEditBtn;
        private Label reportsPatientNameLbl;
        private Panel panel10;
        private Button historyReportBtn;
        private FlowLayoutPanel reportsFlowLayoutPanel;
        private TabControl reportsTabControl;
        private TabPage ReportsPlansTabPage;
        private TabPage ReportsExercisesTabPage;
        private Button reportsBackBtn;
        private TabPage generalReports;
        private Panel panel46;
        private Button button4;
        private Label label117;
        private Label label118;
        private Button genRepShowBtn;
        private Panel panel81;
        private Label label121;
        private TextBox genRepPnameFilterText;
        private Button button7;
        private Panel panel80;
        private Panel panel82;
        private Label sumRectMonthsTxt;
        private Panel panel83;
        private Label sumRectDurationTxt;
        private Label label122;
        private Label label123;
        private Panel panel77;
        private Panel panel78;
        private ComboBox genRepToYearCombo;
        private Panel panel79;
        private ComboBox genRepToMonthCombo;
        private Label label120;
        private Panel panel74;
        private Panel panel75;
        private ComboBox genRepFromYearCombo;
        private Panel panel76;
        private ComboBox genRepFromMonthCombo;
        private Label label119;
        private Panel panel90;
        private Label sumRectTimeCountTxt;
        private Panel panel91;
        private Label sumRectTotalTimeTxt;
        private Label label144;
        private Label label145;
        private Panel panel88;
        private Label sumRectSessionsCountTxt;
        private Panel panel89;
        private Label sumRectTotalSessionsTxt;
        private Label label140;
        private Label label141;
        private Panel panel84;
        private Label sumRectPatientsCountTxt;
        private Panel panel85;
        private Label sumRectTotPatientsTxt;
        private Label label132;
        private Label label133;
        private DataGridView generalReportsListView;
        private DataGridViewTextBoxColumn No;
        private DataGridViewTextBoxColumn Date;
        private DataGridViewTextBoxColumn PatientNum;
        private DataGridViewTextBoxColumn Exercises;
        private DataGridViewTextBoxColumn PracticeTime;
        private Panel panel86;
        private Label sumRectExercisesCountTxt;
        private Panel panel87;
        private Label sumRectTotalExercisesTxt;
        private Label label130;
        private Label label131;
        private Button keyBoardBtn;
        private Button button8;
        private Label noPlansAvailableTxt;
        private PictureBox addExPicPbx;
        private Button buttonUp;
        private Button buttonDown;
        private Panel panel4;
        private Button patientSearchMagBtn;
        private Button button10;
        private ListBox PatientsListBox;
        private Button patientListArrowDownBtn;
        private TabControl patientTabControl;
        private TabPage MainPatient;
        private Label noPatientSelectedLbl;
        private TabPage ShowDetails;
        private Label patientInfoIDLbl;
        private Label patientInfoNameLbl;
        private TextBox patientInfoNotesTxt;
        private TextBox patientInfoPhoneTxt;
        private TextBox patientInfoEmailTxt;
        private Label NotesLbl;
        private Label PhoneNumberLbl;
        private Label EmailLbl;
        private Label IDlabel;
        private Label NameLbl;
        private TabPage EditDetails;
        private Label label8;
        private Label label21;
        private Label label10;
        private Label label9;
        private Button patientDetailsEditBtn;
        private Label label27;
        private Label label26;
        private Label label25;
        private Label label24;
        private Label label23;
        private Label label22;
        private Panel panel24;
        private TextBox IdPatientEdit;
        private Panel panel23;
        private Panel panel22;
        private TextBox firstNamePatientEdit;
        private Panel panel25;
        private Panel panel27;
        private Button fastEditDetailsBtn;
        private Panel panel28;
        private TextBox lastNamePatientEdit;
        private TextBox mailPatientEdit;
        private TextBox notesPatientEdit;
        private TextBox phonePatientEdit;
        private TextBox UserPasswordTxtBox;
        private Label incorrectUserTxt;
        private Label passwordTxtLbl;
        private Label userNameTxtLbl;
        private Panel panel92;
        private Label label11;
        private Button historyQuickPlanBtn;
        private Button historyGeneralReportsBtn;
        private Button exercisesGReportsBtn;
        private Button exercisesQplanBtn;
        private Label pHistPViewLblBtn;
        private Button pViewQplanBtn;
        private Panel panel61;
        private Button pViewReportsBtn;
        private Button reportsPlansBtn;
        private Label reportsPHistoryBtn;
        private Button reportsQplanBtn;
        private Button button18;
        private TabPage QuickPlan;
        private Panel panel93;
        private Button button5;
        private Panel panel94;
        private Button qpEditBtn;
        private Label qpPatientNameLbl;
        private Panel panel97;
        private Button qPlanReportsBtn;
        private Label qPlanHistoryBtn;
        private Button qPlanPlanBtn;
        private Panel panel96;
        private Button qPlanExercisesBtn;
        private Panel panel95;
        private Button button9;
        private Button button13;
        private Button button12;
        private Button maleSelectBtn;
        private Button duration5mBtn;
        private Button duration30mBtn;
        private Button duration25mBtn;
        private Button duration20mBtn;
        private Button duration15mBtn;
        private Button duration10mBtn;
        private Button sittingBtn;
        private Button standingBtn;
        private Button qpBalanceBtn;
        private Button qpReactiontimeBtn;
        private Button qpMultitaskingBtn;
        private Button qpMovementplanningBtn;
        private Button qpAerobicBtn;
        private Button qpAgilityBtn;
        private Button qpCoordinationBtn;
        private Button qpGaitBtn;
        private Button qpStartPlanBtn;
        private Button qpSelectNoneBtn;
        private Button qpSelectAllBtn;
        private Button button25;
        private Button button14;
        private Button EditPlanNameBtn;
        private Button SavePlanNameBtn;
        private CheckBox autoGenIDcbx;
        private CheckBox parkinsonModeCBX;
        private Panel parkinsonSessionDataPanel;
        private TabControl ParkinsontabControl;
        private TabPage summaryDataTab;
        private TabPage DataViewTab;
        private Label label31;
        private Label label30;
        private Label label29;
        private Label label28;
        private Label label2;
        private FlowLayoutPanel ParkinsonSessionDataLayoutPanel;
        private Panel panel98;
        private DataGridView ParkinsonDataGridView;
        private DataGridViewTextBoxColumn stepType;
        private DataGridViewTextBoxColumn stepLength;
        private DataGridViewTextBoxColumn stepTime;
        private DataGridViewTextBoxColumn stage;
        private Button allDataBtn;
        private Button stdDevBtn;
        private Button validStepsBtn;
        private Button reportsSelectExercisesBtn;
        private Button reportsSelectPlansBtn;
        private Panel panel11;
        private Label label32;
        private Label label34;
        private Label label5;
        private Label label6;
        private Label label7;
        private Label selectedPracticeLbl;
        private FlowLayoutPanel parametersDataflowLayoutPanel;
        private FlowLayoutPanel selectedParametersFlowLayoutPanel;
        private Panel panel12;
        private Panel panel14;
        private Panel panel58;
        private Label label38;
        private Label label49;
        private PictureBox pictureBox14;
        private DataGridView MeasuresDataGridView;
        private DataGridView plansDataGridView;
        private DataGridViewTextBoxColumn Session;
        private DataGridViewTextBoxColumn sessionDate;
        private DataGridViewTextBoxColumn Total_Distance;
        private DataGridViewTextBoxColumn Step_Count;
        private DataGridViewTextBoxColumn Session_Time_gross;
        private DataGridViewTextBoxColumn Speed;
        private DataGridViewTextBoxColumn Step_hight_Right;
        private DataGridViewTextBoxColumn Step_hight_Left;
        private DataGridViewTextBoxColumn Step_Length_Right;
        private DataGridViewTextBoxColumn Step_Length_Left;
        private DataGridViewTextBoxColumn Movement_Time;
        //private System.Diagnostics.PerformanceCounter performanceCounter1;
    }
}


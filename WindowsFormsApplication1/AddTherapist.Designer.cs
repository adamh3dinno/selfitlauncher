﻿namespace SelfitMain {
    partial class AddTherapist {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.panel1 = new System.Windows.Forms.Panel();
            this.exitAddTherapistBtn = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.eMailTxt = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.telTxt = new System.Windows.Forms.MaskedTextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.lNameTxt = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.fNameTxt = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.panel17 = new System.Windows.Forms.Panel();
            this.femaleSelectionBtn = new System.Windows.Forms.Button();
            this.maleSelectionBtn = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.saveTherapistBtn = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.panel10 = new System.Windows.Forms.Panel();
            this.birthDATcombo = new System.Windows.Forms.ComboBox();
            this.panel11 = new System.Windows.Forms.Panel();
            this.birthMonthCombo = new System.Windows.Forms.ComboBox();
            this.panel12 = new System.Windows.Forms.Panel();
            this.birthYearCombo = new System.Windows.Forms.NumericUpDown();
            this.panel9 = new System.Windows.Forms.Panel();
            this.panel1.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel17.SuspendLayout();
            this.panel10.SuspendLayout();
            this.panel11.SuspendLayout();
            this.panel12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.birthYearCombo)).BeginInit();
            this.panel9.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(56)))), ((int)(((byte)(64)))));
            this.panel1.Controls.Add(this.exitAddTherapistBtn);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(1, 1);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1798, 136);
            this.panel1.TabIndex = 0;
            // 
            // exitAddTherapistBtn
            // 
            this.exitAddTherapistBtn.Image = global::SelfitMain.Properties.Resources.addTherapistClose;
            this.exitAddTherapistBtn.Location = new System.Drawing.Point(1694, 32);
            this.exitAddTherapistBtn.Name = "exitAddTherapistBtn";
            this.exitAddTherapistBtn.Size = new System.Drawing.Size(66, 66);
            this.exitAddTherapistBtn.TabIndex = 1;
            this.exitAddTherapistBtn.Click += new System.EventHandler(this.exitAddTherapistBtn_Click);
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Roboto", 80F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(201)))), ((int)(((byte)(31)))));
            this.label1.Location = new System.Drawing.Point(55, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(689, 88);
            this.label1.TabIndex = 0;
            this.label1.Text = "Add a Therapist";
            // 
            // panel5
            // 
            this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel5.Controls.Add(this.eMailTxt);
            this.panel5.Controls.Add(this.label5);
            this.panel5.Location = new System.Drawing.Point(610, 436);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(580, 68);
            this.panel5.TabIndex = 8;
            // 
            // eMailTxt
            // 
            this.eMailTxt.BackColor = System.Drawing.Color.WhiteSmoke;
            this.eMailTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.eMailTxt.Font = new System.Drawing.Font("Roboto", 28F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.eMailTxt.Location = new System.Drawing.Point(165, 14);
            this.eMailTxt.Name = "eMailTxt";
            this.eMailTxt.Size = new System.Drawing.Size(397, 34);
            this.eMailTxt.TabIndex = 1;
            this.eMailTxt.Text = "?";
            this.eMailTxt.TextChanged += new System.EventHandler(this.eMailTxt_TextChanged);
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Roboto", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.label5.Location = new System.Drawing.Point(29, 12);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(130, 39);
            this.label5.TabIndex = 0;
            this.label5.Text = "Email";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel4
            // 
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel4.Controls.Add(this.telTxt);
            this.panel4.Controls.Add(this.label4);
            this.panel4.Location = new System.Drawing.Point(610, 552);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(580, 68);
            this.panel4.TabIndex = 7;
            // 
            // telTxt
            // 
            this.telTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.telTxt.Font = new System.Drawing.Font("Roboto", 28F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.telTxt.Location = new System.Drawing.Point(165, 17);
            this.telTxt.Mask = "0000000000";
            this.telTxt.Name = "telTxt";
            this.telTxt.Size = new System.Drawing.Size(397, 34);
            this.telTxt.TabIndex = 12;
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Roboto", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.label4.Location = new System.Drawing.Point(29, 12);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(130, 39);
            this.label4.TabIndex = 0;
            this.label4.Text = "Tel";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.lNameTxt);
            this.panel3.Controls.Add(this.label3);
            this.panel3.Location = new System.Drawing.Point(610, 320);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(580, 68);
            this.panel3.TabIndex = 6;
            // 
            // lNameTxt
            // 
            this.lNameTxt.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lNameTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lNameTxt.Font = new System.Drawing.Font("Roboto", 28F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.lNameTxt.Location = new System.Drawing.Point(165, 13);
            this.lNameTxt.Name = "lNameTxt";
            this.lNameTxt.Size = new System.Drawing.Size(397, 34);
            this.lNameTxt.TabIndex = 2;
            this.lNameTxt.Text = "?";
            this.lNameTxt.TextChanged += new System.EventHandler(this.lNameTxt_TextChanged);
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Roboto", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.label3.Location = new System.Drawing.Point(29, 12);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(130, 39);
            this.label3.TabIndex = 0;
            this.label3.Text = "Last Name";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.fNameTxt);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Location = new System.Drawing.Point(610, 204);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(580, 68);
            this.panel2.TabIndex = 5;
            // 
            // fNameTxt
            // 
            this.fNameTxt.BackColor = System.Drawing.Color.WhiteSmoke;
            this.fNameTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.fNameTxt.Font = new System.Drawing.Font("Roboto", 28F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.fNameTxt.Location = new System.Drawing.Point(165, 12);
            this.fNameTxt.Name = "fNameTxt";
            this.fNameTxt.Size = new System.Drawing.Size(397, 34);
            this.fNameTxt.TabIndex = 2;
            this.fNameTxt.Text = "?";
            this.fNameTxt.TextChanged += new System.EventHandler(this.fNameTxt_TextChanged);
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Roboto", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.label6.Location = new System.Drawing.Point(29, 12);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(130, 39);
            this.label6.TabIndex = 0;
            this.label6.Text = "First Name";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel17
            // 
            this.panel17.Controls.Add(this.femaleSelectionBtn);
            this.panel17.Controls.Add(this.maleSelectionBtn);
            this.panel17.Controls.Add(this.label10);
            this.panel17.Location = new System.Drawing.Point(610, 809);
            this.panel17.Name = "panel17";
            this.panel17.Size = new System.Drawing.Size(590, 81);
            this.panel17.TabIndex = 10;
            // 
            // femaleSelectionBtn
            // 
            this.femaleSelectionBtn.BackColor = System.Drawing.Color.White;
            this.femaleSelectionBtn.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.femaleSelectionBtn.FlatAppearance.BorderSize = 3;
            this.femaleSelectionBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.femaleSelectionBtn.Font = new System.Drawing.Font("Roboto", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.femaleSelectionBtn.ForeColor = System.Drawing.Color.Black;
            this.femaleSelectionBtn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.femaleSelectionBtn.Location = new System.Drawing.Point(212, 26);
            this.femaleSelectionBtn.Name = "femaleSelectionBtn";
            this.femaleSelectionBtn.Size = new System.Drawing.Size(170, 48);
            this.femaleSelectionBtn.TabIndex = 2;
            this.femaleSelectionBtn.Text = "Female";
            this.femaleSelectionBtn.UseVisualStyleBackColor = false;
            this.femaleSelectionBtn.Click += new System.EventHandler(this.femaleSelectionBtn_Click);
            // 
            // maleSelectionBtn
            // 
            this.maleSelectionBtn.BackColor = System.Drawing.Color.White;
            this.maleSelectionBtn.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(201)))), ((int)(((byte)(31)))));
            this.maleSelectionBtn.FlatAppearance.BorderSize = 3;
            this.maleSelectionBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.maleSelectionBtn.Font = new System.Drawing.Font("Roboto", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.maleSelectionBtn.ForeColor = System.Drawing.Color.Black;
            this.maleSelectionBtn.Image = global::SelfitMain.Properties.Resources.Vector_2_2;
            this.maleSelectionBtn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.maleSelectionBtn.Location = new System.Drawing.Point(36, 26);
            this.maleSelectionBtn.Name = "maleSelectionBtn";
            this.maleSelectionBtn.Size = new System.Drawing.Size(170, 48);
            this.maleSelectionBtn.TabIndex = 1;
            this.maleSelectionBtn.Text = "Male";
            this.maleSelectionBtn.UseVisualStyleBackColor = false;
            this.maleSelectionBtn.Click += new System.EventHandler(this.maleSelectionBtn_Click);
            // 
            // label10
            // 
            this.label10.Font = new System.Drawing.Font("Roboto", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(32, 2);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(139, 39);
            this.label10.TabIndex = 0;
            this.label10.Text = "Gender";
            // 
            // saveTherapistBtn
            // 
            this.saveTherapistBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(201)))), ((int)(((byte)(31)))));
            this.saveTherapistBtn.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.saveTherapistBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.saveTherapistBtn.Font = new System.Drawing.Font("Roboto", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.saveTherapistBtn.ForeColor = System.Drawing.Color.Black;
            this.saveTherapistBtn.Location = new System.Drawing.Point(610, 977);
            this.saveTherapistBtn.Name = "saveTherapistBtn";
            this.saveTherapistBtn.Size = new System.Drawing.Size(591, 68);
            this.saveTherapistBtn.TabIndex = 11;
            this.saveTherapistBtn.Text = "Save";
            this.saveTherapistBtn.UseVisualStyleBackColor = false;
            this.saveTherapistBtn.Click += new System.EventHandler(this.saveTherapistBtn_Click);
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("Roboto", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(32, 2);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(139, 39);
            this.label8.TabIndex = 0;
            this.label8.Text = "Date of birth";
            // 
            // panel10
            // 
            this.panel10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel10.Controls.Add(this.birthDATcombo);
            this.panel10.Location = new System.Drawing.Point(36, 28);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(170, 48);
            this.panel10.TabIndex = 1;
            // 
            // birthDATcombo
            // 
            this.birthDATcombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.birthDATcombo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.birthDATcombo.Font = new System.Drawing.Font("Roboto", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.birthDATcombo.FormattingEnabled = true;
            this.birthDATcombo.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18",
            "19",
            "20",
            "21",
            "22",
            "23",
            "24",
            "25",
            "26",
            "27",
            "28",
            "29",
            "30",
            "31"});
            this.birthDATcombo.Location = new System.Drawing.Point(22, 5);
            this.birthDATcombo.Name = "birthDATcombo";
            this.birthDATcombo.Size = new System.Drawing.Size(121, 35);
            this.birthDATcombo.TabIndex = 0;
            this.birthDATcombo.SelectedIndexChanged += new System.EventHandler(this.birthDATcombo_SelectedIndexChanged);
            // 
            // panel11
            // 
            this.panel11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel11.Controls.Add(this.birthMonthCombo);
            this.panel11.Location = new System.Drawing.Point(212, 28);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(170, 48);
            this.panel11.TabIndex = 2;
            // 
            // birthMonthCombo
            // 
            this.birthMonthCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.birthMonthCombo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.birthMonthCombo.Font = new System.Drawing.Font("Roboto", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.birthMonthCombo.FormattingEnabled = true;
            this.birthMonthCombo.Items.AddRange(new object[] {
            "Jan",
            "Feb",
            "March",
            "April",
            "May",
            "June",
            "July",
            "Aug",
            "Sep",
            "Oct",
            "Nov",
            "Dec"});
            this.birthMonthCombo.Location = new System.Drawing.Point(24, 6);
            this.birthMonthCombo.Name = "birthMonthCombo";
            this.birthMonthCombo.Size = new System.Drawing.Size(121, 35);
            this.birthMonthCombo.TabIndex = 1;
            this.birthMonthCombo.SelectedIndexChanged += new System.EventHandler(this.birthMonthCombo_SelectedIndexChanged);
            // 
            // panel12
            // 
            this.panel12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel12.Controls.Add(this.birthYearCombo);
            this.panel12.Location = new System.Drawing.Point(388, 28);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(170, 48);
            this.panel12.TabIndex = 3;
            // 
            // birthYearCombo
            // 
            this.birthYearCombo.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.birthYearCombo.Font = new System.Drawing.Font("Roboto", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.birthYearCombo.Location = new System.Drawing.Point(22, 8);
            this.birthYearCombo.Maximum = new decimal(new int[] {
            2200,
            0,
            0,
            0});
            this.birthYearCombo.Minimum = new decimal(new int[] {
            1910,
            0,
            0,
            0});
            this.birthYearCombo.Name = "birthYearCombo";
            this.birthYearCombo.Size = new System.Drawing.Size(132, 32);
            this.birthYearCombo.TabIndex = 12;
            this.birthYearCombo.Value = new decimal(new int[] {
            1960,
            0,
            0,
            0});
            // 
            // panel9
            // 
            this.panel9.Controls.Add(this.panel12);
            this.panel9.Controls.Add(this.panel11);
            this.panel9.Controls.Add(this.panel10);
            this.panel9.Controls.Add(this.label8);
            this.panel9.Location = new System.Drawing.Point(610, 680);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(590, 81);
            this.panel9.TabIndex = 9;
            // 
            // AddTherapist
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1800, 1100);
            this.Controls.Add(this.saveTherapistBtn);
            this.Controls.Add(this.panel17);
            this.Controls.Add(this.panel9);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "AddTherapist";
            this.Text = "AddTherapist";
            this.panel1.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel17.ResumeLayout(false);
            this.panel10.ResumeLayout(false);
            this.panel11.ResumeLayout(false);
            this.panel12.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.birthYearCombo)).EndInit();
            this.panel9.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label exitAddTherapistBtn;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.TextBox eMailTxt;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Panel panel17;
        private System.Windows.Forms.Button femaleSelectionBtn;
        private System.Windows.Forms.Button maleSelectionBtn;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button saveTherapistBtn;
        private System.Windows.Forms.TextBox lNameTxt;
        private System.Windows.Forms.TextBox fNameTxt;
        private System.Windows.Forms.MaskedTextBox telTxt;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.ComboBox birthDATcombo;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.ComboBox birthMonthCombo;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.NumericUpDown birthYearCombo;
    }
}
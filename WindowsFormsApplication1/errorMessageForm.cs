﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SelfitMain {
    public partial class errorMessageForm : Form {
        public errorMessageForm() {
            InitializeComponent();
        }

        public errorMessageForm(string titleMsg, string infoMsg, string extraDataMsg) {
            InitializeComponent();
            titleLbl.Text = titleMsg;
            errorInfoLbl.Text = infoMsg;
            extraInfoTxt.Text = extraDataMsg;
        }

        private void okBtn_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void errorMessageForm_Load(object sender, EventArgs e) {
            this.DialogResult = DialogResult.Yes;
            this.Size = new Size(500,172);
        }

        private void showMoreBtn_Click(object sender, EventArgs e) {
            if (showMoreBtn.Text == "+") {
                this.Size = new Size(500, 520);
                showMoreBtn.Text = "-";
            }
            else if (showMoreBtn.Text == "-") {
                this.Size = new Size(500, 172);
                showMoreBtn.Text = "+";
            }
        }
    }
}

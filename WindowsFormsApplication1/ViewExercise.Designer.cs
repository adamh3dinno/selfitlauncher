﻿namespace SelfitMain {
    partial class ViewExercise {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.panel1 = new System.Windows.Forms.Panel();
            this.closeBtn = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.descriptionTxtBox = new System.Windows.Forms.RichTextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.focusTxt = new System.Windows.Forms.RichTextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.practiceNameLbl = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.addToPlanBtn = new System.Windows.Forms.Button();
            this.exerciseImagePbx = new System.Windows.Forms.PictureBox();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.exerciseImagePbx)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(201)))), ((int)(((byte)(31)))));
            this.panel1.Controls.Add(this.closeBtn);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(1, 1);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1350, 106);
            this.panel1.TabIndex = 0;
            // 
            // closeBtn
            // 
            this.closeBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(201)))), ((int)(((byte)(31)))));
            this.closeBtn.BackgroundImage = global::SelfitMain.Properties.Resources.Blue_Close_ICN;
            this.closeBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.closeBtn.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.closeBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.closeBtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(201)))), ((int)(((byte)(31)))));
            this.closeBtn.Location = new System.Drawing.Point(1268, 20);
            this.closeBtn.Name = "closeBtn";
            this.closeBtn.Size = new System.Drawing.Size(66, 66);
            this.closeBtn.TabIndex = 2;
            this.closeBtn.UseVisualStyleBackColor = false;
            this.closeBtn.Click += new System.EventHandler(this.button1_Click);
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Roboto", 80F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.label1.Location = new System.Drawing.Point(32, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(569, 87);
            this.label1.TabIndex = 0;
            this.label1.Text = "View Exercise";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.White;
            this.panel2.Controls.Add(this.panel5);
            this.panel2.Controls.Add(this.panel4);
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Location = new System.Drawing.Point(869, 107);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(482, 569);
            this.panel2.TabIndex = 2;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.White;
            this.panel5.Controls.Add(this.descriptionTxtBox);
            this.panel5.Controls.Add(this.label6);
            this.panel5.Location = new System.Drawing.Point(0, 219);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(482, 347);
            this.panel5.TabIndex = 2;
            // 
            // descriptionTxtBox
            // 
            this.descriptionTxtBox.BackColor = System.Drawing.Color.White;
            this.descriptionTxtBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.descriptionTxtBox.Font = new System.Drawing.Font("Lucida Sans", 17.25F);
            this.descriptionTxtBox.Location = new System.Drawing.Point(10, 43);
            this.descriptionTxtBox.Name = "descriptionTxtBox";
            this.descriptionTxtBox.ReadOnly = true;
            this.descriptionTxtBox.Size = new System.Drawing.Size(469, 288);
            this.descriptionTxtBox.TabIndex = 3;
            this.descriptionTxtBox.Text = "Data not available yet";
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Roboto", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(181)))), ((int)(((byte)(181)))), ((int)(((byte)(181)))));
            this.label6.Location = new System.Drawing.Point(8, 1);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(195, 39);
            this.label6.TabIndex = 2;
            this.label6.Text = "Description";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.White;
            this.panel4.Controls.Add(this.focusTxt);
            this.panel4.Controls.Add(this.label4);
            this.panel4.Location = new System.Drawing.Point(0, 103);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(482, 110);
            this.panel4.TabIndex = 1;
            // 
            // focusTxt
            // 
            this.focusTxt.BackColor = System.Drawing.Color.White;
            this.focusTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.focusTxt.Font = new System.Drawing.Font("Lucida Sans", 17.25F);
            this.focusTxt.Location = new System.Drawing.Point(9, 38);
            this.focusTxt.Name = "focusTxt";
            this.focusTxt.ReadOnly = true;
            this.focusTxt.Size = new System.Drawing.Size(470, 72);
            this.focusTxt.TabIndex = 4;
            this.focusTxt.Text = "N/A";
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Roboto", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(181)))), ((int)(((byte)(181)))), ((int)(((byte)(181)))));
            this.label4.Location = new System.Drawing.Point(5, 3);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(195, 39);
            this.label4.TabIndex = 1;
            this.label4.Text = "Focus";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.White;
            this.panel3.Controls.Add(this.practiceNameLbl);
            this.panel3.Controls.Add(this.label2);
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(482, 102);
            this.panel3.TabIndex = 0;
            // 
            // practiceNameLbl
            // 
            this.practiceNameLbl.Font = new System.Drawing.Font("Lucida Sans", 17.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.practiceNameLbl.ForeColor = System.Drawing.Color.Black;
            this.practiceNameLbl.Location = new System.Drawing.Point(7, 53);
            this.practiceNameLbl.Name = "practiceNameLbl";
            this.practiceNameLbl.Size = new System.Drawing.Size(472, 28);
            this.practiceNameLbl.TabIndex = 1;
            this.practiceNameLbl.Text = "Chase the Circle";
            this.practiceNameLbl.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Roboto", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(181)))), ((int)(((byte)(181)))), ((int)(((byte)(181)))));
            this.label2.Location = new System.Drawing.Point(8, 17);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(195, 39);
            this.label2.TabIndex = 0;
            this.label2.Text = "Exercise Name";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // addToPlanBtn
            // 
            this.addToPlanBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(201)))), ((int)(((byte)(31)))));
            this.addToPlanBtn.FlatAppearance.BorderSize = 0;
            this.addToPlanBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.addToPlanBtn.Font = new System.Drawing.Font("Roboto", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.addToPlanBtn.Location = new System.Drawing.Point(869, 682);
            this.addToPlanBtn.Name = "addToPlanBtn";
            this.addToPlanBtn.Size = new System.Drawing.Size(479, 60);
            this.addToPlanBtn.TabIndex = 3;
            this.addToPlanBtn.Text = "Add to plan";
            this.addToPlanBtn.UseVisualStyleBackColor = false;
            this.addToPlanBtn.Click += new System.EventHandler(this.addToPlanBtn_Click);
            // 
            // exerciseImagePbx
            // 
            this.exerciseImagePbx.Image = global::SelfitMain.Properties.Resources.Floor2;
            this.exerciseImagePbx.Location = new System.Drawing.Point(1, 107);
            this.exerciseImagePbx.Name = "exerciseImagePbx";
            this.exerciseImagePbx.Size = new System.Drawing.Size(853, 643);
            this.exerciseImagePbx.TabIndex = 1;
            this.exerciseImagePbx.TabStop = false;
            // 
            // ViewExercise
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.CancelButton = this.closeBtn;
            this.ClientSize = new System.Drawing.Size(1363, 783);
            this.Controls.Add(this.addToPlanBtn);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.exerciseImagePbx);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "ViewExercise";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "ViewExercise";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.ViewExercise_Load);
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.exerciseImagePbx)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button closeBtn;
        private System.Windows.Forms.PictureBox exerciseImagePbx;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label practiceNameLbl;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.RichTextBox descriptionTxtBox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button addToPlanBtn;
        private System.Windows.Forms.RichTextBox focusTxt;
    }
}
﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json;

namespace SelfitMain {
    public class Patient {
        //private Patient p;

        public Patient() {
        }

        public Patient(Patient pd) {
            ID = pd.ID;
            SiteID = pd.SiteID;
            refIDnum = pd.refIDnum;
            HashedID = pd.HashedID;
            LastName = pd.LastName;
            FirstName = pd.FirstName;
            phoneNumber = pd.phoneNumber;
            email = pd.email;
            gender = pd.gender;
            birthYear = pd.birthYear;
            height = pd.height;
            weight = pd.weight;
            shueSize = pd.shueSize;
            siteId = pd.siteId;
        }
        //int: gender,birthYear,height,weight,shueSize,siteId
        public long ID { get; set; } // input
        public long SiteID { get; set; } // input
        public long refIDnum { get; set; }
        public String HashedID { get; set; }
        public String LastName { get; set; }
        public String FirstName { get; set; }
        public String phoneNumber { get; set; }
        public String email { get; set; }
        public int gender { get; set; }
        public int birthYear { get; set; }
        public int height { get; set; }
        public int weight { get; set; }
        public int shueSize { get; set; }
        public int siteId { get; set; }
    }

    class IdentityClient {

        private string URI = string.Empty;
        //private string URI = "http://localhost:49824/";
        //PROD:
        //private string URI = "https://selfitidentityserver.azurewebsites.net";
        // DEV:
        //private string URI = "https://selfitidentityserver-dev.azurewebsites.net";
        static HttpClient http = new HttpClient();

        // Limit retry attempts on calls
        private const int MaxRetries = 3;
        // Retry wait time
        private const int waitTime = 1 * 1000;

        public IdentityClient() {
            http.BaseAddress = new Uri(URI);
            http.DefaultRequestHeaders.Accept.Clear();
            http.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));
        }

        public IdentityClient(int sys_svr) {
            switch (sys_svr) {
                case 0:
                    // local
                    URI = "http://localhost:49824/";
                    break;
                case 1:
                    // DEV
                    URI = "https://selfitidentityserver-dev.azurewebsites.net";
                    break;
                case 2:
                    // Prod
                    URI = "https://selfitidentityserver.azurewebsites.net";
                    break;
                default:
                    break;
            }
            if (URI == string.Empty)
                throw new Exception("CRITICAL ERROR - NO SERVER CONFIGURATION!!!");
            
        }

        private void handleHttpError(HttpRequestException e) {
            MessageBox.Show("Network Error!\nPlease check computer network and try again");
            // Need to handle that data
            //MessageBox.Show("Message :{0} ", e.Message);
            //errorMessageForm errMsg = new errorMessageForm("Network Error!", "Please check computer network and try again", e.Message);
            //errMsg.ShowDialog();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="searchText"></param>
        /// <param name="siteId"></param>
        /// <returns></returns>
        public async Task<List<Patient>> getUsersDataByNameAsync(string searchText1, string searchText2, int siteId) {
            // Get(string fname, string lname, long SiteID) --> http://localhost:49824/api/Patient?fname=&lname=&SiteID=1
            List<Patient> patients = new List<Patient>();
            HttpResponseMessage response = null;
            try {
                for (int i = 0; i < MaxRetries; i++) {
                    response = await http.GetAsync(URI + "/api/Patient?fname=" + searchText1 + "&lname=" + searchText2 + "&SiteID=" + siteId);
                    if (response.IsSuccessStatusCode) {
                        break;
                    }
                    response.Dispose();
                    await Task.Delay(waitTime);
                }
                response.EnsureSuccessStatusCode();
                string responseBody = await response.Content.ReadAsStringAsync();
                //MessageBox.Show("responseBody=" + responseBody);
                // Above three lines can be replaced with new helper method below
                // string responseBody = await client.GetStringAsync(uri);

                patients = JsonConvert.DeserializeObject<List<Patient>>(responseBody);
                //string txt = string.Empty;
                //foreach (Patient p in patients) {
                //    txt += p.refIDnum + " FN: " + p.FirstName + " LN: " + p.LastName + "\n";
                //}
                //MessageBox.Show("Web result:\n" + txt);
            }
            catch (HttpRequestException e) {
                //MessageBox.Show("Network Error!\nPlease check computer network and try again");
                //MessageBox.Show("Message :{0} ", e.Message);
                handleHttpError(e);
            }
            finally {
                response.Dispose();
            }
            return patients;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userID"></param>
        /// <param name="siteId"></param>
        /// <returns></returns>
        public async Task<Patient> getUserDataByIDAsync(string userID, int siteId) {
            // Get(long ID, long SiteID) --> http://localhost:49824/api/Patient?ID=1&SiteID=1
            Patient userData = new Patient();
            HttpResponseMessage response = null;
            try {                
                for (int i = 0; i < MaxRetries; i++) {
                    response = await http.GetAsync(URI + "/api/Patient?ID=" + userID + "&SiteID=" + siteId);
                    if (response.IsSuccessStatusCode) {
                        break;
                    }
                    response.Dispose();
                    await Task.Delay(waitTime);
                }
                response.EnsureSuccessStatusCode();
                string responseBody = await response.Content.ReadAsStringAsync();                
                // string responseBody = await client.GetStringAsync(uri);
                userData = JsonConvert.DeserializeObject<Patient>(responseBody);

                //MessageBox.Show("Web result:\nFirst Name: " + p.FirstName + "\nLast Name: " + p.LastName);
            }
            catch (HttpRequestException e) {
                //MessageBox.Show("Network Error!\nPlease check computer network and try again");
                //MessageBox.Show("Message :{0} ", e.Message);
                handleHttpError(e);
            }
            finally {
                response.Dispose();
            }
            return userData;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pData"></param>
        /// <param name="siteId"></param>
        /// <returns></returns>
        public async Task<long> addNewPatient(Patient pData, int siteId) {
            long retCode = -1;            
            try {
                using (var client = new HttpClient()) {
                    client.BaseAddress = new Uri(URI);
                    HttpResponseMessage response = await client.PostAsJsonAsync("/api/Patient", pData);
                    response.EnsureSuccessStatusCode();
                    string resultStr = await response.Content.ReadAsStringAsync();
                    if (response.IsSuccessStatusCode) {
                        long.TryParse(resultStr, out retCode);
                    }
                }
            }
            catch (HttpRequestException e) {
                //MessageBox.Show("Network Error!\nPlease check computer network and try again");
                MessageBox.Show("Message :{0} ", e.Message);
                handleHttpError(e);
            }
            return retCode;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="pData"></param>
        /// <param name="siteId"></param>
        /// <returns></returns>
        public async Task<int> smallUpdateUserData(Patient pData, int siteId) {
            int hasUpdated = -1;
            try {
                using (var client = new HttpClient()) {
                    client.BaseAddress = new Uri(URI);
                    var response = client.PutAsJsonAsync("/api/Patient?SiteID=" + siteId + "&smallUpdate=true", pData).Result;
                    if (response.IsSuccessStatusCode) {
                        hasUpdated = 0;
                    }
                }
            }
            catch (HttpRequestException e) {
               // MessageBox.Show("Network Error!\nPlease check computer network and try again");
                //MessageBox.Show("Message :{0} ", e.Message);
                handleHttpError(e);
            }
            return hasUpdated;
        }      
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="pData"></param>
        /// <param name="siteId"></param>
        /// <returns></returns>
        public async Task<int> UpdateUserData(Patient pData, int siteId) {
            int hasUpdated = -1;
            try {
                using (var client = new HttpClient()) {
                    client.BaseAddress = new Uri(URI);
                    var response = client.PutAsJsonAsync("/api/Patient?SiteID=" + siteId, pData).Result;
                    if (response.IsSuccessStatusCode) {
                        hasUpdated = 0;
                    }
                }
            }
            catch (HttpRequestException e) {
                //MessageBox.Show("Network Error!\nPlease check computer network and try again");
                //MessageBox.Show("Message :{0} ", e.Message);
                handleHttpError(e);
            }

            return hasUpdated;
        }
    }
}

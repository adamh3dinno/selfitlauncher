﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Windows.Forms;
using Newtonsoft.Json;
using System.Data;

namespace SelfitMain {
    class ManagerRestClient {
        private string URI = string.Empty;

        // LOCAL
        //private string URI = "http://localhost:53075";    
        // PROD
        //private string URI = "https://selfitmanagerwebapplication.azurewebsites.net";
        // DEV
        //private string URI = "https://selfitmanagerwebapplication-dev.azurewebsites.net";
        static HttpClient mgrhttp = new HttpClient();

        // Limit retry attempts on calls
        private const int MaxRetries = 3;

        // Retry wait time
        private const int waitTime = 1 * 1000;

        public ManagerRestClient() {
            mgrhttp.BaseAddress = new Uri(URI);
            mgrhttp.DefaultRequestHeaders.Accept.Clear();
            mgrhttp.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));
        }

        public ManagerRestClient(int sys_svr) {
            switch (sys_svr) {
                case 0:
                    // LOCAL
                    URI = "http://localhost:53075";
                    break;
                case 1:
                    // DEV
                    URI = "https://selfitmanagerwebapplication-dev.azurewebsites.net";
                    break;
                case 2:
                    // PROD
                    URI = "https://selfitmanagerwebapplication.azurewebsites.net";
                    break;
                default:
                    break;
            }
            if (URI == string.Empty)
                throw new Exception("CRITICAL ERROR - NO SERVER CONFIGURATION!!!");
            
        }


        private void handleHttpError(Exception e) {
            MessageBox.Show("Network Error!\nPlease check computer network and try again");
            // Need to handle that data
            //MessageBox.Show("Message : " + e.Message);
            //errorMessageForm errMsg = new errorMessageForm("Network Error!", "Please check computer network and try again", e.Message);
            //errMsg.Show();
        }

        public async Task<int> restGetTherapistRights(string tName, string psw, int site) {
            int result = -1;            
            try {
                using (var client = new HttpClient()) {
                    client.BaseAddress = new Uri(URI);
                    HttpResponseMessage response = null;
                    //api/Therapist?name=<string>&psw=<string>&site=<string>
                    for(int i = 0; i < MaxRetries; i++) {
                        response = await mgrhttp.GetAsync(URI + "/api/Therapist?name=" + tName + "&psw=" + psw + "&site=" + site);
                        if (response.IsSuccessStatusCode) {
                            break;
                        }
                        response.Dispose();
                        await Task.Delay(waitTime);
                    }
                    response.EnsureSuccessStatusCode();
                    string resultStr = await response.Content.ReadAsStringAsync();                    
                    int.TryParse(resultStr, out result);
                }
            }
            catch (Exception e) {
                handleHttpError(e);
            }
            return result;
        }

        public async Task<int> restGetUsersCount(int site) {
            int count = -1;
            try {
                using (var client = new HttpClient()) {
                    client.BaseAddress = new Uri(URI);                    
                    HttpResponseMessage response = null;
                    //api/Patient?siteId=<int>
                    for (int i = 0; i < MaxRetries; i++) {
                        response = await mgrhttp.GetAsync(URI + "/api/Patient?siteId=" + site);
                        if (response.IsSuccessStatusCode) {
                            break;
                        }
                        response.Dispose();
                        await Task.Delay(waitTime);
                    }                   
                    response.EnsureSuccessStatusCode();
                    string resultStr = await response.Content.ReadAsStringAsync();
                    int.TryParse(resultStr, out count);
                }
            }
            catch (HttpRequestException e) {
                handleHttpError(e);
            }
            return count;
        }

        public async Task<PatientData> restGetPatient(long pId, int site) {
            //api/Patient?pId=<long>&site=<int>
            PatientData pData = new PatientData();
            try {
                using (var client = new HttpClient()) {
                    client.BaseAddress = new Uri(URI);
                    HttpResponseMessage response = null;
                    for (int i = 0; i < MaxRetries; i++) {
                        response = await mgrhttp.GetAsync(URI + "/api/Patient?pId=" + pId + "&site=" + site);
                        if (response.IsSuccessStatusCode) {
                            break;
                        }
                        response.Dispose();
                        await Task.Delay(waitTime);
                    }
                    response.EnsureSuccessStatusCode();
                    string responseBody = await response.Content.ReadAsStringAsync();
                    pData = JsonConvert.DeserializeObject<PatientData>(responseBody);
                }
            }
            catch (HttpRequestException e) {
                handleHttpError(e);
            }
            return pData;

        }

        //public async Task
        // GET: api/Patient?pId=<long>&site=<int> -- will return full patient information

        public async Task<int> updatePatientData(long pId, int siteId, PatientData pData) {
            //PUT: api/Patient?id=<long>&site=<int> + [FromBody] Patient
            int hasUpdated = -1;
            try {
                using (var client = new HttpClient()) {
                    client.BaseAddress = new Uri(URI);
                    //MessageBox.Show("/api/Patient?id=" + pId + "&site=" + siteId + " " + pData);
                    var response = client.PutAsJsonAsync("/api/Patient?id=" + pId + "&site=" + siteId, pData).Result;
                    //response.EnsureSuccessStatusCode();
                    if (response.IsSuccessStatusCode) {
                        hasUpdated = 0;
                    }
                }
            }
            catch (HttpRequestException e) {
                handleHttpError(e);
            }
            return hasUpdated;
        }

        public async Task<long> addPatient(PatientData pData, int siteId) {
            long pId = -1;
            // POST: api/Patient?site=<int> + [FromBody] PatientData
            try {
                using (var client = new HttpClient()) {
                    client.BaseAddress = new Uri(URI);
                    HttpResponseMessage response = await client.PostAsJsonAsync("/api/Patient?site=" + siteId, pData);
                    response.EnsureSuccessStatusCode();
                    string resultStr = await response.Content.ReadAsStringAsync();
                    if (response.IsSuccessStatusCode) {
                        long.TryParse(resultStr, out pId);
                    }
                }
            }
            catch (HttpRequestException e) {
                handleHttpError(e);
            }
            return pId;
        }

        public async Task<int> updatePatient(PatientData pData, int siteId, long patientId) {
            int hasUpdated = -1;
            // PUT: api/Patient?id=<long>&site=<int> + [FromBody] Patient
            try {
                using (var client = new HttpClient()) {
                    client.BaseAddress = new Uri(URI);
                    HttpResponseMessage response = await client.PutAsJsonAsync("/api/Patient?id="+ patientId +"&site=" + siteId, pData);
                    response.EnsureSuccessStatusCode();
                    string resultStr = await response.Content.ReadAsStringAsync();
                    if (response.IsSuccessStatusCode) {
                        int.TryParse(resultStr, out hasUpdated);
                    }
                }
            }
            catch (HttpRequestException e) {
                handleHttpError(e);
            }
            return hasUpdated;
        }

        public async Task<DataSet> getUserTestsHistory(long patientId, int siteId) {
            // GET: api/userTests?pid=<long>&site=<int> ==> DataSet<PatientTestData>
            DataSet testData = new DataSet("test_history");
            DataTable ptData = new DataTable("test_history");
            try {
                using (var client = new HttpClient()) {
                    client.BaseAddress = new Uri(URI);
                    HttpResponseMessage response = null;
                    for (int i = 0; i < MaxRetries; i++) {
                        response = await mgrhttp.GetAsync(URI + "/api/userTests?pid=" + patientId + "&site=" + siteId);
                        if (response.IsSuccessStatusCode) {
                            break;
                        }
                        response.Dispose();
                        await Task.Delay(waitTime);
                    }
                    //response.EnsureSuccessStatusCode();
                    if (response.IsSuccessStatusCode) {
                        string responseBody = await response.Content.ReadAsStringAsync();
                        ptData = JsonConvert.DeserializeObject<DataTable>(responseBody);
                        testData.Tables.Add(ptData);
                    }
                }
            }
            catch (HttpRequestException e) {
                handleHttpError(e);
            }
            return testData;
        }


        public async Task<int> addUserTestResult(long patientId, int siteId, PatientTestData ptd) {
            // POST: api/userTests?pId=<long>&site=<int> + [FromBody] PatientTestData            
            int hasAdded = -1;
            try {
                using (var client = new HttpClient()) {
                    client.BaseAddress = new Uri(URI);
                    HttpResponseMessage response = await client.PostAsJsonAsync("/api/userTests?pId=" + patientId + "&site=" + siteId, ptd);
                    response.EnsureSuccessStatusCode();
                    string resultStr = await response.Content.ReadAsStringAsync();
                    if (response.IsSuccessStatusCode) {
                        hasAdded = 0;
                    }
                }
            }
            catch (HttpRequestException e) {
                handleHttpError(e);
            }
            return hasAdded;
        }

        public async Task<DataSet> getPatientSessionInfoFiltered(long patientId, int siteId, string sDate, string eDate) {
            // GET: api/Session?pID=<long>&site=<int>&startDate=<string>&endDate=<string>
            DataSet sessionInfoDs = new DataSet("SessionInfo");
            DataTable sessionInfoDt = new DataTable("SessionInfo");
            try {
                using (var client = new HttpClient()) {
                    client.BaseAddress = new Uri(URI);
                    HttpResponseMessage response = null;
                    for (int i = 0; i < MaxRetries; i++) {
                        response = await mgrhttp.GetAsync(URI + "/api/Session?pID=" + patientId + "&site=" + siteId + "&startDate=" + sDate + "&endDate=" + eDate);
                        if (response.IsSuccessStatusCode) {
                            break;
                        }
                        response.Dispose();
                        await Task.Delay(waitTime);
                    }
                    response.EnsureSuccessStatusCode();
                    if (response.IsSuccessStatusCode) {
                        string responseBody = await response.Content.ReadAsStringAsync();
                        sessionInfoDt = JsonConvert.DeserializeObject<DataTable>(responseBody);
                        sessionInfoDs.Tables.Add(sessionInfoDt);
                    }
                }
            }
            catch (HttpRequestException e) {
                handleHttpError(e);
            }
            return sessionInfoDs;
        }

        public async Task<DataSet> getPatientParkinsonSessionData(long patientId, int sessionId, int site) {
            DataSet pkSessionInfo = new DataSet("pkSessionInfo");
            DataTable pkSessionInfoDt = new DataTable("pkSessionInfo");
            try {
                using (var client = new HttpClient()) {
                    client.BaseAddress = new Uri(URI);
                    HttpResponseMessage response = null;
                    for (int i = 0; i < MaxRetries; i++) {
                        response = await mgrhttp.GetAsync(URI + "/api/ParkinsonData?userId=" + patientId + "&siteId=" + site + "&sessionId=" + sessionId);
                        if (response.IsSuccessStatusCode) {
                            break;
                        }
                        response.Dispose();
                        await Task.Delay(waitTime);
                    }
                    response.EnsureSuccessStatusCode();
                    if (response.IsSuccessStatusCode) {
                        string responseBody = await response.Content.ReadAsStringAsync();
                        pkSessionInfoDt = JsonConvert.DeserializeObject<DataTable>(responseBody);
                        pkSessionInfo.Tables.Add(pkSessionInfoDt);
                    }
                }
            }
            catch (HttpRequestException e) {
                handleHttpError(e);
                //MessageBox.Show("getPatientSessionInfo: " + patientId + ", " + siteId + " Message :{0} ", e.Message);
            }

            return pkSessionInfo;
        }

        public async Task<DataSet> getParkinsonStepsData(long patientId, int sessionId, int site) {
            DataSet pkStepsData = new DataSet("pkStepsData");
            DataTable pkStepsDt = new DataTable("pkStepsData");
            try {
                using (var client = new HttpClient()) {
                    client.BaseAddress = new Uri(URI);
                    HttpResponseMessage response = null;
                    for (int i = 0; i < MaxRetries; i++) {
                        response = await mgrhttp.GetAsync(URI + "/api/ParkinsonStepsData?userId=" + patientId + "&siteId=" + site + "&sessionId=" + sessionId);
                        if (response.IsSuccessStatusCode) {
                            break;
                        }
                        response.Dispose();
                        await Task.Delay(waitTime);
                    }
                    response.EnsureSuccessStatusCode();
                    if (response.IsSuccessStatusCode) {
                        string responseBody = await response.Content.ReadAsStringAsync();
                        pkStepsDt = JsonConvert.DeserializeObject<DataTable>(responseBody);
                        pkStepsData.Tables.Add(pkStepsDt);
                    }
                }
            }
            catch (HttpRequestException e) {
                handleHttpError(e);
                //MessageBox.Show("getPatientSessionInfo: " + patientId + ", " + siteId + " Message :{0} ", e.Message);
            }
            return pkStepsData;
        }


        public async Task<DataSet> getPatientSessionInfo(long patientId, int siteId) {
            // GET: api/Session?pID=<long>&site=<int>
            DataSet sessionInfoDs = new DataSet("SessionInfo");
            DataTable sessionInfoDt = new DataTable("SessionInfo");
            try {
                using (var client = new HttpClient()) {
                    client.BaseAddress = new Uri(URI);
                    HttpResponseMessage response = null;
                    for (int i = 0; i < MaxRetries; i++) {
                        response = await mgrhttp.GetAsync(URI + "/api/Session?patientId=" + patientId + "&siteID=" + siteId);
                        if (response.IsSuccessStatusCode) {
                            break;
                        }
                        response.Dispose();
                        await Task.Delay(waitTime);
                    }
                    response.EnsureSuccessStatusCode();
                    if (response.IsSuccessStatusCode) {
                        string responseBody = await response.Content.ReadAsStringAsync();
                        sessionInfoDt = JsonConvert.DeserializeObject<DataTable>(responseBody);
                        sessionInfoDs.Tables.Add(sessionInfoDt);
                    }
                }
            }
            catch (HttpRequestException e) {
                handleHttpError(e);
                //MessageBox.Show("getPatientSessionInfo: " + patientId + ", " + siteId + " Message :{0} ", e.Message);
            }
            return sessionInfoDs;
        }

        public async Task<List<string>> getPatientSessionData(long patientId, int sessionId, int site) {
            // GET: api/SessionData?patientID=<long>&sessionID=<int>&siteID=<int>
            //return mdb.getUserSessionData(pId, sessionId);
            List<string> sessionData = new List<string>();
            try {
                using (var client = new HttpClient()) {
                    client.BaseAddress = new Uri(URI);
                    HttpResponseMessage response = null;
                    for (int i = 0; i < MaxRetries; i++) {
                        response = await mgrhttp.GetAsync(URI + "/api/SessionData?patientID=" + patientId + "&sessionID=" + sessionId + "&siteID=" + site);
                        if (response.IsSuccessStatusCode) {
                            break;
                        }
                        response.Dispose();
                        await Task.Delay(waitTime);
                    }
                    response.EnsureSuccessStatusCode();
                    string responseBody = await response.Content.ReadAsStringAsync();
                    sessionData = JsonConvert.DeserializeObject<List<string>>(responseBody);
                }
            }
            catch (HttpRequestException e) {
                handleHttpError(e);
            }
            return sessionData;
        }

        public async Task<DataSet> getPatientSessionHistory(long patientId, int site, int limit, int sessionId) {
            // GET: api/SessionHistory?userId=<long>&siteId=<int>&DataLimit=<int>&sessionId=<int>
            //    return mdb.getUserSessionHistory(patientId, limit, sessionId);
            DataSet sessionHistDS = new DataSet("history_data");
            DataTable sHistDT = new DataTable("history_data");
            try {
                using (var client = new HttpClient()) {
                    client.BaseAddress = new Uri(URI);
                    HttpResponseMessage response = null;
                    for (int i = 0; i < MaxRetries; i++) {
                        //MessageBox.Show(URI + "/api/SessionHistory?userId=" + patientId + "&siteId=" + site + "&DataLimit=" + limit + "&sessionId=" + sessionId);
                        response = await mgrhttp.GetAsync(URI + "/api/SessionHistory?userId=" + patientId + "&siteId=" + site + "&DataLimit=" + limit + "&sessionId=" + sessionId);
                        if (response.IsSuccessStatusCode) {
                            break;
                        }
                        response.Dispose();
                        await Task.Delay(waitTime);
                    }
                    response.EnsureSuccessStatusCode();
                    if (response.IsSuccessStatusCode) {
                        string responseBody = await response.Content.ReadAsStringAsync();
                        sHistDT = JsonConvert.DeserializeObject<DataTable>(responseBody);
                        sessionHistDS.Tables.Add(sHistDT);
                    }
                }
            }
            catch (HttpRequestException e) {
                handleHttpError(e);
            }
            return sessionHistDS;
        }

        public async Task<DataSet> getSessionPractices(int sessionId, int site) {
            // GET: api/SessionHistory?siteId=<int>&sessionId=<int>
            DataSet sPracticesDS = new DataSet("session_practices");
            DataTable spDT = new DataTable("session_practices");
            try {
                using (var client = new HttpClient()) {
                    client.BaseAddress = new Uri(URI);
                    HttpResponseMessage response = null;
                    for (int i = 0; i < MaxRetries; i++) {
                        response = await mgrhttp.GetAsync(URI + "/api/SessionHistory?site_id=" + site + "&session=" + sessionId);
                        if (response.IsSuccessStatusCode) {
                            break;
                        }
                        response.Dispose();
                        await Task.Delay(waitTime);
                    }
                    response.EnsureSuccessStatusCode();
                    if (response.IsSuccessStatusCode) {
                        string responseBody = await response.Content.ReadAsStringAsync();
                        //MessageBox.Show("Response Body: " + responseBody);
                        spDT = JsonConvert.DeserializeObject<DataTable>(responseBody);
                        sPracticesDS.Tables.Add(spDT);                        
                    }
                }
            }
            catch (HttpRequestException e) {
                handleHttpError(e);
            }
            return sPracticesDS;
        }

        public async Task<DataSet> getPatientPlanData(int planId, int siteId) {
            // GET: api/Plan?planId=<int>&siteId=<int>
            //   return mdb.getUserPlanData(planId);
            DataSet planDS = new DataSet("plan_data");
            DataTable planDT = new DataTable("plan_data");
            try {
                using (var client = new HttpClient()) {
                    client.BaseAddress = new Uri(URI);
                    HttpResponseMessage response = null;
                    for (int i = 0; i < MaxRetries; i++) {
                        response = await mgrhttp.GetAsync(URI + "/api/Plan?planId=" + planId + "&siteId=" + siteId);
                        if (response.IsSuccessStatusCode) {
                            break;
                        }
                        response.Dispose();
                        await Task.Delay(waitTime);
                    }
                    response.EnsureSuccessStatusCode();
                    if (response.IsSuccessStatusCode) {
                        string responseBody = await response.Content.ReadAsStringAsync();
                        planDT = JsonConvert.DeserializeObject<DataTable>(responseBody);
                        planDS.Tables.Add(planDT);
                    }
                }
            }
            catch (HttpRequestException e) {
                handleHttpError(e);
            }
            return planDS;
        }

        public async Task<int> storePatientPlan(long patientId, int siteId, string planName, List<PlanData> pData) {
            // POST: api/Plan?patient=<long>&site=<int>&plan_name=<string> + [FromBody] List<PlanData>
            //return mdb.storeNewUserPlan(patianteId, planName, plan_practices);
            int pID = -1;
            try {
                using (var client = new HttpClient()) {
                    client.BaseAddress = new Uri(URI);
                    HttpResponseMessage response = await client.PostAsJsonAsync("/api/Plan?patient=" + patientId + "&site=" + siteId + "&plan_name=" + planName, pData);
                    response.EnsureSuccessStatusCode();
                    string resultStr = await response.Content.ReadAsStringAsync();
                    if (response.IsSuccessStatusCode) {
                        int.TryParse(resultStr, out pID);
                    }
                }
            }
            catch (HttpRequestException e) {
                handleHttpError(e);
            }
            return pID;
        }

        public async Task<int> getPateintPlanCount(long patientId) {
            // GET: api/PlanInfo?patientId=<long>
            //return mdb.getUserPlansCount(patianteId);
            int count = 0;
            try {
                using (var client = new HttpClient()) {
                    client.BaseAddress = new Uri(URI);
                    HttpResponseMessage response = null;
                    for (int i = 0; i < MaxRetries; i++) {
                        response = await mgrhttp.GetAsync(URI + "/api/PlanInfo?patientId=" + patientId);
                        if (response.IsSuccessStatusCode) {
                            break;
                        }
                        response.Dispose();
                        await Task.Delay(waitTime);
                    }
                    response.EnsureSuccessStatusCode();
                    string resultStr = await response.Content.ReadAsStringAsync();
                    if (response.IsSuccessStatusCode) {
                        int.TryParse(resultStr, out count);
                    }
                }
            }
            catch (HttpRequestException e) {
                handleHttpError(e);
            }
            return count;
        }

        public async Task<DataSet> getPatientPlansInfo(long patientId, int site) {
            // GET: api/PlanInfo?patientId=<long>&siteId=<int>            
            DataSet pPlanData = new DataSet("plans_info");
            DataTable pPlanDataTable = new DataTable("plans_info");
            try {
                using (var client = new HttpClient()) {
                    client.BaseAddress = new Uri(URI);
                    HttpResponseMessage response = null;
                    for (int i = 0; i < MaxRetries; i++) {
                        response = await mgrhttp.GetAsync(URI + "/api/PlanInfo?patient=" + patientId + "&site="+ site);
                        if (response.IsSuccessStatusCode) {
                            break;
                        }
                        response.Dispose();
                        await Task.Delay(waitTime);
                    }
                    response.EnsureSuccessStatusCode();
                    if (response.IsSuccessStatusCode) {
                        string responseBody = await response.Content.ReadAsStringAsync();
                        pPlanDataTable = JsonConvert.DeserializeObject<DataTable>(responseBody);
                        pPlanData.Tables.Add(pPlanDataTable);
                    }

                }
            }
            catch (HttpRequestException e) {
                handleHttpError(e);
            }

            return pPlanData;
        }

        public async Task<DataSet> getPatientPlanSummaryData(long patientId, int site) {
            // GET: api/PlanSummary?patientId<long>&siteId=<int>            
            DataSet pPlanSumData = new DataSet("PlansReportSummary");
            DataTable pPlanSumDT = new DataTable("PlansReportSummary");
            try {
                using (var client = new HttpClient()) {
                    client.BaseAddress = new Uri(URI);
                    HttpResponseMessage response = null;
                    for (int i = 0; i < MaxRetries; i++) {
                        response = await mgrhttp.GetAsync(URI + "/api/PlanSummary?patientId=" + patientId + "&siteId=" + site);
                        if (response.IsSuccessStatusCode) {
                            break;
                        }
                        response.Dispose();
                        await Task.Delay(waitTime);
                    }
                    response.EnsureSuccessStatusCode();
                    if (response.IsSuccessStatusCode) {
                        string responseBody = await response.Content.ReadAsStringAsync();
                        pPlanSumDT = JsonConvert.DeserializeObject<DataTable>(responseBody);
                        pPlanSumData.Tables.Add(pPlanSumDT);
                    }
                }
            }
            catch (HttpRequestException e) {
                handleHttpError(e);
            }
            return pPlanSumData;
        }

        public async Task<DataSet> getImageNames(int site) {
            // GET: api/PracticeImages?siteId=<int>
            DataSet imageDS = new DataSet("image_info");
            DataTable imageDT = new DataTable("image_info");
            try {
                HttpResponseMessage response = null;
                for (int i = 0; i < MaxRetries; i++) {
                    response = await mgrhttp.GetAsync(URI + "/api/PracticeImages?siteId=" + site);
                    if (response.IsSuccessStatusCode) {
                        break;
                    }
                    response.Dispose();
                    await Task.Delay(waitTime);
                }
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode) {
                    string responseBody = await response.Content.ReadAsStringAsync();
                    imageDT = JsonConvert.DeserializeObject<DataTable>(responseBody);
                    imageDS.Tables.Add(imageDT);
                }                
            }
            catch (HttpRequestException e) {
                handleHttpError(e);                
            }
            return imageDS;
        }

        public async Task<DataSet> getPatientPracticeMeasuresData(long patientId, int site, int practiceId) {
            DataSet ppmDataset = new DataSet("pMeasuresData");
            DataTable ppmDataTable = new DataTable("pMeasuresData");
            try {
                HttpResponseMessage response = null;
                for (int i = 0; i < MaxRetries; i++) {
                    response = await mgrhttp.GetAsync(URI + "/api/Report?patientId=" + patientId + "&siteId=" + site + "&practiceId=" + practiceId);
                    if (response.IsSuccessStatusCode) {
                        break;
                    }
                    response.Dispose();
                    await Task.Delay(waitTime);
                }
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode) {
                    string responseBody = await response.Content.ReadAsStringAsync();
                    ppmDataTable = JsonConvert.DeserializeObject<DataTable>(responseBody);
                    ppmDataset.Tables.Add(ppmDataTable);
                }
            }
            catch (HttpRequestException e) {
                handleHttpError(e);
            }

            return ppmDataset;
        }

        public async Task<DataSet> getPracticeMeasuresInfo(int site) {
            // GET: api/Report
            DataSet pmDS = new DataSet("PracticeMeasures");
            DataTable MeasuresDT = new DataTable("PracticeMeasures");
            try {
                HttpResponseMessage response = null;
                for (int i = 0; i < MaxRetries; i++) {
                    response = await mgrhttp.GetAsync(URI + "/api/Report");
                    if (response.IsSuccessStatusCode) {
                        break;
                    }
                    response.Dispose();
                    await Task.Delay(waitTime);
                }
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode) {
                    string responseBody = await response.Content.ReadAsStringAsync();
                    MeasuresDT = JsonConvert.DeserializeObject<DataTable>(responseBody);
                    pmDS.Tables.Add(MeasuresDT);
                }
            }
            catch (HttpRequestException e) {
                handleHttpError(e);
            }
            return pmDS;
        }

        public async Task<DataSet> getAllActivePractices() {
            // GET: api/PracticeData
            //return mdb.getAllActivePractices();
            DataSet practicesDS = new DataSet("practice_data");
            DataTable practicesDT = new DataTable("practice_data");
            try {
                using (var client = new HttpClient()) {
                    client.BaseAddress = new Uri(URI);
                    HttpResponseMessage response = null;
                    for (int i = 0; i < MaxRetries; i++) {
                        response = await mgrhttp.GetAsync(URI + "/api/PracticeData");
                        if (response.IsSuccessStatusCode) {
                            break;
                        }
                        response.Dispose();
                        await Task.Delay(waitTime);
                    }
                    response.EnsureSuccessStatusCode();
                    if (response.IsSuccessStatusCode) {
                        string responseBody = await response.Content.ReadAsStringAsync();
                        practicesDT = JsonConvert.DeserializeObject<DataTable>(responseBody);
                        practicesDS.Tables.Add(practicesDT);
                    }
                }
            }
            catch (HttpRequestException e) {
                handleHttpError(e);
            }
            return practicesDS;
        }

        public async Task<DataSet> getPerformedPractices(long pId, int SITEID) {
            DataSet pPracticesDS = new DataSet("pPractice_data");
            DataTable practicesDT = new DataTable("pPractice_data");
            try {
                using (var client = new HttpClient()) {
                    client.BaseAddress = new Uri(URI);
                    HttpResponseMessage response = null;
                    for (int i = 0; i < MaxRetries; i++) {
                        response = await mgrhttp.GetAsync(URI + "/api/Report?pId=" + pId + "&site="+ SITEID);
                        if (response.IsSuccessStatusCode) {
                            break;
                        }
                        response.Dispose();
                        await Task.Delay(waitTime);
                    }
                    response.EnsureSuccessStatusCode();
                    if (response.IsSuccessStatusCode) {
                        string responseBody = await response.Content.ReadAsStringAsync();
                        practicesDT = JsonConvert.DeserializeObject<DataTable>(responseBody);
                        pPracticesDS.Tables.Add(practicesDT);
                    }
                }
            }
            catch (HttpRequestException e) {
                handleHttpError(e);
            }
            return pPracticesDS;
        }
    

        public async Task<DataSet> getFilterActivePractices(List<string> filters, int pos) {
            // GET: api/PracticeData?position=<int> [FromBody] List<string> filters
            DataSet practicesDS = new DataSet("practice_filtered_data");
            DataTable practicesDT = new DataTable("practice_filtered_data");
            try {
                using (var client = new HttpClient()) {
                    client.BaseAddress = new Uri(URI);
                    //string join = string.Join("&filters=", filters);
                    string join = string.Empty;
                    foreach (string s in filters) {
                        join += "&filters=" + s;
                    }
                    HttpResponseMessage response = null;
                    //MessageBox.Show(URI + "/api/PracticeData?position=" + pos + join);
                    for (int i = 0; i < MaxRetries; i++) {
                        response = await client.GetAsync (URI + "/api/PracticeData?position=" + pos + join);
                        if (response.IsSuccessStatusCode) {
                            break;
                        }
                        response.Dispose();
                        await Task.Delay(waitTime);
                    }
                    response.EnsureSuccessStatusCode();
                    if (response.IsSuccessStatusCode) {
                        string responseBody = await response.Content.ReadAsStringAsync();
                        practicesDT = JsonConvert.DeserializeObject<DataTable>(responseBody);
                        practicesDS.Tables.Add(practicesDT);
                    }
                }
            }
            catch (HttpRequestException e) {
                handleHttpError(e);
            }
            return practicesDS;
        }

        public async Task<DataSet> getGeneralReportDataFiltered(string fromDateStr, string toDateStr, string patientIdStr, int site) {
            // GET: api/GeneralReport?from=<string>&to=<string>&patientID=<string>&site=<int>
            // return mdb.getGeneralReportDataFiltered(fromDate, toDate, patientIdStr);
            DataSet genRepDS = new DataSet("GeneralReportData");
            DataTable genRepDT = new DataTable("GeneralReportData");
            try {
                using (var client = new HttpClient()) {
                    client.BaseAddress = new Uri(URI);
                    HttpResponseMessage response = null;
                    //MessageBox.Show(URI + "/api/GeneralReport?from=" + fromDateStr + "&to=" + toDateStr + "&patientID=" + patientIdStr + "&site=" + site);
                    for (int i = 0; i < MaxRetries; i++) {
                        response = await mgrhttp.GetAsync(URI + "/api/GeneralReport?from=" + fromDateStr +"&to=" + toDateStr + "&patientID=" + patientIdStr + "&site=" + site);
                        if (response.IsSuccessStatusCode) {
                            break;
                        }
                        response.Dispose();
                        await Task.Delay(waitTime);
                    }
                    response.EnsureSuccessStatusCode();
                    if (response.IsSuccessStatusCode) {
                        string responseBody = await response.Content.ReadAsStringAsync();
                        genRepDT = JsonConvert.DeserializeObject<DataTable>(responseBody);
                        genRepDS.Tables.Add(genRepDT);
                    }
                }
            }
            catch (HttpRequestException e) {
                handleHttpError(e);
            }
            return genRepDS;
        }

        public async Task<DataSet> getSummaryReportDataFiltered(string fromDateStr, string toDateStr, string patientIdStr, int site) {
            // GET: api/SummaryReport?fromDate=<string>&toDate=<string>&userIdStr=<string>&siteId=<int>
            // return mdb.getSummaryReportDataFiltered(fromDate, toDate, patientIdStr);
            DataSet sumRepDS = new DataSet("GeneralReportData");
            DataTable sumRepDT = new DataTable("GeneralReportData");
            try {
                using (var client = new HttpClient()) {
                    client.BaseAddress = new Uri(URI);
                    HttpResponseMessage response = null;
                    //MessageBox.Show(URI + "/api/SummaryReport?fromDate=" + fromDateStr + "&toDate=" + toDateStr + "&userIdStr=" + patientIdStr + "&siteId=" + site);
                    for (int i = 0; i < MaxRetries; i++) {
                        response = await mgrhttp.GetAsync(URI + "/api/SummaryReport?fromDate=" + fromDateStr + "&toDate=" + toDateStr + "&userIdStr=" + patientIdStr + "&siteId=" + site);
                        if (response.IsSuccessStatusCode) {
                            break;
                        }
                        response.Dispose();
                        await Task.Delay(waitTime);
                    }
                    response.EnsureSuccessStatusCode();
                    if (response.IsSuccessStatusCode) {
                        string responseBody = await response.Content.ReadAsStringAsync();
                        sumRepDT = JsonConvert.DeserializeObject<DataTable>(responseBody);
                        sumRepDS.Tables.Add(sumRepDT);
                    }
                }
            }
            catch (HttpRequestException e) {
                handleHttpError(e);
            }
            return sumRepDS;
        }

        public async Task<int> getNewQuickPlanNumber(long pId, int SITEID, int duration, bool standing, bool sitting, List<string> filters) {
            // api/QuickPlan?patient=long&site=int&totalTime=int&standing=bool&sitting=bool [FromBody] List<string> categories
            int qpNum = -1;
            try {
                using (var client = new HttpClient()) {
                    client.BaseAddress = new Uri(URI);
                    string join = string.Empty;
                    foreach (string s in filters) {
                        join += "&categories=" + s;
                    }
                    string req = "&standing=" + standing + "&sitting=" + sitting + join;
                    HttpResponseMessage response = null;
                    //MessageBox.Show("Message :{0} ", req);
                    for (int i = 0; i < MaxRetries; i++) {
                        response = await client.GetAsync(URI + "/api/QuickPlan?patient=" + pId + "&site=" + SITEID + "&totalTime=" + duration + "&standing="+ standing + "&sitting=" + sitting + join);
                        if (response.IsSuccessStatusCode) {
                            break;
                        }
                        response.Dispose();
                        await Task.Delay(waitTime);
                    }
                    response.EnsureSuccessStatusCode();
                    string resultStr = await response.Content.ReadAsStringAsync();
                    //MessageBox.Show("Message: " + resultStr);
                    if (response.IsSuccessStatusCode) {
                        int.TryParse(resultStr, out qpNum);
                    }
                }
            }
            catch (Exception e) {
                handleHttpError(e);
            }
            return qpNum;
        }

        public async Task<DataSet> getPatientSummaryReportData(long userId, int site) {
            // GET: api/SummaryReport?userIdStr=<string>&siteId=<int>
            // return mdb.getPatientSummaryReportData(patientId);            
            DataSet repDS = new DataSet("GeneralReportData");
            DataTable repDT = new DataTable("GeneralReportData");
            try {
                using (var client = new HttpClient()) {
                    client.BaseAddress = new Uri(URI);
                    HttpResponseMessage response = null;
                    for (int i = 0; i < MaxRetries; i++) {
                        response = await mgrhttp.GetAsync(URI + "/api/SummaryReport?userIdStr=" + userId + "&siteId=" + site);
                        if (response.IsSuccessStatusCode) {
                            break;
                        }
                        response.Dispose();
                        await Task.Delay(waitTime);
                    }
                    response.EnsureSuccessStatusCode();
                    if (response.IsSuccessStatusCode) {
                        string responseBody = await response.Content.ReadAsStringAsync();
                        repDT = JsonConvert.DeserializeObject<DataTable>(responseBody);
                        repDS.Tables.Add(repDT);
                    }
                }
            }
            catch (HttpRequestException e) {
                handleHttpError(e);
            }
            return repDS;
        }

    }
}

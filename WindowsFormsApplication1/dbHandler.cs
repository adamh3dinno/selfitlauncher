﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;


namespace SelfitMain
{
    class dbHandler
    {
        private string _conStr;
        MySqlConnection mconnection;
        private MySqlCommand cmd = null;
        private MySqlDataReader rdr = null;
        private MySqlDataAdapter adapter = null;

        /// <summary>
        /// The constructor.
        /// </summary>
        public dbHandler()
        {
            _conStr = "Server=localhost;Database=unity-db;User=unity;Password=123456;Pooling=true;";
            //_conStr = "Server=localhost;Database=unity-db-dev;User=unity;Password=123456;Pooling=true;";
            //_conStr = "Server=localhost;Database=unity-db-hursha;User=unity;Password=123456;Pooling=true;";            
            //_conStr = "Server=selfitdevtestdb.database.windows.net;Database=unity-db;User=apple;Password=2VfmY!R2ny!R06rS;Pooling=true;";

            try {
                
                mconnection = new MySqlConnection(_conStr);
                mconnection.Open();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        ~dbHandler()
        {
            if (mconnection != null)
            {
                if (mconnection.State.ToString() != "Closed")
                {
                    mconnection.Close();
                }
                mconnection.Dispose();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="uName"></param>
        /// <param name="uPass"></param>
        /// <returns>-1 if not found, else the user rights value</returns>
        public int getUserRights(string uName, string uPass)
        {
            int uRights = -1;
            try
            {
                //string query = "SELECT user_type FROM users WHERE user_name = '" + uName + "' AND user_password = '" + uPass + "'";
                string query = "SELECT user_type FROM users WHERE user_password = '" + uName + "'";
                if (mconnection.State.ToString() != "Open")
                    mconnection.Open();
                using (mconnection)
                {
                    using (cmd = new MySqlCommand(query, mconnection))
                    {
                        rdr = cmd.ExecuteReader();
                        if (rdr.HasRows)
                            while (rdr.Read())
                            {
                                uRights = (int)rdr[0];
                            }
                        rdr.Dispose();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            return uRights;
        }

        /// <summary>
        /// getUserID
        /// </summary>
        /// <param name="uName"></param>
        /// <param name="uPass"></param>
        /// <returns></returns>
        public int getUserID(string uName, string uPass)
        {
            int uId = -1;
            try
            {
                //string query = "SELECT user_id FROM users WHERE user_name = '" + uName + "' AND user_password = '" + uPass + "'";
                string query = "SELECT user_id FROM users WHERE user_password = '" + uName + "'";
                if (mconnection.State.ToString() != "Open")
                    mconnection.Open();
                using (mconnection)
                {
                    using (cmd = new MySqlCommand(query, mconnection))
                    {
                        rdr = cmd.ExecuteReader();
                        if (rdr.HasRows)
                            while (rdr.Read())
                            {
                                uId = (int)rdr[0];
                            }
                        rdr.Dispose();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            return uId;
        }

        public int getUserLevel(int uId)
        {
            int uLevel = 1;
            try
            {
                string query = "SELECT user_level FROM user_info WHERE user_id = " + uId;
                if (mconnection.State.ToString() != "Open")
                    mconnection.Open();
                using (mconnection)
                {
                    using (cmd = new MySqlCommand(query, mconnection))
                    {
                        rdr = cmd.ExecuteReader();
                        if (rdr.HasRows)
                            while (rdr.Read())
                            {
                                uLevel = (int)rdr[0];
                            }
                        rdr.Dispose();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            return uLevel;
        }

        public int getSiteIDnumber() {
            int site = -1;
            try {
                string query = "SELECT site_id FROM system_settings";
                if (mconnection.State.ToString() != "Open")
                    mconnection.Open();
                using (mconnection) {
                    using (cmd = new MySqlCommand(query, mconnection)) {
                        rdr = cmd.ExecuteReader();
                        if (rdr.HasRows)
                            while (rdr.Read()) {
                                site = (int)rdr[0];
                            }
                        rdr.Dispose();
                    }
                }
            }
            catch (Exception ex) {
                MessageBox.Show(ex.Message);
            }

            return site;
        }


        public int getSystemServer() {
            int server = -1;
            try {
                string query = "SELECT system_server FROM system_settings";
                if (mconnection.State.ToString() != "Open")
                    mconnection.Open();
                using (mconnection) {
                    using (cmd = new MySqlCommand(query, mconnection)) {
                        rdr = cmd.ExecuteReader();
                        if (rdr.HasRows)
                            while (rdr.Read()) {
                                server = (int)rdr[0];
                            }
                        rdr.Dispose();
                    }
                }
            }
            catch (Exception ex) {
                MessageBox.Show(ex.Message);
            }

            return server;
        }

        public int getDefaultLang() {
            int lang = 0;
            try {
                string query = "SELECT default_lang_id FROM system_settings";
                if (mconnection.State.ToString() != "Open")
                    mconnection.Open();
                using (mconnection) {
                    using (cmd = new MySqlCommand(query, mconnection)) {
                        rdr = cmd.ExecuteReader();
                        if (rdr.HasRows)
                            while (rdr.Read()) {
                                lang = (int)rdr[0];
                            }
                        rdr.Dispose();
                    }
                }
            }
            catch (Exception ex) {
                MessageBox.Show(ex.Message);
            }
            return lang;
        }


        public string getUserNotes(long uId)
        {
            string data = "";
            //SELECT user_comments FROM `unity-db`.user_info WHERE user_id = 2;
            try
            {
                string query = "SELECT user_comments FROM user_info WHERE user_id = " + uId;
                if (mconnection.State.ToString() != "Open")
                    mconnection.Open();
                using (mconnection)
                {
                    using (cmd = new MySqlCommand(query, mconnection))
                    {
                        rdr = cmd.ExecuteReader();
                        if (rdr.HasRows)
                            while (rdr.Read())
                            {
                                data = rdr[0].ToString();
                            }
                        rdr.Dispose();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            return data;
        }

        public bool setUserNotes(long uId, string notes) {
            bool stored = false;
            try 
            {
                string query = "UPDATE user_info SET user_comments = '" + notes + "' WHERE user_id = " + uId;
                if (mconnection.State.ToString() != "Open")
                    mconnection.Open();
                using (mconnection) {
                    using (cmd = new MySqlCommand(query, mconnection)) {
                        int result = cmd.ExecuteNonQuery();
                        if (result > 0)
                            stored = true;
                    }
                }
            }
            catch (Exception ex) {
                MessageBox.Show(ex.Message);
            }

            return stored;
        }

        public string getSystemPath() {
            string sysPath = string.Empty;
            try {
                string query = "SELECT selfit_path FROM system_settings";
                if (mconnection.State.ToString() != "Open")
                    mconnection.Open();
                using (mconnection) {
                    using (cmd = new MySqlCommand(query, mconnection)) {
                        rdr = cmd.ExecuteReader();
                        if (rdr.HasRows)
                            while (rdr.Read()) {
                                sysPath = rdr[0].ToString();
                            }
                        rdr.Dispose();
                    }
                }
            }
            catch (Exception ex) {
                MessageBox.Show(ex.Message);
            }

            return sysPath;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="path"></param>
        public void storeSystemPath(string path) {
            try {
                string query = "UPDATE system_settings SET selfit_path =  '" + path + "'";
                if (mconnection.State.ToString() != "Open")
                    mconnection.Open();
                using (mconnection) {
                    using (cmd = new MySqlCommand(query, mconnection)) {
                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex) {
                MessageBox.Show(ex.Message);
            }
        }

        /// <summary>
        ///  
        /// </summary>
        /// <returns></returns>
        public bool getHebSpeachState()
        {
            bool state = false;
            //try
            //{
            //    string query = "SELECT need_heb_sound FROM system_settings";
            //    if (mconnection.State.ToString() != "Open")
            //        mconnection.Open();
            //    using (mconnection)
            //    {
            //        using (cmd = new MySqlCommand(query, mconnection))
            //        {
            //            rdr = cmd.ExecuteReader();
            //            if (rdr.HasRows)
            //                while (rdr.Read())
            //                {
            //                    state = (int)rdr[0] == 1;
            //                }
            //            rdr.Dispose();
            //        }
            //    }
            //}
            //catch (Exception ex)
            //{
            //    MessageBox.Show(ex.Message);
            //}
            return state;
        }

        public bool getDemoState() {
            bool state = false;
            try {
                string query = "SELECT is_demo_mode FROM system_settings";
                if (mconnection.State.ToString() != "Open")
                    mconnection.Open();
                using (mconnection) {
                    using (cmd = new MySqlCommand(query, mconnection)) {
                        rdr = cmd.ExecuteReader();
                        if (rdr.HasRows)
                            while (rdr.Read()) {
                                state = (int)rdr[0] == 1;
                            }
                        rdr.Dispose();
                    }
                }
            }
            catch (Exception ex) {
                MessageBox.Show(ex.Message);
            }
            return state;
        }

        /// <summary>
        ///  
        /// </summary>
        /// <returns></returns>
        public bool getRateControlstate()
        {
            bool state = false;
            try
            {
                string query = "SELECT need_rate FROM system_settings";
                if (mconnection.State.ToString() != "Open")
                    mconnection.Open();
                using (mconnection)
                {
                    using (cmd = new MySqlCommand(query, mconnection))
                    {
                        rdr = cmd.ExecuteReader();
                        if (rdr.HasRows)
                            while (rdr.Read())
                            {
                                state = (int)rdr[0] == 1;
                            }
                        rdr.Dispose();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            return state;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="st"></param>
        public void setHebSpeachState(bool st)
        {
            int state = st ? 1:0;
            try
            {
                string query = "UPDATE system_settings SET need_heb_sound =  " + st;
                if (mconnection.State.ToString() != "Open")
                    mconnection.Open();
                using (mconnection)
                {
                    using (cmd = new MySqlCommand(query, mconnection))
                    {
                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="st"></param>
        public void setRateState(bool st)
        {
            int state = st ? 1 : 0;
            try
            {
                string query = "UPDATE system_settings SET need_rate =  " + st;
                if (mconnection.State.ToString() != "Open")
                    mconnection.Open();
                using (mconnection)
                {
                    using (cmd = new MySqlCommand(query, mconnection))
                    {
                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="uId"></param>
        /// <param name="Level"></param>
        public void setUserDifficultyLevel(int uId, int Level)
        {
            try
            {
                string query = "UPDATE user_info SET user_level = " + Level + " WHERE user_id = " + uId;
                if (mconnection.State.ToString() != "Open")
                    mconnection.Open();
                using (mconnection)
                {
                    using (cmd = new MySqlCommand(query, mconnection))
                    {
                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pId"></param>
        /// <returns></returns>
        public string getPracticeExe(string pId)
        {
            string practice = null;
            try
            {
                string query = "SELECT practice_executable FROM practice_data WHERE practice_id= " + pId;
                if (mconnection.State.ToString() != "Open")
                    mconnection.Open();
                using (mconnection)
                {
                    using (cmd = new MySqlCommand(query, mconnection))
                    {
                        rdr = cmd.ExecuteReader();
                        if (rdr.HasRows)
                            while (rdr.Read())
                            {
                                practice = rdr[0].ToString();
                            }
                        rdr.Dispose();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            return practice;
        }

        public string getUserTotalSessionsTime(int uId)
        {
            string total = null;
            try
            {
                string query = "SELECT SEC_TO_TIME(SUM(TIME_TO_SEC(TIMEDIFF(end_time, start_time)))) as Total " +
                               "FROM sessions WHERE user_id = " + uId + 
                               " AND end_time > start_time";
                if (mconnection.State.ToString() != "Open")
                    mconnection.Open();
                using (mconnection)
                {
                    using (cmd = new MySqlCommand(query, mconnection))
                    {
                        rdr = cmd.ExecuteReader();
                        if (rdr.HasRows)
                            while (rdr.Read())
                            {
                                total = rdr[0].ToString();
                            }
                        rdr.Dispose();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            return total;
        }

        public int getUserPlansCount(long uId) {
            int pCount = 0;
            try {
                string query = "SELECT COUNT(*) FROM plans_info WHERE plan_user_id = " + uId;                
                if (mconnection.State.ToString() != "Open")
                    mconnection.Open();
                using (mconnection) {
                    using (cmd = new MySqlCommand(query, mconnection)) {
                        rdr = cmd.ExecuteReader();
                        if (rdr.HasRows)
                            while (rdr.Read()) {
                                pCount = int.Parse(rdr[0].ToString());
                            }
                        rdr.Dispose();
                    }
                }
            }
            catch (Exception ex) {
                MessageBox.Show(ex.Message);
            }

            return pCount;
        }

        //public string getPatientFullNameById(int pId) 
        //{
        //    string pFullName = string.Empty;
        //    //SELECT CONCAT(user_fName, ' ' ,user_lName) as userName FROM `unity-db`.user_info;
        //    try {
        //        string query = "SELECT CONCAT(user_fName, ' ' ,user_lName) as userName " +
        //            "FROM .user_info WHERE user_id = " + pId;
        //        if (mconnection.State.ToString() != "Open")
        //            mconnection.Open();
        //        using (mconnection) {
        //            using (cmd = new MySqlCommand(query, mconnection)) {
        //                rdr = cmd.ExecuteReader();
        //                if (rdr.HasRows)
        //                    while (rdr.Read()) {
        //                        pFullName = rdr[0].ToString();
        //                    }
        //                rdr.Dispose();
        //            }
        //        }
        //    }
        //    catch (Exception ex) {
        //        MessageBox.Show(ex.Message);
        //    }
        //    return pFullName;
        //}

        /// <summary>
        /// 
        /// </summary>
        /// <param name="uId"></param>
        /// <returns></returns>
        public List<string> getUserInformation(long uId)
        {
            var retList = new List<string>();
            string query = string.Empty;
            try
            {
                query = "SELECT * FROM user_info WHERE user_id = " + uId;
                if (mconnection.State.ToString() != "Open")
                    mconnection.Open();
                using (mconnection)
                {
                    using (cmd = new MySqlCommand(query, mconnection))
                    {
                        rdr = cmd.ExecuteReader();
                        if (rdr.HasRows)
                            while (rdr.Read())
                            {
                                for (int i = 0; i < rdr.FieldCount; i++)
                                {
                                    retList.Add(rdr[i].ToString());
                                }
                            }
                        rdr.Dispose();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            return retList;
        }


        public int getUsersCount() {
            int count = 0;
            string query = string.Empty;
            try {
                query = "SELECT COUNT(user_id) FROM user_info";
                if (mconnection.State.ToString() != "Open")
                    mconnection.Open();                
                using (mconnection) {                    
                    using (cmd = new MySqlCommand(query, mconnection)) {
                        rdr = cmd.ExecuteReader();
                        if (rdr.HasRows)
                            while (rdr.Read()) {
                                //MessageBox.Show("Count = " + count);
                                count = int.Parse(rdr[0].ToString());
                            }
                        rdr.Dispose();
                    }
                }
            }
            catch (Exception ex) {
                MessageBox.Show(ex.Message);
            }
            return count;
        }

        public bool isExistsUser(int uId) {
            bool exists = false;
            try {
                string query = "SELECT * FROM user_info WHERE user_id = " + uId;
                if (mconnection.State.ToString() != "Open")
                    mconnection.Open();
                using (mconnection) {
                    using (cmd = new MySqlCommand(query, mconnection)) {
                        rdr = cmd.ExecuteReader();
                        if (rdr.HasRows)
                            exists = true;
                        rdr.Dispose();
                    }
                }

            }
            catch (Exception ex) {
                MessageBox.Show(ex.Message);
            }
            return exists;
        }

        public List<string> getAllTherapists() {
            var retList = new List<string>();
            string query = string.Empty;
            try {
                query = "SELECT user_id, user_name,user_lastName FROM users";
                if (mconnection.State.ToString() != "Open")
                    mconnection.Open();
                using (mconnection) {
                    using (cmd = new MySqlCommand(query, mconnection)) {
                        rdr = cmd.ExecuteReader();
                        if (rdr.HasRows)
                            while (rdr.Read()) {
                                retList.Add(rdr[0].ToString() + " \t" + rdr[1].ToString() + " " + rdr[2].ToString());
                            }
                        rdr.Dispose();
                    }
                }
            }
            catch (Exception ex) {
                MessageBox.Show(ex.Message);
            }
            return retList;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public DataSet getImageNames() {
            DataSet imgData = new DataSet();
            string query = string.Empty;
            try 
            {
                query = "SELECT practice_id, image_id FROM practice_data WHERE is_active = 1";
                if (mconnection.State.ToString() != "Open")
                    mconnection.Open();
                using (mconnection) {
                    using (adapter = new MySqlDataAdapter(query, mconnection)) {
                        adapter.Fill(imgData, "image_info");
                    }
                }

            }
            catch(Exception ex) {
                MessageBox.Show(ex.Message);
            }

            return imgData;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="uId"></param>
        /// <returns></returns>
        public DataSet getUserPatiantes(int uId)
        {            
            string query = string.Empty;
            DataSet retList = new DataSet();
            try
            {
                query = "SELECT * FROM user_info WHERE user_therapistId = " + uId;
                if (mconnection.State.ToString() != "Open")
                    mconnection.Open();
                using (mconnection)
                {
                    using (adapter = new MySqlDataAdapter(query, mconnection))
                    {                       
                        adapter.Fill(retList, "user_info");
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            return retList;
        }

        public DataSet getGeneralReportData() {
            DataSet repData = new DataSet();
            string query = string.Empty;
            try {
                query = "SELECT * FROM session_report;";
                if (mconnection.State.ToString() != "Open")
                    mconnection.Open();
                using (mconnection) {
                    using (adapter = new MySqlDataAdapter(query, mconnection)) {
                        adapter.Fill(repData, "GeneralReportData");
                    }
                }
            }
            catch (Exception ex) {
                MessageBox.Show(ex.Message);
            }

            return repData;
        }

        public DataSet getGeneralReportDataFiltered(string from, string to, string pID) {
            DataSet repData = new DataSet();
            string query = string.Empty;
            try {
                query = "SELECT * FROM session_report " +
                        "WHERE Date between '"+ from +"' " +
                        "AND '"+ to +"' " +
                        "AND user_id like '" + pID + "'";
                if (mconnection.State.ToString() != "Open")
                    mconnection.Open();
                using (mconnection) {
                    using (adapter = new MySqlDataAdapter(query, mconnection)) {
                        adapter.Fill(repData, "GeneralReportData");
                    }
                }
            }
            catch (Exception ex) {
                MessageBox.Show(ex.Message);
            }

            return repData;
        }

        public DataSet getSummaryReportDataFiltered(string from, string to, string pID) {
            // SELECT COUNT(session_id) AS SessionCount, COUNT(distinct user_id) as Patients, SUM(Exercises) AS TotalExercises, SUM(Duration) AS TotalSeconds, MIN(Date) AS FromDate, MAX(Date) AS ToDate, TIMESTAMPDIFF(MONTH, MIN(Date),MAX(Date))  AS TotalMonth
            //FROM `unity - db`.session_report
            //  WHERE Date between '2019-10-24' and '2019-11-26';
            DataSet repData = new DataSet();
            string query = string.Empty;
            try {
                query = "SELECT COUNT(session_id) AS SessionCount, " +
                        "COUNT(distinct user_id) as Patients, " +
                        "SUM(Exercises) AS TotalExercises, " +
                        "SUM(Duration) AS TotalSeconds, " +
                        "MIN(Date) AS FromDate, " +
                        "MAX(Date) AS ToDate, " +
                        "TIMESTAMPDIFF(MONTH, MIN(Date),MAX(Date))  AS TotalMonth " +
                        "FROM session_report " +
                        "WHERE Date between '" + from + "' " +
                        "AND '" + to + "' " +
                        "AND user_id like '" + pID + "'";
                if (mconnection.State.ToString() != "Open")
                    mconnection.Open();
                using (mconnection) {
                    using (adapter = new MySqlDataAdapter(query, mconnection)) {
                        adapter.Fill(repData, "GeneralReportSummary");
                    }
                }
            }
            catch (Exception ex) {
                MessageBox.Show(ex.Message);
            }

            return repData;
        }

        public DataSet getPatientSummaryReportData(long pID) {
            // SELECT COUNT(session_id) AS SessionCount, COUNT(distinct user_id) as Patients, SUM(Exercises) AS TotalExercises, SUM(Duration) AS TotalSeconds, MIN(Date) AS FromDate, MAX(Date) AS ToDate, TIMESTAMPDIFF(MONTH, MIN(Date),MAX(Date))  AS TotalMonth
            //FROM `unity - db`.session_report
            //  WHERE Date between '2019-10-24' and '2019-11-26';
            DataSet repData = new DataSet();
            string query = string.Empty;
            try {
                query = "SELECT COUNT(session_id) AS SessionCount, " +
                        "COUNT(distinct user_id) as Patients, " +
                        "SUM(Exercises) AS TotalExercises, " +
                        "SUM(Duration) AS TotalSeconds, " +
                        "MIN(Date) AS FromDate, " +
                        "MAX(Date) AS ToDate, " +
                        "TIMESTAMPDIFF(MONTH, MIN(Date),MAX(Date))  AS TotalMonth " +
                        "FROM session_report " +
                        "WHERE user_id = " + pID;
                if (mconnection.State.ToString() != "Open")
                    mconnection.Open();
                using (mconnection) {
                    using (adapter = new MySqlDataAdapter(query, mconnection)) {
                        adapter.Fill(repData, "GeneralReportSummary");
                    }
                }
            }
            catch (Exception ex) {
                MessageBox.Show(ex.Message);
            }

            return repData;
        }

        public DataSet getPatientPlanSummaryData(long pID) {
            DataSet plansData = new DataSet();
            string query = string.Empty;
            try {
                query = "SELECT count(distinct plan_number), sum(reps)  FROM plans WHERE plan_user = " + pID;
                if (mconnection.State.ToString() != "Open")
                    mconnection.Open();
                using (mconnection) {
                    using (adapter = new MySqlDataAdapter(query, mconnection)) {
                        adapter.Fill(plansData, "PlansReportSummary");
                    }
                }
            }
            catch (Exception ex) {
                MessageBox.Show(ex.Message);
            }
            return plansData;
        }


        public DataSet getUserSessionInfo(long uId) 
        {
            string query = string.Empty;
            DataSet retList = new DataSet();
            try {
                query = "SELECT sessions.session_id, plans_info.plan_name, CAST(sessions.start_time AS DATE) AS Date, COUNT(sessions.session_id) as Practices, sessions.plan_id" +
                        " FROM sessions, practice_log, plans_info" +
                        " WHERE sessions.user_id = " + uId +
                        " AND sessions.session_id = practice_log.session_id" +
                        " AND sessions.plan_id = plans_info.plan_number" +
                        " GROUP BY sessions.session_id ORDER BY sessions.start_time DESC";
                if (mconnection.State.ToString() != "Open")
                    mconnection.Open();
                using (mconnection) {
                    using (adapter = new MySqlDataAdapter(query, mconnection)) {
                        adapter.Fill(retList, "SessionInfo");
                    }
                }
            }
            catch (Exception ex) {
                MessageBox.Show(ex.Message);
            }
            return retList;
        }

        public DataSet getUserSessionInfo(long uId, DateTime date) {
            string query = string.Empty;            
            DataSet retList = new DataSet();
            try {
                string sDate = date.Year + "-" + date.Month + "-01" + " 00:00:00";
                string eDate = date.Year + "-" + date.Month + "-" + DateTime.DaysInMonth(date.Year, date.Month) + " 23:00:00";

                //MessageBox.Show("Date between: " + sDate + " & " + eDate);
                query = "SELECT sessions.session_id, plans_info.plan_name, CAST(sessions.start_time AS DATE) AS Date, COUNT(sessions.session_id) as Practices, sessions.plan_id" +
                        " FROM sessions, practice_log, plans_info" +
                        " WHERE sessions.user_id = " + uId +                        
                        " AND sessions.session_id = practice_log.session_id" +
                        " AND sessions.plan_id = plans_info.plan_number" +
                        " AND sessions.start_time > '" + sDate + "'" +
                        " AND sessions.start_time < '" + eDate + "'" +
                        " GROUP BY sessions.session_id ORDER BY sessions.start_time DESC";
                if (mconnection.State.ToString() != "Open")
                    mconnection.Open();
                using (mconnection) {
                    using (adapter = new MySqlDataAdapter(query, mconnection)) {
                        adapter.Fill(retList, "SessionInfo");
                    }
                }
            }
            catch (Exception ex) {
                MessageBox.Show(ex.Message);
            }
            return retList;
        }


        public DataSet getUsersPlansInfo(long uId) {
            // SELECT plans_info.plan_number, plans_info.plan_name, COUNT(plans.plan_id) AS count 
            // FROM plans_info JOIN plans WHERE plans_info.plan_number = plans.plan_number 
            // AND plans_info.plan_user_id = uId GROUP BY plans.plan_number

            string query = string.Empty;
            DataSet pList = new DataSet();
            try {
                query = "SELECT plans_info.plan_number, plans_info.plan_name, COUNT(plans.plan_id) AS count " +
                    "FROM plans_info JOIN plans " +
                    "WHERE plans_info.plan_number = plans.plan_number " +
                    "AND plans_info.plan_user_id = " + uId +
                    " GROUP BY plans.plan_number";
                if (mconnection.State.ToString() != "Open")
                    mconnection.Open();
                using (mconnection) {
                    using (adapter = new MySqlDataAdapter(query, mconnection)) {
                        adapter.Fill(pList, "plans_info");
                    }
                }
            }
            catch (Exception ex) {
                MessageBox.Show(ex.Message);
            }
            return pList;
        }

        public DataSet getUserPlanData(int pId) {
            string query = string.Empty;
            DataSet pList = new DataSet();
            try {
                query = "SELECT practice_data.practice_id, practice_data.practice_name, plans.practice_id, plans.reps FROM plans, practice_data where plan_number = " + pId +
                        " AND plans.practice_id = practice_data.unity_id" +
                        " AND practice_data.is_active = 1" +
                        " ORDER BY order_num";
                if (mconnection.State.ToString() != "Open")
                    mconnection.Open();
                using (mconnection) {
                    using (adapter = new MySqlDataAdapter(query, mconnection)) {
                        adapter.Fill(pList, "plan_data");
                    }
                }
            }
            catch (Exception ex) {
                MessageBox.Show(ex.Message);
            }
            return pList;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="uId"></param>
        /// <param name="pName"></param>
        /// <param name="practice_Data"></param>
        /// <returns></returns>
        public int storeNewUserPlan(long uId, string pName, List<LogInForm.practice_data> practice_Data) {
            int result = 0, planNum = -1;
            // 1. Get max plan number+1 and set as plan number
            // 2. Store the plan for each entry
            // 3. Store the plan info
            try {
                string query = "SELECT MAX(plan_number) FROM plans";
                if (mconnection.State.ToString() != "Open")
                    mconnection.Open();
                using (mconnection) {
                    using (cmd = new MySqlCommand(query, mconnection)) {
                        rdr = cmd.ExecuteReader();
                        if (rdr.HasRows)
                            while (rdr.Read()) {
                                planNum = 1 + int.Parse(rdr[0].ToString());
                            }
                        rdr.Dispose();
                    }
                    //MessageBox.Show("plan num:" + planNum);
                    if (planNum > -1) {
                        int order = 0;
                        foreach (LogInForm.practice_data pd in practice_Data) {
                            if (insertPlanEntry(planNum, pd.practice_unityId, order, uId, pd.practice_reps) > 0)
                                    result++;
                            order++;
                        }
                        
                    }
                }
                if (result > 0) {
                    // Store the plan info
                    addPlanInfo(planNum, pName, uId);
                }
            }
            catch (Exception ex) {
                MessageBox.Show(ex.Message);
            }
            return planNum;
        }

        public bool isPlanNameExists(string pName) {
            bool ret = false;
            try {
                string query = "SELECT * FROM plans_info WHERE plan_name = '" + pName +"'";
                if (mconnection.State.ToString() != "Open")
                    mconnection.Open();
                using (mconnection) {
                    using (cmd = new MySqlCommand(query, mconnection)) {
                        rdr = cmd.ExecuteReader();
                        if (rdr.HasRows)
                            ret = true;
                        rdr.Dispose();
                    }
                }

            }
            catch (Exception ex) {
                MessageBox.Show(ex.Message);
            }
            return ret;
        }

        public void addPlanInfo(int pNum, string planName, long uId) {
            try {
                // First check if name exists in the DB, if yes, add #1 and try again with #2...x until not exists
                string tmpName = planName;
                bool exists;
                int idx = 0; 
                while ((exists = isPlanNameExists(tmpName)) == true) {
                    tmpName = planName + "#" + idx;
                    idx++;
                }


                // Store the plan name
                string query = "INSERT INTO plans_info(plan_number, plan_name, plan_user_id) " +
                                "VALUES (" + pNum + ", '" + tmpName + "', " + uId + ")";
                if (mconnection.State.ToString() != "Open")
                    mconnection.Open();
                using (mconnection) {
                    using (cmd = new MySqlCommand(query, mconnection)) {
                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex) {
                MessageBox.Show(ex.Message);
            }
        }

        public int insertPlanEntry(int pNum, int practiceId, int order, long uId, int reps) {
            int res = -1;
            try {
                string query = "INSERT INTO plans(plan_number, practice_id, order_num, plan_user, reps) " +
                                "VALUES (" + pNum + ", " + practiceId + ", " + order + ", " + uId + ", " + reps + ")";
                if (mconnection.State.ToString() != "Open")
                    mconnection.Open();
                using (mconnection) {
                    using (cmd = new MySqlCommand(query, mconnection)) {
                        res = cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex) {
                MessageBox.Show(ex.Message);
            }
            return res;
        }

        /// <summary>
        /// getUserpracticeList
        /// </summary>
        /// <param name="uId"></param>
        /// <returns></returns>
        public DataSet getUserpracticeList(int uId)
        {
            string query = string.Empty;
            DataSet retList = new DataSet();
            try
            {
                query = "SELECT practice_log.practice_userId, practice_log.practice_id, practice_data.practice_name " +
                        "FROM practice_log, practice_data " +
                        "WHERE practice_log.practice_id = practice_data.practice_id " +
                        "AND practice_userId = " + uId +
                        " GROUP BY practice_log.practice_id ORDER BY practice_log.practice_id";
                if (mconnection.State.ToString() != "Open")
                    mconnection.Open();
                using (mconnection)
                {
                    using (adapter = new MySqlDataAdapter(query, mconnection))
                    {
                        adapter.Fill(retList, "user_practices");
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            return retList;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="uId"></param>
        /// <returns></returns>
        public DataSet getuserPracticeDates(int uId)
        {
            string query = string.Empty;
            DataSet retList = new DataSet();
            try
            {
                query = "SELECT Date FROM sessionlog where user_id = " + uId +
                        " group by Date";
                if (mconnection.State.ToString() != "Open")
                    mconnection.Open();
                using (mconnection)
                {
                    using (adapter = new MySqlDataAdapter(query, mconnection))
                    {
                        adapter.Fill(retList, "UserPractice_Dates");
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            return retList;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="uId"></param>
        /// <param name="pDate"></param>
        /// <returns></returns>
        public DataSet getUsersDaysPractices(int uId, string pDate)
        {
            // SELECT * FROM `unity-db`.sessionlog where user_id = 37 and Date = '2018-07-22 00:00:00'
            string query = string.Empty;
            DataSet retList = new DataSet();
            try
            {
                query = "SELECT * FROM sessionlog where user_id = " + uId +
                        " AND Date = '" + pDate + "'";
                //MessageBox.Show(query);
                if (mconnection.State.ToString() != "Open")
                    mconnection.Open();
                using (mconnection)
                {
                    using (adapter = new MySqlDataAdapter(query, mconnection))
                    {
                        adapter.Fill(retList, "UserDays_Practices");
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            return retList;
        }

        public DataSet getSessionPractices(string sId)
        {
            string query = string.Empty;
            DataSet retList = new DataSet();
            try
            {
                query = "SELECT * FROM practice_log_view where session_id = " + sId;
                //MessageBox.Show(query);
                if (mconnection.State.ToString() != "Open")
                    mconnection.Open();
                using (mconnection)
                {
                    using (adapter = new MySqlDataAdapter(query, mconnection))
                    {
                        adapter.Fill(retList, "Sessions_Practices");
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            return retList;
        }


        public DataSet getUserPracticeLog(int uId, string pName)
        {
            string query = string.Empty;
            DataSet retList = new DataSet();
            try
            {
                query = "SELECT practice_log.practice_ts, TIMEDIFF(practice_log.practice_EndTime , practice_log.practice_ts) as Duration, practice_log.practice_level, " +
                        "practice_measure1, practice_measure2, practice_measure3, practice_measure4, practice_measure5, practice_measure6, practice_measure7, practice_measure8 " +
                        "FROM practice_log, practice_data " +
                        "WHERE practice_log.practice_id = practice_data.practice_id " +
                        "AND practice_userId = " + uId +
                        " AND practice_data.practice_name = '" + pName + "' " +
                        "ORDER BY practice_log.practice_ts";
                if (mconnection.State.ToString() != "Open")
                    mconnection.Open();
                using (mconnection)
                {
                    using (adapter = new MySqlDataAdapter(query, mconnection))
                    {
                        adapter.Fill(retList, "UserPractice_log");
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            return retList;
        }

        public DataSet getPracticeHeaders(string pName)
        {
            string query = string.Empty;
            DataSet retList = new DataSet();
            try
            {
                query = "SELECT " +
                    "(select measure_name FROM practice_measures where practice_measures1 = measure_id), " +
                    "(select measure_name FROM practice_measures where practice_measures2 = measure_id), " +
                    "(select measure_name FROM practice_measures where practice_measures3 = measure_id), " +
                    "(select measure_name FROM practice_measures where practice_measures4 = measure_id), " +
                    "(select measure_name FROM practice_measures where practice_measures5 = measure_id), " +
                    "(select measure_name FROM practice_measures where practice_measures6 = measure_id), " +
                    "(select measure_name FROM practice_measures where practice_measures7 = measure_id), " +
                    "(select measure_name FROM practice_measures where practice_measures8 = measure_id) " +
                    "FROM practice_data " +
                    "WHERE practice_name = '" + pName + "' ";
                    
                if (mconnection.State.ToString() != "Open")
                    mconnection.Open();
                using (mconnection)
                {
                    using (adapter = new MySqlDataAdapter(query, mconnection))
                    {
                        adapter.Fill(retList, "Practice_headers");
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            return retList;
        }    


        public DataSet getPatientSessionLog(int uId)
        {
            string query = string.Empty;
            DataSet retList = new DataSet();
            try
            {
                query = "SELECT session_id, start_time, TIME(start_time) as StartTime, TIME(end_time) as EndTime, " +
                        "TIMEDIFF(end_time, start_time) as Total FROM sessions " + 
                        "WHERE user_id = " + uId;
                if (mconnection.State.ToString() != "Open")
                    mconnection.Open();
                using (mconnection)
                {
                    using (adapter = new MySqlDataAdapter(query, mconnection))
                    {
                        adapter.Fill(retList, "session_info");
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            return retList;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sId"></param>
        /// <returns></returns>
        public DataSet getSessionPractices(int sId) {
            string query = string.Empty;
            DataSet retList = new DataSet();
            try {
                query = "SELECT * FROM session_practices_view where session_id = " + sId;
                if (mconnection.State.ToString() != "Open")
                    mconnection.Open();
                using (mconnection) {
                    using (adapter = new MySqlDataAdapter(query, mconnection)) {
                        adapter.Fill(retList, "session_practices");
                    }
                }
            }
            catch (Exception ex) {
                MessageBox.Show(ex.Message);
            }
            return retList;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sId"></param>
        /// <returns></returns>
        public DataSet getPracticeInfo(int sId) {
            string query = string.Empty;
            DataSet retList = new DataSet();
            try {
                query = "SELECT practice_id, practice_name FROM practice_data WHERE practice_id =" + sId;
                if (mconnection.State.ToString() != "Open")
                    mconnection.Open();
                using (mconnection) {
                    using (adapter = new MySqlDataAdapter(query, mconnection)) {
                        adapter.Fill(retList, "practice_info");
                    }
                }
            }
            catch (Exception ex) {
                MessageBox.Show(ex.Message);
            }
            return retList;
        }

        public DataSet getPracticeViewInfo(int sId) {
            string query = string.Empty;
            DataSet retList = new DataSet();
            try {
                query = "SELECT practice_data.practice_name, practice_info.* FROM practice_info, practice_data " +
                        "WHERE practice_data.practice_id = practice_info.practice_id AND practice_data.practice_id =" + sId;
                if (mconnection.State.ToString() != "Open")
                    mconnection.Open();
                using (mconnection) {
                    using (adapter = new MySqlDataAdapter(query, mconnection)) {
                        adapter.Fill(retList, "practice_view_info");
                    }
                }
            }
            catch (Exception ex) {
                MessageBox.Show(ex.Message);
            }
            return retList;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sId"></param>
        /// <returns></returns>
        public DataSet getPatientPracticeLog(int sId)
        {
            string query = string.Empty;
            DataSet retList = new DataSet();
            try
            {
                query = "SELECT practice_data.practice_name, TIME(practice_log.practice_ts), TIME(practice_log.practice_EndTime), " +
                        "TIMEDIFF(practice_log.practice_EndTime, practice_log.practice_ts) as Total, practice_log.practice_level, practice_log.practice_measure1 " +
                        "FROM practice_log, practice_data " + 
                        "WHERE practice_log.practice_id = practice_data.practice_id " +
                        "AND session_id = " + sId;
                if (mconnection.State.ToString() != "Open")
                    mconnection.Open();
                using (mconnection)
                {
                    using (adapter = new MySqlDataAdapter(query, mconnection))
                    {
                        adapter.Fill(retList, "practice_info");
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            return retList;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public DataSet getAllActivePractices() {
            string query = string.Empty;
            DataSet retList = new DataSet();
            try {
                query = "SELECT practice_id, practice_name, unity_id FROM practice_data WHERE is_active = 1 AND unity_id > -1";
                if (mconnection.State.ToString() != "Open")
                    mconnection.Open();
                using (mconnection) {
                    using (adapter = new MySqlDataAdapter(query, mconnection)) {
                        adapter.Fill(retList, "practice_data");
                    }
                }
            }
            catch (Exception ex) {
                MessageBox.Show(ex.Message);
            }
            return retList;
        }

        public DataSet getFilterActivePractices(List<string> filters, int p_pos) {
            string query = string.Empty;
            DataSet retList = new DataSet();
            try {
                query = "SELECT * FROM practice_filters_view WHERE practice_position = " + p_pos;
                if (filters.Count > 0) {
                    int index = 1;
                    foreach (string fString in filters) {
                        // add the nested query on top of the existing
                        string nqName = "q" + index;
                        query = "SELECT " + nqName + ".* FROM (" + query + ") " + nqName + " WHERE " + nqName + "." + fString + " > 1 order by " + nqName + "." + fString + " desc"; 
                        index++;
                    }
                }
                //MessageBox.Show(query);
                if (mconnection.State.ToString() != "Open")
                    mconnection.Open();
                using (mconnection) {
                    using (adapter = new MySqlDataAdapter(query, mconnection)) {
                        adapter.Fill(retList, "practice_filtered_data");
                    }
                }
            }
            catch (Exception ex) {
                MessageBox.Show(ex.Message);
            }
            return retList;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sID"></param>
        /// <returns></returns>
        public List<string> getUserSessionData(long uID, int sID) {
            var retList = new List<string>();
            string query = string.Empty;
            try {
                query = "SELECT * FROM session_data WHERE user_id = " + uID + " AND session_id = " + sID;
                if (mconnection.State.ToString() != "Open")
                    mconnection.Open();
                using (mconnection) {
                    using (cmd = new MySqlCommand(query, mconnection)) {
                        rdr = cmd.ExecuteReader();
                        if (rdr.HasRows)
                            while (rdr.Read()) {
                                for (int i = 0; i < rdr.FieldCount; i++) {
                                    retList.Add(rdr[i].ToString());
                                }
                            }
                        rdr.Dispose();
                    }
                }
            }
            catch (Exception ex) {
                MessageBox.Show(ex.Message);
            }
            return retList;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="uID"></param>
        /// <param name="limit"></param>
        /// <returns></returns>
        public DataSet getUserSessionHistory(long uID, int limit, int sessionID) {
            DataSet resData = new DataSet();
            string query = string.Empty;
            try {
                query = "SELECT * FROM (SELECT * FROM session_data WHERE user_id = " + 
                    uID + " AND session_id <= " + sessionID + " order by session_id DESC LIMIT " + limit + ") as tbl ORDER BY tbl.session_id";
                if (mconnection.State.ToString() != "Open")
                    mconnection.Open();
                using (mconnection) {
                    using (adapter = new MySqlDataAdapter(query, mconnection)) {
                        adapter.Fill(resData, "history_data");
                    }
                }
            }
            catch (Exception ex) {
                MessageBox.Show(ex.Message);
            }
            return resData;
        }

        public DataSet getUserTestsHistory(long uId) {
            DataSet resData = new DataSet();
            string query = string.Empty;
            try {
                query = "SELECT user_tests.time, test_types.test_name, test_score, test_time, test_steps " +
                        "FROM user_tests, test_types " +
                        "WHERE test_types.test_id = user_tests.test_type AND user_id = " + uId;
                if (mconnection.State.ToString() != "Open")
                    mconnection.Open();
                using (mconnection) {
                    using (adapter = new MySqlDataAdapter(query, mconnection)) {
                        adapter.Fill(resData, "test_history");
                    }
                }
            }
            catch (Exception ex) {
                MessageBox.Show(ex.Message);
            }

            return resData;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="qr">'user_therapistId', 'user_fName', 'user_lName', 'user_gender', 'user_byear', 'user_hight', 'user_whight', 'user_phone1', 'user_email'</param>
        public int add_user(string qr)
        {
            int result = -1;
            try
            {
                int userId = 1 + getMaxuserId();                
                string query = "INSERT INTO user_info(user_id, user_therapistId, user_fName, user_lName, user_gender, user_byear, user_hight, user_whight, user_phone1, user_email, user_comments)"
                    + " VALUES ('" + userId.ToString() + "'," + qr + ")";
                if (mconnection.State.ToString() != "Open")
                    mconnection.Open();
                using (mconnection)
                {
                    using (cmd = new MySqlCommand(query, mconnection))
                    {
                        result = cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fName"></param>
        /// <param name="lName"></param>
        /// <param name="eMail"></param>
        /// <param name="pHone"></param>
        /// <param name="bDate"></param>
        /// <returns>1 on success</returns>
        public int addTherapist(string fName, string lName, string eMail, string pHone, string bDate) {
            //INSERT INTO `unity-db`.users(user_name, user_lastName,user_type,user_email,user_phone, user_bdate) 
            //VALUES("Vicktor", "Hason", 2, "hason@gashsash.com", "001-555-3449", '1968-07-20')
            int result = -1;
            try {
                string query = "INSERT INTO users(user_name, user_lastName,user_type,user_email,user_phone, user_bdate)" +
                    "VALUES('" + fName + "', '" + lName + "', 2, '" + eMail + "', '" + pHone + "', '" + bDate + "')";
                if (mconnection.State.ToString() != "Open")
                    mconnection.Open();
                using (mconnection) {
                    using (cmd = new MySqlCommand(query, mconnection)) {
                        result = cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex) {
                MessageBox.Show(ex.Message);
            }
            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="notes"></param>
        /// <param name="bYear"></param>
        /// <param name="gender"></param>
        /// <param name="height"></param>
        /// <param name="weight"></param>
        /// <param name="shoeS"></param>
        /// <param name="level"></param>
        /// <returns></returns>
        public int update_patient(long id, string notes, string bYear, int gender, int height, int weight, int shoeS, int level, int lang) {
            int result = -1;
            try {                
                //userId.ToString() + ", '" + firstNameEdit.Text + "','" + lastNameEdit.Text + "'," + gender.ToString() + ",'" + bYearEdit.Value + "'," + hieghtEdit.Value.ToString()
                //+"," + weightEdit.Value.ToString() + ",'" + phoneEdit.Text + "','" + emailEdit.Text + "','" + NotesEditBox.Text + "'";
                string query = 
                "UPDATE user_info SET" +
" user_gender = " + gender +
", user_hight = " + height +
", user_whight = " + weight +
", user_comments = '"+ notes +
"', user_level_profile = " + level +
", user_byear = '" + bYear +
"' , lang_id = " + lang +
", user_shoe_size = " + shoeS +
" WHERE user_id = " + id;
                //string message = query;
                //string caption = "query";
                //MessageBoxButtons buttons = MessageBoxButtons.OK;
                //MessageBox.Show(message, caption, buttons);
                if (mconnection.State.ToString() != "Open")
                    mconnection.Open();
                using (mconnection) {
                    using (cmd = new MySqlCommand(query, mconnection)) {
                        result = cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex) {
                MessageBox.Show(ex.Message);
            }
            return result;
        }

        public int add_user_testResults(long uId, int testType, int tScore, int tTime, int tSteps) {
            int result = -1;
            try {
                string query = "INSERT INTO user_tests(user_id, time, test_type, test_score, test_time, test_steps) " +
                    "VALUES ("+ uId +", NOW(), "+ testType + ", " + tScore +", " +tTime+ ", " +tSteps+ ")";
                if (mconnection.State.ToString() != "Open")
                    mconnection.Open();
                using (mconnection) {
                    using (cmd = new MySqlCommand(query, mconnection)) {
                        result = cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex) {
                MessageBox.Show(ex.Message);
            }
            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="lang"></param>
        /// <param name="gender"></param>
        /// <param name="bYear"></param>
        /// <param name="height"></param>
        /// <param name="weight"></param>
        /// <param name="notes"></param>
        /// <param name="shoeS"></param>
        /// <param name="level"></param>
        /// <returns></returns>
        public long add_patient(long userId, int lang, int gender, string bYear, int height, int weight, string notes, int shoeS,  int level) {
            long result = -1;
            try {
                //int userId = 1 + getMaxuserId();
                //userId.ToString() + ", '" + firstNameEdit.Text + "','" + lastNameEdit.Text + "'," + gender.ToString() + ",'" + bYearEdit.Value + "'," + hieghtEdit.Value.ToString()
                //+"," + weightEdit.Value.ToString() + ",'" + phoneEdit.Text + "','" + emailEdit.Text + "','" + NotesEditBox.Text + "'";
                string query = "INSERT INTO user_info(user_id, lang_id, user_gender, user_byear, user_hight, user_whight, user_comments, user_level_profile, user_shoe_size)"
                    + " VALUES (" + userId + " ," + lang + ", "+ gender + ",'"+ bYear +"'," + height+","+ weight +",'"+notes+"', " + level + ", " + shoeS + ")";
                string message = query;
                //string caption = "query";
                //MessageBoxButtons buttons = MessageBoxButtons.OK;
                //MessageBox.Show(message, caption, buttons);
                if (mconnection.State.ToString() != "Open")
                    mconnection.Open();
                using (mconnection) {
                    using (cmd = new MySqlCommand(query, mconnection)) {
                        result = cmd.LastInsertedId;
                    }
                }
            }
            catch (Exception ex) {
                MessageBox.Show(ex.Message);
            }
            return result;
        }
        

        public int getMaxuserId() {
            int uNum = -1;
            try {
                string query = "SELECT MAX(user_id) FROM user_info";
                if (mconnection.State.ToString() != "Open")
                    mconnection.Open();
                using (mconnection) {
                    using (cmd = new MySqlCommand(query, mconnection)) {
                        rdr = cmd.ExecuteReader();
                        if (rdr.HasRows)
                            while (rdr.Read()) {
                                uNum = (int)rdr[0];
                            }
                        rdr.Dispose();
                    }
                }
            }
            catch (Exception ex) {
                MessageBox.Show(ex.Message);
            }
            return uNum;
            //
        }
    }
}

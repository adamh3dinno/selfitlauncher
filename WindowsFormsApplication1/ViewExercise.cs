﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SelfitMain {
    public partial class ViewExercise : Form {
        private dbHandler dbh;
        private int practiceId;
        private int panelId;
        private LogInForm loginform;

        public ViewExercise() {
            InitializeComponent();
            dbh = new dbHandler();
        }

        public ViewExercise(int pId, LogInForm mainForm, int panel) {
            InitializeComponent();
            dbh = new dbHandler();
            practiceId = pId;
            panelId = panel;
            loginform = mainForm;
        }

        private void ViewExercise_Load(object sender, EventArgs e) {
            // Get the practice details
            //MessageBox.Show("Practice ID: " + practiceId);
            try {
                DataSet pInfo = dbh.getPracticeViewInfo(practiceId);
                DataTable pdt = pInfo.Tables["practice_view_info"];
                if (pdt.Rows.Count > 0) {
                    DataRow row = pdt.Rows[0];
                    practiceNameLbl.Text = row[0].ToString();
                    descriptionTxtBox.Text = row[2].ToString();
                    focusTxt.Text = row[5].ToString();
                }
                string iconPath = Application.StartupPath + "\\practice_images\\" + loginform.getImageName(practiceId) + "big.jpg";
                if (File.Exists(iconPath))
                    exerciseImagePbx.Image = Image.FromFile(iconPath);
                else
                    exerciseImagePbx.Image = Image.FromFile(Application.StartupPath + "\\practice_images\\0big.jpg"); // default image
            }
            catch(Exception ex) {
                MessageBox.Show(ex.Message);
            }
        }

        private void button1_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void addToPlanBtn_Click(object sender, EventArgs e) {
            loginform.addPracticeToPlan(panelId);
            this.Close();
        }
    }
}

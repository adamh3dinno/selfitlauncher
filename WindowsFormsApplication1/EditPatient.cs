﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SelfitMain {
    public partial class EditPatient : Form {
        private static string INVALID_ID = "Please type ID";
        private static string INVALID_GENDER = "Please select a gender";
        private static string INVALID_NAME = "Please type first name";
        private IdentityClient Identity;
        private DataHandler datahandler;
        //private dbHandler dbh;
        // private msDbHandler dbh;
        private bool isNewPatient;
        long patientId = -1;
        private string userNotes = string.Empty;
        string bYear = "1900";
        int lang = 1;
        int gender;
        int heightUnits = 0; // 0 = CM, 1 = Inch
        int weightUnits = 0; // 0 = KG, 1 = Pound
        int shoesizeUnits = 0; // 0 = EU, 1 = US
        int height = 0;
        int weight = 0;
        int shoeSize = 0;
        int uLevel = 2;
        bool is_data_changed = false;
        int SITEID = -1;
        private Process keyBoardProcess;
        private bool keyBoardOn = false;

        public Patient patientData { get; set; }

        public EditPatient() {
            InitializeComponent();
            fNameTxt.Text = "";
            lNameTxt.Text = "";
            pIdTxt.Text = "";
            eMailTxt.Text = "";
            notesTxt.Text = "";
            SelectGenderTxt.Text = "";
            isNewPatient = true;
            editPatientCaption.Text = "Add a Patient";
            gender = 0;
            
            datahandler = new DataHandler();
            Identity = new IdentityClient(datahandler.getSYS_Server());
            SITEID = datahandler.getSiteID();            
            //dbh = new dbHandler();
            //dbh = new msDbHandler();
            // Need to get new ID from DB, since we're on single machine, we don't need provision

            pIdTxt.Enabled = true;
            tenMAddBtn.Visible = false;
            tugAddBtn.Visible = false;
            dgiAddBtn.Visible = false;
            mocaAddBtn.Visible = false;
            TestsDataGridView.Visible = false;
            fill_ComboItems();
            patientData = new Patient();
            autoGenIDBtn.Visible = false;

            if (Properties.Settings.Default.autoGenID == true) {
                int userCount = datahandler.getTotalPatientsCount();
                pIdTxt.Text = (1 + userCount).ToString();
                pIdTxt.Enabled = false;
                autoGenIDBtn.Visible = true;
                patientIDlbl.Text = "Patient Number";
            }
            lang = datahandler.getDefultLang();
            setLangView();
        }

        public EditPatient(long pId, Patient p) { // Handle edit existing patient
            //autoGenIDBtn.Visible = false;
            InitializeComponent();
            SelectGenderTxt.Text = "";
            patientId = pId;
            
            datahandler = new DataHandler();
            Identity = new IdentityClient(datahandler.getSYS_Server());
            //dbh = new dbHandler();
            //dbh = new msDbHandler();
            SITEID = datahandler.getSiteID();            
            patientData = new Patient(p);
            if (Properties.Settings.Default.autoGenID == true) {
                patientIDlbl.Text = "Patient Number";
            }
            isNewPatient = false;
            List<string> userInfo = new List<string>();
            userInfo = datahandler.getPatientInfo(pId);
            fill_ComboItems();

            pIdTxt.Text = patientId.ToString();
            if (userInfo.Count > 0) {
                fNameTxt.Text = patientData.FirstName;
                lNameTxt.Text = patientData.LastName;
                string siteTxt = patientData.SiteID.ToString();
                string idText = LogInForm.Decrypt(patientData.HashedID);
                pIdTxt.Text = idText.Substring(0, idText.Length - siteTxt.Length);
                pIdTxt.Enabled = false;
                eMailTxt.Text = patientData.email;
                phoneTxt.Text = patientData.phoneNumber;
                gender = patientData.gender;
                setGenderButtons();
                birthYearCombo.Text = bYear = patientData.birthYear.ToString();                
                shoeCombo.Text = patientData.shueSize.ToString();
                shoeSize = patientData.shueSize;
                heightCombo.Text = patientData.height.ToString();
                height = patientData.height;
                weightCombo.Text = patientData.weight.ToString();
                weight = patientData.weight;
                // 
                //0=user_id, 1=lang_id, 2=user_gender, 3=user_byear, 4=user_hight, 5=user_whight, 6=user_status_summary, 7=user_comments, 8=user_addrCity, 9=user_imageNamePath, 10=user_level, 11=user_shoe_size
                if (int.Parse(userInfo[0]) != patientId)
                    throw new System.ArgumentException("ID from database is differant from UI", "DataBase");
                lang = int.Parse(userInfo[1]);
                setLangView();
                userNotes = notesTxt.Text = userInfo[7];
                //birthYearCombo.Text = userInfo[3];
                //gender = int.Parse(userInfo[2]);                
                //shoeCombo.Text = userInfo[11];
                //heightCombo.Text = userInfo[4];
                //weightCombo.Text = userInfo[5];
                tenMAddBtn.Visible = true;
                tugAddBtn.Visible = true;
                dgiAddBtn.Visible = true;
                mocaAddBtn.Visible = true;
                TestsDataGridView.Visible = true;
                updateTestsDataGridView();
                is_data_changed = false;                
                needSave();
            }
            else {
                MessageBox.Show("Invalid user ID: " + pId + " User does not exist in database!");
                this.Close();
            }
        }

        private void fill_ComboItems() {
            // Birth year combo
            int curYear = 2020;
            for (int i = curYear; i > 1910; i--) {
                birthYearCombo.Items.Add(i);
            }
            // height Combo 
            int h = 210;
            for (int i = h; i > 120; i--) {
                heightCombo.Items.Add(i);
            }
            // weight Combo
            int w = 180;
            for (int i = w; i > 20; i--) {
                weightCombo.Items.Add(i);
            }
            // shoeCombo
            int s = 48;
            for (int i = s; i > 20; i--) {
                shoeCombo.Items.Add(i);
            }
        }

        private void birthYearCombo_DropDown(object sender, EventArgs e) {
            if (birthYearCombo.Text == "")
                birthYearCombo.SelectedIndex = (2020 - 1910) / 2;
        }

        private void heightCombo_DropDown(object sender, EventArgs e) {
            if (heightCombo.Text == "")
                heightCombo.SelectedIndex = (210 - 120) / 2;
        }

        private void weightCombo_DropDown(object sender, EventArgs e) {
            if (weightCombo.Text == "")
                weightCombo.SelectedIndex = (180 - 20) / 2;

        }

        private void shoeCombo_DropDown(object sender, EventArgs e) {
            if (shoeCombo.Text == "")
                shoeCombo.SelectedIndex = (48 - 20) / 2;
        }

        private void updateTestsDataGridView() {
            if (patientId > -1) {
                TestsDataGridView.Rows.Clear();
                DataSet testsDataSet = datahandler.getPatientTestsHistory(patientId);
                DataTable testsDataTable = testsDataSet.Tables[0];
                foreach (DataRow dr in testsDataTable.Rows) {
                    TestsDataGridView.Rows.Add(dr[0], dr[1], (int.Parse(dr[2].ToString()) > -1) ? dr[2] : "", (int.Parse(dr[3].ToString()) > -1) ? dr[3] : "", (int.Parse(dr[4].ToString()) > -1) ? dr[4] : "");
                }
            }
        }

        private void needSave() {
            if ((fNameTxt.Text != patientData.FirstName) || (lNameTxt.Text != patientData.LastName)
                || (eMailTxt.Text != patientData.email) || (phoneTxt.Text != patientData.phoneNumber) || (notesTxt.Text != userNotes) || (gender != patientData.gender) 
                || (bYear != patientData.birthYear.ToString()) || (shoeCombo.Text != patientData.shueSize.ToString()) || (heightCombo.Text != patientData.height.ToString())
                || (weightCombo.Text != patientData.weight.ToString())
                || is_data_changed) {
                is_data_changed = true;
            }
            else {
                is_data_changed = false;
            }
            if (is_data_changed || isNewPatient) {
                saveDataBtn.Enabled = true;
                saveDataBtn.BackColor = Color.FromArgb(255, 201, 31);
                saveDataBtn.ForeColor = Color.Black;
            }
            else {                
                saveDataBtn.Enabled = false;
                saveDataBtn.BackColor = Color.FromArgb(196, 196, 196);
                saveDataBtn.ForeColor = Color.White;
            }
        }

        private bool canClose() {
            bool cClose = (is_data_changed);
            return cClose;
        }

        private void closeBtn_Click(object sender, EventArgs e) {
            close_keyboard();
            if (!canClose())
                this.Close();
            else {
                //string message = "You have made some changes here, do you want to save changes?\n";
                //string caption = "Lost data";
                //MessageBoxButtons buttons = MessageBoxButtons.YesNoCancel;
                YesNoDialog yesNoDialog = new YesNoDialog();
                //DialogResult result = MessageBox.Show(message, caption, buttons);
                DialogResult result = yesNoDialog.ShowDialog();

                if (result == System.Windows.Forms.DialogResult.Yes) {
                    saveData();
                }
                if (result == System.Windows.Forms.DialogResult.No) {
                    this.Close();
                }
            }
        }

        private void saveDataBtn_Click(object sender, EventArgs e) {
            close_keyboard();
            saveData();
        }

        private void saveData() { 
            long res = -1;
            // check minimum requirements for new record
            //1. ID
            if (pIdTxt.Text == "") {
                SelectGenderTxt.Text = INVALID_ID;
                //this.DialogResult = DialogResult.Abort;
                return;
            }
            else {
                SelectGenderTxt.Text = "";
            }
        
            //2. geneder
            if (gender == 0) {
                SelectGenderTxt.Text = INVALID_GENDER;
                //this.DialogResult = DialogResult.Abort;
                return;
            }
            else {
                SelectGenderTxt.Text = "";
            }
            //3. first name filed
            if (fNameTxt.Text == "") {
                SelectGenderTxt.Text = INVALID_NAME;
                return;
            }
            else {
                SelectGenderTxt.Text = "";
            }

            patientData.siteId = SITEID;
            patientData.SiteID = SITEID;
            patientData.ID = long.Parse(pIdTxt.Text);
            patientData.FirstName = fNameTxt.Text;
            patientData.LastName = lNameTxt.Text;
            patientData.email = eMailTxt.Text;
            patientData.phoneNumber = phoneTxt.Text;
            patientData.birthYear = int.Parse(bYear);
            patientData.shueSize = shoeSize;
            patientData.height = height;
            patientData.weight = weight;
            patientData.gender = gender;

            if (isNewPatient) { //TO-DO: save extra data into db
                // *********** NOTE, patientId should be -1 in that stage! **********************//
                // first add the patient to identity
                Task t = Task.Run(async () => { res = await Identity.addNewPatient(patientData, SITEID); });
                Task.WaitAll(t);
                if (res == -1) {
                    MessageBox.Show("Identity: Failed adding new patient!");
                    //this.DialogResult = DialogResult.Cancel;
                    return;
                }
                else {  // if success - get patient data from the identity server (to extract the patient hashId)                     
                    t = Task.Run(async () => { patientData = await Identity.getUserDataByIDAsync(patientData.ID.ToString(), SITEID); });
                    Task.WaitAll(t);
                    //MessageBox.Show("HashID: " + patientData.HashedID);
                    patientId = (long)patientData.refIDnum;
                    if (patientId <= 0) {
                        MessageBox.Show("Invalid patient ID!");
                        //this.DialogResult = DialogResult.Cancel;
                        return;
                    }
                }
                // now store the info to the main server
                res = datahandler.addPatient(patientData, lang, notesTxt.Text, uLevel);
                // Save tests results: add_user_testResults
                if (tenMsecTxtBox.Text != "" && tenMstepsTxtBox.Text != "") {
                    datahandler.AddPatientTestResults(patientId, 1, -1, int.Parse(tenMsecTxtBox.Text), int.Parse(tenMstepsTxtBox.Text));
                }
                if (tugTxtBox.Text != "") {
                    datahandler.AddPatientTestResults(patientId, 2, -1, int.Parse(tugTxtBox.Text), -1);
                }
                if (dgiCombo.Text != "Choose" && dgiCombo.Text != "") {
                    datahandler.AddPatientTestResults(patientId, 3, int.Parse(dgiCombo.Text), -1, -1);
                }
                if (mocaCombo.Text != "Choose" && mocaCombo.Text != "") {
                    datahandler.AddPatientTestResults(patientId, 4, int.Parse(mocaCombo.Text), -1, -1);
                }
            }
            else {

                // Update identity server                

                // Store data at the identity server
                
                Task t = Task.Run(async () => { res = await Identity.UpdateUserData(patientData, SITEID); });
                Task.WaitAll(t);
                if (res == -1) {
                    MessageBox.Show("Failed update the patient data!");
                    //this.DialogResult = DialogResult.Cancel;
                    return;
                }
                // Update selfit DB
                res = datahandler.updatePatient(patientData, lang, notesTxt.Text, uLevel);
                if (res != 0)
                    MessageBox.Show("data was not updated!");
                this.Close();
            }

            if (res != -1) {
                this.DialogResult = DialogResult.OK;
                //this.Close();
            }
            else {
                this.DialogResult = DialogResult.Cancel;
                return;
            }
        }
        // saveDataBtn_Click


        private void fNameTxt_TextChanged(object sender, EventArgs e) {            
            needSave();
        }

        private void lNameTxt_TextChanged(object sender, EventArgs e) {
            needSave();
        }

        private void pIdTxt_TextChanged(object sender, EventArgs e) {
            needSave();
        }

        private void eMailTxt_TextChanged(object sender, EventArgs e) {
            needSave();
        }

        private void telTxt_TextChanged(object sender, EventArgs e) {
            needSave();
        }

        private void userLevelCombo_SelectedIndexChanged(object sender, EventArgs e) {
            uLevel = userLevelCombo.SelectedIndex + 1;
            is_data_changed = true;
        }

        
        private void birthYearCombo_DropDownClosed_1(object sender, EventArgs e) {
            //updateBdateStr();
            bYear = birthYearCombo.Text;
            if (bYear != patientData.birthYear.ToString())
                is_data_changed = true;
            needSave();
        }

        private void maleSelectBtn_Click(object sender, EventArgs e) {
            if (gender != 1) {
                gender = 1;
                setGenderButtons();
                is_data_changed = true;
                SelectGenderTxt.Visible = true;
            }
            needSave();
        }

        private void femaleSelectBtn_Click(object sender, EventArgs e) {
            if (gender != 2) {
                gender = 2;
                setGenderButtons();
                is_data_changed = true;
                SelectGenderTxt.Visible = true;
            }
            needSave();
        }

        private void setGenderButtons() {
            if (gender == 1) {
                maleSelectBtn.Image = Properties.Resources.Vector_2_2;
                maleSelectBtn.FlatAppearance.BorderColor = Color.FromArgb(255, 201, 31);
                femaleSelectBtn.Image = null;
                femaleSelectBtn.FlatAppearance.BorderColor = Color.White;
            }
            if (gender == 2) {
                femaleSelectBtn.Image = Properties.Resources.Vector_2_2;
                femaleSelectBtn.FlatAppearance.BorderColor = Color.FromArgb(255, 201, 31);
                maleSelectBtn.Image = null;
                maleSelectBtn.FlatAppearance.BorderColor = Color.White;
            }
        }

        private void shoeCombo_SelectedIndexChanged(object sender, EventArgs e) {
            shoeSize = int.Parse(shoeCombo.Text);
            if (shoeSize != patientData.shueSize)
                is_data_changed = true;
        }

        private void heightCombo_ValueChanged(object sender, EventArgs e) {
            height = int.Parse(heightCombo.Text);
            if (height != patientData.height)
                is_data_changed = true;
        }

        private void weightCombo_ValueChanged(object sender, EventArgs e) {
            weight = int.Parse(weightCombo.Text);
            if (weight != patientData.weight)
                is_data_changed = true;
        }

        private void LangSelectionCBX_SelectedIndexChanged(object sender, EventArgs e) {
            //if (LangSelectionCBX.Text == "English")
            //    lang = 0;
            //if (LangSelectionCBX.Text == "Hebrew")
            //    lang = 1;
            if (lang != LangSelectionCBX.SelectedIndex) {
                is_data_changed = true;
                //lang = LangSelectionCBX.SelectedIndex;
            }
            
            needSave();
        }

        private void setLangView() {
            LangSelectionCBX.SelectedIndex = lang;
        }

        private bool is_numeric(string str) {
            int nl = 0;
            return int.TryParse(str, out nl);
        }

        private void tenMsecTxtBox_KeyUp(object sender, KeyEventArgs e) {
            if (!is_numeric(tenMsecTxtBox.Text)) {
                if (tenMsecTxtBox.Text.Length > 1)
                    tenMsecTxtBox.Text = tenMsecTxtBox.Text.Remove(tenMsecTxtBox.Text.Length - 1);
                else
                    tenMsecTxtBox.Text = "";
            }
        }

        private void tugTxtBox_KeyUp(object sender, KeyEventArgs e) {
            if (!is_numeric(tugTxtBox.Text)) {
                if (tugTxtBox.Text.Length > 1)
                    tugTxtBox.Text = tugTxtBox.Text.Remove(tugTxtBox.Text.Length - 1);
                else
                    tugTxtBox.Text = "";
            }
        }

        private void tenMstepsTxtBox_KeyUp(object sender, KeyEventArgs e) {
            if (!is_numeric(tenMstepsTxtBox.Text)) {
                if (tenMstepsTxtBox.Text.Length > 1)
                    tenMstepsTxtBox.Text = tenMstepsTxtBox.Text.Remove(tenMstepsTxtBox.Text.Length - 1);
                else
                    tenMstepsTxtBox.Text = "";
            }
        }

        private void tenMAddBtn_Click(object sender, EventArgs e) {
            datahandler.AddPatientTestResults(patientId, 1, -1, int.Parse(tenMsecTxtBox.Text), int.Parse(tenMstepsTxtBox.Text));
            updateTestsDataGridView();
        }

        private void tugAddBtn_Click(object sender, EventArgs e) {
            datahandler.AddPatientTestResults(patientId, 2, -1, int.Parse(tugTxtBox.Text), -1);
            updateTestsDataGridView();
        }

        private void dgiAddBtn_Click(object sender, EventArgs e) {
            datahandler.AddPatientTestResults(patientId, 3, int.Parse(dgiCombo.Text), -1, -1);
            updateTestsDataGridView();
        }

        private void mocaAddBtn_Click(object sender, EventArgs e) {
            datahandler.AddPatientTestResults(patientId, 4, int.Parse(mocaCombo.Text), -1, -1);
            updateTestsDataGridView();
        }

        private void tenMsecTxtBox_TextChanged(object sender, EventArgs e) {
            tenMAddBtn.Enabled = (tenMsecTxtBox.Text != "" && tenMstepsTxtBox.Text != "");
        }

        private void tenMstepsTxtBox_TextChanged(object sender, EventArgs e) {
            tenMAddBtn.Enabled = (tenMsecTxtBox.Text != "" && tenMstepsTxtBox.Text != "");
        }

        private void tugTxtBox_TextChanged(object sender, EventArgs e) {
            tugAddBtn.Enabled = (tugTxtBox.Text != "");
        }

        private void dgiCombo_SelectedIndexChanged(object sender, EventArgs e) {
            dgiAddBtn.Enabled = (dgiCombo.Text != "Choose" || dgiCombo.Text != "");
        }

        private void mocaCombo_SelectedIndexChanged(object sender, EventArgs e) {
            mocaAddBtn.Enabled = (mocaCombo.Text != "Choose" || mocaCombo.Text != "");
        }

        private void heightCombo_SelectedIndexChanged(object sender, EventArgs e) {
            height = int.Parse(heightCombo.Text);
            if (height != patientData.height)
                is_data_changed = true;
            needSave();
        }

        private void heightCMbtn_Click(object sender, EventArgs e) {
            heightUnits = 0;
            setHeightButtons();
        }

        private void heightINCHbtn_Click(object sender, EventArgs e) {
            heightUnits = 1;
            setHeightButtons();
        }

        private void setHeightButtons() {
            if (heightUnits == 0) {
                heightCMbtn.Image = Properties.Resources.Vector_2_2;
                heightCMbtn.FlatAppearance.BorderColor = Color.FromArgb(255, 201, 31);
                heightINCHbtn.Image = null;
                heightINCHbtn.FlatAppearance.BorderColor = Color.White;
            }
            if (heightUnits == 1) {
                heightINCHbtn.Image = Properties.Resources.Vector_2_2;
                heightINCHbtn.FlatAppearance.BorderColor = Color.FromArgb(255, 201, 31);
                heightCMbtn.Image = null;
                heightCMbtn.FlatAppearance.BorderColor = Color.White;
            }
        }

        private void weightKGbtn_Click(object sender, EventArgs e) {
            weightUnits = 0;
            setWeightButtons();
        }

        private void weightPOUNDbtn_Click(object sender, EventArgs e) {
            weightUnits = 1;
            setWeightButtons();
        }
        private void setWeightButtons() {
            if (weightUnits == 0) {
                weightKGbtn.Image = Properties.Resources.Vector_2_2;
                weightKGbtn.FlatAppearance.BorderColor = Color.FromArgb(255, 201, 31);
                weightPOUNDbtn.Image = null;
                weightPOUNDbtn.FlatAppearance.BorderColor = Color.White;
            }
            if (weightUnits == 1) {
                weightPOUNDbtn.Image = Properties.Resources.Vector_2_2;
                weightPOUNDbtn.FlatAppearance.BorderColor = Color.FromArgb(255, 201, 31);
                weightKGbtn.Image = null;
                weightKGbtn.FlatAppearance.BorderColor = Color.White;
            }
        }

        private void shoeEUbtn_Click(object sender, EventArgs e) {
            shoesizeUnits = 0;
            setShoesizeButtons();
        }

        private void shoeUSbtn_Click(object sender, EventArgs e) {
            shoesizeUnits = 1;
            setShoesizeButtons();
        }
        private void setShoesizeButtons() {
            if (shoesizeUnits == 0) {
                shoeEUbtn.Image = Properties.Resources.Vector_2_2;
                shoeEUbtn.FlatAppearance.BorderColor = Color.FromArgb(255, 201, 31);
                shoeUSbtn.Image = null;
                shoeUSbtn.FlatAppearance.BorderColor = Color.White;
            }
            if (shoesizeUnits == 1) {
                shoeUSbtn.Image = Properties.Resources.Vector_2_2;
                shoeUSbtn.FlatAppearance.BorderColor = Color.FromArgb(255, 201, 31);
                shoeEUbtn.Image = null;
                shoeEUbtn.FlatAppearance.BorderColor = Color.White;
            }
        }

        private void fNameTxt_TextChanged_1(object sender, EventArgs e) {
            if (sender != this)
                needSave();
        }

        private void lNameTxt_TextChanged_1(object sender, EventArgs e) {
            if (sender != this)
                needSave();
        }

        private void eMailTxt_TextChanged_1(object sender, EventArgs e) {
            if (sender != this)
                needSave();            
        }

        private void phoneTxt_TextChanged(object sender, EventArgs e) {
            if (sender != this)
                needSave();            
        }

        private void notesTxt_TextChanged(object sender, EventArgs e) {
            if (sender != this)
                needSave();
        }

        

        private void lNameTxt_KeyUp(object sender, KeyEventArgs e) {
            if (e.KeyCode == Keys.Enter) {
                pIdTxt.Focus();
            }
            needSave();
        }

        private void eMailTxt_KeyUp(object sender, KeyEventArgs e) {
            if (e.KeyCode == Keys.Enter) {
                phoneTxt.Focus();
            }
            needSave();
        }

        private void phoneTxt_KeyUp(object sender, KeyEventArgs e) {
            if (e.KeyCode == Keys.Enter) {
                notesTxt.Focus();
            }
            needSave();
        }

        private void notesTxt_KeyUp(object sender, KeyEventArgs e) {
            needSave();
        }

        private void weightCombo_SelectedIndexChanged(object sender, EventArgs e) {
            weight = int.Parse(weightCombo.Text);
            if (weight != patientData.weight)
                is_data_changed = true;
            needSave();
        }

        private void shoeCombo_SelectedIndexChanged_1(object sender, EventArgs e) {
            shoeSize = int.Parse(shoeCombo.Text);
            if (shoeSize != patientData.shueSize)
                is_data_changed = true;
            needSave();
        }
               

        private void heightCombo_DropDownClosed(object sender, EventArgs e) {
            height = int.Parse(heightCombo.Text);
            if (height != patientData.height)
                is_data_changed = true;
            needSave();
        }

        private void weightCombo_SelectionChangeCommitted(object sender, EventArgs e) {
            weight = int.Parse(weightCombo.Text);
            if (weight != patientData.weight)
                is_data_changed = true;
            needSave();
        }

        private void shoeCombo_DropDownClosed(object sender, EventArgs e) {
            shoeSize = int.Parse(shoeCombo.Text);
            if (shoeSize != patientData.shueSize)
                is_data_changed = true;
            needSave();
        }

        private void weightCombo_DropDownClosed(object sender, EventArgs e) {
            weight = int.Parse(weightCombo.Text);
            if (weight != patientData.weight)
                is_data_changed = true;
            needSave();
        }

        private void LangSelectionCBX_DropDownClosed(object sender, EventArgs e) {
            if (LangSelectionCBX.Text == "English")
                lang = 0;
            if (LangSelectionCBX.Text == "Hebrew")
                lang = 1;
            is_data_changed = true;
            needSave();
        }
        private void load_virtual_keyboard() {
            //var simu = new InputSimulator();          
            if (keyBoardOn == false) {
                //simu.Keyboard.ModifiedKeyStroke(new[] { VirtualKeyCode.LWIN, VirtualKeyCode.CONTROL }, VirtualKeyCode.VK_O);                

                Wow64Interop.EnableWow64FSRedirection(false);
                string currDir = Directory.GetCurrentDirectory();
                Directory.SetCurrentDirectory(Environment.SystemDirectory);
                keyBoardProcess = new Process();
                keyBoardProcess.StartInfo.UseShellExecute = true;
                keyBoardProcess.StartInfo.WorkingDirectory = Environment.SystemDirectory;
                keyBoardProcess.StartInfo.FileName = "osk.exe";
                keyBoardProcess.StartInfo.WindowStyle = ProcessWindowStyle.Normal;
                keyBoardProcess.Start();
                keyBoardOn = true;
                Directory.SetCurrentDirectory(currDir);
                Wow64Interop.EnableWow64FSRedirection(true);
            }
            else {
                close_keyboard();
                keyBoardOn = false;
            }
        }

        private void close_keyboard() {
            if (keyBoardProcess != null ) {
                try {
                    if (!keyBoardProcess.HasExited) {
                        keyBoardProcess.Kill();
                        keyBoardProcess.Close();
                    }
                }
                catch (Exception ex) {
                    keyBoardOn = false;
                }
            }
            keyBoardOn = false;
        }

        private void fNameTxt_MouseUp(object sender, MouseEventArgs e) {
            load_virtual_keyboard();
        }

        private void fNameTxt_KeyUp(object sender, KeyEventArgs e) {
            if (e.KeyCode == Keys.Enter) {
                lNameTxt.Focus();
            }
            needSave();
        }

        private void pIdTxt_KeyUp(object sender, KeyEventArgs e) {
            if (e.KeyCode == Keys.Enter) {
                eMailTxt.Focus();
            }
            needSave();
        }

        private void lNameTxt_MouseClick(object sender, MouseEventArgs e) {
            load_virtual_keyboard();
        }

        private void fNameTxt_MouseClick(object sender, MouseEventArgs e) {
            load_virtual_keyboard();
        }

        private void pIdTxt_MouseClick(object sender, MouseEventArgs e) {
            load_virtual_keyboard();
        }

        private void eMailTxt_MouseClick(object sender, MouseEventArgs e) {
            load_virtual_keyboard();
        }

        private void phoneTxt_MouseClick(object sender, MouseEventArgs e) {
            load_virtual_keyboard();
        }

        private void notesTxt_MouseClick(object sender, MouseEventArgs e) {
            load_virtual_keyboard();
        }

        private void birthYearCombo_SelectedIndexChanged(object sender, EventArgs e) {
            bYear = birthYearCombo.Text;
            if (bYear != patientData.birthYear.ToString())
                is_data_changed = true;
            needSave();
        }

        private void autoGenIDBtn_Click(object sender, EventArgs e) {
            
        }

        private void LangSelectionCBX_SelectedIndexChanged_1(object sender, EventArgs e) {
            int tlang = 0;
            if (LangSelectionCBX.Text == "English")
                tlang = 0;
            if (LangSelectionCBX.Text == "Hebrew")
                tlang = 1;
            if (lang != tlang) {
                is_data_changed = true;
                lang = tlang;
            }

            needSave();
        }
    }    
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SelfitMain {
    public partial class AddTherapist : Form {
        private dbHandler dbh;
        string bDate = "1999-9-9";
        private int gender = 0;
        public AddTherapist() {
            InitializeComponent();
            fNameTxt.Text = "";
            lNameTxt.Text = "";
            telTxt.Text = "";
            eMailTxt.Text = "";            
            dbh = new dbHandler();
        }

        private void needSave() {
            if (fNameTxt.Text == "" || lNameTxt.Text == "" || eMailTxt.Text == "") {
                saveTherapistBtn.Enabled = false;
                saveTherapistBtn.BackColor = Color.FromArgb(196, 196, 196);
            }
            else {
                saveTherapistBtn.Enabled = true;
                saveTherapistBtn.BackColor = Color.FromArgb(255, 201, 31);
            }
        }

        private void exitAddTherapistBtn_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void saveTherapistBtn_Click(object sender, EventArgs e) {
            //INSERT INTO `unity-db`.users(user_name, user_lastName,user_type,user_email,user_phone, user_bdate) 
            //VALUES("Vicktor", "Hason", 2, "hason@gashsash.com", "001-555-3449", '1968-07-20')
            int res = dbh.addTherapist(fNameTxt.Text,lNameTxt.Text,eMailTxt.Text, telTxt.Text,bDate);
            if (res != -1) {                
                this.Close();
            }
        }

        private void fNameTxt_TextChanged(object sender, EventArgs e) {
            needSave();
        }

        private void lNameTxt_TextChanged(object sender, EventArgs e) {
            needSave();
        }

        private void eMailTxt_TextChanged(object sender, EventArgs e) {
            needSave();
        }

        private void telTxt_TextChanged(object sender, EventArgs e) {
            needSave();
        }

        private void birthDATcombo_SelectedIndexChanged(object sender, EventArgs e) {
            updateBdateStr();
        }        

        private void birthMonthCombo_SelectedIndexChanged(object sender, EventArgs e) {
            updateBdateStr();
        }

        private void birthYearCombo_SelectedIndexChanged(object sender, EventArgs e) {
            updateBdateStr();
        }

        private void updateBdateStr() {
            string bDay = (birthDATcombo.Text != "") ? bDay = birthDATcombo.Text: "1";
            string bMonth = (birthMonthCombo.SelectedIndex != 0) ? bMonth = birthMonthCombo.SelectedIndex.ToString() : "1";
            string bYear = (birthYearCombo.Text != "") ? birthYearCombo.Text : "1999";
            bDate = bYear + "-" + bMonth + "-" + bDay;
        }

        private void maleSelectionBtn_Click(object sender, EventArgs e) {
            if (gender != 1) {
                gender = 1;
                setGenderButtons();
            }
        }

        private void femaleSelectionBtn_Click(object sender, EventArgs e) {
            if (gender != 2) {
                gender = 2;
                setGenderButtons();
            }
        }

        private void otherSelectionBtn_Click(object sender, EventArgs e) {
            //if (gender != 3) {
            //    gender = 3;
             //   setGenderButtons();
            //}
        }

        private void setGenderButtons() {
            if (gender == 1) {
                maleSelectionBtn.Image = Properties.Resources.Vector_2_2;
                maleSelectionBtn.FlatAppearance.BorderColor = Color.FromArgb(255, 201, 31);
                femaleSelectionBtn.Image = null;
                femaleSelectionBtn.FlatAppearance.BorderColor = Color.White;
            }
            if (gender == 2) {
                femaleSelectionBtn.Image = Properties.Resources.Vector_2_2;
                femaleSelectionBtn.FlatAppearance.BorderColor = Color.FromArgb(255, 201, 31);
                maleSelectionBtn.Image = null;
                maleSelectionBtn.FlatAppearance.BorderColor = Color.White;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.Diagnostics;
using System.Globalization;
using System.Security.Cryptography;
using System.IO;
using WindowsInput;
using WindowsInput.Native;
using System.Runtime.InteropServices;

namespace SelfitMain {

    public partial class LogInForm : Form {

        public static string pExe = "\\selfitpilotkinect2018\\SelfitPilot.exe";
        private const string patientSearchTxtTEXT = "Enter patient name OR patient number";

        public static int SCROLL_VALUE = 16;

        //private dbHandler dbh;
        private DataHandler datahandler;
        private IdentityClient Identity;
        //private msDbHandler dbh;
        private int userId;
        //private DataTable dt;
        private DataTable sessionDt;
        private DataTable plansTable;
        private long patianteId = -1;
        private string patientName = string.Empty;
        private int therapistId = -1;
        private int SITEID = -1;
        //string exName = "";
        private int selectedPlan = -1;
        private bool keyBoardOn = false;
        private bool fastEditDataChanged = false;

        public struct practice_log_data {
            public int p_id { get; set; }
            public string p_name { get; set; }
            public int p_reps { get; set; }
        }

        public struct practice_data {
            public int practice_id { get; set; }
            public string practice_name { get; set; }
            public int practice_unityId { get; set; }
            public int practice_reps { get; set; }
            public bool is_visible { get; set; }

        }

        List<practice_data> active_practices = new List<practice_data>();
        List<practice_data> plan_practices = new List<practice_data>();

        public struct selection_filter {
            public string filter_name { get; set; }
            public bool active { get; set; }
            public selection_filter(string name) {
                filter_name = name;
                active = false;
            }
        }

        public struct imageContainer {
            public int practice_id { get; set; }
            public string imageName { get; set; }

            public imageContainer(int id, string name) {
                practice_id = id;
                imageName = name;
            }
        }

        List<selection_filter> standing_list = new List<selection_filter>();
        List<string> standing = new List<string>() { "Lower extremity strength", "Lower extremity ROM", "Upper extremity strength", "Upper extremity ROM", "Balance", "Movement planning", "Multi tasking", "Reaction time", "Gait", "Coordination", "Agility", "Aerobic", "All" };
        List<string> active_standing = new List<string>();
        List<selection_filter> sitting_list = new List<selection_filter>();
        List<string> sitting = new List<string>() { "Lower extremity strength", "Lower extremity ROM", "Upper extremity strength", "Upper extremity ROM", "Balance", "Movement planning", "Multi tasking", "Reaction time", "Coordination", "Agility", "All" };
        List<string> active_sitting = new List<string>();
        List<imageContainer> image_names = new List<imageContainer>();
        DataTable practiceMeasuresInfo = new DataTable();

        private Process keyBoardProcess;// = new Process();

        public LogInForm() {
            InitializeComponent();
            MainTabControl.SelectTab("LoginPage");
            //dbh = new dbHandler();
            //dbh = new msDbHandler();
            //MessageBox.Show("User is: " + userId);
            
            datahandler = new DataHandler();
            SITEID = datahandler.getSiteID();
            
            Identity = new IdentityClient(datahandler.getSYS_Server());
            canStart(false);
            //keyBoardProcess.StartInfo.FileName
            // initialize filters
            foreach (string s in standing) {
                standing_list.Add(new selection_filter(s));
            }
            foreach (string s in sitting) {
                sitting_list.Add(new selection_filter(s));
            }
            dateTimePicker1.MaxDate = DateTime.Today;

            loadLoginPage();
        }



        private void button1_Click(object sender, EventArgs e) {
            this.Close();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public string getImageName(int id) {
            imageContainer res = new imageContainer(0, "0");
            res = image_names.Find(x => x.practice_id == id);

            return res.imageName;
        }

        private bool UserNotExistsFlag = false;

        private void OkBtn_Click(object sender, EventArgs e) {
            userId = datahandler.getTherapistRights(UserNameTxtBox.Text, UserPasswordTxtBox.Text, SITEID);
            keyBoardOn = false;
            close_keyboard();

            //MessageBox.Show("User is: " + UserNameTxtBox.Text + " Passwor: " + UserPasswordTxtBox.Text + " ID:" + userId);
            if (userId == -1) {
                UserNotExistsFlag = incorrectUserTxt.Visible = true;

                //MessageBox.Show("User not exists or invalid password!");
            }
            else {
                // Store user name for next time
                Properties.Settings.Default.userName = UserNameTxtBox.Text;
                Properties.Settings.Default.Save();
                loadHomePage();
            }
        }


        private void CancelBtn_Click(object sender, EventArgs e) {
            Close();
            Application.Exit();
        }

        private void LogInForm_Load(object sender, EventArgs e) {
            UserNameTxtBox.Focus();
            MainTabControl.Location = new Point(-1, -22);
            //clearPatientDetailes();

        }

        public void loadHomePage() {

            //dbHandler dbh = new dbHandler();  
            // fill the image names data
            DataSet imgData = datahandler.getImageData();
            DataTable imgTable = imgData.Tables[0];
            foreach (DataRow dr in imgTable.Rows) {
                imageContainer ic = new imageContainer();
                ic.practice_id = int.Parse(dr[0].ToString());
                ic.imageName = dr[1].ToString();
                image_names.Add(ic);
            }

            // fill practice measures categories table
            DataSet measursData = datahandler.getPracticeMeasuresInfo();
            practiceMeasuresInfo = measursData.Tables[0];            

            userId = -1;
            patianteId = -1;
            patientName = string.Empty;
            therapistId = -1;
            selectedPlan = -1;
            patientSearchTxt.Text = patientSearchTxtTEXT;
            patientTabControl.ItemSize = new Size(68, 1);
            patientSearchTxt.SelectionStart = patientSearchTxt.Text.Length;
            PatientsListBox.Items.Clear();
            int userCount = datahandler.getTotalPatientsCount();
            patientCountLbl.Text = "(" + userCount.ToString() + " available)";
            canStart(false);

            useHebSpeachCBX.Checked = datahandler.getSpeachState();

            MainTabControl.SelectTab("homePage");
            patientSearchTxt.Focus();
            patientSearchTxt.Select();
        }

        private void updatePatientsListBox(List<Patient> pl) {
            PatientsListBox.Items.Clear();
            if (pl.Count > 0) {
                //clearPatientInfo();
                //patientDetailsPanel.Visible = false;
                patientTabControl.SelectTab("MainPatient");
                foreach (Patient p in pl) {
                    //MessageBox.Show(p.FirstName);
                    PatientsListBox.Items.Add("\t" + p.refIDnum + "\t" + p.FirstName + " " + p.LastName);
                }
                if (PatientsListBox.Items.Count > 21)
                    patientListArrowDownBtn.Visible = true;
                else
                    patientListArrowDownBtn.Visible = false;
                PatientsListBox.Invalidate();
            }
        }

        List<Patient> pList;

        private void patientSearchTxt_KeyUp(object sender, KeyEventArgs e) {
            if (e.KeyCode == Keys.Enter) {// && startBtn.Enabled == true) {
                patientSearchOperation();
            }
            if (patientSearchTxt.Text.Contains("ControlK"))
                patientSearchTxt.Text = "";
            if (patientSearchTxt.Text.Contains(patientSearchTxtTEXT))
                patientSearchTxt.Text = new KeysConverter().ConvertToString(e.KeyCode);
            if (patientSearchTxt.Text != patientSearchTxtTEXT) {
                //patientSearchTxt.Text = patientSearchTxt.Text.Trim();
                if (!is_numericL(patientSearchTxt.Text)) {
                    //pNameMsgLbl.Text = "Only numbers allowed";
                    //if (patientSearchTxt.Text.Length > 1) {
                    //    patientSearchTxt.Text = patientSearchTxt.Text.Remove(patientSearchTxt.Text.Length - 1);

                    //}
                    //else
                    //    patientSearchTxt.Text = "";
                }
                else { // search by ID
                    //pNameMsgLbl.Text = "";
                    if (!is_numericL(new KeysConverter().ConvertToString(e.KeyCode)))
                        patientSearchTxt.Text = patientSearchTxt.Text.Trim();
                }
            }
            //if (patientSearchTxt.Text == "")   {
            //    patientSearchTxt.Text = "Enter patient’s ID (Example - 126)";
            //}            
            patientSearchTxt.SelectionStart = patientSearchTxt.Text.Length;
            //  
            canStart();
        }

        private void patientSearchMagBtn_Click(object sender, EventArgs e) {
            patientSearchOperation();
        }

        private void patientSearchTxt_TextChanged(object sender, EventArgs e) {
            patientSearchOperation();
        }

        private void patientSearchOperation() {
            patianteId = -1;
            canStart();
            if (patientSearchTxt.Text != patientSearchTxtTEXT && patientSearchTxt.Text != "") {
                pList = new List<Patient>();
                if (is_numericL(patientSearchTxt.Text)) {
                    //MessageBox.Show("Search ID: " + patientSearchTxt.Text);
                    Patient p = new Patient();
                    try {
                        Task t = Task.Run(async () => { p = await Identity.getUserDataByIDAsync(patientSearchTxt.Text, SITEID); });
                        Task.WaitAll(t);
                    }
                    catch (Exception ex) {
                        // Show UI message for network connectivity                        
                    }
                    if (p != null)
                        pList.Add(p);
                }
                else {
                    //MessageBox.Show("Search Name: " + patientSearchTxt.Text);
                    string fnSearchStr = string.Empty;
                    string lnSearchStr = string.Empty;
                    string[] searchList = patientSearchTxt.Text.Split(' ');
                    if (searchList.Length >= 1)
                        fnSearchStr = searchList[0];
                    if (searchList.Length >= 2)
                        lnSearchStr = searchList[1];
                    try {
                        Task t = Task.Run(async () => { pList = await Identity.getUsersDataByNameAsync(fnSearchStr, lnSearchStr, SITEID); });
                        //   Identity.getUsersDataByNameAsync(patientSearchTxt.Text, "", 1).Result;
                        Task.WaitAll(t);
                    }
                    catch (Exception ex) {
                        // Show UI message for network connectivity                        
                    }
                    //pList = t.

                }
                updatePatientsListBox(pList);
                return;
            }
            else {
                PatientsListBox.Items.Clear();
                clearPatientInfo();
                //patientDetailsPanel.Visible = false;
                patientTabControl.SelectTab("MainPatient");
            }
        }

        private void PatientsListBox_SelectedIndexChanged(object sender, EventArgs e) {
            this.PatientsListBox.Invalidate();
            // Get user ID from the selected
            if (PatientsListBox.SelectedItem == null) {
                return;
            }
            SELECTED_PatientsListBox = PatientsListBox.SelectedIndices[0];
            string[] idStr = PatientsListBox.SelectedItem.ToString().Split('\t');
            int uId;
            string siteTxt = SITEID.ToString();
            if (int.TryParse(idStr[1], out uId)) {
                patianteId = uId; // ref ID
                //patientDetailsPanel.Visible = true;
                patientTabControl.SelectTab("ShowDetails");
                //patientInfoNameTxt.Text = idStr[2];
                Patient pt = pList.Find(item => item.refIDnum == uId);
                if (pt != null) // check item isn't null
                {
                    patientInfoNameLbl.Text = pt.FirstName + " " + pt.LastName;
                    string idText = Decrypt(pt.HashedID);
                    patientInfoIDLbl.Text = IdPatientEdit.Text = idText.Substring(0, idText.Length - siteTxt.Length);
                    patientInfoEmailTxt.Text = mailPatientEdit.Text = pt.email;
                    patientInfoPhoneTxt.Text = phonePatientEdit.Text = pt.phoneNumber;
                    patientInfoNotesTxt.Text = notesPatientEdit.Text = datahandler.getPatientNotes(patianteId);
                    // fill in edit info
                    firstNamePatientEdit.Text = pt.FirstName;
                    lastNamePatientEdit.Text = pt.LastName;

                }
                else {
                    clearPatientInfo();
                }

            }
            //MessageBox.Show("User is: " + patianteId);
            canStart();
        }

        private void clearPatientInfo() {
            patianteId = -1;
            patientInfoNameLbl.Text = "";
            patientInfoIDLbl.Text = "";
            patientInfoEmailTxt.Text = "";
            patientInfoPhoneTxt.Text = "";
            patientInfoNotesTxt.Text = "";
        }

        private void canStart() {
            if (patianteId > -1) {
                startBtn.Enabled = true;
                // startBtn.BackColor = Color.FromArgb(55, 201, 31);
                startBtn.BackColor = System.Drawing.ColorTranslator.FromHtml("#FFC91F");
            }
            else {
                startBtn.Enabled = false;
                startBtn.BackColor = Color.FromArgb(196, 196, 196);
            }
        }

        private bool is_numericL(string str) {
            long nl = 0;
            return long.TryParse(str, out nl);
        }

        private void homePage_Click(object sender, EventArgs e) {
            var uinfoList = new List<string>();

            //uinfoList = dbh.getUserInformation(userId);
            if (uinfoList.Count > 0) {
                //uNameTxt.Text = "Hello, " + uinfoList[2];
                DateTime localDate = DateTime.Now;
                //dateTimelbl.Text = localDate.ToString();
            }
        }


        private void signOutBtn() {
            userId = 0;
            UserNameTxtBox.Text = "";
            //PasswordTextBox.Text = "";
            MainTabControl.SelectTab(0);
        }

        private void button1_Click_1(object sender, EventArgs e) {
            signOutBtn();
        }

        private void exitBtn_Click(object sender, EventArgs e) {
            signOutBtn();
        }


        private void button3_Click(object sender, EventArgs e) {
            MainTabControl.SelectTab(1);
        }

        private void button20_Click(object sender, EventArgs e) {
            signOutBtn();
        }


        private void LeftToRightBtn_Click(object sender, EventArgs e) {
            exercisesFlowPanel.FlowDirection = FlowDirection.LeftToRight;
        }

        private void topDownBtn_Click(object sender, EventArgs e) {
            exercisesFlowPanel.FlowDirection = FlowDirection.TopDown;
        }

        bool isPracticeAppRunning = false;


        /// <summary>
        /// 
        /// </summary>
        /// <param name="appExe"></param>
        /// <param name="args"></param>
        private void runExeApp(string appExe, string args) {
            string baseRunningDirectory = string.Empty;

            string sysPath = datahandler.GetSystemPath();
            if (sysPath != string.Empty) {
                sysPath = sysPath.Replace("\\", "\\\\");
                baseRunningDirectory = sysPath;
            }
            else {
                baseRunningDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
                baseRunningDirectory += "\\Selfit-kinect-2018";// SelfitPilot
            }

            // set current directory to run the unity system
            string currPath = Directory.GetCurrentDirectory();
            Directory.SetCurrentDirectory(baseRunningDirectory);

            baseRunningDirectory += "\\SelfitPilot.exe";

            //Process practiceApp = new Process();
            //practiceApp.StartInfo.FileName = baseRunningDirectory + appExe;
            appExe = baseRunningDirectory;

            //System.Diagnostics.Debug.WriteLine(appExe);
            //MessageBox.Show("App EXE: " + appExe + " Args = " + args);
            //practiceApp.StartInfo.Arguments = "mytextfile.txt";
            Process practiceApp = new Process();
            practiceApp.EnableRaisingEvents = true;
            practiceApp.Exited += new EventHandler(practiceApp_Exited);
            practiceApp.StartInfo.FileName = appExe;
            practiceApp.StartInfo.Arguments = args;
            practiceApp.Start();
            //System.Threading.Thread.Sleep(2000);

            // restore last directory it was
            Directory.SetCurrentDirectory(currPath);
        }

        private void practiceApp_Exited(object sender, System.EventArgs e) {
            //MessageBox.Show("isPracticeAppRunning cleared");
            isPracticeAppRunning = false;
        }

        private void button8_Click(object sender, EventArgs e) {
            //if (pNameLbl.Text != "")
            //{
            //   needSaveDifficultyChanges();
            //   tabControl1.SelectTab(5);
            //}
        }

        private void button33_Click(object sender, EventArgs e) {
            MainTabControl.SelectTab(1);
        }


        private void LeftToRightBtn_Click_1(object sender, EventArgs e) {
            exercisesFlowPanel.FlowDirection = FlowDirection.LeftToRight;
        }

        private void topDownBtn_Click_1(object sender, EventArgs e) {
            exercisesFlowPanel.FlowDirection = FlowDirection.TopDown;
        }


        private void button35_Click(object sender, EventArgs e) {
            signOutBtn();
        }

        private void button13_Click(object sender, EventArgs e) {
            MainTabControl.SelectTab(6);
        }

        private void button45_Click(object sender, EventArgs e) {
            signOutBtn();
        }

        private void button46_Click(object sender, EventArgs e) {
            MainTabControl.SelectTab(1);
        }

        private void useHebSpeachCBX_CheckedChanged(object sender, EventArgs e) {
            bool status = false;
            if (useHebSpeachCBX.Checked == true) {
                status = true;
            }
            datahandler.SetSpeachState(status);
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e) {
            bool status = false;
            if (checkBox1.Checked == true) {
                status = true;
            }
            //dbh.setRateState(status);
        }

        private void button9_Click_1(object sender, EventArgs e) {
            MainTabControl.SelectTab(1);
        }

        private void button51_Click(object sender, EventArgs e) {
            MainTabControl.SelectTab(3);
        }

        private void button53_Click(object sender, EventArgs e) {
            signOutBtn();
        }



        /// <summary>
        /// Handle the list view export to CSV file
        /// </summary>
        /// <param name="listView"></param>
        /// <param name="filePath"></param>
        /// <param name="includeHidden"></param>
        public static void ListViewToCSV(ListView listView, string filePath, bool includeHidden, string hName) {
            //make header string
            StringBuilder result = new StringBuilder();

            // Write practice name in the first line            
            result.Append(String.Format("\"Practice Name:\",\"{0}\"", hName));
            result.AppendLine();

            // Write the data
            WriteCSVRow(result, listView.Columns.Count, i => includeHidden || listView.Columns[i].Width > 0, i => listView.Columns[i].Text);

            //export data rows
            foreach (ListViewItem listItem in listView.Items)
                WriteCSVRow(result, listView.Columns.Count, i => includeHidden || listView.Columns[i].Width > 0, i => listItem.SubItems[i].Text);

            File.WriteAllText(filePath, result.ToString());
        }

        private static void WriteCSVRow(StringBuilder result, int itemsCount, Func<int, bool> isColumnNeeded, Func<int, string> columnValue) {
            bool isFirstTime = true;
            for (int i = 0; i < itemsCount; i++) {
                if (!isColumnNeeded(i))
                    continue;

                if (!isFirstTime)
                    result.Append(",");
                isFirstTime = false;

                result.Append(String.Format("\"{0}\"", columnValue(i)));
            }
            result.AppendLine();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        private void SignOutNbtn_Click(object sender, EventArgs e) {
            close_keyboard();
            loadLoginPage();
        }

        private void loadLoginPage() {
            userId = -1;
            therapistId = -1;
            UserNameTxtBox.Text = "";
            UserNameTxtBox.Focus();
            UserPasswordTxtBox.Text = "";
            if (Properties.Settings.Default.userName != string.Empty) {
                UserNameTxtBox.Text = Properties.Settings.Default.userName;
                UserPasswordTxtBox.Focus();
            }
            MainTabControl.SelectTab("LoginPage");
        }


        private void canStart(bool cs) {
            if (cs) {
                startBtn.Enabled = true;
                // startBtn.BackColor = Color.FromArgb(55, 201, 31);
                startBtn.BackColor = System.Drawing.ColorTranslator.FromHtml("#FFC91F");
            }
            else {
                startBtn.Enabled = false;
                startBtn.BackColor = Color.FromArgb(196, 196, 196);
            }
        }

        private void startBtn_Click(object sender, EventArgs e) {
            // Get the patient name from the DB
            //int pId = -1;
            //if (int.TryParse(patientSearchTxt.Text, out pId))
            //    patianteId = pId;
            if (fastEditDataChanged == true) { // need to save the data
                //MessageBox.Show(" need to save the data");
                storePatientFastEditData();
            }

            Patient pt = pList.Find(item => item.refIDnum == patianteId);
            string initials = string.Empty;
            if (pt.gender == 1) { initials = "Mr."; }
            if (pt.gender == 2) { initials = "Ms."; }

            close_keyboard();
            patientName = initials + " " + patientInfoNameLbl.Text; //dbh.getPatientFullNameById(patianteId);
            // Go to history page
            //loadHistoryPage();
            if (Properties.Settings.Default.ParkinsonPilotMode == true) {
                pViewQplanBtn.Enabled = false;
                loadPlansPviewBtn.Enabled = false;
                historyQuickPlanBtn.Enabled = false;
                createNewPlanBtn.Enabled = false;
                createPlanBtn.Enabled = false;
                load_plans_view();
            }
            else {
                loadQuickPlan();
            }
        }

        private void storePatientFastEditData() {
            Patient pToStore = new Patient();
            pToStore.SiteID = SITEID;
            pToStore.ID = long.Parse(IdPatientEdit.Text);
            pToStore.FirstName = firstNamePatientEdit.Text;
            pToStore.LastName = lastNamePatientEdit.Text;
            pToStore.phoneNumber = phonePatientEdit.Text;
            pToStore.email = mailPatientEdit.Text;

            // Store data at the identity server
            Task t = Task.Run(async () => { await Identity.smallUpdateUserData(pToStore, SITEID); });

            // store the notes
            datahandler.setPatientNotes(patianteId, notesPatientEdit.Text);

            // update labels
            patientInfoNameLbl.Text = pToStore.FirstName + " " + pToStore.LastName;
            var indexOf = pList.IndexOf(pList.Find(item => item.refIDnum == patianteId));
            pList[indexOf].FirstName = pToStore.FirstName;
            pList[indexOf].LastName = pToStore.LastName;
            pList[indexOf].phoneNumber = pToStore.phoneNumber;
            pList[indexOf].email = pToStore.email;
        }

        private void fillSessionPanel() {
            DataSet userSessionInfo = new DataSet();
            if (dateTimePicker1.Checked == true) {
                //MessageBox.Show("Data was changed by the user");
                userSessionInfo = datahandler.getPatientSessionInfo(patianteId, dateTimePicker1.Value);
            }
            else {
                userSessionInfo = datahandler.getPatientSessionInfo(patianteId);
            }
            SessionsFlowLayoutPanel.Controls.Clear();
            sessionDt = userSessionInfo.Tables[0];

            int panelIndex = 0;
            foreach (DataRow row in sessionDt.Rows) {
                createPlanBtn.Visible = false;
                Panel sPanel = new Panel();
                sPanel.Name = "panel" + row[2].ToString();
                sPanel.Height = 79;
                sPanel.Width = 344;
                sPanel.Margin = new Padding(3, 1, 0, 0);
                sPanel.BackColor = Color.FromArgb(238, 238, 238);

                Label sDateLbl = new Label();
                sDateLbl.Size = new Size(180, 28);
                sDateLbl.Text = row[4].ToString().Replace('/', '.');
                //sDateLbl.Location = new Point(5, 11);
                sDateLbl.ForeColor = Color.Black;
                sDateLbl.Font = new Font("Roboto", 28, GraphicsUnit.Pixel);
                sDateLbl.TextAlign = ContentAlignment.MiddleRight;
                sDateLbl.Click += sPanel_click;
                sDateLbl.Tag = panelIndex;
                sPanel.Controls.Add(sDateLbl);

                Label sPlanName = new Label();
                sPlanName.Size = new Size(200, 28);
                sPlanName.Text = row[3].ToString();
                sPlanName.ForeColor = Color.Black;
                sPlanName.Font = new Font("Roboto", 18, GraphicsUnit.Pixel);
                sPlanName.TextAlign = ContentAlignment.MiddleRight;
                sPlanName.Location = new Point(0, 45);
                sPlanName.Tag = panelIndex;
                sPlanName.Click += sPanel_click;
                sPanel.Controls.Add(sPlanName);

                Label exCountlbl = new Label();
                exCountlbl.Size = new Size(98, 32);
                exCountlbl.Text = row[5].ToString();
                exCountlbl.ForeColor = Color.Black;
                exCountlbl.Font = new Font("Roboto", 30, GraphicsUnit.Pixel);
                exCountlbl.TextAlign = ContentAlignment.MiddleRight;
                exCountlbl.Location = new Point(219, 20);
                exCountlbl.Click += sPanel_click;
                exCountlbl.Tag = panelIndex;
                sPanel.Controls.Add(exCountlbl);
                sPanel.Click += sPanel_click;
                sPanel.MouseMove += SessionsFlowLayoutPanel_MouseMove;
                sPanel.MouseDown += SessionsFlowLayoutPanel_MouseDown;
                sPanel.MouseWheel += SessionsFlowLayoutPanel_MouseWheel;
                sPanel.Tag = panelIndex;
                SessionsFlowLayoutPanel.Controls.Add(sPanel);
                panelIndex++;
            }
            if (panelIndex == 0) {
                createPlanBtn.Visible = true;
                noPlansAvailableTxt.Visible = true;
                clearExcersiesList();
            }
            else {
                // select first plan
                noPlansAvailableTxt.Visible = false;
                Panel P = (Panel)SessionsFlowLayoutPanel.Controls[0];
                P.BackColor = Color.FromArgb(255, 201, 31);
                PlanDateTxt.Text = sessionDt.Rows[0][4].ToString().Replace('/', '.');
                PlanNameTxt.Text = sessionDt.Rows[0][3].ToString();
                PlanNameTxt.Visible = true;
                updateHistoryData(int.Parse(sessionDt.Rows[0][2].ToString()));
                selectedPlan = int.Parse(sessionDt.Rows[0][6].ToString());
                updateExcersiesList(int.Parse(sessionDt.Rows[0][2].ToString()), selectedPlan);
            }
        }//

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e) {
            fillSessionPanel();
        }

        private void loadHistoryPage() {
            sessionDatainfoPanel.Visible = false;
            dateTimePicker1.Checked = false;
            dateTimePicker1.Value = dateTimePicker1.MaxDate;

            exTitleHeader.Visible = false;
            historyTotalExeTxt.Text = "";
            selectAndEditPlanBtn.Enabled = false;
            selectAndEditPlanBtn.Visible = false;
            startPlanBtn.Enabled = false;
            startPlanBtn.Visible = false;
            exerciesLayout.Controls.Clear();
            PlanDateTxt.Text = "";
            PlanNameTxt.Text = "";
            PlanNameTxt.Visible = false;
            selectedPlan = -1;
            historyPatientNameLbl.Text = patientName;
            fillSessionPanel();
            if (keyBoardOn == true) {
                load_virtual_keyboard();
            }
            MainTabControl.SelectTab("HistoryTab");
        }

        private void sPanel_click(object sender, EventArgs e) {
            int tagId = -1;
            if (sender is Label) {
                Label T = (Label)sender;
                tagId = int.Parse(T.Tag.ToString());
            }
            if (sender is Panel) {
                Panel P = (Panel)sender;
                tagId = int.Parse(P.Tag.ToString());
            }
            for (int i = 0; i < SessionsFlowLayoutPanel.Controls.Count; i++) {
                var V = SessionsFlowLayoutPanel.Controls[i];
                if (V is Panel) {
                    Panel P = (Panel)V;
                    if (int.Parse(V.Tag.ToString()) == tagId) {
                        P.BackColor = Color.FromArgb(255, 201, 31);
                        PlanDateTxt.Text = sessionDt.Rows[i][4].ToString().Replace('/', '.');
                        PlanNameTxt.Text = sessionDt.Rows[i][3].ToString();
                        PlanNameTxt.Visible = true;
                        int sessionId = int.Parse(sessionDt.Rows[i][2].ToString());
                        updateHistoryData(sessionId);
                        selectedPlan = int.Parse(sessionDt.Rows[i][6].ToString());
                        //MessageBox.Show("Session ID: " + sessionId +  " Plan ID: " + selectedPlan);
                        updateExcersiesList(sessionId, selectedPlan);
                    }
                    else {
                        P.BackColor = Color.FromArgb(238, 238, 238);
                        //selectedPlan = -1;
                        //PlanDateTxt.Text = "";
                        //PlanNameTxt.Text = "";
                    }
                }
            }
            //MessageBox.Show(tagId.ToString());
        }

        private void clearExcersiesList() {
            sessionDatainfoPanel.Visible = false;
            exerciesLayout.Controls.Clear();
            historyTotalExeTxt.Text = "";
            selectAndEditPlanBtn.Enabled = false;
            selectAndEditPlanBtn.Visible = false;
            startPlanBtn.Enabled = false;
            startPlanBtn.Visible = false;
            exTitleHeader.Visible = false;
        }

        private void updateExcersiesList(int sessionId, int planId) {
            exerciesLayout.Controls.Clear();
            DataSet pListInfo = new DataSet();
            pListInfo = datahandler.getSessionPractices(sessionId);
            DataTable pListDT = pListInfo.Tables[0];
            List<practice_log_data> pData = new List<practice_log_data>();
            int index = 0;
            int lastPID = -1;

            practice_log_data pd = new practice_log_data();
            foreach (DataRow row in pListDT.Rows) {
                int practiceId = int.Parse(row[3].ToString());
                if (practiceId != lastPID) {
                    pd.p_id = practiceId;
                    pd.p_name = row[5].ToString();
                    pd.p_reps = 1;

                    pData.Add(pd);
                    index++;
                    lastPID = practiceId;
                }
                else {
                    pd.p_reps++;
                    pData[index - 1] = pd;
                }
            }
            if (index > 0) {
                // fill in the data to the view
                int pIndex = 0;
                int reps = 0;
                foreach (practice_log_data tpD in pData) {
                    Panel pPanel = new Panel();
                    pPanel.Name = "panel" + pIndex;
                    pPanel.Height = 74; //396, 74
                    pPanel.Width = 370;
                    pPanel.BackColor = Color.White;

                    Label idxLabel = new Label();
                    idxLabel.Size = new Size(40, 29);
                    idxLabel.Text = (pIndex + 1).ToString("D2");
                    //idxLabel.Text = tpD.p_id.ToString();
                    idxLabel.ForeColor = Color.Black;
                    idxLabel.Font = new Font("Roboto", 20, GraphicsUnit.Pixel);
                    idxLabel.TextAlign = ContentAlignment.MiddleRight;
                    idxLabel.Location = new Point(8, 23);
                    pPanel.Controls.Add(idxLabel);

                    PictureBox picBox = new PictureBox();
                    picBox.Size = new Size(33, 33);
                    picBox.Location = new Point(65, 21);
                    //string iconPath = Application.StartupPath + "\\practice_images\\" + tpD.p_id + "s.png";                    
                    string iconPath = Application.StartupPath + "\\practice_images\\" + getImageName(tpD.p_id) + "s.png";
                    if (File.Exists(iconPath))
                        picBox.Image = Image.FromFile(iconPath);
                    else
                        picBox.Image = Image.FromFile(Application.StartupPath + "\\practice_images\\0s.png");
                    pPanel.Controls.Add(picBox);

                    Label exNameLbl = new Label();
                    exNameLbl.Size = new Size(174, 29);
                    exNameLbl.Text = tpD.p_name;
                    exNameLbl.ForeColor = Color.Black;
                    exNameLbl.Font = new Font("Roboto", 20, GraphicsUnit.Pixel);
                    exNameLbl.TextAlign = ContentAlignment.MiddleLeft;
                    exNameLbl.Location = new Point(115, 23);
                    pPanel.Controls.Add(exNameLbl);

                    Label repsLbl = new Label();
                    repsLbl.Size = new Size(60, 29);
                    reps += tpD.p_reps;
                    repsLbl.Text = tpD.p_reps.ToString();
                    repsLbl.ForeColor = Color.Black;
                    repsLbl.Font = new Font("Roboto", 20, GraphicsUnit.Pixel);
                    repsLbl.TextAlign = ContentAlignment.MiddleCenter;
                    repsLbl.Location = new Point(300, 23);
                    pPanel.Controls.Add(repsLbl);
                    pPanel.MouseMove += exerciesLayout_MouseMove;
                    pPanel.MouseDown += exerciesLayout_MouseDown;
                    pPanel.MouseWheel += exerciesLayout_MouseWheel;

                    exerciesLayout.Controls.Add(pPanel);
                    pIndex++;
                }
                selectAndEditPlanBtn.Enabled = true;
                selectAndEditPlanBtn.Visible = true;
                startPlanBtn.Enabled = true;
                startPlanBtn.Visible = true;
                exTitleHeader.Visible = true;
                historyTotalExeTxt.Text = "Completed " + reps + " Exercises";
            }
            else {
                exerciesLayout.Controls.Clear();
                historyTotalExeTxt.Text = "";
                selectAndEditPlanBtn.Enabled = false;
                selectAndEditPlanBtn.Visible = false;
                startPlanBtn.Enabled = false;
                startPlanBtn.Visible = false;
                exTitleHeader.Visible = false;
            }

        }


        /// <summary>
        /// handleFloatParse - set defaul 0 value and return tryparse or the default value
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        private float handleFloatParse(string data) {
            float value = 0f;
            float.TryParse(data, out value);
            return value;
        }

        private void validStepsBtn_Click(object sender, EventArgs e) {
            updateParkinsonStepsData(0, ParkinsonStepsData);
            setParkinsonStepsDataBtnState(0);
        }

        private void stdDevBtn_Click(object sender, EventArgs e) {
            updateParkinsonStepsData(1, ParkinsonStepsData);
            setParkinsonStepsDataBtnState(1);
        }

        private void allDataBtn_Click(object sender, EventArgs e) {
            updateParkinsonStepsData(2, ParkinsonStepsData);
            setParkinsonStepsDataBtnState(2);
        }

        private void setParkinsonStepsDataBtnState(int state) {
            if (state == 0) {
                validStepsBtn.Image = Properties.Resources.Vector_2_2;
                validStepsBtn.FlatAppearance.BorderColor = Color.FromArgb(255, 201, 31);
                stdDevBtn.Image = null;
                stdDevBtn.FlatAppearance.BorderColor = Color.Black;
                allDataBtn.Image = null;
                allDataBtn.FlatAppearance.BorderColor = Color.Black;
            }
            if (state == 1) {
                validStepsBtn.Image = null;
                validStepsBtn.FlatAppearance.BorderColor = Color.Black;
                stdDevBtn.Image = Properties.Resources.Vector_2_2;
                stdDevBtn.FlatAppearance.BorderColor = Color.FromArgb(255, 201, 31);
                allDataBtn.Image = null;
                allDataBtn.FlatAppearance.BorderColor = Color.Black;
            }
            if (state == 2) {
                validStepsBtn.Image = null;
                validStepsBtn.FlatAppearance.BorderColor = Color.Black;
                stdDevBtn.Image = null;
                stdDevBtn.FlatAppearance.BorderColor = Color.Black;
                allDataBtn.Image = Properties.Resources.Vector_2_2;
                allDataBtn.FlatAppearance.BorderColor = Color.FromArgb(255, 201, 31);
            }
        }

        private string toStepType(int sNum) {
            string stepType = string.Empty;
            //step_type	description
            //10  valid right step
            //11  Deviation false flag right step
            //12  non valid right step
            //20  valid left step
            //21  Deviation false flag left step
            //22  non valid left step

            switch (sNum) {
                case 10:
                    stepType = "Valid right";
                    break;
                case 20:
                    stepType = "Valid left";
                    break;
                case 11:
                    stepType = "Deviation right";
                    break;
                case 21:
                    stepType = "Deviation left";
                    break;
                case 12:
                    stepType = "Invalid right";
                    break;
                case 22:
                    stepType = "Invalid left";
                    break;
                default:
                    break;
            }
            return stepType;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sNum"></param>
        /// <returns></returns>
        public string toStageText(int sNum) {
            string stageText = string.Empty;
            if (sNum == 0)
                stageText = "Evaluation";
            if (sNum == 1)
                stageText = "Steps hint";
            if (sNum == 2)
                stageText = "Metronome";
            return stageText;
        }

        private void updateParkinsonStepsData(int mode, DataSet sData) {
            // Mode - 0 default (view only valid steps - left + right
            //        1 view STD deviation as well
            //        2 view all steps
            if (sData != null && sData.Tables[0] != null) {
                ParkinsonDataGridView.Visible = true;
                DataTable stDT = sData.Tables[0];
                ParkinsonDataGridView.Rows.Clear();
                foreach (DataRow sstDTrow in stDT.Rows) {
                    //"step_type": 10,
                    //"step_length": 0.7197971,
                    //"step_time": 1.235157,
                    //"pratice_stage": 1,
                    //"practice_pass": 2,
                    //"step_num_in_stage": 2,
                    //"stage_type": 0
                    int stepType = int.Parse(sstDTrow[0].ToString());
                    if (mode == 0) // default
                        if (stepType == 10 || stepType == 20) {
                            ParkinsonDataGridView.Rows.Add(toStepType(stepType), sstDTrow[1].ToString(), sstDTrow[2].ToString(), toStageText(int.Parse(sstDTrow[6].ToString())));
                        }
                    if (mode == 1) {
                        if (stepType == 10 || stepType == 20 || stepType == 11 || stepType == 21) {
                            ParkinsonDataGridView.Rows.Add(toStepType(stepType), sstDTrow[1].ToString(), sstDTrow[2].ToString(), toStageText(int.Parse(sstDTrow[6].ToString())));
                        }
                    }
                    if (mode == 2) {
                        ParkinsonDataGridView.Rows.Add(toStepType(stepType), sstDTrow[1].ToString(), sstDTrow[2].ToString(), toStageText(int.Parse(sstDTrow[6].ToString())));
                    }
                }
            }
            else {
                ParkinsonDataGridView.Visible = false;
            }
        }

        DataSet ParkinsonStepsData = new DataSet();

        private void updateHistoryData(int sessionId) {
            //MessageBox.Show("SessionID:" + sessionId);
            var sessionInfo = new List<string>();
            sessionInfo = datahandler.getPatientSessionData(patianteId, sessionId);

            // grab the data for the graphs
            DataSet historyInfo = new DataSet();
            historyInfo = datahandler.getPatientSessionHistory(patianteId, 5, sessionId);
            if (Properties.Settings.Default.ParkinsonPilotMode) {
                sessionDatainfoPanel.Visible = false;
                parkinsonSessionDataPanel.Visible = true;
                DataSet parkinsonSessionDS = new DataSet();
                parkinsonSessionDS = datahandler.getParkinsonSessionData(patianteId, sessionId);
                ParkinsonSessionDataLayoutPanel.Controls.Clear();
                ParkinsontabControl.SelectTab("summaryDataTab");

                DataTable psDT = parkinsonSessionDS.Tables[0];
                foreach (DataRow psRow in psDT.Rows) {
                    Panel stagePanel = new Panel();
                    stagePanel.Size = new Size(956, 100);
                    stagePanel.Location = new Point(0, 0);
                    stagePanel.BackColor = Color.FromArgb(238, 238, 238);
                    stagePanel.BorderStyle = BorderStyle.FixedSingle;
                    stagePanel.Margin = new Padding(0, 0, 0, 0);

                    Label pStage = new Label();
                    pStage.Size = new Size(150, 36);
                    pStage.AutoSize = false;
                    pStage.Location = new Point(9, 25);
                    pStage.Font = new Font("Roboto", 24, GraphicsUnit.Pixel);
                    pStage.ForeColor = Color.Black;
                    pStage.TextAlign = ContentAlignment.MiddleLeft;
                    int stageNum = int.Parse(psRow[1].ToString());
                    pStage.Text = toStageText(stageNum);
                    stagePanel.Controls.Add(pStage);

                    Label sCount = new Label();
                    sCount.Size = new Size(120, 36);
                    sCount.AutoSize = false;
                    sCount.Location = new Point(162, 25);
                    sCount.Font = new Font("Roboto", 24, GraphicsUnit.Pixel);
                    sCount.ForeColor = Color.Black;
                    sCount.TextAlign = ContentAlignment.MiddleCenter;
                    sCount.Text = psRow[2].ToString();
                    stagePanel.Controls.Add(sCount);

                    Label avgStep = new Label();
                    avgStep.Size = new Size(120, 36);
                    avgStep.AutoSize = false;
                    avgStep.Location = new Point(325, 25);
                    avgStep.Font = new Font("Roboto", 24, GraphicsUnit.Pixel);
                    avgStep.ForeColor = Color.Black;
                    avgStep.TextAlign = ContentAlignment.MiddleCenter;
                    avgStep.Text = psRow[3].ToString();
                    stagePanel.Controls.Add(avgStep);

                    Label stdDev = new Label();
                    stdDev.Size = new Size(120, 36);
                    stdDev.AutoSize = false;
                    stdDev.Location = new Point(540, 25);
                    stdDev.Font = new Font("Roboto", 24, GraphicsUnit.Pixel);
                    stdDev.ForeColor = Color.Black;
                    stdDev.TextAlign = ContentAlignment.MiddleCenter;
                    stdDev.Text = psRow[4].ToString();
                    stagePanel.Controls.Add(stdDev);

                    Label sTime = new Label();
                    sTime.Size = new Size(120, 36);
                    sTime.AutoSize = false;
                    sTime.Location = new Point(740, 25);
                    sTime.Font = new Font("Roboto", 24, GraphicsUnit.Pixel);
                    sTime.ForeColor = Color.Black;
                    sTime.TextAlign = ContentAlignment.MiddleCenter;
                    sTime.Text = psRow[5].ToString();
                    stagePanel.Controls.Add(sTime);

                    ParkinsonSessionDataLayoutPanel.Controls.Add(stagePanel);
                }

                ParkinsonStepsData = datahandler.getParkinsonStepsData(patianteId, sessionId);
                updateParkinsonStepsData(0, ParkinsonStepsData);
                setParkinsonStepsDataBtnState(0);

            }
            else {
                parkinsonSessionDataPanel.Visible = false;
                if (sessionInfo.Count > 0) {
                    sessionDatainfoPanel.Visible = true;
                    // Fill-in the data into the control...
                    TimeSpan time = TimeSpan.FromSeconds(handleFloatParse(sessionInfo[6]));
                    histTimeDataLbl.Text = time.ToString(@"hh\:mm\:ss") + " Sec";
                    histDistanceDataLbl.Text = string.Format("{0:F2} M", handleFloatParse(sessionInfo[4]));
                    histStepCountDataLbl.Text = sessionInfo[5] + " Steps";
                    histSpeedDataLbl.Text = string.Format("{0:F2} M/S", handleFloatParse(sessionInfo[7]));
                    histHRightDataLbl.Text = string.Format("{0:F2} CM", handleFloatParse(sessionInfo[8]));
                    histHLeftDataLbl.Text = string.Format("{0:F2} CM", handleFloatParse(sessionInfo[9]));
                    time = TimeSpan.FromSeconds(handleFloatParse(sessionInfo[12]));
                    histMoveTimeDataLbl.Text = time.ToString(@"hh\:mm\:ss") + " Sec";
                    // handle graph data fill
                    clearChartData();
                    DataTable historyDT = historyInfo.Tables[0];

                    //histTimeRateLbl
                    float histTimeRateLast = 0;
                    //histDistanceRateLbl
                    float histDistanceRateLast = 0;
                    //histStepCountRateLbl
                    float histStepCountRateLast = 0;
                    //histSpeedRateLbl
                    float histSpeedRateLast = 0;
                    //histHRightRateLbl
                    float histHRightRateLast = 0;
                    //histHLeftRateLbl
                    float histHLeftRateLast = 0;
                    //histMoveTimeRateLbl
                    float histMoveTimeRateLast = 0;
                    int index = 0;
                    foreach (DataRow hRow in historyDT.Rows) {
                        float value = 0f;
                        if (float.TryParse(hRow[6].ToString(), out value)) {
                            //if (histTimeRateLast != 0) {
                            //    float val = ((value / histTimeRateLast) - 1) * 100;
                            //    if (val >= 0) {
                            //        histTimeRateLbl.ForeColor = Color.FromArgb(83, 167, 0);
                            //    }
                            //    else {
                            //        histTimeRateLbl.ForeColor = Color.DarkRed;
                            //    }
                            //    histTimeRateLbl.Text = string.Format("{0:F0}%", val);                            
                            //}
                            setRateDiffLabelValue(histTimeRateLbl, value, histTimeRateLast);
                            histTimeRateLast = value;
                            timeChart.Series[0].Points.Add(value, index);
                        }
                        if (float.TryParse(hRow[4].ToString(), out value)) {
                            setRateDiffLabelValue(histDistanceRateLbl, value, histDistanceRateLast);
                            histDistanceRateLast = value;
                            DistanceChart.Series[0].Points.Add(value, index);
                        }
                        if (float.TryParse(hRow[5].ToString(), out value)) {
                            setRateDiffLabelValue(histStepCountRateLbl, value, histStepCountRateLast);
                            histStepCountRateLast = value;
                            stepNumChart.Series[0].Points.Add(value, index);
                        }
                        if (float.TryParse(hRow[7].ToString(), out value)) {
                            setRateDiffLabelValue(histSpeedRateLbl, histSpeedRateLast, value);
                            histSpeedRateLast = value;
                            speedChart.Series[0].Points.Add(value, index);
                        }
                        if (float.TryParse(hRow[8].ToString(), out value)) {
                            setRateDiffLabelValue(histHRightRateLbl, histHRightRateLast, value);
                            histHRightRateLast = value;
                            stepHRchart.Series[0].Points.Add(value, index);
                        }
                        if (float.TryParse(hRow[9].ToString(), out value)) {
                            setRateDiffLabelValue(histHLeftRateLbl, histHLeftRateLast, value);
                            histHLeftRateLast = value;
                            stepHLchart.Series[0].Points.Add(value, index);
                        }
                        if (float.TryParse(hRow[12].ToString(), out value)) {
                            setRateDiffLabelValue(histMoveTimeRateLbl, histMoveTimeRateLast, value);
                            histMoveTimeRateLast = value;
                            moveTimeChart.Series[0].Points.Add(value, index);
                        }
                        index++;
                    }
                }
                else {
                    sessionDatainfoPanel.Visible = false;
                }
            }
        }

        private void setRateDiffLabelValue(Label lbl, float currVal, float lastVal) {
            lbl.ForeColor = Color.FromArgb(83, 167, 0);
            float val = 0.0f;
            if (lastVal != 0) {
                val = ((currVal / lastVal) - 1) * 100;
                if (val < 0)
                    lbl.ForeColor = Color.DarkRed;
            }
            lbl.Text = string.Format("{0:F0}%", val);
        }

        private void clearChartData() {
            timeChart.Series[0].Points.Clear();
            DistanceChart.Series[0].Points.Clear();
            stepNumChart.Series[0].Points.Clear();
            speedChart.Series[0].Points.Clear();
            stepHRchart.Series[0].Points.Clear();
            stepHLchart.Series[0].Points.Clear(); moveTimeChart.Series[0].Points.Clear();
        }

        private void historyExitBtn_Click(object sender, EventArgs e) {
            loadHomePage();
        }

        private void createNewPlanBtn_Click(object sender, EventArgs e) {
            loadPlans(-1);
        }


        private void startPlanBtn_Click(object sender, EventArgs e) {
            start_plan(selectedPlan);
        }

        private void selectAndEditPlanBtn_Click(object sender, EventArgs e) {
            // Need to pass plan #
            if (selectedPlan > -1) {
                loadPlans(selectedPlan);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="planId"></param>
        private void loadPlans(int planId) {
            // if planId = -1 it's a plan from scratch, else it's plan from existing with new id
            // get all practices
            if (planId == -1) {
                selectedPlan = -1;
                exercisesFlowPanel.Controls.Clear();
                //planViewTotalexeLbl.Text = "0 Exercises | 0 Reps.";
                planViewSelectEditBtn.Enabled = false;
                planViewStartPlanBtn.Enabled = false;
                plan_practices.Clear();
                updateExecisesCounterText();
                exercisesFlowPanel.Height = 2;
                addExPicPbx.Visible = true;
            }
            else {
                addExPicPbx.Visible = false;
                //1. get the plan details from DB
                //2. fill the plan_practices with the data
                //3. update the UI
                plan_practices.Clear();
                DataSet planData = new DataSet();
                planData = datahandler.getPlanData(planId);
                DataTable planDataTable = planData.Tables[0];
                foreach (DataRow pRow in planDataTable.Rows) {
                    practice_data pData = new practice_data();
                    pData.practice_id = int.Parse(pRow["practice_id"].ToString());
                    pData.practice_name = pRow["practice_name"].ToString();
                    pData.practice_unityId = int.Parse(pRow["practice_unity_id"].ToString());
                    pData.practice_reps = int.Parse(pRow["reps"].ToString());
                    plan_practices.Add(pData);
                }
                updateExercisesFlowPanel();

            }
            // Set the pateint name label
            planPatientNameLbl.Text = patientName;
            // Set the execrsise name label
            //1. get new plan# from plan_info
            //2. Give it a temporary name contains the #
            //3. plans.plan_number will be added new # and connect to db on save only
            int pNum = 1 + datahandler.getPatientPlanCount(patianteId);

            DateTime dateTime = DateTime.Now;
            planNameLbl.Text = "Plan #" + pNum + ", " + dateTime.ToString("dd.MM.yyyy");

            update_filters();
            updatePracticePanel();
            update_practicePanel_states();
            MainTabControl.SelectTab("PlanPage");
        }

        private void update_filters() {
            filterFlowLayoutPanel.Controls.Clear();
            int idf = 0;
            // Add header - standing
            Label standingLbl = new Label();
            standingLbl.Size = new Size(280, 55);
            standingLbl.Padding = new Padding(0, 0, 0, 0);
            standingLbl.Margin = new Padding(0, 0, 0, 2);
            standingLbl.BackColor = Color.FromArgb(211, 209, 212);
            standingLbl.Text = "   Standing";
            standingLbl.TextAlign = ContentAlignment.MiddleLeft;
            standingLbl.Font = new Font("Roboto", 20, GraphicsUnit.Pixel);
            standingLbl.ForeColor = Color.Black;
            standingLbl.BorderStyle = BorderStyle.None;
            filterFlowLayoutPanel.Controls.Add(standingLbl);

            foreach (selection_filter sf in standing_list) {
                Label filterLbl = new Label();
                filterLbl.Name = sf.filter_name.Replace(" ", "_");
                filterLbl.Size = new Size(317, 50);
                if (sf.active == false) {
                    filterLbl.BackColor = Color.FromArgb(238, 238, 238);
                }
                else {
                    filterLbl.BackColor = Color.FromArgb(255, 201, 31);
                }
                filterLbl.TextAlign = ContentAlignment.MiddleLeft;
                filterLbl.Text = "      " + sf.filter_name;
                filterLbl.Padding = new Padding(0, 0, 0, 0);
                filterLbl.Margin = new Padding(0, 0, 0, 2);
                filterLbl.Font = new Font("Roboto", 18, GraphicsUnit.Pixel);
                filterLbl.ForeColor = Color.Black;
                filterLbl.BorderStyle = BorderStyle.None;
                filterLbl.Tag = idf;
                filterLbl.Click += filterStanding_lblClick;
                filterLbl.MouseMove += filterFlowLayoutPanel_MouseMove;
                filterLbl.MouseDown += filterFlowLayoutPanel_MouseDown;
                filterLbl.MouseWheel += filterFlowLayoutPanel_MouseWheel;
                filterFlowLayoutPanel.Controls.Add(filterLbl);
                idf++;
            }

            idf = 0;
            Label sittingLbl = new Label();
            sittingLbl.Size = new Size(317, 55);
            sittingLbl.BackColor = Color.FromArgb(211, 209, 212);
            sittingLbl.Text = "   Sitting";
            sittingLbl.TextAlign = ContentAlignment.MiddleLeft;
            sittingLbl.Margin = new Padding(0, 0, 0, 2);
            sittingLbl.Font = new Font("Roboto", 20, GraphicsUnit.Pixel);
            sittingLbl.ForeColor = Color.Black;
            sittingLbl.BorderStyle = BorderStyle.None;
            filterFlowLayoutPanel.Controls.Add(sittingLbl);

            foreach (selection_filter sf in sitting_list) {
                Label filterLbl = new Label();
                filterLbl.Name = sf.filter_name.Replace(" ", "_");
                filterLbl.Size = new Size(280, 50);
                if (sf.active == false) {
                    filterLbl.BackColor = Color.FromArgb(238, 238, 238);
                }
                else {
                    filterLbl.BackColor = Color.FromArgb(255, 201, 31);
                }
                filterLbl.TextAlign = ContentAlignment.MiddleLeft;
                filterLbl.Text = "      " + sf.filter_name;
                filterLbl.Margin = new Padding(0, 0, 0, 2);
                filterLbl.Font = new Font("Roboto", 18, GraphicsUnit.Pixel);
                filterLbl.ForeColor = Color.Black;
                filterLbl.BorderStyle = BorderStyle.None;
                filterLbl.Tag = idf;
                filterLbl.Click += filterSitting_lblClick;
                filterLbl.MouseMove += filterFlowLayoutPanel_MouseMove;
                filterLbl.MouseDown += filterFlowLayoutPanel_MouseDown;
                filterLbl.MouseWheel += filterFlowLayoutPanel_MouseWheel;
                filterFlowLayoutPanel.Controls.Add(filterLbl);
                idf++;
            }
        }

        private void FilterLbl_MouseWheel(object sender, MouseEventArgs e) {
            throw new NotImplementedException();
        }

        private void filterStanding_lblClick(object sender, EventArgs e) {
            Label pnl = sender as Label;
            int pId = int.Parse(pnl.Tag.ToString());
            //MessageBox.Show(pId.ToString());
            if (standing_list[pId].active == false) {
                if (standing_list[pId].filter_name != "All") {
                    standing_list[pId] = new selection_filter() { filter_name = standing_list[pId].filter_name, active = true };
                    active_standing.Add(standing_list[pId].filter_name.Replace(" ", "_"));
                }
                else {
                    // reset the list
                    for (int i = 0; i < standing_list.Count; i++) {
                        standing_list[i] = new selection_filter() { filter_name = standing_list[i].filter_name, active = false };
                    }
                    active_standing.Clear();
                }
            }
            else {
                standing_list[pId] = new selection_filter() { filter_name = standing_list[pId].filter_name, active = false };
                active_standing.Remove(standing_list[pId].filter_name.Replace(" ", "_"));
            }
            update_filters();
            updatePracticePanel();
            update_practicePanel_states();
        }

        private void filterSitting_lblClick(object sender, EventArgs e) {
            Label pnl = sender as Label;
            int pId = int.Parse(pnl.Tag.ToString());
            if (sitting_list[pId].active == false) {
                if (sitting_list[pId].filter_name != "All") {
                    sitting_list[pId] = new selection_filter() { filter_name = sitting_list[pId].filter_name, active = true };
                    active_sitting.Add(sitting_list[pId].filter_name.Replace(" ", "_"));
                }
                else {
                    // reset the list
                    for (int i = 0; i < sitting_list.Count; i++) {
                        sitting_list[i] = new selection_filter() { filter_name = sitting_list[i].filter_name, active = false };
                    }
                    active_sitting.Clear();
                }
            }
            else {
                sitting_list[pId] = new selection_filter() { filter_name = sitting_list[pId].filter_name, active = false };
                active_sitting.Remove(sitting_list[pId].filter_name.Replace(" ", "_"));
            }
            update_filters();
            updatePracticePanel();
            update_practicePanel_states();
        }

        private void updatePracticePanel() {
            active_practices.Clear();
            //
            //DataSet allPractics = dbh.getFilterActivePractices();

            if (active_standing.Count == 0 && active_sitting.Count == 0) {
                DataSet allPractics = datahandler.getActivePractices();
                DataTable ap = allPractics.Tables[0];
                foreach (DataRow prow in ap.Rows) {
                    practice_data pd = new practice_data();
                    pd.practice_id = int.Parse(prow[0].ToString());
                    pd.practice_name = prow[1].ToString();
                    pd.practice_unityId = int.Parse(prow[2].ToString());
                    pd.practice_reps = 1;
                    pd.is_visible = true;
                    active_practices.Add(pd);
                }
            }
            if (active_standing.Count > 0) {
                DataSet allPractics = datahandler.getFilterActivePractices(active_standing, 1);
                DataTable ap = allPractics.Tables[0];
                foreach (DataRow prow in ap.Rows) {
                    practice_data pd = new practice_data();
                    pd.practice_id = int.Parse(prow[0].ToString());
                    pd.practice_name = prow[1].ToString();
                    pd.practice_unityId = int.Parse(prow[0].ToString());
                    pd.practice_reps = 1;
                    pd.is_visible = true;
                    active_practices.Add(pd);
                }
            }

            if (active_sitting.Count > 0) {
                DataSet allPractics = datahandler.getFilterActivePractices(active_sitting, 2);
                DataTable ap = allPractics.Tables[0];
                foreach (DataRow prow in ap.Rows) {
                    practice_data pd = new practice_data();
                    pd.practice_id = int.Parse(prow[0].ToString());
                    pd.practice_name = prow[1].ToString();
                    pd.practice_unityId = int.Parse(prow[0].ToString());
                    pd.practice_reps = 1;
                    pd.is_visible = true;
                    active_practices.Add(pd);
                }
            }
            practicesLayoutPanel.Controls.Clear();
            int idx = 0;
            foreach (practice_data pd in active_practices) {
                Panel practicePanel = new Panel();
                practicePanel.Name = "pPanel" + idx;
                practicePanel.Height = 276; //276, 276
                practicePanel.Width = 276;
                practicePanel.BackColor = Color.White;
                //practicePanel.BackgroundImage = Properties.Resources.inAndOutBtn;
                string iconPath = Application.StartupPath + "\\practice_images\\" + getImageName(pd.practice_id) + ".png";
                if (!File.Exists(iconPath))
                    iconPath = Application.StartupPath + "\\practice_images\\0.png"; // default image
                practicePanel.BackgroundImage = Image.FromFile(iconPath);
                practicePanel.BackgroundImageLayout = ImageLayout.Center;
                practicePanel.Tag = idx;
                practicePanel.BorderStyle = BorderStyle.FixedSingle;
                practicePanel.DoubleClick += onPracticePanel_click;

                Label practiceNameLbl = new Label();
                practiceNameLbl.Size = new Size(250, 31);
                practiceNameLbl.Text = pd.practice_name;// + " " + pd.practice_id;
                practiceNameLbl.ForeColor = Color.Black;
                practiceNameLbl.Font = new Font("Roboto", 20, GraphicsUnit.Pixel);
                practiceNameLbl.TextAlign = ContentAlignment.MiddleLeft;
                practiceNameLbl.Location = new Point(14, 5);
                practicePanel.Controls.Add(practiceNameLbl);

                Button practiceBtn = new Button();
                practiceBtn.Name = "practiceBtn";
                practiceBtn.Size = new Size(76, 76);
                practiceBtn.Location = new Point(196, 196);
                practiceBtn.FlatStyle = FlatStyle.Flat;
                practiceBtn.BackgroundImage = Properties.Resources.planAddBtn;
                practiceBtn.BackgroundImageLayout = ImageLayout.Center;
                practiceBtn.FlatAppearance.BorderSize = 0;
                practiceBtn.BackColor = Color.Transparent;
                practiceBtn.Tag = idx;
                practiceBtn.Click += practiceBtnPlanBtn_Click;

                practicePanel.MouseWheel += practicesLayoutPanel_MouseWheel;
                practicePanel.MouseDown += practicesLayoutPanel_MouseDown;
                practicePanel.MouseMove += practicesLayoutPanel_MouseMove;
                practicePanel.Controls.Add(practiceBtn);

                practicesLayoutPanel.Controls.Add(practicePanel);
                idx++;
            }
        }

        private void onPracticePanel_click(object sender, EventArgs e) {
            Panel pnl = sender as Panel;
            int pId = int.Parse(pnl.Tag.ToString());
            ViewExercise viewForm = new ViewExercise(active_practices[pId].practice_id, this, pId);
            viewForm.StartPosition = FormStartPosition.CenterScreen;
            viewForm.Show();
        }

        private void createPlanBtn_Click(object sender, EventArgs e) {
            loadPlans(-1);
        }

        private void button63_Click(object sender, EventArgs e) {

        }

        private void practiceBtnPlanBtn_Click(object sender, EventArgs e) {
            Button btn = sender as Button;
            int pId = int.Parse(btn.Tag.ToString());
            //MessageBox.Show("Practice ID: " + pId);
            addPracticeToPlan(pId);
        }

        public void addPracticeToPlan(int pId) {
            plan_practices.Add(active_practices[pId]);
            addExercisesToPlan(active_practices[pId]);
            updateExecisesCounterText();
            //btn.Enabled = false;
            set_practicePanelState(false, pId);
        }

        private void update_practicePanel_states() {
            int idx = 0;
            foreach (practice_data pd in active_practices) {
                if (plan_practices.Exists(x => x.practice_id == pd.practice_id)) {
                    //MessageBox.Show("Update panel: " + idx);
                    set_practicePanelState(false, idx);
                }
                else
                    set_practicePanelState(true, idx);
                idx++;
            }
        }

        private void set_practicePanelState(bool state, int idx) {
            foreach (Control obj in practicesLayoutPanel.Controls) {
                Panel pn = obj as Panel;
                int panelId = int.Parse(pn.Tag.ToString());
                if (panelId == idx) {
                    //MessageBox.Show("Panel ID: " + panelId + " btn: " + idx);
                    pn.Enabled = state;
                    string iconPath = Application.StartupPath + "\\practice_images\\" + getImageName(active_practices[idx].practice_id);
                    if (state)
                        iconPath += ".png";
                    else
                        iconPath += "d.png";
                    if (!File.Exists(iconPath))
                        iconPath = Application.StartupPath + "\\practice_images\\0.png";
                    pn.BackgroundImage = Image.FromFile(iconPath);
                    foreach (Button btn in pn.Controls.OfType<Button>()) {
                        if (btn != null && btn.Name == "practiceBtn") {
                            btn.Enabled = state;
                            if (!state)
                                btn.BackgroundImage = Properties.Resources.btn_add_disabled;
                            else
                                btn.BackgroundImage = Properties.Resources.planAddBtn;
                        }
                    }
                }
            }
        }

        private void addExercisesToPlan(practice_data pd) {
            int idx = exercisesFlowPanel.Controls.Count;
            Panel exePanel = new Panel();
            exePanel.Name = "exePanel" + idx;
            exePanel.Size = new Size(554, 95);
            exePanel.BackColor = Color.White;
            exePanel.BorderStyle = BorderStyle.None;
            exePanel.Margin = new Padding(0, 2, 0, 0);

            Label exeNumLbl = new Label();
            exeNumLbl.Size = new Size(36, 23);
            exeNumLbl.Location = new Point(65, 38);
            exeNumLbl.ForeColor = Color.Black;
            exeNumLbl.TextAlign = ContentAlignment.MiddleLeft;
            exeNumLbl.Font = new Font("Roboto", 20, GraphicsUnit.Pixel);
            exeNumLbl.Text = (idx + 1).ToString("D2");
            exePanel.Controls.Add(exeNumLbl);

            PictureBox practiceImage = new PictureBox();
            practiceImage.Size = new Size(40, 40);
            practiceImage.Location = new Point(114, 29);
            string iconPath = Application.StartupPath + "\\practice_images\\" + getImageName(pd.practice_id) + "s.png";
            if (File.Exists(iconPath))
                practiceImage.Image = Image.FromFile(iconPath);
            else
                practiceImage.Image = Image.FromFile(Application.StartupPath + "\\practice_images\\0s.png"); // default image
            exePanel.Controls.Add(practiceImage);

            Label practiceNameLbl = new Label();
            practiceNameLbl.Size = new Size(181, 30);
            practiceNameLbl.Text = pd.practice_name;
            practiceNameLbl.ForeColor = Color.Black;
            practiceNameLbl.Font = new Font("Roboto", 20, GraphicsUnit.Pixel);
            practiceNameLbl.TextAlign = ContentAlignment.MiddleLeft;
            practiceNameLbl.Location = new Point(168, 34);
            exePanel.Controls.Add(practiceNameLbl);

            ComboBox exeCombo = new ComboBox();
            exeCombo.Size = new Size(65, 45);
            exeCombo.Location = new Point(380, 27);
            exeCombo.ForeColor = Color.Black;
            exeCombo.DropDownWidth = 75;
            exeCombo.Font = new Font("Roboto", 30, GraphicsUnit.Pixel);
            exeCombo.DropDownStyle = ComboBoxStyle.DropDownList;
            exeCombo.FlatStyle = FlatStyle.Flat;
            exeCombo.Tag = idx;
            for (int i = 1; i < 11; i++) {
                exeCombo.Items.Add(i);
            }
            exeCombo.Text = pd.practice_reps.ToString();
            exeCombo.TextChanged += exeCombo_TextChanged;
            exePanel.Controls.Add(exeCombo);

            Button moveUpBtn = new Button();
            moveUpBtn.Size = new Size(48, 38);
            moveUpBtn.Location = new Point(3, 3);
            moveUpBtn.FlatStyle = FlatStyle.Flat;
            moveUpBtn.FlatAppearance.BorderSize = 0;
            moveUpBtn.ImageAlign = ContentAlignment.BottomCenter;
            moveUpBtn.Text = "";
            moveUpBtn.Image = Properties.Resources.UP_ICNB;
            moveUpBtn.Tag = idx;
            moveUpBtn.Click += onMoveUpBtn_pressed;
            exePanel.Controls.Add(moveUpBtn);

            Button moveDnBtn = new Button();
            moveDnBtn.Size = new Size(48, 38);
            moveDnBtn.Location = new Point(3, 54);
            moveDnBtn.FlatStyle = FlatStyle.Flat;
            moveDnBtn.FlatAppearance.BorderSize = 0;
            moveDnBtn.ImageAlign = ContentAlignment.TopCenter;
            moveDnBtn.Text = "";
            moveDnBtn.Image = Properties.Resources.DN_ICNB;
            moveDnBtn.Tag = idx;
            moveDnBtn.Click += onMoveDnBtn_pressed;
            exePanel.Controls.Add(moveDnBtn);

            Button exeDelBtn = new Button();
            exeDelBtn.Size = new Size(48, 76);
            exeDelBtn.Location = new Point(471, 11);
            exeDelBtn.FlatStyle = FlatStyle.Flat;
            exeDelBtn.FlatAppearance.BorderSize = 0;
            exeDelBtn.BackgroundImageLayout = ImageLayout.Center;
            exeDelBtn.Text = "";
            exeDelBtn.BackgroundImage = Properties.Resources.X_ICNB;
            exeDelBtn.Tag = idx;
            exeDelBtn.Click += onExeDelBtn_pressed;
            exePanel.Controls.Add(exeDelBtn);
            exePanel.MouseMove += exercisesFlowPanel_MouseMove;
            exePanel.MouseDown += exercisesFlowPanel_MouseDown;
            exePanel.MouseWheel += exercisesFlowPanel_MouseWheel;

            exercisesFlowPanel.Controls.Add(exePanel);
            //exercisesFlowPanel.MouseDown += new System.EventHandler(exercisesFlowPanel_MouseDown);

            if (exercisesFlowPanel.Height < 632)
                exercisesFlowPanel.Height += 97;
        }


        private void updateExecisesCounterText() {
            int count = plan_practices.Count;
            // Get reps
            int totReps = 0;
            foreach (practice_data pd in plan_practices) {
                totReps += pd.practice_reps;
            }
            exercisesTotalsTxt.Text = count.ToString("D2") + " Exercises | " + totReps.ToString("D2") + " Reps.";
            if (count > 0) {
                exeSaveAndStartBtn.Enabled = true;
                exeSaveAndStartBtn.ForeColor = Color.Black;
                exeSaveAndStartBtn.BackColor = Color.FromArgb(255, 201, 31);
                exeSaveForLaterBtn.Enabled = true;
                exeSaveForLaterBtn.ForeColor = Color.White;
                exeSaveForLaterBtn.BackColor = Color.Black;
                addExPicPbx.Visible = false;
            }
            else {
                exeSaveAndStartBtn.Enabled = false;
                exeSaveAndStartBtn.ForeColor = Color.Gray;
                exeSaveAndStartBtn.BackColor = Color.LightGray;
                exeSaveForLaterBtn.Enabled = false;
                exeSaveForLaterBtn.ForeColor = Color.Gray;
                exeSaveForLaterBtn.BackColor = Color.LightGray;
                addExPicPbx.Visible = true;
            }
        }

        private void onMoveUpBtn_pressed(object sender, EventArgs e) {
            Button btn = sender as Button;
            int bId = int.Parse(btn.Tag.ToString());
            if (bId > 0) { // we act only if it's not in the first position
                practice_data tmp = plan_practices[bId];
                plan_practices[bId] = plan_practices[bId - 1];
                plan_practices[bId - 1] = tmp;
                updateExercisesFlowPanel();
            }
        }

        private void onMoveDnBtn_pressed(object sender, EventArgs e) {
            Button btn = sender as Button;
            int bId = int.Parse(btn.Tag.ToString());
            if (bId < plan_practices.Count - 1) {
                practice_data tmp = plan_practices[bId];
                plan_practices[bId] = plan_practices[bId + 1];
                plan_practices[bId + 1] = tmp;
                updateExercisesFlowPanel();
            }
        }

        private void onExeDelBtn_pressed(object sender, EventArgs e) {
            Button btn = sender as Button;
            int bId = int.Parse(btn.Tag.ToString());
            plan_practices.RemoveAt(bId);
            updateExercisesFlowPanel();
            update_practicePanel_states();
        }

        private void updateExercisesFlowPanel() {
            exercisesFlowPanel.Controls.Clear();
            exercisesFlowPanel.Height = 2;

            foreach (practice_data pd in plan_practices) {
                addExercisesToPlan(pd);
            }
            updateExecisesCounterText();
        }

        private void exeCombo_TextChanged(object sender, EventArgs e) {
            ComboBox cbx = sender as ComboBox;
            int pId = int.Parse(cbx.Tag.ToString());
            practice_data tmp = plan_practices[pId];
            int reps = tmp.practice_reps;
            if (cbx.Text != null && cbx.Text != "" && cbx.Text.All(char.IsDigit)) {
                reps = int.Parse(cbx.Text);
                tmp.practice_reps = reps;
                plan_practices[pId] = tmp;
                updateExecisesCounterText();
            }
            else {
                cbx.Text = reps.ToString();
            }
        }

        private void exeSaveAndStartBtn_Click(object sender, EventArgs e) {
            //int pos = 0;
            //string txt = string.Empty;
            //foreach (practice_data pd in plan_practices) {
            //    txt += "#:" + pos + " pId: " + pd.practice_unityId + " reps: " + pd.practice_reps +"\n";
            //    pos++;
            //}
            //MessageBox.Show(txt);

            // 1. Generate new plan ID(number) in DB
            // 2. Store plan data in plans
            // 3. Store plan name in plan_info
            //
            int plan = datahandler.storeNewPatientPlan(patianteId, planNameLbl.Text, plan_practices);
            //MessageBox.Show("Plan # " + plan + " added to DB");
            if (plan > -1) {
                selectedPlan = plan;

                // Load selfit app with this plan #
                //MessageBox.Show("Plan # " + plan);
                start_plan(selectedPlan);
            }
        }

        private void exeSaveForLaterBtn_Click(object sender, EventArgs e) {
            int plan = datahandler.storeNewPatientPlan(patianteId, planNameLbl.Text, plan_practices);
            if (plan > -1)
                load_plans_view();
        }


        private void addTherapistBtn_Click(object sender, EventArgs e) {
            AddTherapist addTherapist = new AddTherapist();
            addTherapist.FormClosing += new FormClosingEventHandler(childFormClosing);
            addTherapist.Show();
        }

        private void childFormClosing(object sender, FormClosingEventArgs e) {
            if (Properties.Settings.Default.ParkinsonPilotMode == true) {
                load_plans_view();
            }
            else {
                loadHomePage();
            }
        }

        private void AddPatientBtn_Click(object sender, EventArgs e) {
            EditPatient addPatientForm = new EditPatient();
            addPatientForm.StartPosition = FormStartPosition.CenterScreen;
            addPatientForm.FormClosing += new FormClosingEventHandler(childFormClosing);
            addPatientForm.Show();
        }

        //pt = new Patient();
        private void historyViewEditBtn_Click(object sender, EventArgs e) {
            Patient pt = pList.Find(item => item.refIDnum == patianteId);
            EditPatient addPatientForm = new EditPatient(patianteId, pt);
            addPatientForm.StartPosition = FormStartPosition.CenterScreen;
            var result = addPatientForm.ShowDialog();
            if (result == DialogResult.OK) {
                if (pList.Remove(pt))
                    pList.Add(addPatientForm.patientData);
                historyPatientNameLbl.Text = getPatientFullName(addPatientForm.patientData);
            }
        }

        private void plansViewEditBtn_Click(object sender, EventArgs e) {
            Patient pt = pList.Find(item => item.refIDnum == patianteId);
            EditPatient addPatientForm = new EditPatient(patianteId, pt);
            addPatientForm.StartPosition = FormStartPosition.CenterScreen;
            addPatientForm.Show();
        }

        private void button35_Click_1(object sender, EventArgs e) {
            load_plans_view();
        }

        private void load_plans_view() {
            //
            planViewPatientNameLbl.Text = patientName;
            EXviewGroupBox.Visible = false;
            searchNameTxt.Text = "";
            if (Properties.Settings.Default.ParkinsonPilotMode == false)
                planViewCreatePlanBtn.Visible = true;
            // Default view is pateints practices
            setPlanBtnState(1);
            updatePlansViewFlowPanel(patianteId);
            MainTabControl.SelectTab("PlanView");

        }

        private void updatePlansViewFlowPanel(long uID) {
            EXviewGroupBox.Visible = false;
            if (Properties.Settings.Default.ParkinsonPilotMode == false)
                planViewCreatePlanBtn.Visible = true;
            DataSet plansData = new DataSet();
            long showPlanId = uID;
            if (Properties.Settings.Default.ParkinsonPilotMode) {
                showPlanId = 0;
            }
            plansData = datahandler.getPatientPlansInfo(showPlanId);
            planViewLayoutPanel.Controls.Clear();
            //selectedPlan = -1;
            if (plansData.Tables[0] == null)
                return;

            plansTable = plansData.Tables[0];
            int index = 0;
            foreach (DataRow pRow in plansTable.Rows) {
                if (searchNameTxt.Text != "") {
                    string srch = searchNameTxt.Text.ToUpper();
                    string pNameStr = pRow[1].ToString().ToUpper();
                    if (!pNameStr.Contains(srch)) {
                        continue;
                    }
                }
                Panel planPanel = new Panel();
                planPanel.Name = "planPanel" + pRow[0].ToString();
                planPanel.Size = new Size(893, 80);
                planPanel.Location = new Point(0, 0);
                planPanel.BackColor = Color.FromArgb(238, 238, 238);

                planPanel.BorderStyle = BorderStyle.FixedSingle;
                planPanel.Margin = new Padding(0, 0, 0, 0);

                Label pNum = new Label();
                pNum.Size = new Size(90, 36);
                pNum.Location = new Point(31, 25);
                pNum.Text = pRow[0].ToString();
                pNum.ForeColor = Color.Black;
                pNum.Font = new Font("Roboto", 30, GraphicsUnit.Pixel);
                pNum.TextAlign = ContentAlignment.MiddleLeft;
                pNum.Tag = index;
                pNum.Click += planPanel_click;
                planPanel.Controls.Add(pNum);

                Label pName = new Label();
                pName.Size = new Size(273, 36);
                pName.Location = new Point(152, 25);
                pName.Text = pRow[1].ToString();
                pName.ForeColor = Color.Black;
                pName.Font = new Font("Roboto", 30, GraphicsUnit.Pixel);
                pName.TextAlign = ContentAlignment.MiddleLeft;
                pName.Tag = index;
                pName.Click += planPanel_click;
                planPanel.Controls.Add(pName);

                Label pCount = new Label();
                pCount.Size = new Size(52, 36);
                pCount.Location = new Point(449, 25);
                pCount.Text = pRow[4].ToString();
                pCount.ForeColor = Color.Black;
                pCount.Font = new Font("Roboto", 30, GraphicsUnit.Pixel);
                pCount.TextAlign = ContentAlignment.MiddleLeft;
                pCount.Tag = index;
                pCount.Click += planPanel_click;
                planPanel.Controls.Add(pCount);

                Label pTime = new Label();
                pTime.Size = new Size(168, 36);
                pTime.Location = new Point(642, 25);
                pTime.Text = "N/A";
                pTime.ForeColor = Color.Black;
                pTime.Font = new Font("Roboto", 30, GraphicsUnit.Pixel);
                pTime.TextAlign = ContentAlignment.MiddleLeft;
                pTime.Tag = index;
                pTime.Click += planPanel_click;
                planPanel.Controls.Add(pTime);

                planPanel.Tag = index;
                planPanel.Click += planPanel_click;
                planPanel.MouseMove += planViewLayoutPanel_MouseMove;
                planPanel.MouseDown += planViewLayoutPanel_MouseDown;
                PlanPanel.MouseWheel += planViewLayoutPanel_MouseWheel;

                planViewLayoutPanel.Controls.Add(planPanel);
                index++;
            }
        }

        private void planPanel_click(object sender, EventArgs e) {
            int tagId = -1;
            if (sender is Label) {
                Label T = (Label)sender;
                tagId = int.Parse(T.Tag.ToString());
            }
            if (sender is Panel) {
                Panel P = (Panel)sender;
                tagId = int.Parse(P.Tag.ToString());
            }
            for (int i = 0; i < planViewLayoutPanel.Controls.Count; i++) {
                var V = planViewLayoutPanel.Controls[i];
                if (V is Panel) {
                    Panel P = (Panel)V;
                    if (int.Parse(V.Tag.ToString()) == tagId) {
                        P.BackColor = Color.FromArgb(255, 201, 31);
                        EXviewGroupBox.Visible = true;
                        planViewCreatePlanBtn.Visible = false;
                        viewPracticeNameLbl.Text = plansTable.Rows[i][1].ToString();
                        selectedPlan = int.Parse(plansTable.Rows[i][0].ToString());
                        updatePlanDetailsFlowPanel(selectedPlan);

                    }
                    else {
                        P.BackColor = Color.FromArgb(238, 238, 238);
                    }
                }
            }
        }

        private void updatePlanDetailsFlowPanel(int plan) {
            PlanDetailsFlowPanel.Controls.Clear();
            DataSet planData = new DataSet();
            if (planViewState == 2) { //|| Properties.Settings.Default.ParkinsonPilotMode) { // smart plan pressed
                planData = datahandler.getPlanData(plan, true);
            }
            else {
                planData = datahandler.getPlanData(plan);
            }
            DataTable planDataTable = planData.Tables[0];
            int reps = 0;
            int idx = 0;
            foreach (DataRow pRow in planDataTable.Rows) {
                Panel pvPanel = new Panel();
                pvPanel.Name = "pViewPanel" + idx;
                pvPanel.Size = new Size(500, 74);
                pvPanel.BackColor = Color.WhiteSmoke;
                pvPanel.BorderStyle = BorderStyle.FixedSingle;
                pvPanel.Margin = new Padding(0, 0, 0, 0);

                Label pvNumLbl = new Label();
                pvNumLbl.Size = new Size(40, 29);
                pvNumLbl.Location = new Point(8, 23);
                pvNumLbl.ForeColor = Color.Black;
                pvNumLbl.TextAlign = ContentAlignment.MiddleCenter;
                pvNumLbl.Font = new Font("Roboto", 20, GraphicsUnit.Pixel);
                pvNumLbl.Text = (idx + 1).ToString("D2");
                pvPanel.Controls.Add(pvNumLbl);

                PictureBox practiceImage = new PictureBox();
                practiceImage.Size = new Size(40, 40);
                practiceImage.Location = new Point(65, 20);
                string iconPath = Application.StartupPath + "\\practice_images\\" + getImageName(int.Parse(pRow["practice_id"].ToString())) + "s.png";
                if (File.Exists(iconPath))
                    practiceImage.Image = Image.FromFile(iconPath);
                else
                    practiceImage.Image = Image.FromFile(Application.StartupPath + "\\practice_images\\0s.png"); // default image
                pvPanel.Controls.Add(practiceImage);

                Label practiceNameLbl = new Label();
                practiceNameLbl.Size = new Size(207, 29);
                practiceNameLbl.Text = pRow["practice_name"].ToString();
                practiceNameLbl.ForeColor = Color.Black;
                practiceNameLbl.Font = new Font("Roboto", 20, GraphicsUnit.Pixel);
                practiceNameLbl.TextAlign = ContentAlignment.MiddleCenter;
                practiceNameLbl.Location = new Point(131, 23);
                pvPanel.Controls.Add(practiceNameLbl);

                Label pReps = new Label();
                pReps.Size = new Size(72, 29);
                pReps.Text = pRow["reps"].ToString();
                pReps.ForeColor = Color.Black;
                pReps.Font = new Font("Roboto", 20, GraphicsUnit.Pixel);
                pReps.TextAlign = ContentAlignment.MiddleCenter;
                pReps.Location = new Point(381, 23);
                pvPanel.Controls.Add(pReps);

                PlanDetailsFlowPanel.Controls.Add(pvPanel);
                idx++;
                reps += int.Parse(pRow["reps"].ToString());
            }
            planViewTotalexeLbl.Text = idx + " Exercises | " + reps + " Reps.";
            if (idx > 0) {
                planViewSelectEditBtn.Enabled = true;
                planViewStartPlanBtn.Enabled = true;
            }
            else {
                planViewSelectEditBtn.Enabled = false;
                planViewStartPlanBtn.Enabled = false;
            }
            if (Properties.Settings.Default.ParkinsonPilotMode)
                planViewSelectEditBtn.Enabled = false;
        }

        private void planVviewAndEditBtn_Click(object sender, EventArgs e) {
            Patient pt = pList.Find(item => item.refIDnum == patianteId);
            EditPatient addPatientForm = new EditPatient(patianteId, pt);
            addPatientForm.StartPosition = FormStartPosition.CenterScreen;
            addPatientForm.Show();
        }

        private void button13_Click_1(object sender, EventArgs e) {
            load_plans_view();
        }

        private void planViewCreatePlanBtn_Click(object sender, EventArgs e) {
            loadPlans(-1);
        }

        private int planViewState = 1;
        private void smartPlansBtn_Click(object sender, EventArgs e) {
            setPlanBtnState(2);
            updatePlansViewFlowPanel(0);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="type"></param> 1 = patient only, 2 = show smart plans
        private void setPlanBtnState(int type) {
            if (type == 1) {
                // set the buttons
                //smartPlansBtn.BackColor = Color.FromArgb(238, 238, 238);
                //allPlansBtn.BackColor = Color.FromArgb(255, 201, 31);
                planViewState = 1;
            }
            if (type == 2) {
                //smartPlansBtn.BackColor = Color.FromArgb(255, 201, 31);
                //allPlansBtn.BackColor = Color.FromArgb(238, 238, 238);
                planViewState = 2;
            }
        }

        private void allPlansBtn_Click(object sender, EventArgs e) {
            setPlanBtnState(1);
            updatePlansViewFlowPanel(patianteId);
        }

        private void pHistPViewLblBtn_Click(object sender, EventArgs e) {
            loadHistoryPage();
        }

        private void patientHistoryBtnLbl_Click(object sender, EventArgs e) {
            loadHistoryPage();
        }

        private void loadPlansPviewBtn_Click(object sender, EventArgs e) {
            loadPlans(-1);
        }

        private void planViewSelectEditBtn_Click(object sender, EventArgs e) {
            if (selectedPlan > -1) {
                loadPlans(selectedPlan);
            }
        }

        private void planViewStartPlanBtn_Click(object sender, EventArgs e) {
            start_plan(selectedPlan);
        }

        private void start_plan(int planId) {
            if (isPracticeAppRunning == false) {
                isPracticeAppRunning = true;
                string pExe = "SelfitPilot";
                string args = "UserName \"" + patientName + "\"";
                args += " -userIdentity " + patianteId.ToString();
                args += " -planId " + planId;
                //MessageBox.Show("Run CMD: " + planId + " Args: " + args);
                runExeApp(pExe, args);

                if (!datahandler.IsDemoMode()) {
                    loadHomePage();
                }
                else {
                    if (Properties.Settings.Default.ParkinsonPilotMode) {

                    }
                    else {
                        loadHistoryPage();
                    }
                }
            }
        }



        // 198, 661
        //198, 729
        //198, 803

        private void UserNameTxtBox_KeyUp_1(object sender, KeyEventArgs e) {
            if (UserNotExistsFlag) {
                UserNotExistsFlag = incorrectUserTxt.Visible = false;
            }
            else {
                if (e.KeyCode == Keys.Enter) {
                    UserPasswordTxtBox.Focus();
                }
            }
        }

        private void EditPlanNameBtn_Click(object sender, EventArgs e) {
            // Change the plan name
            // take care duplicate names in db, if does, add #
            planNameLbl.Enabled = true;
            planNameLbl.Focus();
            SavePlanNameBtn.Visible = true;
            EditPlanNameBtn.Visible = false;
        }

        private void SavePlanNameBtn_Click(object sender, EventArgs e) {
            // SavePlanNameBtn
            planNameLbl.Enabled = false;
            SavePlanNameBtn.Visible = false;
            EditPlanNameBtn.Visible = true;
        }

        private void MainTabControl_KeyUp(object sender, KeyEventArgs e) {
            if (e.KeyCode == Keys.Control && e.KeyCode == Keys.F2) {
                MainTabControl.SelectTab("Settings");
            }
        }

        private void LogInForm_KeyUp(object sender, KeyEventArgs e) {
            if (e.KeyCode == Keys.Control && e.KeyCode == Keys.F2) {
                MainTabControl.SelectTab("Settings");
            }
        }

        private void LogInForm_KeyDown(object sender, KeyEventArgs e) {
            if (e.KeyCode == Keys.F2 && e.Modifiers == Keys.Control) {
                MessageBox.Show("Control + F2 pressed");
                MainTabControl.SelectTab("Settings");
            }
        }



        private void MainTabControl_KeyDown(object sender, KeyEventArgs e) {
            if (e.KeyCode == Keys.F2 && e.Modifiers == Keys.Control) {
                //MessageBox.Show("Control + F2 pressed");
                selfitPathTxt.Text = datahandler.GetSystemPath();
                autoGenIDcbx.Checked = Properties.Settings.Default.autoGenID;
                parkinsonModeCBX.Checked = Properties.Settings.Default.ParkinsonPilotMode;
                MainTabControl.SelectTab("Settings");
            }
        }

        private void button3_Click_1(object sender, EventArgs e) {
            loadLoginPage();
        }

        private void selfitPathTxt_TextChanged(object sender, EventArgs e) {
            //
        }

        private void button2_Click(object sender, EventArgs e) {
            string path = selfitPathTxt.Text;
            path = path.Replace("\\", "\\\\");
            datahandler.SetSystemPath(path);
        }

        private void plansExitBtn_Click(object sender, EventArgs e) {
            loadHomePage();
        }

        private void pViewExitBtn_Click(object sender, EventArgs e) {
            loadHomePage();
        }


        private void searchNameTxt_TextChanged(object sender, EventArgs e) {
            //
            if (planViewState == 1)
                updatePlansViewFlowPanel(patianteId);
            else
                updatePlansViewFlowPanel(0);
        }

        private void LoginExitBtn_Click(object sender, EventArgs e) {
            close_keyboard();
            datahandler.close();
            Application.Exit();
        }

        private void AddPatientBtn_Click_1(object sender, EventArgs e) {
            EditPatient addPatientForm = new EditPatient();
            addPatientForm.StartPosition = FormStartPosition.CenterScreen;
            //addPatientForm.FormClosing += new FormClosingEventHandler(childFormClosing);
            //addPatientForm.Show();
            if (addPatientForm.ShowDialog(this) == DialogResult.OK) {
                Patient pt = addPatientForm.patientData;
                patianteId = pt.refIDnum;
                if (pList == null) {
                    pList = new List<Patient>();
                }
                pList.Add(pt);

                patientName = getPatientFullName(pt);
                //MessageBox.Show("Patient ID: " + patianteId );                
                addPatientForm.Dispose();
                if (Properties.Settings.Default.ParkinsonPilotMode == true) {
                    pViewQplanBtn.Enabled = false;
                    loadPlansPviewBtn.Enabled = false;
                    historyQuickPlanBtn.Enabled = false;
                    createNewPlanBtn.Enabled = false;
                    createPlanBtn.Enabled = false;
                    load_plans_view();
                }
                else {
                    loadQuickPlan();
                }
            }
        }

        private string getPatientFullName(Patient p) {
            string fullName = string.Empty;
            if (p.gender == 1) { fullName = "Mr."; }
            if (p.gender == 2) { fullName = "Ms."; }
            fullName += " " + p.FirstName + " " + p.LastName;
            return fullName;
        }

        private void historyReportBtn_Click(object sender, EventArgs e) {
            load_report_view();
        }

        private void load_report_view() {
            reportsTabControl.ItemSize = new Size(58, 1);
            /*DataSet planSumData = datahandler.getPatientPlanSummaryData(patianteId);
            if (planSumData.Tables[0].Rows.Count > 0) {
                DataRow pDrow = planSumData.Tables[0].Rows[0];
                //reportsPlansBtnTxt.Text = pDrow[0].ToString() + " Plans with " + pDrow[1].ToString() + " Exercieses"; //3 Plans contains 25 Exercieses
            }

            DataSet pSummaryData = datahandler.getPatientSummaryReportData(patianteId);
            DataTable sumDataTable = pSummaryData.Tables[0];
            if (sumDataTable.Rows.Count > 0) {
                DataRow dataRow = sumDataTable.Rows[0];
                //reportsSessionsBtnTxt.Text = dataRow[0].ToString() + " Sessions \n" + dataRow[2].ToString() + " Exercieses"; // 6 Sessions 34 Exercises
                int totSec = int.Parse(dataRow[3].ToString());
                TimeSpan time = TimeSpan.FromSeconds(totSec);
                //reportsExercisesBtnTxt.Text = dataRow[2].ToString() + " Exercieses \nTotal Time " + time.ToString(@"hh") + ":" + time.ToString(@"mm") + " Min"; // 34 Execises 03:42 hours
            }*/
            // ReportsPlansDataGridView

            // Fill the list of sessions (performed plans)
            reportsPlansListviewUpdate();

            // Set the default button status and view
            reportsSelectExercisesBtn.Image = Properties.Resources.Vector_2_2;
            reportsSelectExercisesBtn.FlatAppearance.BorderColor = Color.FromArgb(255, 201, 31);
            reportsSelectPlansBtn.Image = null;
            reportsSelectPlansBtn.FlatAppearance.BorderColor = Color.Black;
            selectedMeasures.Clear();
            reportsExercisesListviewUpdate();
            reportsTabControl.SelectTab("ReportsExercisesTabPage");
            reportsPatientNameLbl.Text = patientName;

            MainTabControl.SelectTab("reports");
        }

        DataTable planSessions = new DataTable();
        /// <summary>
        /// 
        /// </summary>
        private void reportsPlansListviewUpdate() {
            DataSet userSessionInfo = new DataSet();
            userSessionInfo = datahandler.getPatientSessionInfo(patianteId);
            reportsFlowLayoutPanel.Controls.Clear();
            plansDataGridView.Rows.Clear();
            planSessions = userSessionInfo.Tables[0];
            DataTable plansDT = new DataTable();
            if (userSessionInfo.Tables[0].Rows.Count > 0)
                plansDT = planSessions.DefaultView.ToTable(true, "plan_id", "plan_name");
            int reportPanelIndex = 0;
            foreach (DataRow dRow in plansDT.Rows) {
                Panel spPanel = new Panel();
                spPanel.Name = "panel" + dRow[0].ToString();
                spPanel.Height = 54;
                spPanel.Width = 357;
                spPanel.Margin = new Padding(3, 1, 0, 0);
                spPanel.BackColor = Color.White;

                Label sPlanName = new Label();
                sPlanName.Size = new Size(247, 28);
                sPlanName.Text = dRow[1].ToString();
                sPlanName.ForeColor = Color.Black;
                sPlanName.Font = new Font("Roboto", 18, GraphicsUnit.Pixel);
                sPlanName.TextAlign = ContentAlignment.MiddleLeft;
                sPlanName.Location = new Point(5, 11);
                sPlanName.Tag = dRow[0].ToString();
                sPlanName.Click += spPanel_click;
                spPanel.Controls.Add(sPlanName);                              
                spPanel.Click += spPanel_click;

                spPanel.Tag = dRow[0].ToString();
                reportsFlowLayoutPanel.Controls.Add(spPanel);
                reportPanelIndex++;
            }
        }

        DataTable patientReportInfo = new DataTable();

        /// <summary>
        /// 
        /// </summary>
        private void reportsExercisesListviewUpdate() {
            DataSet userExercisesInfo = new DataSet();
            userExercisesInfo = datahandler.getPerformedPractices(patianteId);
            reportsFlowLayoutPanel.Controls.Clear();
            selectedParametersFlowLayoutPanel.Controls.Clear();
            MeasuresDataGridView.Rows.Clear();
            MeasuresDataGridView.Columns.Clear();
            DataTable planSessions = userExercisesInfo.Tables[0];
            int reportPanelIndex = 0;
            foreach (DataRow dRow in planSessions.Rows) {
                Panel spPanel = new Panel();
                spPanel.Name = "panel" + dRow[0].ToString();
                spPanel.Height = 54;
                spPanel.Width = 357;
                spPanel.Margin = new Padding(3, 1, 0, 0);
                spPanel.BackColor = Color.White;

                PictureBox picBox = new PictureBox();
                picBox.Size = new Size(33, 33);
                picBox.Location = new Point(3, 9);
                picBox.Tag = dRow[0].ToString();
                picBox.Click += spPanel_click;
                int practiceId = int.Parse(dRow[0].ToString());
                string iconPath = Application.StartupPath + "\\practice_images\\" + getImageName(practiceId) + "s.png";
                if (File.Exists(iconPath))
                    picBox.Image = Image.FromFile(iconPath);
                else
                    picBox.Image = Image.FromFile(Application.StartupPath + "\\practice_images\\0s.png");
                spPanel.Controls.Add(picBox);

                Label sPlanName = new Label();
                sPlanName.Size = new Size(218, 28);
                sPlanName.Text = dRow[1].ToString();
                sPlanName.ForeColor = Color.Black;
                sPlanName.Font = new Font("Roboto", 18, GraphicsUnit.Pixel);
                sPlanName.TextAlign = ContentAlignment.MiddleLeft;
                sPlanName.Location = new Point(47, 11);
                sPlanName.Tag = dRow[0].ToString();
                sPlanName.Click += spPanel_click;
                spPanel.Controls.Add(sPlanName);

                Label exCountlbl = new Label();
                exCountlbl.Size = new Size(78, 32);
                exCountlbl.Text = dRow[2].ToString();
                exCountlbl.ForeColor = Color.Black;
                exCountlbl.Font = new Font("Roboto", 30, GraphicsUnit.Pixel);
                exCountlbl.TextAlign = ContentAlignment.MiddleCenter;
                exCountlbl.Location = new Point(265, 11);
                exCountlbl.Click += spPanel_click;
                exCountlbl.Tag = dRow[0].ToString();
                spPanel.Controls.Add(exCountlbl);
                spPanel.Click += spPanel_click;

                spPanel.Tag = dRow[0].ToString();
                reportsFlowLayoutPanel.Controls.Add(spPanel);
                reportPanelIndex++;
            }
        }

        //
        private void spPanel_click(object sender, EventArgs e) {
            int tagId = -1;
            if (sender is Label) {
                Label T = (Label)sender;
                tagId = int.Parse(T.Tag.ToString());
            }
            if (sender is Panel) {
                Panel P = (Panel)sender;
                tagId = int.Parse(P.Tag.ToString());
            }
            for (int i = 0; i < reportsFlowLayoutPanel.Controls.Count; i++) {
                var V = reportsFlowLayoutPanel.Controls[i];
                if (V is Panel) {
                    Panel P = (Panel)V;
                    if (int.Parse(V.Tag.ToString()) == tagId) {
                        P.BackColor = Color.FromArgb(255, 201, 31);                        
                        if (reportsTabControl.SelectedTab.Name == "ReportsExercisesTabPage") {
                            //MessageBox.Show("Selected practice #: " + tagId);
                            updateMeasuresList(tagId);
                        }
                        else {
                            //Update the plansDataGridView
                            updatePracticeList(tagId);

                        }

                    }
                    else {
                        P.BackColor = Color.White;                        
                    }
                }
            }
            //MessageBox.Show(tagId.ToString());
        }

        private void updatePracticeList(int planId) {
            //plansDataGridView.Columns.Clear();
            plansDataGridView.Rows.Clear();
            DataRow[] plansDR = planSessions.Select("plan_id = " + planId);
            for (int i = 0; i < plansDR.Length; i++) {
                DataSet historyInfo = new DataSet();
                int sessionId = int.Parse(plansDR[i][2].ToString());
                historyInfo = datahandler.getPatientSessionHistory(patianteId, 1, sessionId);
                DataTable historyDT = historyInfo.Tables[0];
                foreach (DataRow hRow in historyDT.Rows) {
                    plansDataGridView.Rows.Add(plansDR[i][2], plansDR[i][4], hRow[4], hRow[5], hRow[6], string.Format("{0:F2}", hRow[7]), string.Format("{0:F2}", hRow[8]),
                        string.Format("{0:F2}", hRow[9]), string.Format("{0:F2}", hRow[10]), string.Format("{0:F2}", hRow[11]), string.Format("{0:F2}", hRow[12]));
                }
            }

        }

        private void updateMeasuresList(int practiceId) {
            // Prepare the data for the view
            MeasuresDataGridView.Columns.Clear();
            MeasuresDataGridView.Rows.Clear();
            selectedMeasures.Clear();
            DataSet repData = datahandler.getPatientPracticeMeasuresData(patianteId, practiceId);
            patientReportInfo.Clear();
            patientReportInfo = repData.Tables[0];

            selectedParametersFlowLayoutPanel.Controls.Clear();
            DataRow[] mList = practiceMeasuresInfo.Select("practice_id = " + practiceId);
            for (int i = 0; i < mList.Length; i++) {
                Panel mPanel = new Panel();
                mPanel.Name = "measurePanel" + i;
                mPanel.Tag = mList[i][1].ToString();
                mPanel.Size = new Size(305, 54);
                mPanel.Margin = new Padding(3, 1, 0, 0);
                mPanel.BackColor = Color.FromArgb(238, 238, 238);

                Label mName = new Label();
                mName.Size = new Size(265, 41);
                mName.Location = new Point(13, 6);
                mName.Text = mList[i][2].ToString();
                mName.ForeColor = Color.Black;
                mName.Click += mPanel_click;
                mName.Font = new Font("Roboto", 18, GraphicsUnit.Pixel);
                mName.Tag = mList[i][1].ToString();

                mPanel.Click += mPanel_click;
                mPanel.Controls.Add(mName);
                selectedParametersFlowLayoutPanel.Controls.Add(mPanel);
            }
        }


        List<int> selectedMeasures = new List<int>();
        //
        private void mPanel_click(object sender, EventArgs e) {
            int tagId = -1;
            if (sender is Label) {
                Label T = (Label)sender;
                tagId = int.Parse(T.Tag.ToString());
            }
            if (sender is Panel) {
                Panel P = (Panel)sender;
                tagId = int.Parse(P.Tag.ToString());
            }

            if (tagId != -1) {
                if (selectedMeasures.Contains(tagId)) {
                    var itemToRemove = selectedMeasures.Single(r => r == tagId);
                    selectedMeasures.Remove(itemToRemove);
                }
                else
                    if (selectedMeasures.Count < 3)
                        selectedMeasures.Add(tagId);
            }
            updateMeasuresDataView();
        }
         
        private void updateMeasuresDataView() { 

            // Update the button lock
            for (int i = 0; i < selectedParametersFlowLayoutPanel.Controls.Count; i++) {
                var V = selectedParametersFlowLayoutPanel.Controls[i];
                if (V is Panel) {
                    Panel P = (Panel)V;
                    if (selectedMeasures.Contains( int.Parse(V.Tag.ToString()) )) {
                        P.BackColor = Color.FromArgb(255, 201, 31);                     
                    }
                    else {
                        P.BackColor = Color.FromArgb(238, 238, 238);                        
                    }
                }
            }
            // update the data in MeasuresDataGridView
            if (selectedParametersFlowLayoutPanel.Controls.Count > 0) {
                MeasuresDataGridView.Columns.Clear();
                MeasuresDataGridView.Rows.Clear();
                MeasuresDataGridView.Columns.Add("SessionID", "#");
                MeasuresDataGridView.Columns.Add("Date", "Date");

                string mListStr = string.Join(",", selectedMeasures);
                //MessageBox.Show(mListStr);
                foreach (int mId in selectedMeasures) {
                 //   DataRow[] mList = practiceMeasuresInfo.Select("measureId = " + mId);
                 //   string mName = mList[0][4].ToString();
                    MeasuresDataGridView.Columns.Add("measure_" + mId, mId.ToString());
                }

                // patientReportInfo
                DataTable sessionDr = new DataTable();
                if (patientReportInfo.Rows.Count > 0)
                sessionDr = patientReportInfo.DefaultView.ToTable(true, "sessionId");
                foreach (DataRow sessionRow in sessionDr.Rows) {
                    //MessageBox.Show("Sessions:" + string.Join(",", sessionRow[0]));
                    if (mListStr != string.Empty) {
                        DataRow[] sessionDataEntry = patientReportInfo.Select("sessionId = " + sessionRow[0] + " AND measureId in(" + mListStr + ")");
                        string[] measuresData = new string[3];
                        for (int i = 0; i < sessionDataEntry.Length; i++) {
                            //MessageBox.Show(sessionDataEntry[i][5].ToString());
                            float data = 0;
                            if (float.TryParse(sessionDataEntry[i][5].ToString(), out data) && sessionDataEntry[i][5].ToString().Contains("."))
                                measuresData[i] = string.Format("{0:F2}", data);
                            else
                                measuresData[i] = sessionDataEntry[i][5].ToString();
                            MeasuresDataGridView.Columns[2+i].HeaderText = sessionDataEntry[i][4].ToString();
                        }
                        //string mDataStr = string.Join(",", measuresData.ToArray<string>());
                        MeasuresDataGridView.Rows.Add(sessionRow[0], sessionDataEntry[0]["practiceTime"], measuresData[0], measuresData[1], measuresData[2]);
                    }
                    else {
                        MeasuresDataGridView.Rows.Clear();
                    }
                }            

            }


        }

        private void reportsCloseBtn_Click(object sender, EventArgs e) {
            loadHomePage();
        }

        private void reportsPatientEditBtn_Click(object sender, EventArgs e) {
            historyViewEditBtn_Click(sender, e);
        }

        private void reportsBackBtn_Click(object sender, EventArgs e) {
            loadHistoryPage();
        }

        private void reportsBtn_Click(object sender, EventArgs e) {
            loadGeneralReports();
        }

        public void loadGeneralReports() {
            genRepPnameFilterText.Text = "";
            //DataSet repData = dbh.getGeneralReportData();
            //generalReportsListView.Rows.Clear();
            //DataTable dataTable = repData.Tables["GeneralReportData"];
            //int index = 1;
            //foreach (DataRow dr in dataTable.Rows) {
            //    generalReportsListView.Rows.Add(index,  dr[3].ToString().Split(' ')[0], dr[1].ToString(), dr[2].ToString(), dr[6].ToString());                
            //    index++;
            //}
            updateReportInformation();
            MainTabControl.SelectTab("GeneralReports");

        }

        private void button4_Click(object sender, EventArgs e) {
            loadHomePage();
        }

        private void generalReportsColseBtn_Click(object sender, EventArgs e) {
            loadHomePage();
        }

        private void genRepPnameFilterText_TextChanged(object sender, EventArgs e) {
            needShowBtn();
        }

        private void genRepFromMonthCombo_SelectedIndexChanged(object sender, EventArgs e) {
            needShowBtn();
        }

        private void genRepToMonthCombo_SelectedIndexChanged(object sender, EventArgs e) {
            needShowBtn();
        }


        private void needShowBtn() {
            if (genRepPnameFilterText.Text != "" || (genRepFromMonthCombo.Text != "" && genRepToMonthCombo.Text != "")) {
                genRepShowBtn.Enabled = true;
            }
            else {
                genRepShowBtn.Enabled = false;
            }
        }

        private void genRepShowBtn_Click(object sender, EventArgs e) {
            updateReportInformation();
        }

        public void updateReportInformation() {
            string pID = genRepPnameFilterText.Text != "" ? genRepPnameFilterText.Text : "%";

            string fromDate = "2018-1-1";
            string formattedDate = "From 01, 2019";
            int fYear = 2018;
            int fromM = 1;
            DateTime localDate = DateTime.Now;
            string toDate = localDate.Year.ToString() + "-" + localDate.Month.ToString() + "-" + localDate.Day.ToString();
            int tyear = localDate.Year;
            int tmonth = localDate.Month;
            if (genRepFromMonthCombo.Text != "") {
                fromM = genRepFromMonthCombo.SelectedIndex + 1;
                fromDate = genRepFromYearCombo.Text + "-" + fromM.ToString() + "-1";
                formattedDate = "From " + fromM.ToString("D2") + "," + genRepFromYearCombo.Text;
                fYear = int.Parse(genRepFromYearCombo.Text);
            }
            if (genRepToMonthCombo.Text != "") {
                tyear = int.Parse(genRepToYearCombo.Text);
                tmonth = genRepToMonthCombo.SelectedIndex + 1;
                toDate = genRepToYearCombo.Text + "-" + tmonth.ToString() + "-" + DateTime.DaysInMonth(tyear, tmonth);
                formattedDate += "\nTo " + tmonth.ToString("D2") + ", " + genRepToYearCombo.Text;
            }
            else {
                formattedDate += "\nTo " + localDate.Month.ToString("D2") + ", " + localDate.Year.ToString("D4");
            }

            //MessageBox.Show("from: " + fromDate + " to: " + toDate + " ID: " + pID);
            DataSet repData = datahandler.getGeneralReportDataFiltered(fromDate, toDate, pID);
            if (repData.Tables.Count == 0)
                return;
            generalReportsListView.Rows.Clear();
            DataTable dataTable = repData.Tables[0];
            int index = 1;
            foreach (DataRow dr in dataTable.Rows) {
                generalReportsListView.Rows.Add(index, dr[3].ToString().Split(' ')[0], dr[1].ToString(), dr[2].ToString(), dr[6].ToString());
                index++;
            }
            //getSummaryReportDataFiltered
            DataSet sumDataSet = datahandler.getSummaryReportDataFiltered(fromDate, toDate, pID);
            DataTable sumDataTable = sumDataSet.Tables[0];
            int mCount = (Math.Abs(tyear - fYear) * 12) + tmonth - fromM;
            if (sumDataTable.Rows.Count > 0) {
                // Fill the data rectangles
                DataRow dataRow = sumDataTable.Rows[0];
                sumRectDurationTxt.Text = formattedDate;
                sumRectMonthsTxt.Text = mCount.ToString("D2");
                sumRectTotPatientsTxt.Text = "Total " + dataRow[1].ToString() + " Patients";
                sumRectPatientsCountTxt.Text = dataRow[1].ToString();
                sumRectTotalSessionsTxt.Text = "Total " + dataRow[0].ToString() + " Sessions";
                sumRectSessionsCountTxt.Text = dataRow[0].ToString();
                sumRectTotalExercisesTxt.Text = "Total " + dataRow[2].ToString() + " Exercises";
                sumRectExercisesCountTxt.Text = dataRow[2].ToString();
                int totSec = 0;
                if (int.TryParse(dataRow[3].ToString(), out totSec)) {
                    TimeSpan time = TimeSpan.FromSeconds(totSec);
                    sumRectTotalTimeTxt.Text = "Total Time " + time.ToString(@"hh") + " Hr / " + time.ToString(@"mm") + " Min";
                    sumRectTimeCountTxt.Text = (totSec / 3600).ToString("X1");
                }
                else {
                    sumRectTotalTimeTxt.Text = "Total Time 0 Hr / 0 Min";
                    sumRectTimeCountTxt.Text = "0";
                }
            }

        }

        private void keyBoardBtn_Click(object sender, EventArgs e) {
            load_virtual_keyboard();
            UserNameTxtBox.Focus();
        }

        private void UserPasswordTxtBox_DoubleClick(object sender, EventArgs e) {
            load_virtual_keyboard();
            UserPasswordTxtBox.Focus();
        }



        private void load_virtual_keyboard() {
            //var simu = new InputSimulator();          
            if (keyBoardOn == false) {
                //simu.Keyboard.ModifiedKeyStroke(new[] { VirtualKeyCode.LWIN, VirtualKeyCode.CONTROL }, VirtualKeyCode.VK_O);                

                Wow64Interop.EnableWow64FSRedirection(false);
                string currDir = Directory.GetCurrentDirectory();
                Directory.SetCurrentDirectory(Environment.SystemDirectory);
                keyBoardProcess = new Process();
                keyBoardProcess.StartInfo.UseShellExecute = true;
                keyBoardProcess.StartInfo.WorkingDirectory = Environment.SystemDirectory;
                keyBoardProcess.StartInfo.FileName = "osk.exe";
                keyBoardProcess.StartInfo.WindowStyle = ProcessWindowStyle.Normal;
                keyBoardProcess.Start();
                keyBoardOn = true;
                Directory.SetCurrentDirectory(currDir);
                Wow64Interop.EnableWow64FSRedirection(true);
            }
            else {
                close_keyboard();
                keyBoardOn = false;
            }
        }

        private void close_keyboard() {
            if (keyBoardProcess != null) {
                try {
                    if (!keyBoardProcess.HasExited) {
                        keyBoardProcess.Kill();
                        keyBoardProcess.Close();
                    }
                }
                catch (Exception ex) {
                    keyBoardOn = false;
                }
            }
            keyBoardOn = false;
        }



        /// -practicesLayoutPanel-------------------------------------------------------------------------------------------------------------
        /// 
        int SCROLL = 600;
        private void buttonUp_Click(object sender, EventArgs e) {
            if (practicesLayoutPanel.VerticalScroll.Value - (SCROLL) > 0) {
                practicesLayoutPanel.VerticalScroll.Value -= (SCROLL);
                practicesLayoutPanel.VerticalScroll.Value -= (SCROLL);
            }
            else {
                practicesLayoutPanel.VerticalScroll.Value = practicesLayoutPanel.VerticalScroll.Minimum;
                practicesLayoutPanel.VerticalScroll.Value = practicesLayoutPanel.VerticalScroll.Minimum;
            }
        }

        private void buttonDown_Click(object sender, EventArgs e) {
            if (practicesLayoutPanel.VerticalScroll.Value + (SCROLL) <= practicesLayoutPanel.VerticalScroll.Maximum) {
                practicesLayoutPanel.VerticalScroll.Value += (SCROLL);
                practicesLayoutPanel.VerticalScroll.Value += (SCROLL);
            }
            else {
                practicesLayoutPanel.VerticalScroll.Value = practicesLayoutPanel.VerticalScroll.Maximum;
                practicesLayoutPanel.VerticalScroll.Value = practicesLayoutPanel.VerticalScroll.Maximum;
            }
        }


        private void practicesLayoutPanel_MouseEnter(object sender, EventArgs e) {
            //practicesLayoutPanel.AutoScroll = false;
        }

        bool on_practicesLayoutPanelHandleDelta = false;

        private void HandleDelta_practicesLayoutPanel(int delta) {
            on_practicesLayoutPanelHandleDelta = true;
            int multiplyer = 1;
            if (delta < 0) { // -value                
                if (practicesLayoutPanel.VerticalScroll.Value + (SCROLL_VALUE * multiplyer) <= practicesLayoutPanel.VerticalScroll.Maximum) {
                    layoutPanelVerticalScrollUp(practicesLayoutPanel, SCROLL_VALUE);
                }
                else {
                    layoutPanelVerticalScrollUp(practicesLayoutPanel, practicesLayoutPanel.VerticalScroll.Maximum);
                }
            }
            if (delta > 0) { // +value                
                if (practicesLayoutPanel.VerticalScroll.Value - (SCROLL_VALUE * multiplyer) >= 0) {
                    layoutPanelVerticalScrollDown(practicesLayoutPanel, SCROLL_VALUE);
                }
                else {
                    layoutPanelVerticalScrollDown(practicesLayoutPanel, 0);
                }
            }
            on_practicesLayoutPanelHandleDelta = false;
        }

        public void layoutPanelVerticalScrollUp(FlowLayoutPanel p, int val) {
            if (p.VerticalScroll.Value + val <= p.VerticalScroll.Maximum)
                p.VerticalScroll.Value += val;
            if (p.VerticalScroll.Value + val <= p.VerticalScroll.Maximum)
                p.VerticalScroll.Value += val;
        }

        public void layoutPanelVerticalScrollDown(FlowLayoutPanel p, int val) {
            p.VerticalScroll.Value -= val;
            p.VerticalScroll.Value -= val;
        }

        private void practicesLayoutPanel_MouseWheel(object sender, MouseEventArgs e) {
            HandleDelta_practicesLayoutPanel(e.Delta);
            base.OnMouseWheel(e);
        }

        private Point _practicesLayoutPanel_mouseLastPosition;

        private void practicesLayoutPanel_MouseDown(object sender, MouseEventArgs e) {
            if (e.Button == MouseButtons.Left) {// && on_practicesLayoutPanelHandleDelta == false) {
                _practicesLayoutPanel_mouseLastPosition = e.Location;
            }
            base.OnMouseDown(e);
        }

        private void practicesLayoutPanel_MouseMove(object sender, MouseEventArgs e) {
            if ((MouseButtons & MouseButtons.Left) != 0) {
                int delta = e.Y - _practicesLayoutPanel_mouseLastPosition.Y;
                //if (delta != 0)
                //    DeltaLabel.Text = "Delta = " + delta;                
                if (Math.Abs(delta) > 10 & !on_practicesLayoutPanelHandleDelta)
                    HandleDelta_practicesLayoutPanel(delta);
                // _practicesLayoutPanel_mouseLastPosition = e.Location;
            }
            base.OnMouseMove(e);
        }
        //-practicesLayoutPanel--------------------------------------------------------------------------------------------------------------

        private void sessionDatainfoPanel_MouseDown(object sender, MouseEventArgs e) {

        }

        private void sessionDatainfoPanel_MouseMove(object sender, MouseEventArgs e) {

        }

        bool on_exercisesFlowPanelHandleDelta = false;

        private void HandleDelta_exercisesFlowPanel(int delta) {
            on_exercisesFlowPanelHandleDelta = true;
            if (delta < 0) {
                if (exercisesFlowPanel.VerticalScroll.Value + SCROLL_VALUE <= exercisesFlowPanel.VerticalScroll.Maximum)
                    layoutPanelVerticalScrollUp(exercisesFlowPanel, SCROLL_VALUE);
                //exercisesFlowPanel.VerticalScroll.Value += SCROLL_VALUE;
                else
                    layoutPanelVerticalScrollUp(exercisesFlowPanel, exercisesFlowPanel.VerticalScroll.Maximum);
            }
            if (delta > 0) {
                if (exercisesFlowPanel.VerticalScroll.Value - SCROLL_VALUE >= 0)
                    layoutPanelVerticalScrollDown(exercisesFlowPanel, SCROLL_VALUE);
                //exercisesFlowPanel.VerticalScroll.Value -= SCROLL_VALUE;
                else
                    layoutPanelVerticalScrollDown(exercisesFlowPanel, 0);// exercisesFlowPanel.VerticalScroll.Minimum;
            }
            on_exercisesFlowPanelHandleDelta = false;
        }

        private void exercisesFlowPanel_MouseWheel(object sender, MouseEventArgs e) {
            HandleDelta_exercisesFlowPanel(e.Delta);
            base.OnMouseWheel(e);
        }

        private Point _mouseLastPosition;
        private void exercisesFlowPanel_MouseDown(object sender, MouseEventArgs e) {
            if (e.Button == MouseButtons.Left) {
                _mouseLastPosition = e.Location;
            }
            base.OnMouseDown(e);
        }

        private void exercisesFlowPanel_MouseMove(object sender, MouseEventArgs e) {
            if ((MouseButtons & MouseButtons.Left) != 0) {
                var delta = e.Y - _mouseLastPosition.Y;
                if (Math.Abs(delta) > 10 & !on_exercisesFlowPanelHandleDelta)
                    HandleDelta_exercisesFlowPanel(delta);
                //_mouseLastPosition = e.Location;
            }
            base.OnMouseMove(e);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="delta"></param>
        private void HandleDelta_filterFlowLayoutPanel(int delta) {
            if (delta < 0) {
                if (filterFlowLayoutPanel.VerticalScroll.Value + SCROLL_VALUE <= filterFlowLayoutPanel.VerticalScroll.Maximum)
                    layoutPanelVerticalScrollUp(filterFlowLayoutPanel, SCROLL_VALUE);
                //filterFlowLayoutPanel.VerticalScroll.Value += SCROLL_VALUE;
                else
                    layoutPanelVerticalScrollUp(filterFlowLayoutPanel, filterFlowLayoutPanel.VerticalScroll.Maximum);
            }
            if (delta > 0) {
                if (filterFlowLayoutPanel.VerticalScroll.Value - SCROLL_VALUE >= 0)
                    layoutPanelVerticalScrollDown(filterFlowLayoutPanel, SCROLL_VALUE);
                //filterFlowLayoutPanel.VerticalScroll.Value -= SCROLL_VALUE;
                else
                    layoutPanelVerticalScrollDown(filterFlowLayoutPanel, 0);
                //filterFlowLayoutPanel.VerticalScroll.Value = filterFlowLayoutPanel.VerticalScroll.Minimum;
            }
        }

        private void filterFlowLayoutPanel_MouseWheel(object sender, MouseEventArgs e) {
            HandleDelta_filterFlowLayoutPanel(e.Delta);
            base.OnMouseWheel(e);
        }

        private void filterFlowLayoutPanel_MouseDown(object sender, MouseEventArgs e) {
            if (e.Button == MouseButtons.Left) {
                _mouseLastPosition = e.Location;
            }
            base.OnMouseDown(e);
        }

        private void filterFlowLayoutPanel_MouseMove(object sender, MouseEventArgs e) {
            if ((MouseButtons & MouseButtons.Left) != 0) {
                var delta = e.Y - _mouseLastPosition.Y;
                if (Math.Abs(delta) > 4)
                    HandleDelta_filterFlowLayoutPanel(delta);
                //_mouseLastPosition = e.Location;
            }
            base.OnMouseMove(e);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="delta"></param>        
        private void HandleDelta_SessionsFlowLayoutPanel(int delta) {
            if (delta < 0) {
                if (SessionsFlowLayoutPanel.VerticalScroll.Value + SCROLL_VALUE <= SessionsFlowLayoutPanel.VerticalScroll.Maximum)
                    layoutPanelVerticalScrollUp(SessionsFlowLayoutPanel, SCROLL_VALUE);
                else
                    layoutPanelVerticalScrollUp(SessionsFlowLayoutPanel, SessionsFlowLayoutPanel.VerticalScroll.Maximum);
            }
            if (delta > 0) {
                if (SessionsFlowLayoutPanel.VerticalScroll.Value - SCROLL_VALUE >= 0)
                    layoutPanelVerticalScrollDown(SessionsFlowLayoutPanel, SCROLL_VALUE);
                else
                    layoutPanelVerticalScrollDown(SessionsFlowLayoutPanel, 0);
            }
        }

        private void SessionsFlowLayoutPanel_MouseWheel(object sender, MouseEventArgs e) {
            //HandleDelta_SessionsFlowLayoutPanel(e.Delta);
            base.OnMouseWheel(e);
        }

        private void SessionsFlowLayoutPanel_MouseDown(object sender, MouseEventArgs e) {
            if (e.Button == MouseButtons.Left) {
                _mouseLastPosition = e.Location;
            }
            base.OnMouseDown(e);
        }

        private void SessionsFlowLayoutPanel_MouseMove(object sender, MouseEventArgs e) {
            if ((MouseButtons & MouseButtons.Left) != 0) {
                var delta = e.Y - _mouseLastPosition.Y;
                if (Math.Abs(delta) > 10)
                    HandleDelta_SessionsFlowLayoutPanel(delta);
                //_mouseLastPosition = e.Location;
            }
            base.OnMouseMove(e);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="delta"></param>        
        private void HandleDelta_exerciesLayout(int delta) {
            if (delta < 0) {
                if (exerciesLayout.VerticalScroll.Value + SCROLL_VALUE <= exerciesLayout.VerticalScroll.Maximum)
                    layoutPanelVerticalScrollUp(exerciesLayout, SCROLL_VALUE);
                //exerciesLayout.VerticalScroll.Value += SCROLL_VALUE;
                else
                    layoutPanelVerticalScrollUp(exerciesLayout, exerciesLayout.VerticalScroll.Maximum);
            }
            if (delta > 0) {
                if (exerciesLayout.VerticalScroll.Value - SCROLL_VALUE >= 0)
                    layoutPanelVerticalScrollDown(exerciesLayout, SCROLL_VALUE);
                //exerciesLayout.VerticalScroll.Value -= SCROLL_VALUE;
                else
                    layoutPanelVerticalScrollDown(exerciesLayout, 0);
            }
        }

        private void exerciesLayout_MouseWheel(object sender, MouseEventArgs e) {
            //HandleDelta_exerciesLayout(e.Delta);
            base.OnMouseWheel(e);
        }

        private void exerciesLayout_MouseDown(object sender, MouseEventArgs e) {
            if (e.Button == MouseButtons.Left) {
                _mouseLastPosition = e.Location;
            }
            base.OnMouseDown(e);
        }

        private void exerciesLayout_MouseMove(object sender, MouseEventArgs e) {
            if ((MouseButtons & MouseButtons.Left) != 0) {
                var delta = e.Y - _mouseLastPosition.Y;
                if (Math.Abs(delta) > 10)
                    HandleDelta_exerciesLayout(delta);
                //_mouseLastPosition = e.Location;
            }
            base.OnMouseMove(e);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="delta"></param>        
        private void HandleDelta_planViewLayoutPanel(int delta) {
            if (delta < 0) {
                if (planViewLayoutPanel.VerticalScroll.Value + SCROLL_VALUE <= planViewLayoutPanel.VerticalScroll.Maximum)
                    layoutPanelVerticalScrollUp(planViewLayoutPanel, SCROLL_VALUE);
                else
                    layoutPanelVerticalScrollUp(planViewLayoutPanel, planViewLayoutPanel.VerticalScroll.Maximum);
            }
            if (delta > 0) {
                if (planViewLayoutPanel.VerticalScroll.Value - SCROLL_VALUE >= 0)
                    layoutPanelVerticalScrollDown(planViewLayoutPanel, SCROLL_VALUE);
                else
                    layoutPanelVerticalScrollDown(planViewLayoutPanel, 0);
            }
        }

        private void planViewLayoutPanel_MouseWheel(object sender, MouseEventArgs e) {
            HandleDelta_planViewLayoutPanel(e.Delta);
            base.OnMouseWheel(e);
        }

        private void planViewLayoutPanel_MouseDown(object sender, MouseEventArgs e) {
            if (e.Button == MouseButtons.Left) {
                _mouseLastPosition = e.Location;
            }
            base.OnMouseDown(e);
        }

        private void planViewLayoutPanel_MouseMove(object sender, MouseEventArgs e) {
            if ((MouseButtons & MouseButtons.Left) != 0) {
                var delta = e.Y - _mouseLastPosition.Y;
                if (Math.Abs(delta) > 4)
                    HandleDelta_planViewLayoutPanel(delta);
                _mouseLastPosition = e.Location;
            }
            base.OnMouseMove(e);
        }

        private void homeKeyBoardKey_Click(object sender, EventArgs e) {
            load_virtual_keyboard();
        }

        private void UserNameTxtBox_MouseUp(object sender, MouseEventArgs e) {
            load_virtual_keyboard();
        }

        private void patientSearchTxt_MouseUp(object sender, MouseEventArgs e) {
            load_virtual_keyboard();
        }

        private void exercisesTotalsTxt_Click(object sender, EventArgs e) {

        }

        private void patientSearchTxt_MouseDown(object sender, MouseEventArgs e) {
            if (patientSearchTxt.Text.Contains(patientSearchTxtTEXT)) {
                patientSearchTxt.Text = "";
            }
        }

        private void patientSearchTxt_Leave(object sender, EventArgs e) {
            if (patientSearchTxt.Text == "") {
                patientSearchTxt.Text = patientSearchTxtTEXT;
                //patientDetailsPanel.Visible = false;
                patientTabControl.SelectTab("MainPatient");
            }
        }

        private int SELECTED_PatientsListBox = -1;

        private void PatientsListBox_DrawItem(object sender, DrawItemEventArgs e) {
            int index = e.Index;
            if (index >= 0) {
                //SELECTED_PatientsListBox = index;            
                Graphics g = e.Graphics;
                // Get the item details
                Font font = PatientsListBox.Font;
                Color colour = PatientsListBox.ForeColor;
                string text = PatientsListBox.Items[index].ToString();
                //MessageBox.Show(text);

                foreach (int selectedIndex in this.PatientsListBox.SelectedIndices) {
                    if (index == selectedIndex) {
                        // Draw the new background colour
                        e.DrawBackground();
                        g.FillRectangle(new SolidBrush(Color.FromArgb(255, 201, 31)), e.Bounds);
                        colour = Color.White;
                        text = "\u2713 " + text;
                    }
                }

                // Print the text
                g.DrawString(text, font, new SolidBrush(colour), (float)e.Bounds.X, (float)e.Bounds.Y);
                e.DrawFocusRectangle();
            }
        }


        private void patientDetailsEditBtn_Click(object sender, EventArgs e) {

            patientTabControl.SelectTab("EditDetails");
        }

        /// <summary>
        /// Internal use
        /// //////////////////////////////////////////////////////////////////////////////////////////////////////
        /// </summary>
        private static string users_key { get; set; } = "a46a3132536fa34fa9680e7ac1699d32";

        /// <summary>
        ///
        /// </summary>
        /// <param name="cipher"></param>
        /// <returns></returns>
        public static string Decrypt(string cipher) {
            using (var md5 = new MD5CryptoServiceProvider()) {
                using (var tdes = new TripleDESCryptoServiceProvider()) {
                    tdes.Key = md5.ComputeHash(UTF8Encoding.UTF8.GetBytes(users_key));
                    tdes.Mode = CipherMode.ECB;
                    tdes.Padding = PaddingMode.PKCS7;
                    using (var transform = tdes.CreateDecryptor()) {
                        byte[] cipherBytes = Convert.FromBase64String(cipher);
                        byte[] bytes = transform.TransformFinalBlock(cipherBytes, 0, cipherBytes.Length);
                        return UTF8Encoding.UTF8.GetString(bytes);
                    }
                }
            }
        }

        private void isFastEditDataChanged() {
            if ((firstNamePatientEdit.Text + " " + lastNamePatientEdit.Text != patientInfoNameLbl.Text) ||
                (mailPatientEdit.Text != patientInfoEmailTxt.Text) || (phonePatientEdit.Text != patientInfoPhoneTxt.Text) || (notesPatientEdit.Text != patientInfoNotesTxt.Text)) {
                fastEditDataChanged = true;
            }
            else {
                fastEditDataChanged = false;
            }
        }

        private void firstNamePatientEdit_TextChanged(object sender, EventArgs e) {
            isFastEditDataChanged();
        }

        private void lastNamePatientEdit_TextChanged(object sender, EventArgs e) {
            isFastEditDataChanged();
        }

        private void mailPatientEdit_TextChanged(object sender, EventArgs e) {
            isFastEditDataChanged();
        }

        private void phonePatientEdit_TextChanged(object sender, EventArgs e) {
            isFastEditDataChanged();
        }

        private void notesPatientEdit_TextChanged(object sender, EventArgs e) {
            isFastEditDataChanged();
        }

        private void fastEditDetailsBtn_Click(object sender, EventArgs e) {
            Patient pt = pList.Find(item => item.refIDnum == patianteId);

            EditPatient addPatientForm = new EditPatient(patianteId, pt);
            addPatientForm.StartPosition = FormStartPosition.CenterScreen;
            addPatientForm.FormClosing += new FormClosingEventHandler(editPatientFormClosing);
            addPatientForm.Show();
        }

        private void patientRefreshData() {
            // Modify the array
            //MessageBox.Show("User is: " + patianteId);
            pList = new List<Patient>();
            Task t = Task.Run(async () => { pList = await Identity.getUsersDataByNameAsync(patientSearchTxt.Text, "", SITEID); });
            //   Identity.getUsersDataByNameAsync(patientSearchTxt.Text, "", 1).Result;
            Task.WaitAll(t);

            updatePatientsListBox(pList);

            string siteTxt = SITEID.ToString();
            long uId = patianteId; // ref ID                
            patientTabControl.SelectTab("ShowDetails");
            Patient pt = pList.Find(item => item.refIDnum == uId);
            if (pt != null) // check item isn't null
                {
                patientInfoNameLbl.Text = pt.FirstName + " " + pt.LastName;
                string idText = Decrypt(pt.HashedID);
                patientInfoIDLbl.Text = IdPatientEdit.Text = idText.Substring(0, idText.Length - siteTxt.Length);
                patientInfoEmailTxt.Text = mailPatientEdit.Text = pt.email;
                patientInfoPhoneTxt.Text = phonePatientEdit.Text = pt.phoneNumber;
                patientInfoNotesTxt.Text = notesPatientEdit.Text = datahandler.getPatientNotes(patianteId);
                // fill in edit info
                firstNamePatientEdit.Text = pt.FirstName;
                lastNamePatientEdit.Text = pt.LastName;
            }
            else {
                //clearPatientInfo();
            }

            PatientsListBox.SetSelected(SELECTED_PatientsListBox, true);
            PatientsListBox.Invalidate();
            canStart();
        }

        private void editPatientFormClosing(object sender, FormClosingEventArgs e) {
            patientRefreshData();
            MainTabControl.SelectTab("homePage");

            patientTabControl.SelectTab("ShowDetails");
        }

        private void UserPasswordTxtBox_TextChanged(object sender, EventArgs e) {
            if (UserPasswordTxtBox.Text != "") {
                passwordTxtLbl.Visible = false;
            }
            else {
                passwordTxtLbl.Visible = true;
            }
        }

        private void UserNameTxtBox_TextChanged(object sender, EventArgs e) {
            if (UserNameTxtBox.Text != "") {
                userNameTxtLbl.Visible = false;
            }
            else {
                userNameTxtLbl.Visible = true;
            }
        }

        private void UserPasswordTxtBox_KeyUp(object sender, KeyEventArgs e) {
            if (UserNotExistsFlag) {
                UserNotExistsFlag = incorrectUserTxt.Visible = false;
            }
            else {
                if (e.KeyCode == Keys.Enter) {
                    OkBtn_Click(sender, e);
                }
            }
        }

        private void passwordTxtLbl_Click(object sender, EventArgs e) {
            passwordTxtLbl.Visible = false;
            UserPasswordTxtBox.Focus();
        }

        private void userNameTxtLbl_Click(object sender, EventArgs e) {
            userNameTxtLbl.Visible = false;
            UserNameTxtBox.Focus();
        }

        private void label11_Click(object sender, EventArgs e) {

        }

        private void exercisesGReportsBtn_Click(object sender, EventArgs e) {
            load_report_view();
        }

        private void exercisesQplanBtn_Click(object sender, EventArgs e) {
            loadQuickPlan();
        }

        private void loadQuickPlan() {
            standingBtnState = false;
            sittingBtnState = false;
            setPostureBtnState();
            qpPatientNameLbl.Text = patientName;
            qpDuration = 0;
            setDurationBtnState();
            resetActionBtnState();
            startQPbtnState();
            MainTabControl.SelectTab("QuickPlan");
        }

        private void resetActionBtnState() {
            balanceBtnState = false;
            setActionBtnState(qpBalanceBtn, balanceBtnState);
            movementplanningBtnState = false;
            setActionBtnState(qpMovementplanningBtn, movementplanningBtnState);
            multitaskingBtnState = false;
            setActionBtnState(qpMultitaskingBtn, multitaskingBtnState);
            reactiontimeBtnState = false;
            setActionBtnState(qpReactiontimeBtn, reactiontimeBtnState);
            gaitBtnState = false;
            setActionBtnState(qpGaitBtn, gaitBtnState);
            coordinationBtnState = false;
            setActionBtnState(qpCoordinationBtn, coordinationBtnState);
            agilityBtnState = false;
            setActionBtnState(qpAgilityBtn, agilityBtnState);
            aerobicBtnState = false;
            setActionBtnState(qpAerobicBtn, aerobicBtnState);
        }

        private void historyQuickPlanBtn_Click(object sender, EventArgs e) {
            loadQuickPlan();
        }

        private void button5_Click(object sender, EventArgs e) {
            loadHomePage();
        }

        private void qPlanExercisesBtn_Click(object sender, EventArgs e) {
            loadPlans(-1);
        }

        private void qPlanHistoryBtn_Click(object sender, EventArgs e) {
            loadHistoryPage();
        }

        private void qPlanPlanBtn_Click(object sender, EventArgs e) {
            load_plans_view();
        }

        private void qPlanReportsBtn_Click(object sender, EventArgs e) {
            load_report_view();
        }

        private void pViewQplanBtn_Click(object sender, EventArgs e) {
            loadQuickPlan();
        }

        private void reportsQplanBtn_Click(object sender, EventArgs e) {
            loadQuickPlan();
        }


        private void planNameEditTxt_KeyUp(object sender, KeyEventArgs e) {
            if (e.KeyCode == Keys.Enter) {
                planNameLbl.Text = planNameLbl.Text.TrimEnd(Convert.ToChar(Keys.Enter));
                planNameLbl.Visible = false;
                planNameLbl.Enabled = false;
                SavePlanNameBtn.Visible = false;
                EditPlanNameBtn.Visible = true;
                planNameLbl.Visible = true;
            }
        }
        /// <summary>
        /// /////////////////////////////////// Quick plan
        /// </summary>
        ///
        private bool standingBtnState = false;
        private bool sittingBtnState = false;
        private int qpDuration = 0;

        private void startQPbtnState() {
            if (qpDuration > 0 &&
                (standingBtnState || sittingBtnState) &&
                (balanceBtnState ||
                movementplanningBtnState ||
                multitaskingBtnState ||
                reactiontimeBtnState ||
                gaitBtnState ||
                coordinationBtnState ||
                agilityBtnState ||
                aerobicBtnState)) {
                qpStartPlanBtn.Enabled = true;
                qpStartPlanBtn.BackColor = Color.FromArgb(255, 201, 31);
            }
            else {
                qpStartPlanBtn.Enabled = false;
                qpStartPlanBtn.BackColor = Color.Silver;
            }
        }

        private void standingBtn_Click(object sender, EventArgs e) {
            if (!standingBtnState)
                standingBtnState = true;
            else
                standingBtnState = false;
            setPostureBtnState();
        }

        private void sittingBtn_Click(object sender, EventArgs e) {
            if (!sittingBtnState)
                sittingBtnState = true;
            else
                sittingBtnState = false;
            setPostureBtnState();
        }

        private void setPostureBtnState() {
            if (standingBtnState == true) {
                standingBtn.Image = Properties.Resources.Vector_2_2;
                standingBtn.FlatAppearance.BorderColor = Color.FromArgb(255, 201, 31);
            }
            else {
                standingBtn.Image = null;
                standingBtn.FlatAppearance.BorderColor = Color.Black;
            }
            if (sittingBtnState == true) {
                sittingBtn.Image = Properties.Resources.Vector_2_2;
                sittingBtn.FlatAppearance.BorderColor = Color.FromArgb(255, 201, 31);
            }
            else {
                sittingBtn.Image = null;
                sittingBtn.FlatAppearance.BorderColor = Color.Black;
            }
            startQPbtnState();
        }

        private void historyGeneralReportsBtn_Click(object sender, EventArgs e) {
            load_report_view();
        }

        private void pHistPViewLblBtn_Click_1(object sender, EventArgs e) {
            loadHistoryPage();
        }

        private void reportsExercisesBtn_Click(object sender, EventArgs e) {
            loadPlans(-1);
        }

        private void reportsPlansBtn_Click(object sender, EventArgs e) {
            load_plans_view();
        }

        private void reportsPHistoryBtn_Click(object sender, EventArgs e) {
            loadHistoryPage();
        }

        private void duration5mBtn_Click(object sender, EventArgs e) {
            qpDuration = 5;
            setDurationBtnState();
        }

        private void duration10mBtn_Click(object sender, EventArgs e) {
            qpDuration = 10;
            setDurationBtnState();
        }

        private void duration15mBtn_Click(object sender, EventArgs e) {
            qpDuration = 15;
            setDurationBtnState();
        }

        private void duration20mBtn_Click(object sender, EventArgs e) {
            qpDuration = 20;
            setDurationBtnState();
        }

        private void duration25mBtn_Click(object sender, EventArgs e) {
            qpDuration = 25;
            setDurationBtnState();
        }

        private void duration30mBtn_Click(object sender, EventArgs e) {
            qpDuration = 30;
            setDurationBtnState();
        }

        private void setDurationBtnState() {
            switch (qpDuration) {
                case 0:
                    updateDurationBtnState(duration5mBtn, false);
                    updateDurationBtnState(duration10mBtn, false);
                    updateDurationBtnState(duration15mBtn, false);
                    updateDurationBtnState(duration20mBtn, false);
                    updateDurationBtnState(duration25mBtn, false);
                    updateDurationBtnState(duration30mBtn, false);
                    break;
                case 5:
                    updateDurationBtnState(duration5mBtn, true);
                    updateDurationBtnState(duration10mBtn, false);
                    updateDurationBtnState(duration15mBtn, false);
                    updateDurationBtnState(duration20mBtn, false);
                    updateDurationBtnState(duration25mBtn, false);
                    updateDurationBtnState(duration30mBtn, false);
                    break;
                case 10:
                    updateDurationBtnState(duration5mBtn, false);
                    updateDurationBtnState(duration10mBtn, true);
                    updateDurationBtnState(duration15mBtn, false);
                    updateDurationBtnState(duration20mBtn, false);
                    updateDurationBtnState(duration25mBtn, false);
                    updateDurationBtnState(duration30mBtn, false);
                    break;
                case 15:
                    updateDurationBtnState(duration5mBtn, false);
                    updateDurationBtnState(duration10mBtn, false);
                    updateDurationBtnState(duration15mBtn, true);
                    updateDurationBtnState(duration20mBtn, false);
                    updateDurationBtnState(duration25mBtn, false);
                    updateDurationBtnState(duration30mBtn, false);
                    break;
                case 20:
                    updateDurationBtnState(duration5mBtn, false);
                    updateDurationBtnState(duration10mBtn, false);
                    updateDurationBtnState(duration15mBtn, false);
                    updateDurationBtnState(duration20mBtn, true);
                    updateDurationBtnState(duration25mBtn, false);
                    updateDurationBtnState(duration30mBtn, false);
                    break;
                case 25:
                    updateDurationBtnState(duration5mBtn, false);
                    updateDurationBtnState(duration10mBtn, false);
                    updateDurationBtnState(duration15mBtn, false);
                    updateDurationBtnState(duration20mBtn, false);
                    updateDurationBtnState(duration25mBtn, true);
                    updateDurationBtnState(duration30mBtn, false);
                    break;
                case 30:
                    updateDurationBtnState(duration5mBtn, false);
                    updateDurationBtnState(duration10mBtn, false);
                    updateDurationBtnState(duration15mBtn, false);
                    updateDurationBtnState(duration20mBtn, false);
                    updateDurationBtnState(duration25mBtn, false);
                    updateDurationBtnState(duration30mBtn, true);
                    break;

                default:
                    break;
            }
            startQPbtnState();
        }

        private void updateDurationBtnState(Button btn, bool state) {
            if (state) {
                btn.Image = Properties.Resources.Vector_2_2;
                btn.FlatAppearance.BorderColor = Color.FromArgb(255, 201, 31);
            }
            else {
                btn.Image = null;
                btn.FlatAppearance.BorderColor = Color.Black;
            }
        }

        private bool balanceBtnState = false;
        private bool movementplanningBtnState = false;
        private bool multitaskingBtnState = false;
        private bool reactiontimeBtnState = false;
        private bool gaitBtnState = false;
        private bool coordinationBtnState = false;
        private bool agilityBtnState = false;
        private bool aerobicBtnState = false;


        private void qpBalanceBtn_Click(object sender, EventArgs e) {
            if (!balanceBtnState)
                balanceBtnState = true;
            else
                balanceBtnState = false;
            setActionBtnState(qpBalanceBtn, balanceBtnState);
        }

        private void qpMovementplanningBtn_Click(object sender, EventArgs e) {
            if (!movementplanningBtnState)
                movementplanningBtnState = true;
            else
                movementplanningBtnState = false;
            setActionBtnState(qpMovementplanningBtn, movementplanningBtnState);
        }

        private void qpMultitaskingBtn_Click(object sender, EventArgs e) {
            if (!multitaskingBtnState)
                multitaskingBtnState = true;
            else
                multitaskingBtnState = false;
            setActionBtnState(qpMultitaskingBtn, multitaskingBtnState);
        }

        private void qpReactiontimeBtn_Click(object sender, EventArgs e) {
            if (!reactiontimeBtnState)
                reactiontimeBtnState = true;
            else
                reactiontimeBtnState = false;
            setActionBtnState(qpReactiontimeBtn, reactiontimeBtnState);
        }

        private void qpGaitBtn_Click(object sender, EventArgs e) {
            if (!gaitBtnState)
                gaitBtnState = true;
            else
                gaitBtnState = false;
            setActionBtnState(qpGaitBtn, gaitBtnState);
        }

        private void qpCoordinationBtn_Click(object sender, EventArgs e) {
            if (!coordinationBtnState)
                coordinationBtnState = true;
            else
                coordinationBtnState = false;
            setActionBtnState(qpCoordinationBtn, coordinationBtnState);
        }

        private void qpAgilityBtn_Click(object sender, EventArgs e) {
            if (!agilityBtnState)
                agilityBtnState = true;
            else
                agilityBtnState = false;
            setActionBtnState(qpAgilityBtn, agilityBtnState);
        }

        private void qpAerobicBtn_Click(object sender, EventArgs e) {
            if (!aerobicBtnState)
                aerobicBtnState = true;
            else
                aerobicBtnState = false;
            setActionBtnState(qpAerobicBtn, aerobicBtnState);
        }

        private void setActionBtnState(Button btn, bool state) {
            if (state) {
                btn.BackColor = Color.FromArgb(255, 201, 31);
            }
            else {
                btn.BackColor = Color.White;
            }
            startQPbtnState();
        }

        private void qpSelectAllBtn_Click(object sender, EventArgs e) {
            balanceBtnState = true;
            setActionBtnState(qpBalanceBtn, balanceBtnState);
            movementplanningBtnState = true;
            setActionBtnState(qpMovementplanningBtn, movementplanningBtnState);
            multitaskingBtnState = true;
            setActionBtnState(qpMultitaskingBtn, multitaskingBtnState);
            reactiontimeBtnState = true;
            setActionBtnState(qpReactiontimeBtn, reactiontimeBtnState);
            gaitBtnState = true;
            setActionBtnState(qpGaitBtn, gaitBtnState);
            coordinationBtnState = true;
            setActionBtnState(qpCoordinationBtn, coordinationBtnState);
            agilityBtnState = true;
            setActionBtnState(qpAgilityBtn, agilityBtnState);
            aerobicBtnState = true;
            setActionBtnState(qpAerobicBtn, aerobicBtnState);
        }

        private void qpSelectNoneBtn_Click(object sender, EventArgs e) {
            balanceBtnState = false;
            setActionBtnState(qpBalanceBtn, balanceBtnState);
            movementplanningBtnState = false;
            setActionBtnState(qpMovementplanningBtn, movementplanningBtnState);
            multitaskingBtnState = false;
            setActionBtnState(qpMultitaskingBtn, multitaskingBtnState);
            reactiontimeBtnState = false;
            setActionBtnState(qpReactiontimeBtn, reactiontimeBtnState);
            gaitBtnState = false;
            setActionBtnState(qpGaitBtn, gaitBtnState);
            coordinationBtnState = false;
            setActionBtnState(qpCoordinationBtn, coordinationBtnState);
            agilityBtnState = false;
            setActionBtnState(qpAgilityBtn, agilityBtnState);
            aerobicBtnState = false;
            setActionBtnState(qpAerobicBtn, aerobicBtnState);
        }

        private void qpStartPlanBtn_Click(object sender, EventArgs e) {
            // Automatically create new plan:
            // Plan name: QP + number
            // Plan content: duration + types (according to selections)
            //      [practice_position] -- standing/sitting 1/2
            //      Filters:
            //      [balance]
            //      [movement_planning]
            //      [multi_tasking]
            //      [reaction_time]
            //      [gait]
            //      [coordination]
            //      [agility]
            //      [aerobic]

            plan_practices.Clear(); // Clear the plan data for iteration            
            List<string> availableFilters = new List<string>();
            // Fill in selection into the list
            if (balanceBtnState)
                availableFilters.Add("balance");
            if (movementplanningBtnState)
                availableFilters.Add("movement_planning");
            if (multitaskingBtnState)
                availableFilters.Add("multi_tasking");
            if (reactiontimeBtnState)
                availableFilters.Add("reaction_time");
            if (gaitBtnState)
                availableFilters.Add("gait");
            if (coordinationBtnState)
                availableFilters.Add("coordination");
            if (agilityBtnState)
                availableFilters.Add("agility");
            if (aerobicBtnState)
                availableFilters.Add("aerobic");

            int qpNumber = datahandler.createQuickPlan(patianteId, qpDuration, availableFilters, true, true);

            //plan_practices
            //MessageBox.Show("Plan # " + qpNumber);
            start_plan(qpNumber);
        }

        private void planNameLbl_MouseUp(object sender, MouseEventArgs e) {
            load_virtual_keyboard();
        }

        private void autoGenIDcbx_CheckedChanged(object sender, EventArgs e) {
            Properties.Settings.Default.autoGenID = autoGenIDcbx.Checked;
            Properties.Settings.Default.Save();
        }

        private void parkinsonModeCBX_CheckedChanged(object sender, EventArgs e) {
            Properties.Settings.Default.ParkinsonPilotMode = parkinsonModeCBX.Checked;
            Properties.Settings.Default.Save();
        }

        private void qpEditBtn_Click(object sender, EventArgs e) {
            Patient pt = pList.Find(item => item.refIDnum == patianteId);
            EditPatient addPatientForm = new EditPatient(patianteId, pt);
            addPatientForm.StartPosition = FormStartPosition.CenterScreen;
            addPatientForm.Show();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e) {

        }

        private void reportsSelectPlansBtn_Click(object sender, EventArgs e) {
            reportsSelectPlansBtn.Image = Properties.Resources.Vector_2_2;
            reportsSelectPlansBtn.FlatAppearance.BorderColor = Color.FromArgb(255, 201, 31);
            reportsSelectExercisesBtn.Image = null;
            reportsSelectExercisesBtn.FlatAppearance.BorderColor = Color.Black;
            reportsPlansListviewUpdate();
            reportsTabControl.SelectTab("ReportsPlansTabPage");
        }

        private void reportsSelectExercisesBtn_Click(object sender, EventArgs e) {
            reportsSelectExercisesBtn.Image = Properties.Resources.Vector_2_2;
            reportsSelectExercisesBtn.FlatAppearance.BorderColor = Color.FromArgb(255, 201, 31);
            reportsSelectPlansBtn.Image = null;
            reportsSelectPlansBtn.FlatAppearance.BorderColor = Color.Black;
            reportsExercisesListviewUpdate();
            reportsTabControl.SelectTab("ReportsExercisesTabPage");
        }

        private void pViewReportsBtn_Click(object sender, EventArgs e) {
            load_report_view();
        }

        private void label3_Click(object sender, EventArgs e) {

        }

        ///

    }

    public class Wow64Interop {
        [DllImport("Kernel32.Dll", EntryPoint = "Wow64EnableWow64FsRedirection")]
        public static extern bool EnableWow64FSRedirection(bool enable);
    }
}

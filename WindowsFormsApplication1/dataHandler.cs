﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SelfitMain {
    public class PatientData {
        public long userId { get; set; }
        public int lang { get; set; }
        public int gender { get; set; }
        public int birthYear { get; set; }
        public int height { get; set; }
        public int weight { get; set; }
        public String status_summarey { get; set; }
        public String notes { get; set; }
        public int shueSize { get; set; }
        public int siteId { get; set; }

        public List<string> toStringList() {
            List<string> pData = new List<string>();
            pData.Add(userId.ToString());
            pData.Add(lang.ToString());
            pData.Add(gender.ToString());
            pData.Add(birthYear.ToString());
            pData.Add(height.ToString());
            pData.Add(weight.ToString());
            pData.Add(status_summarey);
            pData.Add(notes);
            pData.Add(shueSize.ToString());
            return pData;
        }

        public void Clear() {
            userId = -1;
            lang = 0;
            gender = 0;
            birthYear = 0;
            height = 0;
            weight = 0;
            status_summarey = null;
            notes = null;
            shueSize = 0;
            siteId = 0;
        }
        
        public void updateData(Patient pt, int language, string userNotes, int site) {
            userId = pt.refIDnum;
            lang = language;
            gender = pt.gender;
            birthYear = pt.birthYear;
            height = pt.height;
            weight = pt.weight;
            notes = userNotes;
            shueSize = pt.shueSize;
            siteId = site;
        }
    }

    public class PatientTestData {
        public long patientId { get; set; }
        public string testName { get; set; }
        public int testTypeId { get; set; }
        public int testScore { get; set; }
        public int testTime { get; set; }
        public int steps { get; set; }
        public int siteId { get; set; }
    }

    public class PlanData {
        public int plan_id { get; set; }
        public int plan_number { get; set; }
        public int practice_id { get; set; }
        public string practice_name { get; set; }
        public int practice_unity_id { get; set; }
        public int order_num { get; set; }
        public long plan_user { get; set; }
        public int site_id { get; set; }
        public int reps { get; set; }
    }

    public class PlanInfo {
        public int plan_number { get; set; }
        public string plan_name { get; set; }
        public long plan_user { get; set; }
        public int site_id { get; set; }
        public int count { get; set; }
    }

    public class PlanSummary {
        public int plan_count { get; set; }
        public int reps { get; set; }
    }

    public class ImageData {
        public int practice_id { get; set; }
        public int image_id { get; set; }
    }

    public class PracticeData {
        public int practice_id { get; set; }
        public string practice_name { get; set; }
        public int unity_id { get; set; }
    }

    public class DataHandler {
        ///
        private dbHandler ldb;  // MYSQL db
        //private msDbHandler mdb; //MS SQL DB
        private ManagerRestClient mrc; // Rest API
        private int SITEID = -1;
        private int SYS_SERVER = -1;
        PatientData patientData;

        /// <summary>
        /// Public constractor
        /// </summary>
        public DataHandler() {
            ldb = new dbHandler();
            SITEID = getSiteID();
            SYS_SERVER = getSYS_Server();
            //mdb = new msDbHandler();
            mrc = new ManagerRestClient(SYS_SERVER);
            patientData = new PatientData();
        }

        public void close() {
            
        }

        // local  MySql DB API

        public int getSiteID() {
            if (SITEID == -1)
                SITEID = ldb.getSiteIDnumber();
            //MessageBox.Show("SITEID=" + SITEID);
            return SITEID;
        }

        public int getSYS_Server() {
            if (SYS_SERVER == -1)
                SYS_SERVER = ldb.getSystemServer();
            return SYS_SERVER;
        }

        public int getDefultLang() {
            return ldb.getDefaultLang();
        }

        public bool getSpeachState() {
            return ldb.getHebSpeachState();
        }

        public string GetSystemPath() {
            return ldb.getSystemPath();
        }

        public void SetSystemPath(string p) {
            ldb.storeSystemPath(p);
        }

        public void SetSpeachState(bool s) {
            ldb.setHebSpeachState(s);
        }

        public bool IsDemoMode() {
            return ldb.getDemoState();
        }


        // remote DB (rest)
        // therapist
        public int getTherapistRights(string tName, string psw, int site) {
            //api/Therapist?name=<string>&psw=<string>&site=<string>
            //return mdb.getUserRights(tName, psw);// TO DO: + need to modify function  
            int res = -1;
            try {
                Task t = Task.Run(async () => { res = await mrc.restGetTherapistRights(tName, psw, site); });
                Task.WaitAll(t);
            }
            catch (Exception ex) {
                MessageBox.Show(ex.ToString());
            }

            return res;
        }

        // patients
        public int getTotalPatientsCount() {
            //api/Patient?siteId=<int>
            //return mdb.getUsersCount();
            int totalP = -1;
            try {
                Task t = Task.Run(async () => { totalP = await mrc.restGetUsersCount(SITEID); });
                Task.WaitAll(t);
            }
            catch (Exception ex) {
                MessageBox.Show(ex.ToString());
            }
            return totalP;
        }

        public string getPatientNotes(long pId) {
            if (patientData != null) {
                if (patientData.userId == pId) {
                    return patientData.notes;
                }
            }
            try {
                Task t = Task.Run(async () => { patientData = await mrc.restGetPatient(pId, SITEID); });
                Task.WaitAll(t);
            }
            catch (Exception ex) {
                MessageBox.Show(ex.ToString());
            }
            //return mdb.getUserNotes(pId);
            if (patientData != null)
                return patientData.notes;
            else
                return null;
        }

        public void setPatientNotes(long pId, string note) {
            if (patientData == null || patientData.userId == pId) {
                // we have to load the patinet data
                //MessageBox.Show("we have to load the patinet data");
                try {
                    Task t = Task.Run(async () => { patientData = await mrc.restGetPatient(pId, SITEID); });
                    Task.WaitAll(t);
                }
                catch (Exception ex) {
                    MessageBox.Show(ex.ToString());
                }
            }
            if (patientData.notes != note) {
                patientData.notes = note; // update the notes                    

                // now update the patient data: PUT: api/Patient?id=<long>&site=<int> + [FromBody] Patient
                if (patientData != null) { // make sure the data loaded
                    int res = -1;
                    try {                        
                        Task t = Task.Run(async () => { res = await mrc.updatePatient(patientData, SITEID, pId); });
                        Task.WaitAll(t);
                    }
                    catch (Exception ex) {
                        MessageBox.Show(ex.ToString());
                    }
                    if (res == -1) {
                        MessageBox.Show("Data not saved!!!");
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pId"></param>
        /// <returns></returns>
        public List<string> getPatientInfo(long pId) {
            List<string> pData = new List<string>();
            //return mdb.getUserInformation(pId);
            if (patientData != null) {
                if (patientData.userId == pId) {
                    return patientData.toStringList();
                }
            }
            try {
                Task t = Task.Run(async () => { patientData = await mrc.restGetPatient(pId, SITEID); });
                Task.WaitAll(t);
            }
            catch (Exception ex) {
                MessageBox.Show(ex.ToString());
            }
            //return mdb.getUserNotes(pId);
            if (patientData != null)
                return patientData.toStringList();
            return pData;
        }

        public long addPatient(Patient pt, int lang, string notes, int level) {
            // POST: api/Patient?site=<int> + [FromBody] Patient
            patientData.updateData(pt, lang, notes, SITEID);
            long patientId = 0;
            //return mdb.add_patient(pt.refIDnum, lang, pt.gender, pt.birthYear.ToString(), pt.height, pt.weight, notes, pt.shueSize, level);
            try {
                Task t = Task.Run(async () => { patientId = await mrc.addPatient(patientData, SITEID); });
                Task.WaitAll(t);
            }
            catch (Exception ex) {
                MessageBox.Show(ex.ToString());
            }
            return patientId;
        }

        public long updatePatient(Patient pt, int lang, string notes, int level) {
            patientData.updateData(pt, lang, notes, SITEID);
            int hasUpdated = 0;

            //return mdb.update_patient(pt.refIDnum, notes, pt.birthYear.ToString(), pt.gender, pt.height, pt.weight, pt.shueSize, level, lang);
            // PUT: api/Patient?id=<long>&site=<int> + [FromBody] Patient
            try {
                Task t = Task.Run(async () => { hasUpdated = await mrc.updatePatient(patientData, SITEID, pt.refIDnum); });
                Task.WaitAll(t);
            }
            catch (Exception ex) {
                MessageBox.Show(ex.ToString());
            }
            return hasUpdated;
        }

        public DataSet getPatientTestsHistory(long pId) {
            // GET: api/userTests?pid=<long>&site=<int>
            //return mdb.getUserTestsHistory(pId);
            DataSet td = new DataSet();
            try {
                Task t = Task.Run(async () => { td = await mrc.getUserTestsHistory(pId, SITEID); });
                Task.WaitAll(t);
            }
            catch (Exception ex) {
                MessageBox.Show(ex.ToString());
            }
            return td;
        }

        public void AddPatientTestResults(long patientId, int tType, int tScore, int tTime, int steps) {
            // POST: api/userTests?pId=<long>&site=<int> + [FromBody] PatientTestData
            //mdb.add_user_testResults(patientId, tType, tScore, tTime, steps);
            int hasAdded = -1;
            PatientTestData pTestData = new PatientTestData();
            pTestData.patientId = patientId;
            pTestData.siteId = SITEID;
            pTestData.testTypeId = tType;
            pTestData.testScore = tScore;
            pTestData.testTime = tTime;
            pTestData.steps = steps;
            try {
                Task t = Task.Run(async () => { hasAdded = await mrc.addUserTestResult(patientId, SITEID, pTestData); });
                Task.WaitAll(t);
            }
            catch (Exception ex) {
                MessageBox.Show(ex.ToString());
            }
        }

        // session
        public DataSet getPatientSessionInfo(long pId, DateTime date) {
            // GET: api/Session?pID=<long>&site=<int>&startDate=<string>&endDate=<string>
            //return mdb.getUserSessionInfo(pId, Value);
            DataSet sInfo = new DataSet();
            string sDate = date.Year + "-" + date.Month + "-01" + " 00:00:00";
            string eDate = date.Year + "-" + date.Month + "-" + DateTime.DaysInMonth(date.Year, date.Month) + " 23:00:00";
            try {
                Task t = Task.Run(async () => { sInfo = await mrc.getPatientSessionInfoFiltered(pId, SITEID, sDate, eDate); });
                Task.WaitAll(t);
            }
            catch (Exception ex) {
                MessageBox.Show(ex.ToString());
            }

            return sInfo;
        }

        public DataSet getPatientSessionInfo(long pId) {
            // GET: GET: api/Session?patientId=<long>&siteID=<int> getPatientSessionInfo
            //return mdb.getUserSessionInfo(pId);
            DataSet sInfo = new DataSet();
            try {
                Task t = Task.Run(async () => { sInfo = await mrc.getPatientSessionInfo(pId, SITEID); });
                Task.WaitAll(t);
            }
            catch (Exception ex) {
                MessageBox.Show(ex.ToString());
            }
            return sInfo;
        }

        public  DataSet getParkinsonSessionData(long pId, int sessionId) {
            DataSet sData = new DataSet();
            try {
                Task t = Task.Run(async () => { sData = await mrc.getPatientParkinsonSessionData(pId, sessionId, SITEID); });
                Task.WaitAll(t);
            }
            catch (Exception ex) {
                MessageBox.Show(ex.ToString());
            }
            return sData;
        }

        public DataSet getParkinsonStepsData(long pId, int sessionId) {
            DataSet sData = new DataSet();
            try {
                Task t = Task.Run(async () => { sData = await mrc.getParkinsonStepsData(pId, sessionId, SITEID); });
                Task.WaitAll(t);
            }
            catch (Exception ex) {
                MessageBox.Show(ex.ToString());
            }
            return sData;
        }


        public List<string> getPatientSessionData(long pId, int sessionId) {
            // GET: api/Session?patientID=<long>&sessionID=<int>&siteID=<int>
            //return mdb.getUserSessionData(pId, sessionId); getPatientSessionData
            List<string> sData = new List<string>();
            try {
                Task t = Task.Run(async () => { sData = await mrc.getPatientSessionData(pId, sessionId, SITEID); });
                Task.WaitAll(t);
            }
            catch (Exception ex) {
                MessageBox.Show(ex.ToString());
            }
            return sData;
        }

        public DataSet getPatientSessionHistory(long patientId, int limit, int sessionId) {
            // GET: api/SessionHistory?userId=<long>&siteId=<int>&DataLimit=<int>&sessionId=<int> getPatientSessionHistory(long patientId, int site, int limit, int sessionId)
            //return mdb.getUserSessionHistory(patientId, limit, sessionId);
            DataSet sHist = new DataSet();
            try {
                Task t = Task.Run(async () => { sHist = await mrc.getPatientSessionHistory(patientId, SITEID, limit, sessionId); });
                Task.WaitAll(t);
            }
            catch (Exception ex) {
                MessageBox.Show(ex.ToString());
            }
            return sHist;
        }

        public DataSet getSessionPractices(int sessionId) {
            // GET: api/SessionHistory?siteId=<int>&sessionId=<int> getSessionPractices(int sessionId, int site)
            //return mdb.getSessionPractices(sessionId);
            //MessageBox.Show("Showing session#: " + sessionId);
            DataSet spData = new DataSet();
            try {
                Task t = Task.Run(async () => { spData = await mrc.getSessionPractices(sessionId, SITEID); });
                Task.WaitAll(t);
            }
            catch (Exception ex) {
                MessageBox.Show(ex.ToString());
            }
            return spData;
        }

        // plan
        public DataSet getPlanData(int planId) {
            // GET: api/Plan?planId=<int>&siteId=<int>  getPatientPlanData(int planId, int siteId)
            //return mdb.getUserPlanData(planId);
            DataSet pData = new DataSet();
            try {
                Task t = Task.Run(async () => { pData = await mrc.getPatientPlanData(planId, SITEID); });
                Task.WaitAll(t);
            }
            catch (Exception ex) {
                MessageBox.Show(ex.ToString());
            }
            return pData;
        }

        // plan
        public DataSet getPlanData(int planId, bool is_sPlan) {
            // GET: api/Plan?planId=<int>&siteId=<int>  getPatientPlanData(int planId, int siteId)
            //return mdb.getUserPlanData(planId);            
            DataSet pData = new DataSet();
            try {
                Task t = Task.Run(async () => { pData = await mrc.getPatientPlanData(planId, (is_sPlan)? 0: SITEID); });
                Task.WaitAll(t);
            }
            catch (Exception ex) {
                MessageBox.Show(ex.ToString());
            }
            return pData;
        }

        public int storeNewPatientPlan(long patientId, string planName, List<LogInForm.practice_data> plan_practices) {
            // POST: api/Plan?patient=<long>&site=<int>&plan_name=<string> + [FromBody] List<PlanData>  storePatientPlan(long patientId, int siteId, string planName, PlanData pData)
            //return mdb.storeNewUserPlan(patianteId, planName, plan_practices);
            int stored = -1;
            List<PlanData> planDatas = new List<PlanData>();
            int order = 0;
            foreach (LogInForm.practice_data pData in plan_practices) {
                PlanData planData = new PlanData();
                planData.practice_unity_id = pData.practice_unityId;
                planData.order_num = order;
                order++;
                planData.reps = pData.practice_reps;
                planDatas.Add(planData);
            }
            try {
                Task t = Task.Run(async () => { stored = await mrc.storePatientPlan(patientId, SITEID, planName, planDatas); });
                Task.WaitAll(t);
            }
            catch (Exception ex) {
                MessageBox.Show(ex.ToString());
            }
            return stored;
        }

        // plan info
        public int getPatientPlanCount(long patianteId) {
            // GET: api/PlanInfo?patientId=<long>  getPateintPlanCount(long patientId)
            //return mdb.getUserPlansCount(patianteId);
            int pCount = 0;
            try {
                Task t = Task.Run(async () => { pCount = await mrc.getPateintPlanCount(patianteId); });
                Task.WaitAll(t);
            }
            catch (Exception ex) {
                MessageBox.Show(ex.ToString());
            }
            return pCount;
        }

        public DataSet getPatientPlansInfo(long patientId) {
            // GET: api/PlanInfo?patientId=<long>&siteId=<int>  getPatientPlansInfo(long patientId, int site)
            //return mdb.getUsersPlansInfo(patientId);
            DataSet pPlanInfo = new DataSet();
            try {
                //Task t = Task.Run(async () => { pPlanInfo = await mrc.getPatientPlansInfo(patientId, (patientId == 0)? 0 : SITEID); });
                Task t = Task.Run(async () => { pPlanInfo = await mrc.getPatientPlansInfo(patientId, SITEID); });
                //Task t = Task.Run(async () => { pPlanInfo = await mrc.getPatientPlansInfo(0, 0); });
                Task.WaitAll(t);
            }
            catch (Exception ex) {
                MessageBox.Show(ex.ToString());
            }
            return pPlanInfo;
        }

        // plan summary
        public DataSet getPatientPlanSummaryData(long patientId) {
            // GET: api/PlanSummary?patientId<long>&siteId=<int>  getPatientPlanSummaryData(long patientId, int site)
            //return mdb.getPatientPlanSummaryData(patianteId);
            DataSet pSumData = new DataSet();
            try {
                Task t = Task.Run(async () => { pSumData = await mrc.getPatientPlanSummaryData(patientId, SITEID); });
                Task.WaitAll(t);
            }
            catch (Exception ex) {
                MessageBox.Show(ex.ToString());
            }
            return pSumData;
        }

        // practiceData
        public DataSet getImageData() {
            // GET: api/PracticeImages?siteId=<int>
            //return mdb.getImageNames();
            DataSet imgData = new DataSet();
            try {
                Task t = Task.Run(async () => { imgData = await mrc.getImageNames(SITEID); });
                Task.WaitAll(t);
            }
            catch (Exception ex) {
                MessageBox.Show(ex.ToString());
            }
            return imgData;
        }

        public DataSet getPracticeMeasuresInfo() {
            DataSet pMdata = new DataSet();
            try {
                Task t = Task.Run(async () => { pMdata = await mrc.getPracticeMeasuresInfo(SITEID); });
                Task.WaitAll(t);
            }
            catch (Exception ex) {
                MessageBox.Show(ex.ToString());
            }
            return pMdata;
        }

        public DataSet getPatientPracticeMeasuresData(long patientId, int practiceNum) {
            DataSet pmData = new DataSet();
            try {
                Task t = Task.Run(async () => { pmData = await mrc.getPatientPracticeMeasuresData(patientId, SITEID, practiceNum); });
                Task.WaitAll(t);
            }
            catch (Exception ex) {
                MessageBox.Show(ex.ToString());
            }
            return pmData;
        }

        public DataSet getActivePractices() {
            // GET: api/PracticeData  getAllActivePractices()
            //return mdb.getAllActivePractices();
            DataSet pd = new DataSet();
            try {
                Task t = Task.Run(async () => { pd = await mrc.getAllActivePractices(); });
                Task.WaitAll(t);
            }
            catch (Exception ex) {
                MessageBox.Show(ex.ToString());
            }
            return pd;
        }

        public DataSet getPerformedPractices(long patientId) {
            DataSet pp = new DataSet();
            try {
                Task t = Task.Run(async () => { pp = await mrc.getPerformedPractices(patientId, SITEID); });
                Task.WaitAll(t);
            }
            catch (Exception ex) {
                MessageBox.Show(ex.ToString());
            }
            return pp;
        }

        public DataSet getFilterActivePractices(List<string> filters, int pos) {
            // GET: api/PracticeData?position=<int> & List<string> filters getFilterActivePractices(List<string> filters, int pos)
            //return mdb.getFilterActivePractices(filters, pos);
            DataSet pfData = new DataSet();
            try {
                Task t = Task.Run(async () => { pfData = await mrc.getFilterActivePractices(filters, pos); });
                Task.WaitAll(t);
            }
            catch (Exception ex) {
                MessageBox.Show(ex.ToString());
            }
            return pfData;
        }

        // reportData
        public DataSet getGeneralReportDataFiltered(string fromDate, string toDate, string patientIdStr) {
            // GET: api/GeneralReport?from=<string>&to=<string>&patientID=<string>&site=<int>  getGeneralReportDataFiltered(string fromDateStr, string toDateStr, string patientIdStr, int site)
            //return mdb.getGeneralReportDataFiltered(fromDate, toDate, patientIdStr);
            DataSet genRepDataset = new DataSet();
            try {
                Task t = Task.Run(async () => { genRepDataset = await mrc.getGeneralReportDataFiltered(fromDate, toDate,patientIdStr, SITEID); });
                Task.WaitAll(t);
            }
            catch (Exception ex) {
                MessageBox.Show(ex.ToString());
            }
            return genRepDataset;
        }

        public DataSet getSummaryReportDataFiltered(string fromDate, string toDate, string patientIdStr) {
            // GET: api/SummaryReport?fromDate=<string>&toDate=<string>&userIdStr=<string>&siteId=<int> 
            //return mdb.getSummaryReportDataFiltered(fromDate, toDate, patientIdStr);
            DataSet sumRepDataset = new DataSet();
            try {
                Task t = Task.Run(async () => { sumRepDataset = await mrc.getSummaryReportDataFiltered(fromDate, toDate, patientIdStr, SITEID); });
                Task.WaitAll(t);
            }
            catch (Exception ex) {
                MessageBox.Show(ex.ToString());
            }
            return sumRepDataset;
        }

        public DataSet getPatientSummaryReportData(long patientId) {
            // GET: api/SummaryReport?userIdStr=<string>&siteId=<int>  getPatientSummaryReportData
            //return mdb.getPatientSummaryReportData(patientId);
            DataSet repData = new DataSet();
            try {
                Task t = Task.Run(async () => { repData = await mrc.getPatientSummaryReportData(patientId, SITEID); });
                Task.WaitAll(t);
            }
            catch (Exception ex) {
                MessageBox.Show(ex.ToString());
            }
            return repData;
        }

        public int createQuickPlan(long pId, int duration, List<string> filters, bool standing, bool sitting) {
            // api/QuickPlan?patient=long&site=int&totalTime=int&standing=bool&sitting=bool [FromBody] List<string> categories
            int qPlan = -1;
            try {
                Task t = Task.Run(async () => { qPlan = await mrc.getNewQuickPlanNumber(pId, SITEID, duration, standing, sitting, filters); });
                Task.WaitAll(t);
            }
            catch (Exception ex) {
                MessageBox.Show(ex.ToString());
            }
            return qPlan;
        }


        /* //
         // Private section:
         public DataSet ConvertToDataSet<T>(this IEnumerable<T> source, string name) {
             if (source == null)
                 throw new ArgumentNullException("source ");
             if (string.IsNullOrEmpty(name))
                 throw new ArgumentNullException("name");
             var converted = new DataSet(name);
             converted.Tables.Add(NewTable(name, source));
             return converted;
         }


         private DataTable NewTable<T>(string name, IEnumerable<T> list) {
             PropertyInfo[] propInfo = typeof(T).GetProperties();
             DataTable table = Table<T>(name, list, propInfo);
             IEnumerator<T> enumerator = list.GetEnumerator();
             while (enumerator.MoveNext())
                 table.Rows.Add(CreateRow<T>(table.NewRow(), enumerator.Current, propInfo));
             return table;
         }

         private DataRow CreateRow<T>(DataRow row, T listItem, PropertyInfo[] pi) {
             foreach (PropertyInfo p in pi)
                 row[p.Name.ToString()] = p.GetValue(listItem, null);
             return row;
         }

         private DataTable Table<T>(string name, IEnumerable<T> list, PropertyInfo[] pi) {
             DataTable table = new DataTable(name);
             foreach (PropertyInfo p in pi)
                 table.Columns.Add(p.Name, p.PropertyType);
             return table;
         } */
    }
}

﻿namespace SelfitMain {
    partial class errorMessageForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.titleLbl = new System.Windows.Forms.Label();
            this.errorInfoLbl = new System.Windows.Forms.Label();
            this.okBtn = new System.Windows.Forms.Button();
            this.extraInfoTxt = new System.Windows.Forms.RichTextBox();
            this.showMoreBtn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // titleLbl
            // 
            this.titleLbl.Font = new System.Drawing.Font("Roboto", 22F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.titleLbl.ForeColor = System.Drawing.Color.White;
            this.titleLbl.Location = new System.Drawing.Point(130, 11);
            this.titleLbl.Name = "titleLbl";
            this.titleLbl.Size = new System.Drawing.Size(229, 27);
            this.titleLbl.TabIndex = 10;
            this.titleLbl.Text = "System error";
            this.titleLbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // errorInfoLbl
            // 
            this.errorInfoLbl.Font = new System.Drawing.Font("Roboto", 22F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.errorInfoLbl.ForeColor = System.Drawing.Color.White;
            this.errorInfoLbl.Location = new System.Drawing.Point(6, 52);
            this.errorInfoLbl.Name = "errorInfoLbl";
            this.errorInfoLbl.Size = new System.Drawing.Size(476, 27);
            this.errorInfoLbl.TabIndex = 11;
            this.errorInfoLbl.Text = "????";
            this.errorInfoLbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // okBtn
            // 
            this.okBtn.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.okBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(201)))), ((int)(((byte)(31)))));
            this.okBtn.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.okBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.okBtn.Font = new System.Drawing.Font("Roboto", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.okBtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(56)))), ((int)(((byte)(64)))));
            this.okBtn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.okBtn.Location = new System.Drawing.Point(158, 127);
            this.okBtn.Name = "okBtn";
            this.okBtn.Size = new System.Drawing.Size(160, 32);
            this.okBtn.TabIndex = 12;
            this.okBtn.Text = "OK";
            this.okBtn.UseVisualStyleBackColor = false;
            this.okBtn.Click += new System.EventHandler(this.okBtn_Click);
            // 
            // extraInfoTxt
            // 
            this.extraInfoTxt.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.extraInfoTxt.Location = new System.Drawing.Point(11, 172);
            this.extraInfoTxt.Name = "extraInfoTxt";
            this.extraInfoTxt.ReadOnly = true;
            this.extraInfoTxt.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Horizontal;
            this.extraInfoTxt.Size = new System.Drawing.Size(477, 295);
            this.extraInfoTxt.TabIndex = 13;
            this.extraInfoTxt.Text = "";
            // 
            // showMoreBtn
            // 
            this.showMoreBtn.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(56)))), ((int)(((byte)(64)))));
            this.showMoreBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.showMoreBtn.Font = new System.Drawing.Font("Roboto", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.showMoreBtn.ForeColor = System.Drawing.Color.White;
            this.showMoreBtn.Location = new System.Drawing.Point(2, 119);
            this.showMoreBtn.Name = "showMoreBtn";
            this.showMoreBtn.Size = new System.Drawing.Size(52, 49);
            this.showMoreBtn.TabIndex = 14;
            this.showMoreBtn.Text = "+";
            this.showMoreBtn.UseVisualStyleBackColor = true;
            this.showMoreBtn.Click += new System.EventHandler(this.showMoreBtn_Click);
            // 
            // errorMessageForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(56)))), ((int)(((byte)(64)))));
            this.ClientSize = new System.Drawing.Size(500, 171);
            this.Controls.Add(this.showMoreBtn);
            this.Controls.Add(this.extraInfoTxt);
            this.Controls.Add(this.okBtn);
            this.Controls.Add(this.errorInfoLbl);
            this.Controls.Add(this.titleLbl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "errorMessageForm";
            this.Text = "errorMessageForm";
            this.Load += new System.EventHandler(this.errorMessageForm_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label titleLbl;
        private System.Windows.Forms.Label errorInfoLbl;
        private System.Windows.Forms.Button okBtn;
        private System.Windows.Forms.RichTextBox extraInfoTxt;
        private System.Windows.Forms.Button showMoreBtn;
    }
}
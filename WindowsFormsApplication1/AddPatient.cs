﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SelfitMain {
    public partial class AddPatient : Form {
        private dbHandler dbh;
        public AddPatient() {
            InitializeComponent();
            fNameTxt.Text = "";
            lNamTxt.Text = "";
            pIdTxt.Text = "";
            eMailTxt.Text = "";
            notesTxt.Text = "";
            dbh = new dbHandler();
        }

        private void needSave() {
            if (fNameTxt.Text == "" || lNamTxt.Text == "" || eMailTxt.Text == "") {
                saveDataBtn.Enabled = false;
            }
            else {
                saveDataBtn.Enabled = true;
            }
        }

        private bool canClose() {
            bool cClose = (fNameTxt.Text != "" || lNamTxt.Text != "" || eMailTxt.Text != "" || pIdTxt.Text != "" || notesTxt.Text != "");
            
            return cClose;
        }

        private void button1_Click(object sender, EventArgs e) {
            if (!canClose())
                this.Close();
            else {
                string message = "You have made some changes here, do you want to save changes?\n";
                string caption = "Lost data";
                MessageBoxButtons buttons = MessageBoxButtons.YesNoCancel;
                DialogResult result = MessageBox.Show(message, caption, buttons);

                if (result == System.Windows.Forms.DialogResult.Yes) {
                    //saveNewPatientData();
                    int res = 0;// dbh.add_patient(fNameTxt.Text, lNamTxt.Text, eMailTxt.Text, pIdTxt.Text, notesTxt.Text);
                    if (res != -1)
                        this.Close();
                    
                }
                if (result == System.Windows.Forms.DialogResult.No) { 
                    this.Close();
                }
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e) {
            needSave();
        }

        private void lNamTxt_TextChanged(object sender, EventArgs e) {
            needSave();
        }

        private void eMailTxt_TextChanged(object sender, EventArgs e) {
            needSave();
        }

        private void saveDataBtn_Click(object sender, EventArgs e) {
            int res = 0;//dbh.add_patient(fNameTxt.Text, lNamTxt.Text, eMailTxt.Text, pIdTxt.Text, notesTxt.Text);
            if (res != -1)
                this.Close();
        }
    }
}

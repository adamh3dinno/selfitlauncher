/****** Script reaset production database  ******/
DELETE FROM [dbo].[patient_info] WHERE user_id > 0;
SELECT * FROM [dbo].[patient_info];

DELETE FROM [dbo].[plans] WHERE plan_number > 16;
DBCC CHECKIDENT ('[dbo].[plans]',RESEED, 16)
SELECT * FROM [dbo].[plans];
DBCC CHECKIDENT ('[dbo].[plans]');

DELETE FROM [dbo].[plans_info] WHERE site_id <> 0 ;
SELECT * FROM [dbo].[plans_info] ;

DELETE FROM [dbo].[practice_levels_log];
SELECT * FROM [dbo].[practice_levels_log];

DELETE FROM [dbo].[practice_log];
SELECT * FROM [dbo].[practice_log];

DELETE FROM [dbo].[practice_score_log];
SELECT * FROM [dbo].[practice_score_log];

DELETE FROM [dbo].[sessions] WHERE session_id > 0;
SELECT * FROM [dbo].[sessions];

DELETE FROM [dbo].[sessions_log];
SELECT * FROM [dbo].[sessions_log];

DELETE FROM [dbo].[user_levels_status];
SELECT * FROM [dbo].[user_levels_status];

SELECT * FROM [dbo].[Sites];

SELECT * FROM [dbo].[users]